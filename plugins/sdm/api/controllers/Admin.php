<?php namespace Sdm\Api\Controllers;

use Sdm\Api\Classes\ApiController;

class Admin extends ApiController
{
    public function index()
    {
        return response()->json([
            'result'   => true,
            'response' => [
                'data' => [
                    'update' => [
                        'total'    => $this->countUpdateData(),
                    ],
                    'health' => [
                        'unfinish' => \Sdm\Health\Models\Consultation::whereIsClose(0)->count(),
                    ],
                    'law'    => [
                        'unfinish' => \Sdm\Law\Models\Consultation::whereIsClose(0)->count(),
                    ],
                ]
            ]
        ]);
    }

    public function countUpdateData()
    {
        $attachment       = \Sdm\Member\Models\Attachment::where('status', '=', 'process')->count();
        $langugage        = \Sdm\Member\Models\Language::where('status', '=', 'process')->count();
        $education        = \Sdm\Member\Models\Education::where('status', '=', 'process')->count();
        $educationPolries = \Sdm\Member\Models\EducationPolice::where('status', '=', 'process')->count();
        $training         = \Sdm\Member\Models\Training::where('status', '=', 'process')->count();
        $duty             = \Sdm\Member\Models\Duty::where('status', '=', 'process')->count();
        $position         = \Sdm\Member\Models\Position::where('status', '=', 'process')->count();
        $grade            = \Sdm\Member\Models\Grade::where('status', '=', 'process')->count();
        $work             = \Sdm\Member\Models\Work::where('status', '=', 'process')->count();
        $honor            = \Sdm\Member\Models\Honor::where('status', '=', 'process')->count();
        $brivet           = \Sdm\Member\Models\Brivet::where('status', '=', 'process')->count();
        $total            = $attachment + $langugage + $education + $educationPolries + $training + $duty + $position + $grade + $work + $honor + $brivet;
        return $total;
    }
}
