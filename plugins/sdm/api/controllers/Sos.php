<?php namespace Sdm\Api\Controllers;

use Sdm\Api\Classes\ApiController;

class Sos extends ApiController
{
    public function all()
    {
        $sos = \Sdm\Sos\Models\Sos::whereIsClose(0)->get();
        return response()->json([
            'result'   => true,
            'response' => $this->respondWithCollection($sos, new \Sdm\Api\Transformers\SosTransformer)
        ]);
    }

    public function active()
    {
        $user= \Sdm\User\Models\User::find(input('user_id'));
        $sos = \Sdm\Sos\Models\Sos::whereMemberId($user->member->id)->whereIsClose(0)->orderBy('id', 'desc')->first();
        return response()->json([
            'result'    => true,
            'response'  => $this->respondWithItem($sos, new \Sdm\Api\Transformers\SosTransformer)
        ]);
    }

    public function stop()
    {
        $sos = \Sdm\Sos\Models\Sos::find(post('id'))->update([
            'is_close' => 1
        ]);
        return response()->json([
            'result' => true
        ]);
    }

    public function create()
    {
        $notification = new \Sdm\Core\Classes\Notification;
        $user = \Sdm\User\Models\User::find(post('user_id'));
        $sos  = \Sdm\Sos\Models\Sos::create([
            'member_id' => $user->member->id,
            'latitude'  => post('latitude'),
            'longitude' => post('longitude'),
        ]);

        $tokens = \Sdm\User\Models\Token::whereNotIn('user_id', [$user->id])->get()->pluck('token');
        // $tokens = \Sdm\USer\Models\Token::whereUserId(1)->get()->pluck('token');
        $body   = $sos->member->name.' menekan tombol darurat, periksa sekarang';
        $content = [
            'title'       => 'Pemberitahuan Keadaan darurat',
            'body'        =>  $body,
            'application' => [
                'page'      => 'Sos',
                'parameter' => 'sosId',
                'key'       => $sos->id
            ]
        ];
        $notification->pulse('member', $tokens, $content);

        return response()->json([
            'result'    => true,
            'response'  => $this->respondWithItem($sos, new \Sdm\Api\Transformers\SosTransformer)
        ]);
    }

    public function get($id)
    {
        $sos = \Sdm\Sos\Models\Sos::find($id);
        return response()->json([
            'result'    => true,
            'response'  => $this->respondWithItem($sos, new \Sdm\Api\Transformers\SosTransformer)
        ]);
    }
}
