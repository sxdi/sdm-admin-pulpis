<?php namespace Sdm\Api\Controllers;

use Carbon\Carbon;

use Sdm\Api\Classes\ApiController;

use Sdm\Api\Transformers\HealthConsultationTransformer;

class HealthConsultation extends ApiController
{
    public function thread()
    {
        return response()->json([
            'result'    => true,
            'response'  => [
                'data' => \Sdm\Health\Models\Consultation::whereIsClose(0)->whereUserId(input('id'))->get()
            ],
        ]);
    }

    public function new()
    {
        $consultation = \Sdm\Health\Models\Consultation::create([
            'user_id' => post('user_id'),
            'title'   => post('title'),
        ]);

        $message      = \Sdm\Health\Models\ConsultationDetail::create([
            'user_id'         => post('user_id'),
            'consultation_id' => $consultation->id,
            'message'         => 'Saya ingin konsultasi terkait : '.post('title')
        ]);

        return response()->json([
            'result'   => true,
            'response' => [
                'data' => $consultation
            ],
        ]);
    }

    public function create()
    {
        $notification = new \Sdm\Core\Classes\Notification;
        $consultation = \Sdm\Health\Models\Consultation::find(post('consultation_id'));
        $message      = \Sdm\Health\Models\ConsultationDetail::create([
            'user_id'         => post('user_id'),
            'consultation_id' => $consultation->id,
            'message'         => post('message')
        ]);

        if($consultation->user_id == post('user_id')) {
            // Sent to admin
            $users   = \Sdm\User\Models\User::whereIsHealth(1)->get()->pluck('id');
            $tokens  = \Sdm\User\Models\Token::whereIn('user_id', $users)->get()->pluck('token');
            $body    = post('message');
            $content = [
                'title'       => $consultation->title,
                'body'        => $body,
                'application' => [
                    'page'      => 'HealthChat',
                    'parameter' => 'healthId',
                    'key'       => $consultation->id
                ]
            ];
            $notification->pulse('admin', $tokens, $content);
        }
        else {
            // Sent to member
            $tokens  = \Sdm\User\Models\Token::whereUserId(post('user_id'))->get()->pluck('token');
            $body    = post('message');
            $content = [
                'title'       => $consultation->title,
                'body'        => $body,
                'application' => [
                    'page'      => 'HealthDetail',
                    'parameter' => 'healthId',
                    'key'       => $consultation->id
                ]
            ];
            $notification->pulse('member', $tokens, $content);
        }

        return response()->json([
            'result' => true,
        ]);
    }

    public function tag()
    {
        $rules = [
            'tag' => 'required',
        ];
        $attributeNames = [
            'tag' => 'tanda',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $arrInput     = [];
        $tagInput     = explode(',', post('tag'));
        foreach ($tagInput as $tag) {
            $tg = \Sdm\Health\Models\Tag::firstOrNew([
                'name' => $tag
            ]);
            $tg->save();
            array_push($arrInput, $tg->id);
        }
        $consultation = \Sdm\Health\Models\Consultation::find(post('consultation_id'));
        $consultation->tags()->sync($arrInput);

        return response()->json([
            'result' => true
        ]);
    }

    public function close()
    {
        $consultation = \Sdm\Health\Models\Consultation::whereId(post('consultation_id'))->update([
            'is_close'  => 1,
            'closed_at' => date('Y-m-d H:i:s')
        ]);
        return response()->json([
            'result' => true
        ]);
    }

    public function active()
    {
        $consultations = \Sdm\Health\Models\Consultation::whereIsClose(0)->orderBy('created_at', 'desc')->get();
        return response()->json([
            'result'        => true,
            'consultations' => $this->respondWithCollection($consultations, new HealthConsultationTransformer)
        ]);
    }

    public function history()
    {
        $consultations = \Sdm\Health\Models\Consultation::whereIsClose(1)->orderBy('created_at', 'desc')->get();
        return response()->json([
            'result'        => true,
            'consultations' => $this->respondWithCollection($consultations, new HealthConsultationTransformer)
        ]);
    }

    public function detail($id)
    {
        $consultations = \Sdm\Health\Models\Consultation::find($id);
        $chats         = \Sdm\Health\Models\ConsultationDetail::whereConsultationId($consultations->id)->orderBy('created_at', 'desc')->get();
        $messages      = [];

        foreach ($chats as $key => $message) {
            $data = [
                '_id'       => $key,
                'text'      => $message->message,
                'createdAt' => $message->created_at->format('d F Y H:i'),
                'user'      => [
                    '_id'    => (int) $message->user_id,
                    // 'avatar' => 'https://placeimg.com/140/140/any'
                ]
            ];
            if($message->user->is_health) {
                if($message->user->member->picture) {
                    $data['user']['avatar'] = $message->user->member->picture->getThumb('140', '140', 'crop');
                }
                else {
                    $data['user']['avatar'] = 'https://placeimg.com/140/140/any';
                }

                $data['user']['name'] = $message->user->member->name . ' (Konsultan)';
            }
            else {
                $data['user']['name'] = 'Anggota';
            }
            array_push($messages, $data);
        }

        return response()->json([
            'result'        => true,
            'consultation'  => $this->respondWithItem($consultations, new HealthConsultationTransformer),
            'messages'      => $messages
        ]);
    }
}
