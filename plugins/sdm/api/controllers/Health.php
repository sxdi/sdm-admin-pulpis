<?php namespace Sdm\Api\Controllers;

use Carbon\Carbon;

use Sdm\Api\Classes\ApiController;

class Health extends ApiController
{
    public function dashboard()
    {
        for ($i=0; $i < 6; $i++) {
            $dates[] = \Carbon\Carbon::parse(date('2015-01-01'))->addYear($i)->format('Y');
        }
        $selectedConsultation = input('date_consultation') ? input('date_consultation') : date('Y');
        $selectedHistory      = input('date_history') ? input('date_history') : date('Y');
        $months   = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        foreach ($months as $key => $month) {
            $key = $key + 1;
            if($key < 10) {
                $key = '0'.$key;
            }

            $rrDashboard['text'][]  = substr($month, 0, 3);
            $rrDashboard['month'][] = $month;
            $rrDashboard['value'][] = \Sdm\Health\Models\Consultation::whereDate('created_at', 'like', '%'.$selectedConsultation.'-'.date($key).'%')->count();

            $nnDashboard['text'][]  = substr($month, 0, 3);
            $nnDashboard['month'][] = $month;
            $nnDashboard['value'][] = \Sdm\Member\Models\Health::whereDate('date', 'like', '%'.$selectedHistory.'-'.date($key).'%')->count();
        }

        $tagsHistory = \Sdm\Health\Models\Tag::join('sdm_member_health_tags', 'sdm_member_health_tags.tag_id', '=', 'sdm_health_tags.id')
            ->groupBy('sdm_health_tags.id')
            ->orderBy('tag_count', 'desc')
            ->get([
                'sdm_health_tags.id', 'sdm_health_tags.name', \DB::raw('count(sdm_health_tags.id) as tag_count')
            ]);

        $tagConsultation = \Sdm\Health\Models\Tag::join('sdm_health_consultation_tags', 'sdm_health_consultation_tags.tag_id', '=', 'sdm_health_tags.id')
            ->groupBy('sdm_health_tags.id')
            ->orderBy('tag_count', 'desc')
            ->get([
                'sdm_health_tags.id', 'sdm_health_tags.name', \DB::raw('count(sdm_health_tags.id) as tag_count')
            ]);

        return response()->json([
            'result'   => true,
            'response' => [
                'date'         => [
                    'lists'    => $dates,
                    'selected' => [
                        'consultation' => $selectedConsultation,
                        'history'      => $selectedHistory,
                    ]
                ],
                'consultation' => [
                    'month' => $rrDashboard['month'],
                    'text'  => $rrDashboard['text'],
                    'value' => $rrDashboard['value'],
                    'tags'  => $tagConsultation,
                ],
                'history' => [
                    'month' => $nnDashboard['month'],
                    'text'  => $nnDashboard['text'],
                    'value' => $nnDashboard['value'],
                    'tags'  => $tagsHistory,
                ],
            ]
        ]);
    }

    public function index()
    {
        $healths = \Sdm\Member\Models\Health::orderBy('date', 'desc')->get();
        return response()->json([
            'result'    => true,
            'response'  => $this->respondWithCollection($healths, new \Sdm\Api\Transformers\HealthTransformer)
        ]);
    }

    public function save()
    {
        $rules = [
            'date'      => 'required',
            'title'     => 'required',
            'member_id' => 'required',
            'admin_id'  => 'required',
            'content'   => 'required',
        ];
        $attributeNames = [
            'date'      => 'tanggal pemeriksaan',
            'title'     => 'penyakit',
            'member_id' => 'anggota',
            'admin_id'  => 'required',
            'content'   => 'keterangan',
        ];
        $messages  = [];

        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        if(!post('id')) {
            if(!\Input::hasFile('sickFile')) {
                return response()->json([
                    'result'  => false,
                    'message' => 'Lampiran wajib di upload'
                ]);
            }
        }

        $admin    = \Sdm\Member\Models\Member::find(post('admin_id'));
        $member   = \Sdm\Member\Models\Member::find(post('member_id'));

        $health   = \Sdm\Member\Models\Health::firstOrNew([
            'id'        => post('id'),
        ]);
        $health->member_id    = post('member_id');
        $health->admin_id     = post('admin_id');
        $health->title        = post('title');
        $health->content      = post('content');
        $health->date         = Carbon::parse(post('date'))->format('Y-m-d');
        $health->save();

        // Tag
        if(post('tag')) {
            $arrInput     = [];
            $tagInput     = explode(',', post('tag'));
            foreach ($tagInput as $tag) {
                $tg = \Sdm\Health\Models\Tag::firstOrNew([
                    'name' => $tag
                ]);
                $tg->save();
                array_push($arrInput, $tg->id);
            }

            $health->tags()->sync($arrInput);
        }
        else {
            $health->tags()->detach();
        }

        if(\Input::hasFile('sickFile')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('sickFile');
            $file->save();
            $health->document()->add($file);
        }

        return response()->json([
            'result' => true
        ]);
    }

    public function detail($id)
    {
        $health = \Sdm\Member\Models\Health::find($id);
        return response()->json([
            'result'   => true,
            'response' => $this->respondWithItem($health, new \Sdm\Api\Transformers\HealthTransformer)
        ]);
    }

    public function delete($id)
    {
        $health = \Sdm\Member\Models\Health::find($id);
        $health->document->delete();
        $health->delete();
        return response()->json([
            'result' => true
        ]);
    }

    public function report()
    {
        $healths             = \Sdm\Member\Models\Health::orderBy('date', 'desc')->get();
        $letters             = \Sdm\Letter\Models\Letter::whereIn('id', [6, 12])->get();
        $members             = \Sdm\Member\Models\Member::whereIn('id', $healths->pluck('member_id'))->get();
        $user                = \Sdm\User\Models\User::find(input('user_id'));

        $generate            = new \Sdm\Letter\Models\Generator;
        $generate->member_id = $user->member_id;
        $generate->save();

        foreach ($letters as $letter) {
            $request               = new \Sdm\Letter\Models\Request;
            $request->generator_id = $generate->id;
            $request->letter_id    = $letter->id;
            $request->name         = $letter->name;
            $request->code         = $letter->code;
            $request->is_single    = $letter->is_single;
            $request->save();
        }

        foreach ($members as $member) {
            $requestMember               = new \Sdm\Letter\Models\RequestMember;
            $requestMember->generator_id = $generate->id;
            $requestMember->member_id    = $member->id;
            $requestMember->save();
        }

        return response()->json([
            'result'    => true,
            'response'  => [
                'data' => [
                    'id'   => $generate->id,
                    'file' => $generate->document ? $generate->document->path : (bool) false
                ]
            ]
        ]);
    }
}
