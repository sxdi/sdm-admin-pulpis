<?php namespace Sdm\Api\Controllers;

use Sdm\Api\Classes\ApiController;

class Letter extends ApiController
{
    public function brief()
    {
        $letters = \Sdm\Letter\Models\Letter::whereNull('parent_id')->whereIn('mode', ['urmin','urpers'])->orderBy('name')->get();
        return response()->json([
            'result'   => true,
            'response' => $this->respondwithCollection($letters, new \Sdm\Api\Transformers\LetterTransformer)
        ]);
    }

    public function briefDetail($id)
    {
        $letters = \Sdm\Letter\Models\Letter::whereParentId($id)->orderBy('name')->get();
        return response()->json([
            'result'   => true,
            'response' => $this->respondwithCollection($letters, new \Sdm\Api\Transformers\LetterTransformer)
        ]);
    }

    /**
     * [generateZip description]
     * @return [type] [description]
    */
    public function generateZip()
    {
        if(!post('letter')) {
            return response()->json([
                'result' => false,
                'message'=> 'Tidak ada data berkas terpilih'
            ]);
        }

        $letters  = \Sdm\Letter\Models\Letter::whereIn('id', post('letter'))->get();
        if($letters[0]->parent->mode == 'urmin') {
            if(!post('member')) {
                return response()->json([
                    'result' => false,
                    'message'=> 'Tidak ada data anggota terpilih'
                ]);
            }
            $members  = \Sdm\Member\Models\Member::whereIn('id', post('member'))->get();
        }
        else {
            $members  = [];
        }

        $user                = \Sdm\User\Models\User::find(post('user_id'));

        $generate            = new \Sdm\Letter\Models\Generator;
        $generate->member_id = $user->member_id;
        $generate->save();

        foreach ($letters as $letter) {
            $request               = new \Sdm\Letter\Models\Request;
            $request->generator_id = $generate->id;
            $request->letter_id    = $letter->id;
            $request->name         = $letter->name;
            $request->code         = $letter->code;
            $request->is_single    = $letter->is_single;
            $request->save();
        }

        if(count($members)) {
            foreach ($members as $member) {
                $requestMember               = new \Sdm\Letter\Models\RequestMember;
                $requestMember->generator_id = $generate->id;
                $requestMember->member_id    = $member->id;
                $requestMember->save();
            }
        }

        return response()->json([
            'result'   => true,
            'response' => [
            'data'     => [
                    'id' => $generate->id
                ]
            ]
        ]);
    }
}
