<?php namespace Sdm\Api\Controllers;

use Carbon\Carbon;

use Sdm\Api\Classes\ApiController;

use Sdm\Api\Transformers\MemberTransformer;
use Sdm\Api\Transformers\MemberSimpleTransformer;


class Member extends ApiController
{
    public function index()
    {
        $members = \Sdm\Member\Models\Member::where('name', '!=', '')->orderBy('name')->paginate(10);
        return response()->json([
            'result'     => true,
            'response'   => $this->respondWithCollection($members, new MemberSimpleTransformer),
            'pagination' => [
                'current_page'  => $members->currentPage(),
                'last_page'     => $members->lastPage(),
                'next_page_url' => $members->nextPageUrl(),
                'total'         => $members->total(),
                'count'         => $members->count(),
            ]
        ]);
    }

    public function detail($id)
    {
        $members = \Sdm\Member\Models\Member::find($id);
        return response()->json([
            'result'    => true,
            'response'  => $this->respondWithItem($members, new MemberTransformer)
        ]);
    }

    public function update()
    {
        $member = \Sdm\Member\Models\Member::orderBy('name', 'asc')->whereHas('attachments', function($q){
            return $q->where('status', '=', 'process');
        })->orWhereHas('languages', function($q){
            return $q->where('status', '=', 'process');
        })->orWhereHas('educations', function($q) {
            return $q->where('status', '=', 'process');
        })->orWhereHas('educationPolries', function($q){
            return $q->where('status', '=', 'process');
        })->orWhereHas('trainings', function($q){
            return $q->where('status', '=', 'process');
        })->orWhereHas('duties', function($q){
            return $q->where('status', '=', 'process');
        })->orWhereHas('positions', function($q){
            return $q->where('status', '=', 'process');
        })->orWhereHas('grades', function($q){
            return $q->where('status', '=', 'process');
        })->orWhereHas('works', function($q){
            return $q->where('status', '=', 'process');
        })->orWhereHas('brivets', function($q){
            return $q->where('status', '=', 'process');
        })->orWhereHas('salaries', function($q){
            return $q->where('status', '=', 'process');
        })->take(10)->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondWithCollection($member, new \Sdm\Api\Transformers\MemberUpdateTransformer),
        ]);
    }

    public function updateDetail($id)
    {
        $member = \Sdm\Member\Models\Member::find($id);
        return response()->json([
            'result'    => true,
            'response'  => $this->respondWithItem($member, new \Sdm\Api\Transformers\MemberUpdateTransformer)
        ]);
    }

    public function updateStatus()
    {
        $notification = new \Sdm\Core\Classes\Notification;
        $type         = post('type');
        if(
            $type == 'ktp' ||
            $type == 'npwp' ||
            $type == 'kk' ||
            $type == 'finger' ||
            $type == 'asabri' ||
            $type == 'bpjs' ||
            $type == 'authority' ||
            $type == 'investigator' ||
            $type == 'member' ||
            $type == 'kps' ||
            $type == 'simsa'
        ) {
            $attachment = \Sdm\Member\Models\Attachment::whereId(post('id'))->update([
                'status'  => post('status'),
                'message' => post('message')
            ]);
            $attachment = \Sdm\Member\Models\Attachment::find(post('id'));
            $member     = \Sdm\Member\Models\Member::find($attachment->member_id);
            $tokens     = $member->getToken();

            if(post('status') == 'confirm') {
                if($type == 'ktp') {
                    $content = [
                        'title'       => 'Pemberitahuan',
                        'body'        => 'KTP anda berhasil dikonfirmasi',
                        'application' => [
                            'page' => 'Attachment',
                        ]
                    ];
                }

                if($type == 'npwp') {
                    $content = [
                        'title'       => 'Pemberitahuan',
                        'body'        => 'NPWP anda berhasil dikonfirmasi',
                        'application' => [
                            'page' => 'Attachment',
                        ]
                    ];
                }

                if($type == 'kk') {
                    $content = [
                        'title'       => 'Pemberitahuan',
                        'body'        => 'KK anda berhasil dikonfirmasi',
                        'application' => [
                            'page' => 'Attachment',
                        ]
                    ];
                }

                if($type == 'finger') {
                    $content = [
                        'title'       => 'Pemberitahuan',
                        'body'        => 'Sidik jari anda berhasil dikonfirmasi',
                        'application' => [
                            'page' => 'Attachment',
                        ]
                    ];
                }

                if($type == 'asabri') {
                    $content = [
                        'title'       => 'Pemberitahuan',
                        'body'        => 'Asabri anda berhasil dikonfirmasi',
                        'application' => [
                            'page' => 'Attachment',
                        ]
                    ];
                }

                if($type == 'bpjs') {
                    $content = [
                        'title'       => 'Pemberitahuan',
                        'body'        => 'BPJS anda berhasil dikonfirmasi',
                        'application' => [
                            'page' => 'Attachment',
                        ]
                    ];
                }

                if($type == 'authority') {
                    $content = [
                        'title'       => 'Pemberitahuan',
                        'body'        => 'Tanda kewenangan anda berhasil dikonfirmasi',
                        'application' => [
                            'page' => 'Attachment',
                        ]
                    ];
                }

                if($type == 'investigator') {
                    $content = [
                        'title'       => 'Pemberitahuan',
                        'body'        => 'Registrasi penyidik anda berhasil dikonfirmasi',
                        'application' => [
                            'page' => 'Attachment',
                        ]
                    ];
                }

                if($type == 'member') {
                    $content = [
                        'title'       => 'Pemberitahuan',
                        'body'        => 'Kartu tanda anggota anda berhasil dikonfirmasi',
                        'application' => [
                            'page' => 'Attachment',
                        ]
                    ];
                }

                if($type == 'kps') {
                    $content = [
                        'title'       => 'Pemberitahuan',
                        'body'        => 'KPI/KPS/Karsu anda berhasil dikonfirmasi',
                        'application' => [
                            'page' => 'Attachment',
                        ]
                    ];
                }

                if($type == 'simsa') {
                    $content = [
                        'title'       => 'Pemberitahuan',
                        'body'        => 'Surat izin pemegang senjata anda berhasil dikonfirmasi',
                        'application' => [
                            'page' => 'Attachment',
                        ]
                    ];
                }
            }

            if(post('status') == 'reject') {
                if($type == 'ktp') {
                    $content = [
                        'title'       => 'Pemberitahuan',
                        'body'        => 'KTP anda telah ditolak karena '.post('message'),
                        'application' => [
                            'page' => 'Attachment',
                        ]
                    ];
                }

                if($type == 'npwp') {
                    $content = [
                        'title'       => 'Pemberitahuan',
                        'body'        => 'NPWP anda telah ditolak karena '.post('message'),
                        'application' => [
                            'page' => 'Attachment',
                        ]
                    ];
                }

                if($type == 'kk') {
                    $content = [
                        'title'       => 'Pemberitahuan',
                        'body'        => 'KK anda telah ditolak karena '.post('message'),
                        'application' => [
                            'page' => 'Attachment',
                        ]
                    ];
                }

                if($type == 'finger') {
                    $content = [
                        'title'       => 'Pemberitahuan',
                        'body'        => 'Sidik jari anda telah ditolak karena '.post('message'),
                        'application' => [
                            'page' => 'Attachment',
                        ]
                    ];
                }

                if($type == 'asabri') {
                    $content = [
                        'title'       => 'Pemberitahuan',
                        'body'        => 'Asabri anda telah ditolak karena '.post('message'),
                        'application' => [
                            'page' => 'Attachment',
                        ]
                    ];
                }

                if($type == 'bpjs') {
                    $content = [
                        'title'       => 'Pemberitahuan',
                        'body'        => 'BPJS anda telah ditolak karena '.post('message'),
                        'application' => [
                            'page' => 'Attachment',
                        ]
                    ];
                }

                if($type == 'authority') {
                    $content = [
                        'title'       => 'Pemberitahuan',
                        'body'        => 'Tanda kewenangan anda telah ditolak karena '.post('message'),
                        'application' => [
                            'page' => 'Attachment',
                        ]
                    ];
                }

                if($type == 'investigator') {
                    $content = [
                        'title'       => 'Pemberitahuan',
                        'body'        => 'Registrasi penyidik anda telah ditolak karena '.post('message'),
                        'application' => [
                            'page' => 'Attachment',
                        ]
                    ];
                }

                if($type == 'member') {
                    $content = [
                        'title'       => 'Pemberitahuan',
                        'body'        => 'Kartu tanda anggota anda telah ditolak karena '.post('message'),
                        'application' => [
                            'page' => 'Attachment',
                        ]
                    ];
                }

                if($type == 'kps') {
                    $content = [
                        'title'       => 'Pemberitahuan',
                        'body'        => 'KPI/KPS/Karsu anda telah ditolak karena '.post('message'),
                        'application' => [
                            'page' => 'Attachment',
                        ]
                    ];
                }

                if($type == 'simsa') {
                    $content = [
                        'title'       => 'Pemberitahuan',
                        'body'        => 'Surat izin pemegang senjata anda telah ditolak karena '.post('message'),
                        'application' => [
                            'page' => 'Attachment',
                        ]
                    ];
                }
            }

            $notification->pulse('member', $tokens, $content);

            return response()->json([
                'result' => true,
            ]);
        }

        if($type == 'language') {
            $language = \Sdm\Member\Models\Language::whereId(post('id'))->update([
                'status'  => post('status'),
                'message' => post('message')
            ]);

            $language = \Sdm\Member\Models\Language::find(post('id'));
            $member   = \Sdm\Member\Models\Member::find($language->member_id);
            $tokens   = $member->getToken();
            if(post('status') == 'confirm') {
                $content = [
                    'title'       => 'Pemberitahuan',
                    'body'        => 'Data penguasaan bahasa anda berhasil dikonfirmasi',
                    'application' => [
                        'page' => 'Language',
                    ]
                ];
            }
            if(post('status') == 'reject') {
                $content = [
                    'title'       => 'Pemberitahuan',
                    'body'        => 'Data penguasaan bahasa anda ditolak',
                    'application' => [
                        'page' => 'Language',
                    ]
                ];
            }
            $notification->pulse('member', $tokens, $content);

            return response()->json([
                'result' => true
            ]);
        }

        if($type == 'sport') {
            $sport = \Sdm\Member\Models\Sport::whereId(post('id'))->update([
                'status'  => post('status'),
                'message' => post('message')
            ]);

            $sport  = \Sdm\Member\Models\Sport::find(post('id'));
            $member   = \Sdm\Member\Models\Member::find($sport->member_id);
            $tokens   = $member->getToken();
            if(post('status') == 'confirm') {
                $content = [
                    'title'       => 'Pemberitahuan',
                    'body'        => 'Data penguasaan olah raga anda berhasil dikonfirmasi',
                    'application' => [
                        'page' => 'Sport',
                    ]
                ];
            }
            if(post('status') == 'reject') {
                $content = [
                    'title'       => 'Pemberitahuan',
                    'body'        => 'Data penguasaan olah raga anda ditolak',
                    'application' => [
                        'page' => 'Sport',
                    ]
                ];
            }
            $notification->pulse('member', $tokens, $content);

            return response()->json([
                'result' => true
            ]);
        }

        if($type == 'education') {
            $education = \Sdm\Member\Models\Education::whereId(post('id'))->update([
                'status'  => post('status'),
                'message' => post('message')
            ]);

            $education = \Sdm\Member\Models\Education::find(post('id'));
            $member    = \Sdm\Member\Models\Member::find($education->member_id);
            $tokens    = $member->getToken();
            if(post('status') == 'confirm') {
                $content = [
                    'title'       => 'Pemberitahuan',
                    'body'        => 'Data pendidikan anda berhasil dikonfirmasi',
                    'application' => [
                        'page' => 'Education',
                    ]
                ];
            }
            if(post('status') == 'reject') {
                $content = [
                    'title'       => 'Pemberitahuan',
                    'body'        => 'Data pendidikan anda ditolak',
                    'application' => [
                        'page' => 'Education',
                    ]
                ];
            }
            $notification->pulse('member', $tokens, $content);

            return response()->json([
                'result' => true
            ]);
        }

        if($type == 'educationpolries') {
            $education = \Sdm\Member\Models\EducationPolice::whereId(post('id'))->update([
                'status'  => post('status'),
                'message' => post('message')
            ]);

            $education = \Sdm\Member\Models\EducationPolice::find(post('id'));
            $member    = \Sdm\Member\Models\Member::find($education->member_id);
            $tokens    = $member->getToken();
            if(post('status') == 'confirm') {
                $content = [
                    'title'       => 'Pemberitahuan',
                    'body'        => 'Data pendidikan polri/dikbangspes anda berhasil dikonfirmasi',
                    'application' => [
                        'page' => 'EducationPolri',
                    ]
                ];
            }
            if(post('status') == 'reject') {
                $content = [
                    'title'       => 'Pemberitahuan',
                    'body'        => 'Data pendidikan polri/dikbangspes anda ditolak',
                    'application' => [
                        'page' => 'EducationPolri',
                    ]
                ];
            }
            $notification->pulse('member', $tokens, $content);

            return response()->json([
                'result' => true
            ]);
        }

        if($type == 'honor') {
            $honor = \Sdm\Member\Models\Honor::whereId(post('id'))->update([
                'status'  => post('status'),
                'message' => post('message')
            ]);

            $honor  = \Sdm\Member\Models\Honor::find(post('id'));
            $member = \Sdm\Member\Models\Member::find($honor->member_id);
            $tokens = $member->getToken();
            if(post('status') == 'confirm') {
                $content = [
                    'title'       => 'Pemberitahuan',
                    'body'        => 'Data tanda kehormatan/jasa anda berhasil dikonfirmasi',
                    'application' => [
                        'page' => 'Honor',
                    ]
                ];
            }
            if(post('status') == 'reject') {
                $content = [
                    'title'       => 'Pemberitahuan',
                    'body'        => 'Data tanda kehormatan/jasa anda ditolak',
                    'application' => [
                        'page' => 'Honor',
                    ]
                ];
            }
            $notification->pulse('member', $tokens, $content);

            return response()->json([
                'result' => true
            ]);
        }

        if($type == 'training') {
            $training = \Sdm\Member\Models\Training::whereId(post('id'))->update([
                'status'  => post('status'),
                'message' => post('message')
            ]);

            $training = \Sdm\Member\Models\Training::find(post('id'));
            $member   = \Sdm\Member\Models\Member::find($training->member_id);
            $tokens   = $member->getToken();
            if(post('status') == 'confirm') {
                $content = [
                    'title'       => 'Pemberitahuan',
                    'body'        => 'Data riwayat pelatihan anda berhasil dikonfirmasi',
                    'application' => [
                        'page' => 'Training',
                    ]
                ];
            }
            if(post('status') == 'reject') {
                $content = [
                    'title'       => 'Pemberitahuan',
                    'body'        => 'Data riwayat pelatihan anda ditolak',
                    'application' => [
                        'page' => 'Training',
                    ]
                ];
            }
            $notification->pulse('member', $tokens, $content);

            return response()->json([
                'result' => true
            ]);
        }

        if($type == 'duty') {
            $duty = \Sdm\Member\Models\Duty::whereId(post('id'))->update([
                'status'  => post('status'),
                'message' => post('message')
            ]);

            $duty   = \Sdm\Member\Models\Duty::find(post('id'));
            $member = \Sdm\Member\Models\Member::find($duty->member_id);
            $tokens = $member->getToken();
            if(post('status') == 'confirm') {
                $content = [
                    'title'       => 'Pemberitahuan',
                    'body'        => 'Data riwayat penugasan luar negeri anda berhasil dikonfirmasi',
                    'application' => [
                        'page' => 'Duty',
                    ]
                ];
            }
            if(post('status') == 'reject') {
                $content = [
                    'title'       => 'Pemberitahuan',
                    'body'        => 'Data riwayat penugasan luar negeri anda ditolak',
                    'application' => [
                        'page' => 'Duty',
                    ]
                ];
            }
            $notification->pulse('member', $tokens, $content);

            return response()->json([
                'result' => true
            ]);
        }

        if($type == 'position') {
            $position = \Sdm\Member\Models\Position::whereId(post('id'))->update([
                'status'  => post('status'),
                'message' => post('message')
            ]);

            $position = \Sdm\Member\Models\Position::find(post('id'));
            $member   = \Sdm\Member\Models\Member::find($position->member_id);
            $tokens   = $member->getToken();
            if(post('status') == 'confirm') {
                $content = [
                    'title'       => 'Pemberitahuan',
                    'body'        => 'Data riwayat jabatan anda berhasil dikonfirmasi',
                    'application' => [
                        'page' => 'Position',
                    ]
                ];
            }
            if(post('status') == 'reject') {
                $content = [
                    'title'       => 'Pemberitahuan',
                    'body'        => 'Data riwayat jabatan anda ditolak',
                    'application' => [
                        'page' => 'Position',
                    ]
                ];
            }
            $notification->pulse('member', $tokens, $content);

            return response()->json([
                'result' => true
            ]);
        }

        if($type == 'grade') {
            $grade = \Sdm\Member\Models\Grade::whereId(post('id'))->update([
                'status'  => post('status'),
                'message' => post('message')
            ]);

            $grade  = \Sdm\Member\Models\Grade::find(post('id'));
            $member = \Sdm\Member\Models\Member::find($grade->member_id);
            $tokens = $member->getToken();
            if(post('status') == 'confirm') {
                $content = [
                    'title'       => 'Pemberitahuan',
                    'body'        => 'Data riwayat pangkat anda berhasil dikonfirmasi',
                    'application' => [
                        'page' => 'Grade',
                    ]
                ];
            }
            if(post('status') == 'reject') {
                $content = [
                    'title'       => 'Pemberitahuan',
                    'body'        => 'Data riwayat pangkat anda ditolak',
                    'application' => [
                        'page' => 'Grade',
                    ]
                ];
            }
            $notification->pulse('member', $tokens, $content);

            return response()->json([
                'result' => true
            ]);
        }

        if($type == 'work') {
            $work = \Sdm\Member\Models\Work::whereId(post('id'))->update([
                'status'  => post('status'),
                'message' => post('message')
            ]);

            $work   = \Sdm\Member\Models\Work::find(post('id'));
            $member = \Sdm\Member\Models\Member::find($work->member_id);
            $tokens = $member->getToken();
            if(post('status') == 'confirm') {
                $content = [
                    'title'       => 'Pemberitahuan',
                    'body'        => 'Data riwayat penugasan anda berhasil dikonfirmasi',
                    'application' => [
                        'page' => 'Work',
                    ]
                ];
            }
            if(post('status') == 'reject') {
                $content = [
                    'title'       => 'Pemberitahuan',
                    'body'        => 'Data riwayat penugasan anda ditolak',
                    'application' => [
                        'page' => 'Work',
                    ]
                ];
            }
            $notification->pulse('member', $tokens, $content);

            return response()->json([
                'result' => true
            ]);
        }

        if($type == 'brivet') {
            $brivet = \Sdm\Member\Models\Brivet::whereId(post('id'))->update([
                'status'  => post('status'),
                'message' => post('message')
            ]);

            $brivet = \Sdm\Member\Models\Brivet::find(post('id'));
            $member = \Sdm\Member\Models\Member::find($brivet->member_id);
            $tokens = $member->getToken();
            if(post('status') == 'confirm') {
                $content = [
                    'title'       => 'Pemberitahuan',
                    'body'        => 'Data riwayat brivet anda berhasil dikonfirmasi',
                    'application' => [
                        'page' => 'Brivet',
                    ]
                ];
            }
            if(post('status') == 'reject') {
                $content = [
                    'title'       => 'Pemberitahuan',
                    'body'        => 'Data riwayat brivet anda ditolak',
                    'application' => [
                        'page' => 'Brivet',
                    ]
                ];
            }
            $notification->pulse('member', $tokens, $content);

            return response()->json([
                'result' => true
            ]);
        }

        if($type == 'salary') {
            $salary = \Sdm\Member\Models\Salary::whereId(post('id'))->update([
                'status'  => post('status'),
                'message' => post('message')
            ]);

            $salary = \Sdm\Member\Models\Salary::find(post('id'));
            $member = \Sdm\Member\Models\Member::find($salary->member_id);
            $tokens = $member->getToken();
            if(post('status') == 'confirm') {
                $content = [
                    'title'       => 'Pemberitahuan',
                    'body'        => 'Data riwayat kenaikan gaji anda berhasil dikonfirmasi',
                    'application' => [
                        'page' => 'Salary',
                    ]
                ];
            }
            if(post('status') == 'reject') {
                $content = [
                    'title'       => 'Pemberitahuan',
                    'body'        => 'Data riwayat kenaikan gaji anda ditolak',
                    'application' => [
                        'page' => 'Salary',
                    ]
                ];
            }
            $notification->pulse('member', $tokens, $content);

            return response()->json([
                'result' => true
            ]);
        }
    }

    public function print()
    {
        $letterManager      = new \Sdm\Core\Classes\LetterManager;
        $letter             = \Sdm\Letter\Models\Letter::wherecode(input('code'))->first();
        $members            = \Sdm\Member\Models\Member::whereIn('id', [input('id')])->get();
        $path               = $letter->document->path;

        $request            = new \Sdm\Letter\Models\Request;
        $request->letter_id = $letter->id;
        $request->name      = $letter->name;
        $request->save();

        if(!$letter->is_single) {
            if($letter->code == 'urmin_senpi') {
                $letterManager->processPersonel([
                    'path'    => $path,
                    'request' => $request,
                    'members' => $members,
                ]);
            }

            if($letter->code == 'urmin_things_pdh') {
                $letterManager->processPersonel([
                    'path'    => $path,
                    'request' => $request,
                    'members' => $members,
                ]);
            }

            if($letter->code == 'urmin_rh_polri') {
                $letterManager->processPersonel([
                    'path'    => $path,
                    'request' => $request,
                    'members' => $members,
                    'rh_polri'=> true,
                ]);
            }

            if($letter->code == 'urmin_rh_complete') {
                $letterManager->processPersonel([
                    'path'        => $path,
                    'request'     => $request,
                    'members'     => $members,
                    'rh_complete' => true,
                ]);
            }

            if($letter->code == 'urmin_asabri_ptdh') {
                $letterManager->processAsabriPtdh([
                    'path'    => $path,
                    'request' => $request,
                    'members' => $members
                ]);
            }

            if($letter->code == 'urmin_letter_pdh') {
                $letterManager->processPersonel([
                    'path'    => $path,
                    'request' => $request,
                    'members' => $members
                ]);
            }

            if($letter->code == 'urmin_address_pension') {
                $letterManager->processPersonel([
                    'path'    => $path,
                    'request' => $request,
                    'members' => $members
                ]);
            }

            if($letter->code == 'urmin_lmpa') {
                $letterManager->processPersonel([
                    'path'    => $path,
                    'request' => $request,
                    'members' => $members
                ]);
            }

            if($letter->code == 'urmin_divorce') {
                $letterManager->processPersonel([
                    'path'          => $path,
                    'request'       => $request,
                    'members'       => $members,
                    'urmin_divorce' => true
                ]);
            }

            if($letter->code == 'urmin_marriage') {
                $letterManager->processPersonel([
                    'path'          => $path,
                    'request'       => $request,
                    'members'       => $members,
                    'urmin_marriage' => true
                ]);
            }

            if($letter->code == 'urmin_sarmin_asabri') {
                $letterManager->processSarminAsabri([
                    'path'    => $letter->document->getLocalPath(),
                    'request' => $request,
                    'members' => $members
                ]);
            }

            if($letter->code == 'urmin_request_kpi_kps') {
                $letterManager->processRequestKpi([
                    'path'    => $path,
                    'request' => $request,
                    'members' => $members
                ]);
            }
        }

        // Group Print
        if($letter->code == 'urmin_mapping_personel') {
            $letterManager->processMapingPersonel([
                'path'    => $path,
                'request' => $request,
                'members' => $members,
            ]);
        }

        if($letter->code == 'urmin_trouble_personel') {
            $letterManager->processMapingPersonel([
                'path'    => $path,
                'request' => $request,
                'members' => $members
            ]);
        }

        if($letter->code == 'urmin_yearly_sick') {
            $letterManager->processMapingPersonel([
                'path'    => $path,
                'request' => $request,
                'members' => $members
            ]);
        }

        if($letter->code == 'urmin_member_house') {
            $letterManager->processMapingPersonel([
                'path'    => $path,
                'request' => $request,
                'members' => $members
            ]);
        }

        return response()->json([
            'result'   => true,
            'response' => $request->members[0]->document->path,
        ]);
    }

    public function find()
    {
        $q      = input('q');
        if($q) {
            $member = \Sdm\Member\Models\Member::where('name', 'like', '%'.$q.'%')->orWhere('nrp', 'like', '%'.$q.'%')->take(10)->get();
            return response()->json([
                'result'   => true,
                'response' => $this->respondWithCollection($member, new MemberSimpleTransformer)
            ]);
        }

        return response()->json([
            'result'   => true,
            'response' => [
                'data' => []
            ],
        ]);
    }

    public function search($isAll=null)
    {
        $member = \Sdm\Member\Models\Member::where('id', '>', 0);
        // Form Personal
        if(post('isName') === 'true') {
            if(post('name')) {
                $name  = post('name');
                $names = explode(',', $name);

                if(post('name_logic') == 'same') {
                    $member = $member->where('name', 'like', '%'.$name.'%');
                }

                if(post('name_logic') == 'in') {
                    foreach ($names as $name) {
                        $find = \Sdm\Member\Models\Member::select('id')->where('name', 'like', '%'.$name.'%')->get();
                        if($find) {
                            $inName = [];
                            foreach ($find as $member) {
                                array_push($inName, $member->id);
                            }
                            $member = $member->whereIn('id', $inName);
                        }
                    }
                }
            }
        }

        if(post('isGender') == 'true') {
            $gender  = post('gender');
            $member  = $member->where('gender', 'like', '%'.$gender.'%');
        }

        if(post('isNrp') == 'true') {
            if(post('nrp')) {
                $nrp  = post('nrp');
                $nrps = explode(',', $nrp);

                if(post('nrp_logic') == 'same') {
                    $member = $member->where('nrp', 'like', '%'.$nrp.'%');
                }

                if(post('nrp_logic') == 'in') {
                    foreach ($nrps as $nrp) {
                        $find = \Sdm\Member\Models\Member::select('id')->where('nrp', 'like', '%'.$nrp.'%')->get();
                        if($find) {
                            $inNrp = [];
                            foreach ($find as $member) {
                                array_push($inNrp, $member->id);
                            }
                            $member = $member->whereIn('id', $inNrp);
                        }
                    }
                }
            }
        }

        if(post('isPlace') == 'true') {
            if(post('place')) {
                $place  = post('place');
                $places = explode(',', $place);

                if(post('place_logic') == 'same') {
                    $member = $member->where('place', 'like', '%'.$place.'%');
                }

                if(post('place_logic') == 'in') {
                    foreach ($places as $place) {
                        $find = \Sdm\Member\Models\Member::select('id')->where('place', 'like', '%'.$place.'%')->get();
                        if($find) {
                            $inPlace = [];
                            foreach ($find as $member) {
                                array_push($inPlace, $member->id);
                            }
                            $member = $member->whereIn('id', $inPlace);
                        }
                    }
                }
            }
        }

        if(post('isDob') == 'true') {
            if(post('dob')) {
                $dob  = post('dob');
                $dobs = explode(',', $dob);

                if(post('dob_logic') == 'same') {
                    $member = $member->where('dob', 'like', '%'.$dob.'%');
                }

                if(post('dob_logic') == 'in') {
                    foreach ($dobs as $dob) {
                        $member = $member->orWhere('dob', $dob);
                    }
                }
            }
        }

        if(post('isBlood') == 'true') {
            if(post('blood')) {
                $blood  = post('blood');
                $bloods = explode(',', $blood);

                if(post('blood_logic') == 'same') {
                    $member = $member->where('blood', 'like', '%'.$blood.'%');
                }

                if(post('blood_logic') == 'in') {
                    foreach ($bloods as $blood) {
                        $find = \Sdm\Member\Models\Member::select('id')->where('blood', 'like', '%'.$blood.'%')->get();
                        if($find) {
                            $inBlood = [];
                            foreach ($find as $member) {
                                array_push($inBlood, $member->id);
                            }
                            $member = $member->whereIn('id', $inBlood);
                        }
                    }
                }
            }
        }

        if(post('isReligion') == 'true') {
            if(post('religion')) {
                $religion  = post('religion');
                $religions = explode(',', $religion);

                if(post('religion_logic') == 'same') {
                    $member = $member->where('religion', 'like', '%'.$religion.'%');
                }

                if(post('religion_logic') == 'in') {
                    foreach ($religions as $religion) {
                        $find = \Sdm\Member\Models\Member::select('id')->where('religion', 'like', '%'.$religion.'%')->get();
                        if($find) {
                            $inReligion = [];
                            foreach ($find as $member) {
                                array_push($inReligion, $member->id);
                            }
                            $member = $member->whereIn('id', $inReligion);
                        }
                    }
                }
            }
        }

        if(post('isMarital') == 'true') {
            $marital  = post('marital');
            $maritals = explode(',', $marital);
            if(post('marital_logic') == 'same') {
                $member = $member->where('marital', 'like', '%'.$marital.'%');
            }

            if(post('marital_logic') == 'in') {
                foreach ($maritals as $marital) {
                    $find = \Sdm\Member\Models\Member::select('id')->where('marital', 'like', '%'.$marital.'%')->get();
                    if($find) {
                        $inMarital = [];
                        foreach ($find as $member) {
                            array_push($inMarital, $member->id);
                        }
                        $member = $member->whereIn('id', $inMarital);
                    }
                }
            }
        }

        if(post('isPartner') == 'true') {
            $partner  = post('partner');
            $partners = explode(',', $partner);
            if(post('partner_logic') == 'same') {
                $member = $member->where('name', 'like', '%'.$partner.'%');
            }

            if(post('partner_logic') == 'in') {
                $inPartner = [];
                foreach ($partners as $partner) {
                    $find = \Sdm\Member\Models\Family::select('id')->whereIn('relation', ['wife', 'husband'])->where('name', 'like', '%'.$partner.'%')->get();
                    if($find) {
                        $inPartner = [];
                        foreach ($find as $member) {
                            array_push($inPartner, $member->id);
                        }
                        $member = $member->whereHas('families', function($q) use($inPartner) {
                            return $q->whereIn('id', $inPartner);
                        });
                    }
                }
            }
        }

        if(post('isNationality') == 'true') {
            if(post('nationality')) {
                $nationality   = post('nationality');
                $nationalities = explode(',', $nationality);

                if(post('nationality_logic') == 'same') {
                    $member = $member->where('nationality', 'like', '%'.$nationality.'%');
                }

                if(post('nationality_logic') == 'in') {
                    foreach ($nationalities as $nationality) {
                        $find = \Sdm\Member\Models\Member::select('id')->where('nationality', 'like', '%'.$nationality.'%')->get();
                        if($find) {
                            $inNationality = [];
                            foreach ($find as $member) {
                                array_push($inNationality, $member->id);
                            }
                            $member = $member->whereIn('id', $inNationality);
                        }
                    }
                    $member = $member->whereIn('id', $key);
                }
            }
        }

        if(post('isSkin_color') == 'true') {
            if(post('skin_color')) {
                $skin_color  = post('skin_color');
                $skin_colors = explode(',', $skin_color);

                if(post('skin_color_logic') == 'same') {
                    $member = $member->where('skin_color', 'like', '%'.$skin_color.'%');
                }

                if(post('skin_color_logic') == 'in') {
                    foreach ($skin_colors as $skin_color) {
                        $find = \Sdm\Member\Models\Member::select('id')->where('skin_color', 'like', '%'.$skin_color.'%')->get();
                        if($find) {
                            $inSkinColor = [];
                            foreach ($find as $member) {
                                array_push($inSkinColor, $member->id);
                            }
                            $member = $member->whereIn('id', $inSkinColor);
                        }
                    }
                }
            }
        }

        if(post('isHair_color') == 'true') {
            if(post('hair_color')) {
                $hair_color  = post('hair_color');
                $hair_colors = explode(',', $hair_color);

                if(post('hair_color_logic') == 'same') {
                    $member = $member->where('hair_color', 'like', '%'.$hair_color.'%');
                }

                if(post('hair_color_logic') == 'in') {
                    foreach ($hair_colors as $hair_color) {
                        $find = \Sdm\Member\Models\Member::select('id')->where('hair_color', 'like', '%'.$hair_color.'%')->get();
                        if($find) {
                            $inHairColor = [];
                            foreach ($find as $member) {
                                array_push($inHairColor, $member->id);
                            }
                            $member = $member->whereIn('id', $inHairColor);
                        }
                    }
                }
            }
        }

        if(post('isHair_type') == 'true') {
            if(post('hair_type')) {
                $hair_type  = post('hair_type');
                $hair_types = explode(',', $hair_type);

                if(post('hair_type_logic') == 'same') {
                    $member = $member->where('hair_type', 'like', '%'.$hair_type.'%');
                }

                if(post('hair_type_logic') == 'in') {
                    foreach ($hair_types as $hair_type) {
                        $find = \Sdm\Member\Models\Member::select('id')->where('hair_type', 'like', '%'.$hair_type.'%')->get();
                        if($find) {
                            $inHairType = [];
                            foreach ($find as $member) {
                                array_push($inHairType, $member->id);
                            }
                            $member = $member->whereIn('id', $inHairType);
                        }
                    }
                }
            }
        }

        if(post('isHeight') == 'true') {
            if(post('height')) {
                $height = post('height');
                if(post('height_logic') == 'same') {
                    $member = $member->where('height', '=', $height);
                }

                if(post('height_logic') == 'grather') {
                    $member = $member->where('height', '>', $height);
                }

                if(post('height_logic') == 'lower') {
                    $member = $member->where('height', '<', $height);
                }
            }
        }

        if(post('isWeight') == 'true') {
            if(post('weight')) {
                $weight = post('weight');
                if(post('weight_logic') == 'same') {
                    $member = $member->where('weight', '=', $weight);
                }

                if(post('weight_logic') == 'grather') {
                    $member = $member->where('weight', '>', $weight);
                }

                if(post('weight_logic') == 'lower') {
                    $member = $member->where('weight', '<', $weight);
                }
            }
        }

        if(post('isUniform_head') == 'true') {
            if(post('uniform_head')) {
                $uniform_head = post('uniform_head');
                if(post('uniform_head_logic') == 'same') {
                    $member = $member->where('uniform_head', '=', $uniform_head);
                }

                if(post('uniform_head_logic') == 'grather') {
                    $member = $member->where('uniform_head', '>=', $uniform_head);
                }

                if(post('uniform_head_logic') == 'lower') {
                    $member = $member->where('uniform_head', '<=', $uniform_head);
                }
            }
        }

        if(post('isUniform_feet') == 'true') {
            if(post('uniform_feet')) {
                $uniform_feet = post('uniform_feet');
                if(post('uniform_feet_logic') == 'same') {
                    $member = $member->where('uniform_feet', '=', $uniform_feet);
                }

                if(post('uniform_feet_logic') == 'grather') {
                    $member = $member->where('uniform_feet', '>=', $uniform_feet);
                }

                if(post('uniform_feet_logic') == 'lower') {
                    $member = $member->where('uniform_feet', '<=', $uniform_feet);
                }
            }
        }

        if(post('isUniform_trousers') == 'true') {
            if(post('uniform_trousers')) {
                $uniform_trousers = post('uniform_trousers');
                if(post('uniform_trousers_logic') == 'same') {
                    $member = $member->where('uniform_trousers', '=', $uniform_trousers);
                }

                if(post('uniform_trousers_logic') == 'grather') {
                    $member = $member->where('uniform_trousers', '>=', $uniform_trousers);
                }

                if(post('uniform_trousers_logic') == 'lower') {
                    $member = $member->where('uniform_trousers', '<=', $uniform_trousers);
                }
            }
        }

        if(post('isUniform_clothes') == 'true') {
            if(post('uniform_clothes')) {
                $uniform_clothes = post('uniform_clothes');
                if(post('uniform_clothes_logic') == 'same') {
                    $member = $member->where('uniform_clothes', '=', $uniform_clothes);
                }

                if(post('uniform_clothes_logic') == 'grather') {
                    $member = $member->where('uniform_clothes', '>=', $uniform_clothes);
                }

                if(post('uniform_clothes_logic') == 'lower') {
                    $member = $member->where('uniform_clothes', '<=', $uniform_clothes);
                }
            }
        }

        // Form Attachment
        if(post('isKtp') == 'true') {
            $member = $member->whereHas('attachments', function($q){
                $q->where('type', '=', 'ktp');
            });

            if(post('ktp')) {
                $ktp  = post('ktp');
                $ktps = explode(',', $ktp);

                if(post('ktp_logic') == 'same') {
                    $member = $member->whereHas('attachments', function($q) use($ktp){
                        $q->where('value', 'like', '%'.$ktp.'%');
                    });
                }

                if(post('ktp_logic') == 'in') {
                    $inKtps   = [];
                    foreach ($ktps as $bri) {
                        $ktpList = \Sdm\Member\Models\Attachment::whereType('ktp')->select('id')->where('value', 'like', '%'.$bri.'%')->get();
                        if($ktpList) {
                            foreach ($ktpList as $bri) {
                                array_push($inKtps, $bri->id);
                            }
                        }
                    }

                    $member = $member->whereHas('attachments', function($q) use($inKtps) {
                        return $q->whereIn('id', $inKtps);
                    });
                }
            }
        }

        if(post('isKk') == 'true') {
            $member = $member->whereHas('attachments', function($q){
                $q->where('type', '=', 'kk');
            });

            if(post('kk')) {
                $kk  = post('kk');
                $kks = explode(',', $kk);

                if(post('kk_logic') == 'same') {
                    $member = $member->whereHas('attachments', function($q) use($kk){
                        $q->where('value', 'like', '%'.$kk.'%');
                    });
                }

                if(post('kk_logic') == 'in') {
                    $inKks   = [];
                    foreach ($kks as $bri) {
                        $kkList = \Sdm\Member\Models\Attachment::whereType('kk')->select('id')->where('value', 'like', '%'.$bri.'%')->get();
                        if($kkList) {
                            foreach ($kkList as $bri) {
                                array_push($inKks, $bri->id);
                            }
                        }
                    }

                    $member = $member->whereHas('attachments', function($q) use($inKks) {
                        return $q->whereIn('id', $inKks);
                    });
                }
            }
        }

        if(post('isFinger') == 'true') {
            $member = $member->whereHas('attachments', function($q){
                $q->where('type', '=', 'finger');
            });

            if(post('finger')) {
                $finger  = post('finger');
                $fingers = explode(',', $finger);

                if(post('finger_logic') == 'same') {
                    $member = $member->whereHas('attachments', function($q) use($finger){
                        $q->where('value', 'like', '%'.$finger.'%');
                    });
                }

                if(post('finger_logic') == 'in') {
                    $isFingers   = [];
                    foreach ($fingers as $bri) {
                        $fingerList = \Sdm\Member\Models\Attachment::whereType('finger')->select('id')->where('value', 'like', '%'.$bri.'%')->get();
                        if($fingerList) {
                            foreach ($fingerList as $bri) {
                                array_push($isFingers, $bri->id);
                            }
                        }
                    }

                    $member = $member->whereHas('attachments', function($q) use($isFingers) {
                        return $q->whereIn('id', $isFingers);
                    });
                }
            }
        }

        if(post('isAsabri') == 'true') {
            $member = $member->whereHas('attachments', function($q){
                $q->where('type', '=', 'asabri');
            });

            if(post('asabri')) {
                $asabri  = post('asabri');
                $asabris = explode(',', $asabri);

                if(post('asabri_logic') == 'same') {
                    $member = $member->whereHas('attachments', function($q) use($asabri){
                        $q->where('value', 'like', '%'.$asabri.'%');
                    });
                }

                if(post('asabri_logic') == 'in') {
                    $isAsabris   = [];
                    foreach ($asabris as $bri) {
                        $asabriList = \Sdm\Member\Models\Attachment::whereType('asabri')->select('id')->where('value', 'like', '%'.$bri.'%')->get();
                        if($asabriList) {
                            foreach ($asabriList as $bri) {
                                array_push($isAsabris, $bri->id);
                            }
                        }
                    }

                    $member = $member->whereHas('attachments', function($q) use($isAsabris) {
                        return $q->whereIn('id', $isAsabris);
                    });
                }
            }
        }

        if(post('isAuthority') == 'true') {
            $member = $member->whereHas('attachments', function($q){
                $q->where('type', '=', 'authority');
            });

            if(post('authority')) {
                $authority  = post('authority');
                $authoritys = explode(',', $authority);

                if(post('authority_logic') == 'same') {
                    $member = $member->whereHas('attachments', function($q) use($authority){
                        $q->where('value', 'like', '%'.$authority.'%');
                    });
                }

                if(post('authority_logic') == 'in') {
                    $isAuthorities   = [];
                    foreach ($authoritys as $bri) {
                        $authorityList = \Sdm\Member\Models\Attachment::whereType('authority')->select('id')->where('value', 'like', '%'.$bri.'%')->get();
                        if($authorityList) {
                            foreach ($authorityList as $bri) {
                                array_push($isAuthorities, $bri->id);
                            }
                        }
                    }

                    $member = $member->whereHas('attachments', function($q) use($isAuthorities) {
                        return $q->whereIn('id', $isAuthorities);
                    });
                }
            }
        }

        if(post('isInvestigator') == 'true') {
            $member = $member->whereHas('attachments', function($q){
                $q->where('type', '=', 'investigator');
            });

            if(post('investigator')) {
                $investigator  = post('investigator');
                $investigators = explode(',', $investigator);

                if(post('investigator_logic') == 'same') {
                    $member = $member->whereHas('attachments', function($q) use($investigator){
                        $q->where('value', 'like', '%'.$investigator.'%');
                    });
                }

                if(post('investigator_logic') == 'in') {
                    $isInvestigators   = [];
                    foreach ($investigators as $bri) {
                        $investigatorList = \Sdm\Member\Models\Attachment::whereType('investigator')->select('id')->where('value', 'like', '%'.$bri.'%')->get();
                        if($investigatorList) {
                            foreach ($investigatorList as $bri) {
                                array_push($isInvestigators, $bri->id);
                            }
                        }
                    }

                    $member = $member->whereHas('attachments', function($q) use($isInvestigators) {
                        return $q->whereIn('id', $isInvestigators);
                    });
                }
            }
        }

        if(post('isMember') == 'true') {
            $member = $member->whereHas('attachments', function($q){
                $q->where('type', '=', 'member');
            });

            if(post('member')) {
                $memberP  = post('member');
                $members = explode(',', $memberP);

                if(post('member_logic') == 'same') {
                    $member = $member->whereHas('attachments', function($q) use($memberP){
                        $q->where('value', 'like', '%'.$memberP.'%');
                    });
                }

                if(post('member_logic') == 'in') {
                    $isMembers   = [];
                    foreach ($members as $bri) {
                        $memberList = \Sdm\Member\Models\Attachment::whereType('member')->select('id')->where('value', 'like', '%'.$bri.'%')->get();
                        if($memberList) {
                            foreach ($memberList as $bri) {
                                array_push($isMembers, $bri->id);
                            }
                        }
                    }

                    $member = $member->whereHas('attachments', function($q) use($isMembers) {
                        return $q->whereIn('id', $isMembers);
                    });
                }
            }
        }

        if(post('isBpjs') == 'true') {
            $member = $member->whereHas('attachments', function($q){
                $q->where('type', '=', 'bpjs');
            });

            if(post('bpjs')) {
                $bpjs  = post('bpjs');
                $bpjss = explode(',', $bpjs);

                if(post('bpjs_logic') == 'same') {
                    $member = $member->whereHas('attachments', function($q) use($bpjs){
                        $q->where('value', 'like', '%'.$bpjs.'%');
                    });
                }

                if(post('bpjs_logic') == 'in') {
                    $isBpjs   = [];
                    foreach ($bpjss as $bri) {
                        $bpjsList = \Sdm\Member\Models\Attachment::whereType('bpjs')->select('id')->where('value', 'like', '%'.$bri.'%')->get();
                        if($bpjsList) {
                            foreach ($bpjsList as $bri) {
                                array_push($isBpjs, $bri->id);
                            }
                        }
                    }

                    $member = $member->whereHas('attachments', function($q) use($isBpjs) {
                        return $q->whereIn('id', $isBpjs);
                    });
                }
            }
        }

        if(post('isKps') == 'true') {
            $member = $member->whereHas('attachments', function($q){
                $q->where('type', '=', 'kps');
            });

            if(post('kps')) {
                $kps  = post('kps');
                $kpss = explode(',', $kps);

                if(post('kps_logic') == 'same') {
                    $member = $member->whereHas('attachments', function($q) use($kps){
                        $q->where('value', 'like', '%'.$kps.'%');
                    });
                }

                if(post('kps_logic') == 'in') {
                    $isKps   = [];
                    foreach ($kpss as $bri) {
                        $kpsList = \Sdm\Member\Models\Attachment::whereType('kps')->select('id')->where('value', 'like', '%'.$bri.'%')->get();
                        if($kpsList) {
                            foreach ($kpsList as $bri) {
                                array_push($isKps, $bri->id);
                            }
                        }
                    }

                    $member = $member->whereHas('attachments', function($q) use($isKps) {
                        return $q->whereIn('id', $isKps);
                    });
                }
            }
        }

        if(post('isSimsa') == 'true') {
            $member = $member->whereHas('attachments', function($q){
                $q->where('type', '=', 'simsa');
            });

            if(post('simsa')) {
                $simsa  = post('simsa');
                $simsas = explode(',', $simsa);

                if(post('simsa_logic') == 'same') {
                    $member = $member->whereHas('attachments', function($q) use($simsa){
                        $q->where('value', 'like', '%'.$simsa.'%');
                    });
                }

                if(post('simsa_logic') == 'in') {
                    $inSimsa   = [];
                    foreach ($simsas as $bri) {
                        $simsaList = \Sdm\Member\Models\Attachment::whereType('simsa')->select('id')->where('value', 'like', '%'.$bri.'%')->get();
                        if($simsaList) {
                            foreach ($simsaList as $bri) {
                                array_push($inSimsa, $bri->id);
                            }
                        }
                    }

                    $member = $member->whereHas('attachments', function($q) use($inSimsa) {
                        return $q->whereIn('id', $inSimsa);
                    });
                }
            }
        }

        // Form Primary
        if(post('isLanguage') == 'true') {
            if(post('language')) {
                $language  = post('language');
                $languages = explode(',', $language);

                if(post('language_logic') == 'same') {
                    $member = $member->whereHas('languages', function($q) use($language) {
                        $q->where('name', 'like', '%'.$language.'%');
                    });
                }

                if(post('language_logic') == 'in') {
                    $inLanguage   = [];
                    foreach ($languages as $lang) {
                        $languageList = \Sdm\Member\Models\Language::select('id')->where('name', 'like', '%'.$lang.'%')->get();
                        if($languageList) {
                            foreach ($languageList as $lang) {
                                array_push($inLanguage, $lang->id);
                            }
                        }
                    }

                    $member = $member->whereHas('languages', function($q) use($inLanguage) {
                        return $q->whereIn('id', $inLanguage);
                    });
                }
            }
        }

        if(post('isSport') == 'true') {
            if(post('sport')) {
                $sport  = post('sport');
                $sports = explode(',', $sport);

                if(post('sport_logic') == 'same') {
                    $member = $member->whereHas('sports', function($q) use($sport) {
                        $q->where('name', 'like', '%'.$sport.'%');
                    });
                }

                if(post('sport_logic') == 'in') {
                    $inSport   = [];
                    foreach ($sports as $sp) {
                        $sportList = \Sdm\Member\Models\Language::select('id')->where('name', 'like', '%'.$sp.'%')->get();
                        if($sportList) {
                            foreach ($sportList as $sp) {
                                array_push($inSport, $sp->id);
                            }
                        }
                    }

                    $member = $member->whereHas('sports', function($q) use($inSport) {
                        return $q->whereIn('id', $inSport);
                    });
                }
            }
        }

        if(post('isHonor') == 'true') {
             if(post('honor')) {
                $honor  = post('honor');
                $honors = explode(',', $honor);

                if(post('honor_logic') == 'same') {
                    $member = $member->whereHas('honors', function($q) use($honor) {
                        $q->where('name', 'like', '%'.$honor.'%');
                    });
                }

                if(post('honor_logic') == 'in') {
                    $inHonor   = [];
                    foreach ($honors as $hon) {
                        $honorList = \Sdm\Member\Models\Honor::select('id')->where('name', 'like', '%'.$hon.'%')->get();
                        if($honorList) {
                            foreach ($honorList as $hon) {
                                array_push($inHonor, $hon->id);
                            }
                        }
                    }

                    $member = $member->whereHas('honors', function($q) use($inHonor) {
                        return $q->whereIn('id', $inHonor);
                    });
                }
            }
        }

        if(post('isEducation') == 'true') {
            if(post('education')) {
                $study   = post('education');
                $studies = explode(',', $study);

                if(post('education_logic') == 'same') {
                    $member = $member->whereHas('educations', function($q) use($study){
                        $q->where('study', 'like', '%'.$study.'%');
                    });
                }

                if(post('education_logic') == 'in') {
                    $inEducation   = [];
                    foreach ($studies as $stu) {
                        $studyList = \Sdm\Member\Models\Education::select('id')->where('study', 'like', '%'.$stu.'%')->get();
                        if($studyList) {
                            foreach ($studyList as $stu) {
                                array_push($inEducation, $stu->id);
                            }
                        }
                    }

                    $member = $member->whereHas('educations', function($q) use($inEducation) {
                        return $q->whereIn('id', $inEducation);
                    });
                }
            }
        }

        if(post('isEducation_polri') == 'true') {
            $member = $member->whereHas('educationPolries', function($q) {
                return $q->whereType('polri');
            });

            if(post('education_polri')) {
                $education = post('education_polri');
                $educations = explode(',', $education);

                if(post('education_polri_logic') == 'same') {
                    $member = $member->whereHas('educationPolries', function($q) use($education){
                        return $q->where('name', 'like', '%'.$education.'%');
                    });
                }

                if(post('education_polri_logic') == 'in') {
                    $inEducation   = [];
                    foreach ($educations as $stu) {
                        $studyList = \Sdm\Member\Models\EducationPolice::select('id')->where('name', 'like', '%'.$stu.'%')->get();
                        if($studyList) {
                            foreach ($studyList as $stu) {
                                array_push($inEducation, $stu->id);
                            }
                        }
                    }

                    $member = $member->whereHas('educationPolries', function($q) use($inEducation) {
                        return $q->whereIn('id', $inEducation);
                    });
                }
            }
        }

        if(post('isEducation_dikbangspes') == 'true') {
            $member = $member->whereHas('educationPolries', function($q) {
                return $q->whereType('dikbangspes');
            });

            if(post('education_dikbangspes')) {
                $education = post('education_dikbangspes');
                $educations = explode(',', $education);

                if(post('education_dikbangspes_logic') == 'same') {
                    $member = $member->whereHas('educationPolries', function($q) use($education){
                        return $q->where('name', 'like', '%'.$education.'%');
                    });
                }

                if(post('education_dikbangspes_logic') == 'in') {
                    $inEducation   = [];
                    foreach ($educations as $stu) {
                        $studyList = \Sdm\Member\Models\EducationPolice::select('id')->where('name', 'like', '%'.$stu.'%')->get();
                        if($studyList) {
                            foreach ($studyList as $stu) {
                                array_push($inEducation, $stu->id);
                            }
                        }
                    }

                    $member = $member->whereHas('educationPolries', function($q) use($inEducation) {
                        return $q->whereIn('id', $inEducation);
                    });
                }
            }
        }

        if(post('isTraining') == 'true') {
            if(post('training')) {
                $training = post('training');
                $trainings = explode(',', $training);

                if(post('training_logic') == 'same') {
                    $member = $member->whereHas('trainings', function($q) use($training){
                        return $q->where('name', 'like', '%'.$training.'%');
                    });
                }

                if(post('training_logic') == 'in') {
                    $inTraining   = [];
                    foreach ($trainings as $tra) {
                        $trainingList = \Sdm\Member\Models\Training::select('id')->where('name', 'like', '%'.$tra.'%')->get();
                        if($trainingList) {
                            foreach ($trainingList as $tra) {
                                array_push($inTraining, $tra->id);
                            }
                        }
                    }

                    $member = $member->whereHas('trainings', function($q) use($inTraining) {
                        return $q->whereIn('id', $inTraining);
                    });
                }
            }
        }

        if(post('isDuty') == 'true') {
            if(post('duty')) {
                $duty   = post('duty');
                $duties = explode(',', $duty);

                if(post('duty_logic') == 'same') {
                    $member = $member->whereHas('duties', function($q) use($duty){
                        return $q->where('name', 'like', '%'.$duty.'%');
                    });
                }

                if(post('duty_logic') == 'in') {
                    $inDuties   = [];
                    foreach ($duties as $dut) {
                        $dutyList = \Sdm\Member\Models\Duty::select('id')->where('name', 'like', '%'.$dut.'%')->get();
                        if($dutyList) {
                            foreach ($dutyList as $dut) {
                                array_push($inDuties, $dut->id);
                            }
                        }
                    }

                    $member = $member->whereHas('duties', function($q) use($inDuties) {
                        return $q->whereIn('id', $inDuties);
                    });
                }
            }
        }

        if(post('isPosition') == 'true') {
            if(post('position')) {
                $position   = post('position');
                $positions  = explode(',', $position);

                if(post('position_logic') == 'same') {
                    $member = $member->whereHas('positions', function($q) use($position) {
                        $q->whereIsNow(1);
                        $q->where('name', 'like', '%'.$position.'%');
                    });
                }

                if(post('position_logic') == 'in') {
                    $inPositions   = [];
                    foreach ($positions as $pos) {
                        $positionList = \Sdm\Member\Models\Position::select('id')->whereHas('positions', function($q) use($pos){
                            $q->where('name', 'like', '%'.$pos.'%');
                        })->get();
                        if($positionList) {
                            foreach ($positionList as $pos) {
                                array_push($inPositions, $pos->id);
                            }
                        }
                    }

                    $member = $member->whereHas('positions', function($q) use($inPositions) {
                        return $q->whereIn('id', $inPositions);
                    });
                }
            }
        }

        if(post('isGrade') == 'true') {
            if(post('grade')) {
                $grade   = post('grade');
                $grades  = explode(',', $grade);

                if(post('grade_logic') == 'same') {
                    $member = $member->whereHas('grades', function($q) use($grade){
                        $q->whereIsNow(1);
                        $q->whereHas('parent', function($r) use($grade){
                            $r->where('code', '=', strtoupper($grade));
                        });
                    });
                }

                if(post('grade_logic') == 'in') {
                    $inGrades   = [];
                    foreach ($grades as $gra) {
                        $gradeList = \Sdm\Member\Models\Grade::whereIsNow(1)->select('id')->whereHas('parent', function($q) use($gra){
                            $q->where('code', '=', strtoupper($gra));
                        })->get();
                        if($gradeList) {
                            foreach ($gradeList as $gra) {
                                array_push($inGrades, $gra->id);
                            }
                        }
                    }

                    $member = $member->whereHas('grades', function($q) use($inGrades) {
                        return $q->whereIn('id', $inGrades);
                    });
                }
            }
        }

        if(post('isWork') == 'true') {
            if(post('work')) {
                $work  = post('work');
                $works = explode(',', $work);

                if(post('work_logic') == 'same') {
                    $member = $member->whereHas('works', function($q) use($work){
                        return $q->where('name', 'like', '%'.$work.'%');
                    });
                }

                if(post('work_logic') == 'in') {
                    $inWorks   = [];
                    foreach ($works as $wor) {
                        $workList = \Sdm\Member\Models\Work::select('id')->where('name', 'like', '%'.$wor.'%')->get();
                        if($workList) {
                            foreach ($workList as $wor) {
                                array_push($inWorks, $wor->id);
                            }
                        }
                    }

                    $member = $member->whereHas('works', function($q) use($inWorks) {
                        return $q->whereIn('id', $inWorks);
                    });
                }
            }
        }

        if(post('isBrivet') == 'true') {
            if(post('brivet')) {
                $brivet  = post('brivet');
                $brivets = explode(',', $brivet);

                if(post('brivet_logic') == 'same') {
                    $member = $member->whereHas('brivets', function($q) use($brivet){
                        return $q->where('name', 'like', '%'.$brivet.'%');
                    });
                }

                if(post('brivet_logic') == 'in') {
                    $inBrivets   = [];
                    foreach ($brivets as $bri) {
                        $brivetList = \Sdm\Member\Models\Brivet::select('id')->where('name', 'like', '%'.$bri.'%')->get();
                        if($brivetList) {
                            foreach ($brivetList as $bri) {
                                array_push($inBrivets, $bri->id);
                            }
                        }
                    }

                    $member = $member->whereHas('brivets', function($q) use($inBrivets) {
                        return $q->whereIn('id', $inBrivets);
                    });
                }
            }
        }

        if(!$isAll) {
            $member = $member->orderBy('name', 'asc')->paginate(10);
            return response()->json([
                'result'    => true,
                'response'  => $this->respondWithCollection($member, new MemberSimpleTransformer),
                'pagination'=> [
                    'current_page'  => $member->currentPage(),
                    'last_page'     => $member->lastPage(),
                    'next_page_url' => $member->nextPageUrl(),
                    'total'         => $member->total(),
                    'count'         => $member->count(),
                ]
            ]);
        }

        $member = $member->orderBy('name', 'asc')->get();
        return $member;
    }

    public function download()
    {
        $generator           = new \Sdm\Core\Classes\Generator;
        $letters             = \Sdm\Letter\Models\Letter::whereIn('id', [45, 12])->get();
        $members             = $this->search(true);
        $user                = \Sdm\User\Models\User::find(post('user_id'));

        $generate            = new \Sdm\Letter\Models\Generator;
        $generate->member_id = $user->member_id;
        $generate->save();

        foreach ($letters as $letter) {
            $request               = new \Sdm\Letter\Models\Request;
            $request->generator_id = $generate->id;
            $request->letter_id    = $letter->id;
            $request->name         = $letter->name;
            $request->code         = $letter->code;
            $request->is_single    = $letter->is_single;
            $request->save();
        }

        foreach ($members as $member) {
            $requestMember               = new \Sdm\Letter\Models\RequestMember;
            $requestMember->generator_id = $generate->id;
            $requestMember->member_id    = $member->id;
            $requestMember->save();
        }

        return response()->json([
            'result'    => true,
            'response'  => [
                'data' => [
                    'id'   => $generate->id,
                    'file' => $generate->document ? $generate->document->path : (bool) false
                ]
            ]
        ]);
    }

    public function report($id)
    {
        $generator = \Sdm\Letter\Models\Generator::find($id);
        return response()->json([
            'result'    => true,
            'response'  => [
                'data' => [
                    'id'   => $generator->id,
                    'file' => $generator->document ? $generator->document->path : (bool) false
                ]
            ]
        ]);
    }
}
