<?php namespace Sdm\Api\Controllers;

use Carbon\Carbon;

use Sdm\Api\Classes\ApiController;

use Sdm\Api\Transformers\UserTransformer;
use Sdm\Api\Transformers\MemberFamilyTransformer;
use Sdm\Api\Transformers\MemberLanguageTransformer;
use Sdm\Api\Transformers\MemberSportTransformer;
use Sdm\Api\Transformers\MemberHonorTransformer;
use Sdm\Api\Transformers\MemberEducationTransformer;
use Sdm\Api\Transformers\MemberEducationPolriTransformer;
use Sdm\Api\Transformers\MemberTrainingTransformer;
use Sdm\Api\Transformers\MemberDutyTransformer;
use Sdm\Api\Transformers\MemberPositionTransformer;
use Sdm\Api\Transformers\MemberGradeTransformer;
use Sdm\Api\Transformers\MemberWorkTransformer;
use Sdm\Api\Transformers\MemberBrivetTransformer;
use Sdm\Api\Transformers\MemberSalaryTransformer;
use Sdm\Api\Transformers\MemberResidenceTransformer;

class User extends ApiController
{
    public function get()
    {
        $user = \Sdm\User\Models\User::find(input('id'));
        return response()->json([
            'result'   => true,
            'response' => $this->respondwithItem($user, new UserTransformer)
        ]);
    }

    public function updatePicture()
    {
        if(!\Input::hasFile('pictureFile')) {
            return response()->json([
                'result'  => false,
                'message' => 'Lampiran wajib di upload'
            ]);
        }

        $user     = \Sdm\User\Models\User::find(input('user_id'));
        $member   = \Sdm\Member\Models\Member::find($user->member_id);

        if(\Input::hasFile('pictureFile')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('pictureFile');
            $file->save();
            $member->picture()->add($file);
        }

        return response()->json([
            'result' => true
        ]);
    }

    public function updateProfile()
    {
        $rules = [
            'name'       => 'required',
            'nrp'        => 'required|numeric',
            'phone'      => 'required|numeric',
            'gender'     => 'required|in:male,female',
            'place'      => 'required',
            'dob'        => 'required|date',
            'child'      => 'required|numeric',
            'blood'      => 'required',
            'religion'   => 'required',
            'marital'    => 'required',
            'nationality'=> 'required',
            'skin_color' => 'required',
            'hair_type'  => 'required',
            'hair_color' => 'required',
            'height'     => 'required|numeric',
            'weight'     => 'required|numeric',
            'address'    => 'required'
        ];
        $attributeNames = [
            'name'       => 'nama',
            'nrp'        => 'nomor pokok',
            'gender'     => 'jenis kelamin',
            'child'      => 'anak ke',
            'blood'      => 'golongan darah',
            'religion'   => 'agama',
            'place'      => 'tempat lahir',
            'dob'        => 'tanggal lahir',
            'marital'    => 'status perkawinan',
            'nationality'=> 'suku / kebangsaan',
            'skin_color' => 'warna kulit',
            'hair_type'  => 'jenis rambut',
            'hair_color' => 'warna rambut',
            'height'     => 'tinggi badan',
            'weight'     => 'berat badan',
            'address'    => 'alamat lengkap'
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $user   = \Sdm\User\Models\User::find(post('id'));
        $member = \Sdm\Member\Models\Member::find($user->member->id)->update([
            'name'             => post('name'),
            'nrp'              => post('nrp'),
            'phone'            => post('phone'),
            'gender'           => post('gender'),
            'place'            => post('place'),
            'dob'              => Carbon::parse(post('dob'))->format('Y-m-d'),
            'child'            => post('child'),
            'blood'            => post('blood'),
            'religion'         => post('religion'),
            'marital'          => post('marital'),
            'nationality'      => post('nationality'),
            'skin_color'       => post('skin_color'),
            'hair_type'        => post('hair_type'),
            'hair_color'       => post('hair_color'),
            'height'           => post('height'),
            'weight'           => post('weight'),
            'address'          => post('address'),
            'uniform_head'     => post('uniform_head'),
            'uniform_trousers' => post('uniform_trousers'),
            'uniform_clothes'  => post('uniform_clothes'),
            'uniform_feet'     => post('uniform_feet'),
        ]);

        return response()->json([
            'result'   => true,
        ]);
    }

    public function updatePassword()
    {
        $rules = [
            'password'        => 'required',
            'password_new'    => 'required|min:6',
            'password_retype' => 'required|same:password_new',
        ];
        $attributeNames = [
            'password'        => 'password lama',
            'password_new'    => 'password baru',
            'password_retype' => 'ulangi password',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $user = \Sdm\User\Models\User::find(post('id'));
        if(!\Hash::check(post('password'), $user->passcode)) {
            return response()->json([
                'message' => 'Password lama tidak sesuai'
            ]);
        }

        $user->passcode = \Hash::make(strtolower(post('password_new')));
        $user->save();

        return response()->json([
            'result' => true
        ]);
    }

    public function updateKtp()
    {
        $rules = [
            'ktp' => 'required',
        ];
        $attributeNames = [
            'ktp' => 'nomor ktp',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $user   = \Sdm\User\Models\User::find(post('id'));
        $member = \Sdm\Member\Models\Member::find($user->member->id);

        if(post('update') === 'false') {
            if(!\Input::hasFile('ktpFile')) {
                return response()->json([
                    'result'  => false,
                    'message' => 'Lampiran KTP wajib di upload'
                ]);
            }
        }

        $attachment = \Sdm\Member\Models\Attachment::firstOrNew([
            'member_id' => $member->id,
            'type'      => 'ktp'
        ]);
        $attachment->value = post('ktp');
        $attachment->status= 'process';
        $attachment->save();

        if(post('update') === 'false') {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('ktpFile');
            $file->save();
            $attachment->ktp()->add($file);
        }

        $notification  = new \Sdm\Core\Classes\Notification;
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $urminTokens   = $memberManager->getUrminToken();
        $body          = $member->name. ' melakukan perubahan pada kolom ktp';
        $content       = [
            'title'       => 'Pemberitahuan',
            'body'        =>  $body,
            'application' => [
                'page'      => 'MemberUpdateDetail',
                'parameter' => 'memberId',
                'key'       => $attachment->member_id,
            ]
        ];
        $notification->pulse('admin', $urminTokens, $content);

        return response()->json([
            'result' => true,
        ]);
    }

    public function updateNpwp()
    {
        $rules = [
            'npwp' => 'required',
        ];
        $attributeNames = [
            'npwp' => 'nomor npwp',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $user   = \Sdm\User\Models\User::find(post('id'));
        $member = \Sdm\Member\Models\Member::find($user->member->id);

        if(post('update') === 'false') {
            if(!\Input::hasFile('npwpFile')) {
                return response()->json([
                    'result'  => false,
                    'message' => 'Lampiran NPWP wajib di upload'
                ]);
            }
        }

        $attachment = \Sdm\Member\Models\Attachment::firstOrNew([
            'member_id' => $member->id,
            'type'      => 'npwp'
        ]);
        $attachment->value = post('npwp');
        $attachment->status= 'process';
        $attachment->save();

        if(post('update') === 'false') {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('npwpFile');
            $file->save();
            $attachment->npwp()->add($file);
        }

        $notification  = new \Sdm\Core\Classes\Notification;
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $urminTokens   = $memberManager->getUrminToken();
        $body          = $member->name. ' melakukan perubahan pada kolom npwp';
        $content       = [
            'title'       => 'Pemberitahuan',
            'body'        =>  $body,
            'application' => [
                'page'      => 'MemberUpdateDetail',
                'parameter' => 'memberId',
                'key'       => $attachment->member_id,
            ]
        ];
        $notification->pulse('admin', $urminTokens, $content);

        return response()->json([
            'result' => true,
        ]);
    }

    public function updateKk()
    {
        $rules = [
            'kk' => 'required',
        ];
        $attributeNames = [
            'kk' => 'nomor kk',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $user   = \Sdm\User\Models\User::find(post('id'));
        $member = \Sdm\Member\Models\Member::find($user->member->id);

        if(post('update') === 'false') {
            if(!\Input::hasFile('kkFile')) {
                return response()->json([
                    'result'  => false,
                    'message' => 'Lampiran KTP wajib di upload'
                ]);
            }
        }

        $attachment = \Sdm\Member\Models\Attachment::firstOrNew([
            'member_id' => $member->id,
            'type'      => 'kk'
        ]);
        $attachment->value = post('kk');
        $attachment->status='process';
        $attachment->save();

        if(post('update') === 'false') {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('kkFile');
            $file->save();
            $attachment->kk()->add($file);
        }

        $notification  = new \Sdm\Core\Classes\Notification;
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $urminTokens   = $memberManager->getUrminToken();
        $body          = $member->name. ' melakukan perubahan pada kolom kk';
        $content       = [
            'title'       => 'Pemberitahuan',
            'body'        =>  $body,
            'application' => [
                'page'      => 'MemberUpdateDetail',
                'parameter' => 'memberId',
                'key'       => $attachment->member_id,
            ]
        ];
        $notification->pulse('admin', $urminTokens, $content);

        return response()->json([
            'result' => true,
        ]);
    }

    public function updateFinger()
    {
        $rules = [
            'finger' => 'required',
        ];
        $attributeNames = [
            'finger' => 'nomor sidik jari',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $user   = \Sdm\User\Models\User::find(post('id'));
        $member = \Sdm\Member\Models\Member::find($user->member->id);

        if(post('update') === 'false') {
            if(!\Input::hasFile('fingerFile')) {
                return response()->json([
                    'result'  => false,
                    'message' => 'Lampiran sidik jari wajib di upload'
                ]);
            }
        }

        $attachment = \Sdm\Member\Models\Attachment::firstOrNew([
            'member_id' => $member->id,
            'type'      => 'finger'
        ]);
        $attachment->value = post('finger');
        $attachment->status='process';
        $attachment->save();

        if(post('update') === 'false') {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('fingerFile');
            $file->save();
            $attachment->finger()->add($file);
        }

        $notification  = new \Sdm\Core\Classes\Notification;
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $urminTokens   = $memberManager->getUrminToken();
        $body          = $member->name. ' melakukan perubahan pada kolom sidik jari';
        $content       = [
            'title'       => 'Pemberitahuan',
            'body'        =>  $body,
            'application' => [
                'page'      => 'MemberUpdateDetail',
                'parameter' => 'memberId',
                'key'       => $attachment->member_id,
            ]
        ];
        $notification->pulse('admin', $urminTokens, $content);

        return response()->json([
            'result' => true,
        ]);
    }

    public function updateAsabri()
    {
        $rules = [
            'asabri' => 'required',
        ];
        $attributeNames = [
            'asabri' => 'nomor asabri',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $user   = \Sdm\User\Models\User::find(post('id'));
        $member = \Sdm\Member\Models\Member::find($user->member->id);

        if(post('update') === 'false') {
            if(!\Input::hasFile('asabriFile')) {
                return response()->json([
                    'result'  => false,
                    'message' => 'Lampiran sidik jari wajib di upload'
                ]);
            }
        }

        $attachment = \Sdm\Member\Models\Attachment::firstOrNew([
            'member_id' => $member->id,
            'type'      => 'asabri'
        ]);
        $attachment->value = post('asabri');
        $attachment->status='process';
        $attachment->save();

        if(post('update') === 'false') {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('asabriFile');
            $file->save();
            $attachment->asabri()->add($file);
        }

        $notification  = new \Sdm\Core\Classes\Notification;
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $urminTokens   = $memberManager->getUrminToken();
        $body          = $member->name. ' melakukan perubahan pada kolom asabri';
        $content       = [
            'title'       => 'Pemberitahuan',
            'body'        =>  $body,
            'application' => [
                'page'      => 'MemberUpdateDetail',
                'parameter' => 'memberId',
                'key'       => $attachment->member_id,
            ]
        ];
        $notification->pulse('admin', $urminTokens, $content);

        return response()->json([
            'result' => true,
        ]);
    }

    public function updateBpjs()
    {
        $rules = [
            'bpjs' => 'required',
        ];
        $attributeNames = [
            'bpjs' => 'nomor kartu tanda anggota',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $user   = \Sdm\User\Models\User::find(post('id'));
        $member = \Sdm\Member\Models\Member::find($user->member->id);

        if(post('update') === 'false') {
            if(!\Input::hasFile('bpjsFile')) {
                return response()->json([
                    'result'  => false,
                    'message' => 'Lampiran wajib di upload'
                ]);
            }
        }

        $attachment = \Sdm\Member\Models\Attachment::firstOrNew([
            'member_id' => $member->id,
            'type'      => 'bpjs'
        ]);
        $attachment->value = post('bpjs');
        $attachment->status='process';
        $attachment->save();

        if(post('update') === 'false') {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('bpjsFile');
            $file->save();
            $attachment->bpjs()->add($file);
        }

        $notification  = new \Sdm\Core\Classes\Notification;
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $urminTokens   = $memberManager->getUrminToken();
        $body          = $member->name. ' melakukan perubahan pada kolom bpjs';
        $content       = [
            'title'       => 'Pemberitahuan',
            'body'        =>  $body,
            'application' => [
                'page'      => 'MemberUpdateDetail',
                'parameter' => 'memberId',
                'key'       => $attachment->member_id,
            ]
        ];
        $notification->pulse('admin', $urminTokens, $content);

        return response()->json([
            'result' => true,
        ]);
    }

    public function updateAuthority()
    {
        $rules = [
            'authority' => 'required',
        ];
        $attributeNames = [
            'authority' => 'nomor tanda kewenangan',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $user   = \Sdm\User\Models\User::find(post('id'));
        $member = \Sdm\Member\Models\Member::find($user->member->id);

        if(post('update') === 'false') {
            if(!\Input::hasFile('authorityFile')) {
                return response()->json([
                    'result'  => false,
                    'message' => 'Lampiran wajib di upload'
                ]);
            }
        }

        $attachment = \Sdm\Member\Models\Attachment::firstOrNew([
            'member_id' => $member->id,
            'type'      => 'authority'
        ]);
        $attachment->value = post('authority');
        $attachment->status='process';
        $attachment->save();

        if(post('update') === 'false') {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('authorityFile');
            $file->save();
            $attachment->authority()->add($file);
        }

        $notification  = new \Sdm\Core\Classes\Notification;
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $urminTokens   = $memberManager->getUrminToken();
        $body          = $member->name. ' melakukan perubahan pada kolom tanda kewenangan';
        $content       = [
            'title'       => 'Pemberitahuan',
            'body'        =>  $body,
            'application' => [
                'page'      => 'MemberUpdateDetail',
                'parameter' => 'memberId',
                'key'       => $attachment->member_id,
            ]
        ];
        $notification->pulse('admin', $urminTokens, $content);

        return response()->json([
            'result' => true,
        ]);
    }

    public function updateInvestigator()
    {
        $rules = [
            'investigator' => 'required',
        ];
        $attributeNames = [
            'investigator' => 'nomor registrasi penyidik',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $user   = \Sdm\User\Models\User::find(post('id'));
        $member = \Sdm\Member\Models\Member::find($user->member->id);

        if(post('update') === 'false') {
            if(!\Input::hasFile('investigatorFile')) {
                return response()->json([
                    'result'  => false,
                    'message' => 'Lampiran wajib di upload'
                ]);
            }
        }

        $attachment = \Sdm\Member\Models\Attachment::firstOrNew([
            'member_id' => $member->id,
            'type'      => 'investigator'
        ]);
        $attachment->value = post('investigator');
        $attachment->status='process';
        $attachment->save();

        if(post('update') === 'false') {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('investigatorFile');
            $file->save();
            $attachment->investigator()->add($file);
        }

        $notification  = new \Sdm\Core\Classes\Notification;
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $urminTokens   = $memberManager->getUrminToken();
        $body          = $member->name. ' melakukan perubahan pada kolom tanda kewenangan';
        $content       = [
            'title'       => 'Pemberitahuan',
            'body'        =>  $body,
            'application' => [
                'page'      => 'MemberUpdateDetail',
                'parameter' => 'memberId',
                'key'       => $attachment->member_id,
            ]
        ];
        $notification->pulse('admin', $urminTokens, $content);

        return response()->json([
            'result' => true,
        ]);
    }

    public function updateMember()
    {
        $rules = [
            'member' => 'required',
        ];
        $attributeNames = [
            'member' => 'nomor kartu tanda anggota',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $user   = \Sdm\User\Models\User::find(post('id'));
        $member = \Sdm\Member\Models\Member::find($user->member->id);

        if(post('update') === 'false') {
            if(!\Input::hasFile('memberFile')) {
                return response()->json([
                    'result'  => false,
                    'message' => 'Lampiran wajib di upload'
                ]);
            }
        }

        $attachment = \Sdm\Member\Models\Attachment::firstOrNew([
            'member_id' => $member->id,
            'type'      => 'member'
        ]);
        $attachment->value = post('member');
        $attachment->status='process';
        $attachment->save();

        if(post('update') === 'false') {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('memberFile');
            $file->save();
            $attachment->member()->add($file);
        }

        $notification  = new \Sdm\Core\Classes\Notification;
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $urminTokens   = $memberManager->getUrminToken();
        $body          = $member->name. ' melakukan perubahan pada tanda anggota';
        $content       = [
            'title'       => 'Pemberitahuan',
            'body'        =>  $body,
            'application' => [
                'page'      => 'MemberUpdateDetail',
                'parameter' => 'memberId',
                'key'       => $attachment->member_id,
            ]
        ];
        $notification->pulse('admin', $urminTokens, $content);

        return response()->json([
            'result' => true,
        ]);
    }

    public function updateKps()
    {
        $rules = [
            'kps' => 'required',
        ];
        $attributeNames = [
            'kps' => 'nomor kpi/kps/karsu',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $user   = \Sdm\User\Models\User::find(post('id'));
        $member = \Sdm\Member\Models\Member::find($user->member->id);

        if(post('update') === 'false') {
            if(!\Input::hasFile('kpsFile')) {
                return response()->json([
                    'result'  => false,
                    'message' => 'Lampiran wajib di upload'
                ]);
            }
        }

        $attachment = \Sdm\Member\Models\Attachment::firstOrNew([
            'member_id' => $member->id,
            'type'      => 'kps'
        ]);
        $attachment->value = post('kps');
        $attachment->status='process';
        $attachment->save();

        if(post('update') === 'false') {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('kpsFile');
            $file->save();
            $attachment->kps()->add($file);
        }

        $notification  = new \Sdm\Core\Classes\Notification;
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $urminTokens   = $memberManager->getUrminToken();
        $body          = $member->name. ' melakukan perubahan pada kolom kpi/kps/karsu';
        $content       = [
            'title'       => 'Pemberitahuan',
            'body'        =>  $body,
            'application' => [
                'page'      => 'MemberUpdateDetail',
                'parameter' => 'memberId',
                'key'       => $attachment->member_id,
            ]
        ];
        $notification->pulse('admin', $urminTokens, $content);

        return response()->json([
            'result' => true,
        ]);
    }

    public function updateSimsa()
    {
        $rules = [
            'simsa' => 'required',
        ];
        $attributeNames = [
            'simsa' => 'nomor simsa',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $user   = \Sdm\User\Models\User::find(post('id'));
        $member = \Sdm\Member\Models\Member::find($user->member->id);

        if(post('update') === 'false') {
            if(!\Input::hasFile('simsaFile')) {
                return response()->json([
                    'result'  => false,
                    'message' => 'Lampiran wajib di upload'
                ]);
            }
        }

        $attachment = \Sdm\Member\Models\Attachment::firstOrNew([
            'member_id' => $member->id,
            'type'      => 'simsa'
        ]);
        $attachment->value = post('simsa');
        $attachment->status='process';
        $attachment->save();

        if(post('update') === 'false') {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('simsaFile');
            $file->save();
            $attachment->simsa()->add($file);
        }

        $notification  = new \Sdm\Core\Classes\Notification;
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $urminTokens   = $memberManager->getUrminToken();
        $body          = $member->name. ' melakukan perubahan pada kolom surat izin pemegang senjata';
        $content       = [
            'title'       => 'Pemberitahuan',
            'body'        =>  $body,
            'application' => [
                'page'      => 'MemberUpdateDetail',
                'parameter' => 'memberId',
                'key'       => $attachment->member_id,
            ]
        ];
        $notification->pulse('admin', $urminTokens, $content);

        return response()->json([
            'result' => true,
        ]);
    }


     /**
     * Family Partner
     * @return [type] [description]
    */
    public function savePartner()
    {
        $rules = [
            'relation'       => 'required',
            'place_relation' => 'required',
            'place_date'     => 'required',
            'name'           => 'required',
            'place'          => 'required',
            'dob'            => 'required',
            'education'      => 'required',
        ];
        $attributeNames = [
            'relation'       => 'hubungan',
            'place_relation' => 'tempat kawin',
            'place_date'     => 'tanggal kawin',
            'name'           => 'nama',
            'place'          => 'tempat lahir',
            'dob'            => 'tanggal lahir',
            'education'      => 'pendidikan',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $user     = \Sdm\User\Models\User::find(input('user_id'));
        $member   = \Sdm\Member\Models\Member::find($user->member_id);

        $family   = \Sdm\Member\Models\Family::firstOrNew([
            'id'        => post('id'),
            'member_id' => $member->id
        ]);
        $family->relation       = post('relation');
        $family->relation_place = post('place_relation');
        $family->relation_date  = Carbon::parse(post('place_date'))->format('Y-m-d');
        $family->name           = post('name');
        $family->gender         = post('relation') == 'husband' ? 'male' : 'female';
        $family->job            = post('job');
        $family->place          = post('place');
        $family->dob            = Carbon::parse(post('dob'))->format('Y-m-d');
        $family->education      = post('education');
        $family->content        = post('content');
        $family->save();

        return response()->json([
            'result' => true
        ]);
    }

    /**
     * Family Member
     * @return [type] [description]
    */
    public function getParent()
    {
        $user     = \Sdm\User\Models\User::find(input('id'));
        $member   = \Sdm\Member\Models\Member::find($user->member_id);
        $families = \Sdm\Member\Models\Family::whereMemberId($member->id)->whereIn('relation', ['father', 'mother', 'brother'])->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondwithCollection($families, new MemberFamilyTransformer)
        ]);
    }

    public function getParentDetail($id)
    {
        $family = \Sdm\Member\Models\Family::find($id);
        return response()->json([
            'result'   => true,
            'response' => $this->respondwithItem($family, new MemberFamilyTransformer)
        ]);
    }

    public function saveParent()
    {
        $rules = [
            'relation'  => 'required',
            'name'      => 'required',
            'gender'    => 'required',
            'place'     => 'required',
            'dob'       => 'required',
            'education' => 'required',
        ];
        $attributeNames = [
            'relation'  => 'hubungan',
            'name'      => 'nama',
            'gender'    => 'jenis kelamin',
            'place'     => 'tempat lahir',
            'dob'       => 'tanggal lahir',
            'education' => 'pendidikan',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $user     = \Sdm\User\Models\User::find(input('user_id'));
        $member   = \Sdm\Member\Models\Member::find($user->member_id);

        $family   = \Sdm\Member\Models\Family::firstOrNew([
            'id'        => post('id'),
            'member_id' => $member->id
        ]);
        $family->relation  = post('relation');
        $family->name      = post('name');
        $family->gender    = post('gender');
        $family->job       = post('job');
        $family->place     = post('place');
        $family->dob       = Carbon::parse(post('dob'))->format('Y-m-d');
        $family->education = post('education');
        $family->content   = post('content');
        $family->save();

        return response()->json([
            'result' => true
        ]);
    }

    public function deleteParent($id)
    {
        $family = \Sdm\Member\Models\Family::find($id)->delete();
        return response()->json([
            'result'   => true,
        ]);
    }

    /**
     * Partner Member
     * @return [type] [description]
     */
    public function getPartner()
    {
        $user     = \Sdm\User\Models\User::find(input('id'));
        $member   = \Sdm\Member\Models\Member::find($user->member_id);
        $families = \Sdm\Member\Models\Family::whereMemberId($member->id)->whereIn('relation', ['wife', 'husband'])->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondwithCollection($families, new MemberFamilyTransformer)
        ]);
    }

    /**
     * Member Family
     * @return [type] [description]
     */
    public function getFamily()
    {
        $user     = \Sdm\User\Models\User::find(input('id'));
        $member   = \Sdm\Member\Models\Member::find($user->member_id);
        $families = \Sdm\Member\Models\Family::whereMemberId($member->id)->whereIn('relation', ['child'])->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondwithCollection($families, new MemberFamilyTransformer)
        ]);
    }

    /**
     * Member Residence
     * @return [type] [description]
    */
    public function getResidence()
    {
        $user       = \Sdm\User\Models\User::find(input('id'));
        $member     = \Sdm\Member\Models\Member::find($user->member_id);
        $residences = \Sdm\Member\Models\Residence::whereMemberId($member->id)->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondwithCollection($residences, new MemberResidenceTransformer)
        ]);
    }

    public function saveResidence()
    {
        $rules = [
            'type'     => 'required',
            'province' => 'required',
            'regency'  => 'required',
            'address'  => 'required',
        ];
        $attributeNames = [
            'type'     => 'jenis',
            'province' => 'provinsi',
            'regency'  => 'kota/kabupaten',
            'address'  => 'alamat lengkap',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $user     = \Sdm\User\Models\User::find(input('user_id'));
        $member   = \Sdm\Member\Models\Member::find($user->member_id);

        $residence   = \Sdm\Member\Models\Residence::firstOrNew([
            'id'        => post('id'),
            'member_id' => $member->id,
        ]);
        $residence->type      = post('type');
        $residence->province  = post('province');
        $residence->regency   = post('regency');
        $residence->address   = post('address');
        $residence->save();

        return response()->json([
            'result' => true
        ]);
    }

    public function getResidenceDetail($id)
    {
        $residence = \Sdm\Member\Models\Residence::find($id);
        return response()->json([
            'result'   => true,
            'response' => $this->respondwithItem($residence, new MemberResidenceTransformer)
        ]);
    }

    public function deleteResidence($id)
    {
        $residence = \Sdm\Member\Models\Residence::find($id)->delete();
        return response()->json([
            'result'   => true,
        ]);
    }

    /**
     * Member Language
     * @return [type] [description]
     */
    public function getLanguage()
    {
        $user     = \Sdm\User\Models\User::find(input('id'));
        $member   = \Sdm\Member\Models\Member::find($user->member_id);
        $languages= \Sdm\Member\Models\Language::whereMemberId($member->id)->whereIsForeign(0)->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondwithCollection($languages, new MemberLanguageTransformer)
        ]);
    }

    public function saveLanguage()
    {
        $rules = [
            'name'      => 'required',
            'result'    => 'required',
        ];
        $attributeNames = [
            'name'      => 'bahasa',
            'result'    => 'penguasaan',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $user     = \Sdm\User\Models\User::find(input('user_id'));
        $member   = \Sdm\Member\Models\Member::find($user->member_id);

        $family   = \Sdm\Member\Models\Language::firstOrNew([
            'id'        => post('id'),
            'member_id' => $member->id,
        ]);
        $family->name      = post('name');
        $family->result    = post('result');
        $family->is_foreign= post('is_foreign');
        $family->save();

        return response()->json([
            'result' => true
        ]);
    }

    public function getLanguageDetail($id)
    {
        $language = \Sdm\Member\Models\Language::find($id);
        return response()->json([
            'result'   => true,
            'response' => $this->respondwithItem($language, new MemberLanguageTransformer)
        ]);
    }

    public function deleteLanguage($id)
    {
        $family = \Sdm\Member\Models\Language::find($id)->delete();
        return response()->json([
            'result'   => true,
        ]);
    }


    /**
     * Member Language Foreign
     * @return [type] [description]
     */
    public function getLanguageForeign()
    {
        $user     = \Sdm\User\Models\User::find(input('id'));
        $member   = \Sdm\Member\Models\Member::find($user->member_id);
        $languages= \Sdm\Member\Models\Language::whereMemberId($member->id)->whereIsForeign(1)->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondwithCollection($languages, new MemberLanguageTransformer)
        ]);
    }


    /**
     * Member Sport
     * @return [type] [description]
     */
    public function getSport()
    {
        $user  = \Sdm\User\Models\User::find(input('id'));
        $member= \Sdm\Member\Models\Member::find($user->member_id);
        $sports= \Sdm\Member\Models\Sport::whereMemberId($member->id)->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondwithCollection($sports, new MemberSportTransformer)
        ]);
    }

    public function saveSport()
    {
        $rules = [
            'name'      => 'required',
        ];
        $attributeNames = [
            'name'      => 'jenis',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $user     = \Sdm\User\Models\User::find(input('user_id'));
        $member   = \Sdm\Member\Models\Member::find($user->member_id);

        $sport   = \Sdm\Member\Models\Sport::firstOrNew([
            'id'        => post('id'),
            'member_id' => $member->id,
        ]);
        $sport->name      = post('name');
        $sport->content   = post('content');
        $sport->save();

        return response()->json([
            'result' => true
        ]);
    }

    public function getSportDetail($id)
    {
        $sport = \Sdm\Member\Models\Sport::find($id);
        return response()->json([
            'result'   => true,
            'response' => $this->respondwithItem($sport, new MemberSportTransformer)
        ]);
    }

    public function deleteSport($id)
    {
        $sport = \Sdm\Member\Models\Sport::find($id)->delete();
        return response()->json([
            'result'   => true,
        ]);
    }


    /**
     * Member Honor
     * @return [type] [description]
     */
    public function getHonor()
    {
        $user  = \Sdm\User\Models\User::find(input('id'));
        $member= \Sdm\Member\Models\Member::find($user->member_id);
        $honors= \Sdm\Member\Models\Honor::whereMemberId($member->id)->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondwithCollection($honors, new MemberHonorTransformer)
        ]);
    }

    public function saveHonor()
    {
        $rules = [
            'name'      => 'required',
            'honored'   => 'required|date'
        ];
        $attributeNames = [
            'name'      => 'bintang jasa',
            'honored'   => 'TMT'
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        if(!post('id')) {
            if(!\Input::hasFile('honorFile')) {
                return response()->json([
                    'result'  => false,
                    'message' => 'Lampiran wajib di upload'
                ]);
            }
        }

        $user     = \Sdm\User\Models\User::find(input('user_id'));
        $member   = \Sdm\Member\Models\Member::find($user->member_id);

        $honor   = \Sdm\Member\Models\Honor::firstOrNew([
            'id'        => post('id'),
            'member_id' => $member->id,
        ]);
        $honor->name      = post('name');
        $honor->honored_at= Carbon::parse(post('honored'))->format('Y-m-d');
        $honor->status    = 'process';
        $honor->save();

        if(\Input::hasFile('honorFile')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('honorFile');
            $file->save();
            $honor->document()->add($file);
        }

        $notification  = new \Sdm\Core\Classes\Notification;
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $urminTokens   = $memberManager->getUrminToken();
        $body          = $member->name. ' melakukan perubahan pada kolom tanda kehormatan/jasa';
        $content       = [
            'title'       => 'Pemberitahuan',
            'body'        =>  $body,
            'application' => [
                'page'      => 'MemberUpdateDetail',
                'parameter' => 'memberId',
                'key'       => $honor->member_id,
            ]
        ];
        $notification->pulse('admin', $urminTokens, $content);

        return response()->json([
            'result' => true
        ]);
    }

    public function getHonorDetail($id)
    {
        $honor = \Sdm\Member\Models\Honor::find($id);
        return response()->json([
            'result'   => true,
            'response' => $this->respondwithItem($honor, new MemberHonorTransformer)
        ]);
    }

    public function deleteHonor($id)
    {
        $honor = \Sdm\Member\Models\Honor::find($id);
        $honor->document->delete();
        $honor->delete();
        return response()->json([
            'result'   => true,
        ]);
    }

    /**
     * Member Education
     * @return [type] [description]
     */
    public function getEducation()
    {
        $user       = \Sdm\User\Models\User::find(input('id'));
        $member     = \Sdm\Member\Models\Member::find($user->member_id);
        $educations = \Sdm\Member\Models\Education::whereType('formal')->whereMemberId($member->id)->orderBy('started_at', 'desc')->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondwithCollection($educations, new MemberEducationTransformer)
        ]);
    }

    public function saveEducation()
    {
        $rules = [
            'started_at'   => 'required',
            'level'        => 'required',
            'name'         => 'required',
            'location'     => 'required',
        ];
        $attributeNames = [
            'started_at'   => 'tanggal mulai',
            'level'        => 'tingkat',
            'name'         => 'instansi',
            'location'     => 'lokasi',
        ];
        $messages  = [];

        if(post('is_now') === 'false') {
            $rules['ended_at']              = 'required';
            $rules['is_graduated']          = 'required';
            $attributeNames['ended_at']     = 'tanggal akhir';
            $attributeNames['is_graduated'] = 'hasil';
        }

        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        if(!post('id')) {
            if(post('is_now') === 'false') {
                if(!\Input::hasFile('educationFile')) {
                    return response()->json([
                        'result'  => false,
                        'message' => 'Lampiran wajib di upload'
                    ]);
                }
            }
        }

        $user     = \Sdm\User\Models\User::find(input('user_id'));
        $member   = \Sdm\Member\Models\Member::find($user->member_id);

        $education   = \Sdm\Member\Models\Education::firstOrNew([
            'id'        => post('id'),
            'member_id' => $member->id,
        ]);
        $education->education_id = post('level');
        $education->type         = post('type');
        $education->name         = post('name');
        $education->study        = post('study');
        $education->location     = post('location');
        $education->started_at   = Carbon::parse(post('started_at'))->format('Y-m-d');
        $education->ended_at     = post('is_now') === 'true' ? NULL : Carbon::parse(post('ended_at'))->format('Y-m-d');
        $education->is_graduated = post('is_now') === 'true' ? NULL : (int)post('is_graduated');
        $education->is_now       = post('is_now') === 'true' ? 1 : 0;
        $education->status       = 'process';
        $education->save();

        if(\Input::hasFile('educationFile')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('educationFile');
            $file->save();
            $education->document()->add($file);
        }

        $notification  = new \Sdm\Core\Classes\Notification;
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $urminTokens   = $memberManager->getUrminToken();
        $body          = $member->name. ' melakukan perubahan pada kolom pendidikan formal';
        $content       = [
            'title'       => 'Pemberitahuan',
            'body'        =>  $body,
            'application' => [
                'page'      => 'MemberUpdateDetail',
                'parameter' => 'memberId',
                'key'       => $education->member_id,
            ]
        ];
        $notification->pulse('admin', $urminTokens, $content);

        return response()->json([
            'result' => true
        ]);
    }

    public function getEducationDetail($id)
    {
        $education = \Sdm\Member\Models\Education::find($id);
        return response()->json([
            'result'   => true,
            'response' => $this->respondwithItem($education, new MemberEducationTransformer)
        ]);
    }

    public function deleteEducation($id)
    {
        $education = \Sdm\Member\Models\Education::find($id);
        if($education->document) {
            $education->document->delete();
        }
        $education->delete();
        return response()->json([
            'result'   => true,
        ]);
    }

    /**
     * Member Education Non Formal
     * @return [type] [description]
     */
    public function getEducationNon()
    {
        $user       = \Sdm\User\Models\User::find(input('id'));
        $member     = \Sdm\Member\Models\Member::find($user->member_id);
        $educations = \Sdm\Member\Models\Education::whereType('non')->whereMemberId($member->id)->orderBy('started_at', 'desc')->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondwithCollection($educations, new MemberEducationTransformer)
        ]);
    }

    public function saveEducationNon()
    {
        $rules = [
            'started_at'   => 'required',
            'name'         => 'required',
            'location'     => 'required',
        ];
        $attributeNames = [
            'started_at'   => 'tanggal mulai',
            'name'         => 'instansi',
            'location'     => 'lokasi',
        ];
        $messages  = [];

        if(post('is_now') === 'false') {
            $rules['ended_at']              = 'required';
            $rules['is_graduated']          = 'required';
            $attributeNames['ended_at']     = 'tanggal akhir';
            $attributeNames['is_graduated'] = 'hasil';
        }

        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        if(!post('id')) {
            if(post('is_now') === 'false') {
                if(!\Input::hasFile('educationFile')) {
                    return response()->json([
                        'result'  => false,
                        'message' => 'Lampiran wajib di upload'
                    ]);
                }
            }
        }

        $user     = \Sdm\User\Models\User::find(input('user_id'));
        $member   = \Sdm\Member\Models\Member::find($user->member_id);

        $education   = \Sdm\Member\Models\Education::firstOrNew([
            'id'        => post('id'),
            'member_id' => $member->id,
        ]);
        $education->education_id = post('level');
        $education->type         = post('type');
        $education->name         = post('name');
        $education->study        = post('study');
        $education->location     = post('location');
        $education->started_at   = Carbon::parse(post('started_at'))->format('Y-m-d');
        $education->ended_at     = post('is_now') === 'true' ? NULL : Carbon::parse(post('ended_at'))->format('Y-m-d');
        $education->is_graduated = post('is_now') === 'true' ? NULL : (int)post('is_graduated');
        $education->is_now       = post('is_now') === 'true' ? 1 : 0;
        $education->status       = 'process';
        $education->save();

        if(\Input::hasFile('educationFile')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('educationFile');
            $file->save();
            $education->document()->add($file);
        }

        $notification  = new \Sdm\Core\Classes\Notification;
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $urminTokens   = $memberManager->getUrminToken();
        $body          = $member->name. ' melakukan perubahan pada kolom pendidikan umum';
        $content       = [
            'title'       => 'Pemberitahuan',
            'body'        =>  $body,
            'application' => [
                'page'      => 'MemberUpdateDetail',
                'parameter' => 'memberId',
                'key'       => $education->member_id,
            ]
        ];
        $notification->pulse('admin', $urminTokens, $content);

        return response()->json([
            'result' => true
        ]);
    }

    /**
     * Member Education Polri
     * @return [type] [description]
    */
    public function getEducationPolri()
    {
        $user       = \Sdm\User\Models\User::find(input('id'));
        $member     = \Sdm\Member\Models\Member::find($user->member_id);
        $educations = \Sdm\Member\Models\EducationPolice::whereType('polri')->whereMemberId($member->id)->orderBy('started_at', 'desc')->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondwithCollection($educations, new MemberEducationPolriTransformer)
        ]);
    }

    public function saveEducationPolri()
    {
        $rules = [
            'started_at'   => 'required',
            'name'         => 'required',
            'result'       => 'required',
            'location'     => 'required',
        ];
        $attributeNames = [
            'started_at'   => 'tanggal mulai',
            'name'         => 'nama',
            'result'       => 'rangking',
            'location'     => 'lokasi',
        ];
        $messages  = [];

        if(post('is_now') === 'false') {
            $rules['ended_at']              = 'required';
            $rules['is_graduated']          = 'required';
            $attributeNames['ended_at']     = 'tanggal akhir';
            $attributeNames['is_graduated'] = 'hasil';
        }

        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        if(!post('id')) {
            if(post('is_now') === 'false') {
                if(!\Input::hasFile('educationFile')) {
                    return response()->json([
                        'result'  => false,
                        'message' => 'Lampiran wajib di upload'
                    ]);
                }
            }
        }

        $user     = \Sdm\User\Models\User::find(input('user_id'));
        $member   = \Sdm\Member\Models\Member::find($user->member_id);

        $education   = \Sdm\Member\Models\EducationPolice::firstOrNew([
            'id'        => post('id'),
            'member_id' => $member->id,
        ]);
        $education->type         = post('type');
        $education->name         = post('name');
        $education->result       = post('result');
        $education->location     = post('location');
        $education->started_at   = Carbon::parse(post('started_at'))->format('Y-m-d');
        $education->ended_at     = post('is_now') === 'true' ? NULL : Carbon::parse(post('ended_at'))->format('Y-m-d');
        $education->is_graduated = post('is_now') === 'true' ? NULL : (int)post('is_graduated');
        $education->is_now       = post('is_now') === 'true' ? 1 : 0;
        $education->status       = 'process';
        $education->save();

        if(\Input::hasFile('educationFile')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('educationFile');
            $file->save();
            $education->document()->add($file);
        }

        $notification  = new \Sdm\Core\Classes\Notification;
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $urminTokens   = $memberManager->getUrminToken();
        $body          = $member->name. ' melakukan perubahan pada kolom pendidikan polri/dikbangspes';
        $content       = [
            'title'       => 'Pemberitahuan',
            'body'        =>  $body,
            'application' => [
                'page'      => 'MemberUpdateDetail',
                'parameter' => 'memberId',
                'key'       => $education->member_id,
            ]
        ];
        $notification->pulse('admin', $urminTokens, $content);

        return response()->json([
            'result' => true
        ]);
    }

    public function getEducationPolriDetail($id)
    {
        $education = \Sdm\Member\Models\EducationPolice::find($id);
        return response()->json([
            'result'   => true,
            'response' => $this->respondwithItem($education, new MemberEducationPolriTransformer)
        ]);
    }

    public function deleteEducationPolri($id)
    {
        $education = \Sdm\Member\Models\EducationPolice::find($id);
        if($education->document) {
            $education->document->delete();
        }
        $education->delete();
        return response()->json([
            'result'   => true,
        ]);
    }

    /**
     * Member Education Polri
     * @return [type] [description]
    */
    public function getEducationDikbangspes()
    {
        $user       = \Sdm\User\Models\User::find(input('id'));
        $member     = \Sdm\Member\Models\Member::find($user->member_id);
        $educations = \Sdm\Member\Models\EducationPolice::whereType('dikbangspes')->whereMemberId($member->id)->orderBy('started_at', 'desc')->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondwithCollection($educations, new MemberEducationPolriTransformer)
        ]);
    }

    /**
     * Member Training
     * @return [type] [description]
    */
    public function getTraining()
    {
        $user       = \Sdm\User\Models\User::find(input('id'));
        $member     = \Sdm\Member\Models\Member::find($user->member_id);
        $trainings = \Sdm\Member\Models\Training::whereMemberId($member->id)->orderBy('started_at', 'desc')->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondwithCollection($trainings, new MemberTrainingTransformer)
        ]);
    }

    public function saveTraining()
    {
        $rules = [
            'started_at'   => 'required',
            'name'         => 'required',
            'result'       => 'required',
            'location'     => 'required',
        ];
        $attributeNames = [
            'started_at'   => 'tanggal mulai',
            'name'         => 'nama',
            'result'       => 'peringkat',
            'location'     => 'lokasi',
        ];
        $messages  = [];

        if(post('is_now') === 'false') {
            $rules['ended_at']              = 'required';
            $rules['is_graduated']          = 'required';
            $attributeNames['ended_at']     = 'tanggal akhir';
            $attributeNames['is_graduated'] = 'hasil';
        }

        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        if(!post('id')) {
            if(post('is_now') === 'false') {
                if(!\Input::hasFile('trainingFile')) {
                    return response()->json([
                        'result'  => false,
                        'message' => 'Lampiran wajib di upload'
                    ]);
                }
            }
        }

        $user     = \Sdm\User\Models\User::find(input('user_id'));
        $member   = \Sdm\Member\Models\Member::find($user->member_id);

        $education   = \Sdm\Member\Models\Training::firstOrNew([
            'id'        => post('id'),
            'member_id' => $member->id,
        ]);
        $education->name         = post('name');
        $education->location     = post('location');
        $education->result       = post('result');
        $education->started_at   = Carbon::parse(post('started_at'))->format('Y-m-d');
        $education->ended_at     = post('is_now') === 'true' ? NULL : Carbon::parse(post('ended_at'))->format('Y-m-d');
        $education->is_graduated = post('is_now') === 'true' ? NULL : (int)post('is_graduated');
        $education->is_now       = post('is_now') === 'true' ? 1 : 0;
        $education->status       = 'process';
        $education->save();

        if(\Input::hasFile('trainingFile')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('trainingFile');
            $file->save();
            $education->document()->add($file);
        }

        $notification  = new \Sdm\Core\Classes\Notification;
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $urminTokens   = $memberManager->getUrminToken();
        $body          = $member->name. ' melakukan perubahan pada kolom riwayat pelatihan';
        $content       = [
            'title'       => 'Pemberitahuan',
            'body'        =>  $body,
            'application' => [
                'page'      => 'MemberUpdateDetail',
                'parameter' => 'memberId',
                'key'       => $education->member_id,
            ]
        ];
        $notification->pulse('admin', $urminTokens, $content);

        return response()->json([
            'result' => true
        ]);
    }

    public function getTrainingDetail($id)
    {
        $training = \Sdm\Member\Models\Training::find($id);
        return response()->json([
            'result'   => true,
            'response' => $this->respondwithItem($training, new MemberTrainingTransformer)
        ]);
    }

    public function deleteTraining($id)
    {
        $training = \Sdm\Member\Models\Training::find($id);
        if($training->document) {
            $training->document->delete();
        }
        $training->delete();
        return response()->json([
            'result'   => true,
        ]);
    }

    /**
     * Member Duty
     * @return [type] [description]
    */
    public function getDuty()
    {
        $user    = \Sdm\User\Models\User::find(input('id'));
        $member  = \Sdm\Member\Models\Member::find($user->member_id);
        $duties  = \Sdm\Member\Models\Duty::whereMemberId($member->id)->orderBy('started_at', 'desc')->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondwithCollection($duties, new MemberDutyTransformer)
        ]);
    }

    public function saveDuty()
    {
        $rules = [
            'started_at'   => 'required',
            'origin'       => 'required',
            'name'         => 'required',
        ];
        $attributeNames = [
            'started_at'   => 'tanggal mulai',
            'origin'       => 'negara',
            'name'         => 'nama',
        ];
        $messages  = [];

        if(post('is_now') === 'false') {
            $rules['ended_at']              = 'required';
            $attributeNames['ended_at']     = 'tanggal akhir';
        }

        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        if(!post('id')) {
            if(post('is_now') === 'false') {
                if(!\Input::hasFile('dutyFile')) {
                    return response()->json([
                        'result'  => false,
                        'message' => 'Lampiran wajib di upload'
                    ]);
                }
            }
        }

        $user     = \Sdm\User\Models\User::find(input('user_id'));
        $member   = \Sdm\Member\Models\Member::find($user->member_id);

        $duty   = \Sdm\Member\Models\Duty::firstOrNew([
            'id'        => post('id'),
            'member_id' => $member->id,
        ]);
        $duty->name         = post('name');
        $duty->origin       = post('origin');
        $duty->started_at   = Carbon::parse(post('started_at'))->format('Y-m-d');
        $duty->ended_at     = post('is_now') === 'true' ? NULL : Carbon::parse(post('ended_at'))->format('Y-m-d');
        $duty->is_now       = post('is_now') === 'true' ? 1 : 0;
        $duty->status       = 'process';
        $duty->save();

        if(\Input::hasFile('dutyFile')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('dutyFile');
            $file->save();
            $duty->document()->add($file);
        }

        $notification  = new \Sdm\Core\Classes\Notification;
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $urminTokens   = $memberManager->getUrminToken();
        $body          = $member->name. ' melakukan perubahan pada kolom riwayat tugas luar negeri';
        $content       = [
            'title'       => 'Pemberitahuan',
            'body'        =>  $body,
            'application' => [
                'page'      => 'MemberUpdateDetail',
                'parameter' => 'memberId',
                'key'       => $duty->member_id,
            ]
        ];
        $notification->pulse('admin', $urminTokens, $content);

        return response()->json([
            'result' => true
        ]);
    }

    public function getDutyDetail($id)
    {
        $duty = \Sdm\Member\Models\Duty::find($id);
        return response()->json([
            'result'   => true,
            'response' => $this->respondwithItem($duty, new MemberDutyTransformer)
        ]);
    }

    public function deleteDuty($id)
    {
        $duty = \Sdm\Member\Models\Duty::find($id);
        if($duty->document) {
            $duty->document->delete();
        }
        $duty->delete();
        return response()->json([
            'result'   => true,
        ]);
    }

    /**
     * Member Position
     * @return [type] [description]
    */
    public function getPosition()
    {
        $user       = \Sdm\User\Models\User::find(input('id'));
        $member     = \Sdm\Member\Models\Member::find($user->member_id);
        $positions  = \Sdm\Member\Models\Position::whereMemberId($member->id)->orderBy('started_at', 'desc')->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondwithCollection($positions, new MemberPositionTransformer)
        ]);
    }

    public function savePosition()
    {
        $rules = [
            'started_at'   => 'required',
            'unit'         => 'required',
        ];
        $attributeNames = [
            'started_at'   => 'tanggal mulai',
            'unit'         => 'polda/polres/polsek',
        ];
        $messages  = [];

        if(post('unit') == 99 || post('unit') == '99' ) {
            $rules['unit_name']                = 'required';
            $rules['department_name']          = 'required';
            $rules['name']                     = 'required';
            $rules['number']                   = 'required';
            $attributeNames['unit_name']       = 'polda/polres/polsek';
            $attributeNames['department_name'] = 'unsur';
            $attributeNames['name']            = 'jabatan';
            $attributeNames['number']          = 'nomor surat keputusan';
        }
        else {
            $rules['unit']                = 'required';
            $rules['department']          = 'required';
            $rules['name']                = 'required';
            $rules['number']              = 'required';
            $attributeNames['unit']       = 'polda/polres/polsek';
            $attributeNames['department'] = 'unsur';
            $attributeNames['name']       = 'jabatan';
            $attributeNames['number']     = 'nomor surat keputusan';
        }

        if(post('is_now') === 'false') {
            $rules['ended_at']              = 'required';
            $attributeNames['ended_at']     = 'tanggal akhir';
        }
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        if(!post('id')) {
            if(!\Input::hasFile('positionFile')) {
                return response()->json([
                    'result'  => false,
                    'message' => 'Lampiran wajib di upload'
                ]);
            }
        }

        $user     = \Sdm\User\Models\User::find(input('user_id'));
        $member   = \Sdm\Member\Models\Member::find($user->member_id);
        $position = \Sdm\Member\Models\Position::firstOrNew([
            'id'        => post('id'),
            'member_id' => $member->id,
        ]);

        if(post('unit') == 99 || post('unit') == '99') {
            $unit = \Sdm\Master\Models\Unit::firstOrCreate([
                'name' => post('unit_name')
            ]);

            $department = \Sdm\Master\Models\Department::firstOrCreate([
                'unit_id' => $unit->id,
                'name'    => post('department_name')
            ]);

            $position->department_id = $department->id;
        }
        else {
            $position->department_id = post('department');
        }

        $position->name         = post('name');
        $position->number       = post('number');
        $position->started_at   = Carbon::parse(post('started_at'))->format('Y-m-d');
        $position->ended_at     = post('is_now') === 'true' ? NULL : Carbon::parse(post('ended_at'))->format('Y-m-d');
        $position->is_now       = post('is_now') === 'true' ? 1 : 0;
        $position->status       = 'process';
        $position->save();

        $notification  = new \Sdm\Core\Classes\Notification;
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $urminTokens   = $memberManager->getUrminToken();
        $body          = $member->name. ' melakukan perubahan pada kolom jabatan';
        $content       = [
            'title'       => 'Pemberitahuan',
            'body'        =>  $body,
            'application' => [
                'page'      => 'MemberUpdateDetail',
                'parameter' => 'memberId',
                'key'       => $position->member_id,
            ]
        ];
        $notification->pulse('admin', $urminTokens, $content);

        if(\Input::hasFile('positionFile')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('positionFile');
            $file->save();
            $position->document()->add($file);
        }

        return response()->json([
            'result' => true
        ]);
    }

    public function getPositionDetail($id)
    {
        $position = \Sdm\Member\Models\Position::find($id);
        return response()->json([
            'result'   => true,
            'response' => $this->respondwithItem($position, new MemberPositionTransformer)
        ]);
    }

    public function deletePosition($id)
    {
        $position = \Sdm\Member\Models\Position::find($id);
        if($position->document) {
            $position->document->delete();
        }
        $position->delete();
        return response()->json([
            'result'   => true,
        ]);
    }

    /**
     * Member Grade
     * @return [type] [description]
    */
    public function getGrade()
    {
        $user   = \Sdm\User\Models\User::find(input('id'));
        $member = \Sdm\Member\Models\Member::find($user->member_id);
        $grades = \Sdm\Member\Models\Grade::whereMemberId($member->id)->orderBy('started_at', 'desc')->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondwithCollection($grades, new MemberGradeTransformer)
        ]);
    }

    public function saveGrade()
    {
        $rules = [
            'started_at'   => 'required',
            'level'        => 'required',
            'number'       => 'required',
        ];
        $attributeNames = [
            'started_at'   => 'tanggal mulai',
            'level'        => 'pangkat',
            'number'       => 'nomor surat keputusan',
        ];
        $messages  = [];

        if(post('is_now') === 'false') {
            $rules['ended_at']              = 'required';
            $attributeNames['ended_at']     = 'tanggal akhir';
        }

        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        if(!post('id')) {
            if(!\Input::hasFile('gradeFile')) {
                return response()->json([
                    'result'  => false,
                    'message' => 'Lampiran wajib di upload'
                ]);
            }
        }

        $user     = \Sdm\User\Models\User::find(input('user_id'));
        $member   = \Sdm\Member\Models\Member::find($user->member_id);

        $grade   = \Sdm\Member\Models\Grade::firstOrNew([
            'id'        => post('id'),
            'member_id' => $member->id,
        ]);
        $grade->grade_id   = post('level');
        $grade->number     = post('number');
        $grade->started_at = Carbon::parse(post('started_at'))->format('Y-m-d');
        $grade->ended_at   = post('is_now') === 'true' ? NULL : Carbon::parse(post('ended_at'))->format('Y-m-d');
        $grade->is_now     = post('is_now') === 'true' ? 1 : 0;
        $grade->status     = 'process';
        $grade->save();

        if(\Input::hasFile('gradeFile')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('gradeFile');
            $file->save();
            $grade->document()->add($file);
        }

        $notification  = new \Sdm\Core\Classes\Notification;
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $urminTokens   = $memberManager->getUrminToken();
        $body          = $member->name. ' melakukan perubahan pada kolom riwayat kepangkatan';
        $content       = [
            'title'       => 'Pemberitahuan',
            'body'        =>  $body,
            'application' => [
                'page'      => 'MemberUpdateDetail',
                'parameter' => 'memberId',
                'key'       => $grade->member_id,
            ]
        ];
        $notification->pulse('admin', $urminTokens, $content);

        return response()->json([
            'result' => true
        ]);
    }

    public function getGradeDetail($id)
    {
        $grade = \Sdm\Member\Models\Grade::find($id);
        return response()->json([
            'result'   => true,
            'response' => $this->respondwithItem($grade, new MemberGradeTransformer)
        ]);
    }

    public function deleteGrade($id)
    {
        $grade = \Sdm\Member\Models\Grade::find($id);
        if($grade->document) {
            $grade->document->delete();
        }
        $grade->delete();
        return response()->json([
            'result'   => true,
        ]);
    }

    /**
     * Member Work
     * @return [type] [description]
    */
    public function getWork()
    {
        $user   = \Sdm\User\Models\User::find(input('id'));
        $member = \Sdm\Member\Models\Member::find($user->member_id);
        $works  = \Sdm\Member\Models\Work::whereMemberId($member->id)->orderBy('started_at', 'desc')->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondwithCollection($works, new MemberWorkTransformer)
        ]);
    }

    public function saveWork()
    {
        $rules = [
            'started_at' => 'required',
            'number'     => 'required',
            'name'       => 'required',
            'location'   => 'required',
        ];
        $attributeNames = [
            'started_at' => 'tanggal mulai',
            'number'     => 'nomor surat keputusan',
            'name'       => 'nama operasi',
            'location'   => 'lokasi',
        ];
        $messages  = [];

        if(post('is_now') === 'false') {
            $rules['ended_at']              = 'required';
            $attributeNames['ended_at']     = 'tanggal akhir';
        }

        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        if(!post('id')) {
            if(!\Input::hasFile('workFile')) {
                return response()->json([
                    'result'  => false,
                    'message' => 'Lampiran wajib di upload'
                ]);
            }
        }

        $user     = \Sdm\User\Models\User::find(input('user_id'));
        $member   = \Sdm\Member\Models\Member::find($user->member_id);

        $work   = \Sdm\Member\Models\Work::firstOrNew([
            'id'        => post('id'),
            'member_id' => $member->id,
        ]);
        $work->number     = post('number');
        $work->name       = post('name');
        $work->location   = post('location');
        $work->started_at = Carbon::parse(post('started_at'))->format('Y-m-d');
        $work->ended_at   = post('is_now') === 'true' ? NULL : Carbon::parse(post('ended_at'))->format('Y-m-d');
        $work->is_now     = post('is_now') === 'true' ? 1 : 0;
        $work->status     = 'process';
        $work->save();

        if(\Input::hasFile('workFile')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('workFile');
            $file->save();
            $work->document()->add($file);
        }

        $notification  = new \Sdm\Core\Classes\Notification;
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $urminTokens   = $memberManager->getUrminToken();
        $body          = $member->name. ' melakukan perubahan pada kolom riwayat penugasan';
        $content       = [
            'title'       => 'Pemberitahuan',
            'body'        =>  $body,
            'application' => [
                'page'      => 'MemberUpdateDetail',
                'parameter' => 'memberId',
                'key'       => $work->member_id,
            ]
        ];
        $notification->pulse('admin', $urminTokens, $content);

        return response()->json([
            'result' => true
        ]);
    }

    public function getWorkDetail($id)
    {
        $work = \Sdm\Member\Models\Work::find($id);
        return response()->json([
            'result'   => true,
            'response' => $this->respondwithItem($work, new MemberWorkTransformer)
        ]);
    }

    public function deleteWork($id)
    {
        $work = \Sdm\Member\Models\Work::find($id);
        if($work->document) {
            $work->document->delete();
        }
        $work->delete();
        return response()->json([
            'result'   => true,
        ]);
    }

    /**
     * Member Brivet
     * @return [type] [description]
    */
    public function getBrivet()
    {
        $user   = \Sdm\User\Models\User::find(input('id'));
        $member = \Sdm\Member\Models\Member::find($user->member_id);
        $brivet = \Sdm\Member\Models\Brivet::whereMemberId($member->id)->orderBy('claimed_at', 'desc')->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondwithCollection($brivet, new MemberBrivetTransformer)
        ]);
    }

    public function saveBrivet()
    {
        $rules = [
            'claimed_at' => 'required',
            'name'       => 'required',
            'source'     => 'required',
        ];
        $attributeNames = [
            'claimed_at' => 'tanggal perolehan',
            'name'       => 'jenis',
            'source'     => 'sumber',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        if(!post('id')) {
            if(!\Input::hasFile('brivetFile')) {
                return response()->json([
                    'result'  => false,
                    'message' => 'Lampiran wajib di upload'
                ]);
            }
        }

        $user     = \Sdm\User\Models\User::find(input('user_id'));
        $member   = \Sdm\Member\Models\Member::find($user->member_id);

        $brivet   = \Sdm\Member\Models\Brivet::firstOrNew([
            'id'        => post('id'),
            'member_id' => $member->id,
        ]);
        $brivet->name       = post('name');
        $brivet->source     = post('source');
        $brivet->claimed_at = Carbon::parse(post('claimed_at'))->format('Y-m-d');
        $brivet->status     = 'process';
        $brivet->save();

        if(\Input::hasFile('brivetFile')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('brivetFile');
            $file->save();
            $brivet->document()->add($file);
        }

        $notification  = new \Sdm\Core\Classes\Notification;
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $urminTokens   = $memberManager->getUrminToken();
        $body          = $member->name. ' melakukan perubahan pada kolom riwayat brivet';
        $content       = [
            'title'       => 'Pemberitahuan',
            'body'        =>  $body,
            'application' => [
                'page'      => 'MemberUpdateDetail',
                'parameter' => 'memberId',
                'key'       => $brivet->member_id,
            ]
        ];
        $notification->pulse('admin', $urminTokens, $content);

        return response()->json([
            'result' => true
        ]);
    }

    public function getBrivetDetail($id)
    {
        $brivet = \Sdm\Member\Models\Brivet::find($id);
        return response()->json([
            'result'   => true,
            'response' => $this->respondwithItem($brivet, new MemberBrivetTransformer)
        ]);
    }

    public function deleteBrivet($id)
    {
        $brivet = \Sdm\Member\Models\Brivet::find($id);
        if($brivet->document) {
            $brivet->document->delete();
        }
        $brivet->delete();
        return response()->json([
            'result'   => true,
        ]);
    }

    /**
     * Member Salary
     * @return [type] [description]
    */
    public function getSalary()
    {
        $user   = \Sdm\User\Models\User::find(input('id'));
        $member = \Sdm\Member\Models\Member::find($user->member_id);
        $salary = \Sdm\Member\Models\Salary::whereMemberId($member->id)->orderBy('started_at', 'desc')->get();

        return response()->json([
            'result'   => true,
            'response' => $this->respondwithCollection($salary, new MemberSalaryTransformer)
        ]);
    }

    public function saveSalary()
    {
        $rules = [
            'grade'      => 'required',
            'started_at' => 'required',
            'value'      => 'required|numeric',
            'ended_at'   => 'required',
        ];
        $attributeNames = [
            'grade'      => 'pangkat',
            'started_at' => 'TMT',
            'value'      => 'gaji pokok',
            'ended_at'   => 'TMT Berikutnya',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        if(!post('id')) {
            if(!\Input::hasFile('salaryFile')) {
                return response()->json([
                    'result'  => false,
                    'message' => 'Lampiran wajib di upload'
                ]);
            }
        }

        $user     = \Sdm\User\Models\User::find(input('user_id'));
        $member   = \Sdm\Member\Models\Member::find($user->member_id);

        $salary   = \Sdm\Member\Models\Salary::firstOrNew([
            'id'        => post('id'),
            'member_id' => $member->id,
        ]);
        $salary->grade_id   = post('grade');
        $salary->value      = post('value');
        $salary->started_at = Carbon::parse(post('started_at'))->format('Y-m-d');
        $salary->ended_at   = Carbon::parse(post('ended_at'))->format('Y-m-d');
        $salary->status     = 'process';
        $salary->save();

        if(\Input::hasFile('salaryFile')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('salaryFile');
            $file->save();
            $salary->document()->add($file);
        }

        $notification  = new \Sdm\Core\Classes\Notification;
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $urminTokens   = $memberManager->getUrminToken();
        $body          = $member->name. ' melakukan perubahan pada kolom riwayat kenaikan gaji';
        $content       = [
            'title'       => 'Pemberitahuan',
            'body'        =>  $body,
            'application' => [
                'page'      => 'MemberUpdateDetail',
                'parameter' => 'memberId',
                'key'       => $salary->member_id,
            ]
        ];
        $notification->pulse('admin', $urminTokens, $content);

        return response()->json([
            'result' => true
        ]);
    }

    public function getSalaryDetail($id)
    {
        $salary = \Sdm\Member\Models\Salary::find($id);
        return response()->json([
            'result'   => true,
            'response' => $this->respondwithItem($salary, new MemberSalaryTransformer)
        ]);
    }

    public function deleteSalary($id)
    {
        $salary = \Sdm\Member\Models\Salary::find($id);
        if($salary->document) {
            $salary->document->delete();
        }
        $salary->delete();
        return response()->json([
            'result'   => true,
        ]);
    }
}
