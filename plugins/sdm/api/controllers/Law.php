<?php namespace Sdm\Api\Controllers;

use Carbon\Carbon;

use Sdm\Api\Classes\ApiController;

class Law extends ApiController
{
    public function index()
    {
        $laws = \Sdm\Member\Models\Law::orderBy('date', 'desc')->get();
        return response()->json([
            'result'    => true,
            'response'  => $this->respondWithCollection($laws, new \Sdm\Api\Transformers\LawTransformer)
        ]);
    }

    public function save()
    {
        $rules = [
            'date'      => 'required',
            'title'     => 'required',
            'member_id' => 'required',
            'admin_id'  => 'required',
            'content'   => 'required',
        ];
        $attributeNames = [
            'date'      => 'tanggal kejadian',
            'title'     => 'masalah',
            'member_id' => 'anggota',
            'admin_id'  => 'required',
            'content'   => 'keterangan',
        ];
        $messages  = [];

        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        if(!post('id')) {
            if(!\Input::hasFile('problemFile')) {
                return response()->json([
                    'result'  => false,
                    'message' => 'Lampiran wajib di upload'
                ]);
            }
        }

        $admin    = \Sdm\Member\Models\Member::find(post('admin_id'));
        $member   = \Sdm\Member\Models\Member::find(post('member_id'));

        $law   = \Sdm\Member\Models\Law::firstOrNew([
            'id' => post('id'),
        ]);
        $law->member_id    = post('member_id');
        $law->admin_id     = post('admin_id');
        $law->title        = post('title');
        $law->content      = post('content');
        $law->date         = Carbon::parse(post('date'))->format('Y-m-d');
        $law->save();

        // Tag
        if(post('tag')) {
            $arrInput     = [];
            $tagInput     = explode(',', post('tag'));
            foreach ($tagInput as $tag) {
                $tg = \Sdm\Law\Models\Tag::firstOrNew([
                    'name' => $tag
                ]);
                $tg->save();
                array_push($arrInput, $tg->id);
            }

            $law->tags()->sync($arrInput);
        }
        else {
            $law->tags()->detach();
        }

        if(\Input::hasFile('problemFile')) {
            $file            = new \System\Models\File;
            $file->data      = \Input::file('problemFile');
            $file->save();
            $law->document()->add($file);
        }

        return response()->json([
            'result' => true
        ]);
    }

    public function detail($id)
    {
        $law = \Sdm\Member\Models\Law::find($id);
        return response()->json([
            'result'   => true,
            'response' => $this->respondWithItem($law, new \Sdm\Api\Transformers\LawTransformer)
        ]);
    }

    public function delete($id)
    {
        $law = \Sdm\Member\Models\Law::find($id);
        if($law->document) {
            $law->document->delete();
        }
        $law->delete();
        return response()->json([
            'result' => true
        ]);
    }

    public function report()
    {
        $laws                = \Sdm\Member\Models\Law::orderBy('date', 'desc')->get();
        $letters             = \Sdm\Letter\Models\Letter::whereIn('id', [5, 12])->get();
        $members             = \Sdm\Member\Models\Member::whereIn('id', $laws->pluck('member_id'))->get();
        $user                = \Sdm\User\Models\User::find(input('user_id'));

        $generate            = new \Sdm\Letter\Models\Generator;
        $generate->member_id = $user->member_id;
        $generate->save();

        foreach ($letters as $letter) {
            $request               = new \Sdm\Letter\Models\Request;
            $request->generator_id = $generate->id;
            $request->letter_id    = $letter->id;
            $request->name         = $letter->name;
            $request->code         = $letter->code;
            $request->is_single    = $letter->is_single;
            $request->save();
        }

        foreach ($members as $member) {
            $requestMember               = new \Sdm\Letter\Models\RequestMember;
            $requestMember->generator_id = $generate->id;
            $requestMember->member_id    = $member->id;
            $requestMember->save();
        }

        return response()->json([
            'result'    => true,
            'response'  => [
                'data' => [
                    'id'   => $generate->id,
                    'file' => $generate->document ? $generate->document->path : (bool) false
                ]
            ]
        ]);
    }
}
