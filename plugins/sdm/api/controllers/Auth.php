<?php namespace Sdm\Api\Controllers;

use Sdm\Api\Classes\ApiController;

use Sdm\Api\Transformers\UserTransformer;

class Auth extends ApiController
{
    public function login()
    {
        $rules = [
            'nrp'   => 'required',
            'code'  => 'required',
        ];
        $attributeNames = [
            'nrp'   => 'NRP',
            'code'  => 'password',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $user = \Sdm\User\Models\User::whereCode(post('nrp'))->first();
        if(!$user) {
            return response()->json([
                'message' => 'Pengguna tidak ditemukan',
            ]);
        }

        $password = strtolower(post('code'));
        if(!\Hash::check($password, $user->passcode)) {
            return response()->json([
                'message' => 'Password tidak sesuai'
            ]);
        }

        if(!$user->member) {
            $member = new \Sdm\Member\Models\Member;
            $member->save();

            $user->member_id = $member->id;
            $user->save();
        }

        $token = \Sdm\User\Models\Token::firstOrNew([
            'user_id' => $user->id,
            'token'   => post('token')
        ]);
        $token->save();

        return response()->json([
            'result'   => true,
            'response' => $this->respondwithItem($user, new UserTransformer)
        ]);
    }

    public function logout()
    {
        $token = \Ams\User\Models\Token::whereUserId(post('user_id'))->whereToken(post('token'))->orderBy('id', 'desc')->first();
        if($token->delete()) {
            return response()->json([
                'result' => true,
            ]);
        }
    }
}
