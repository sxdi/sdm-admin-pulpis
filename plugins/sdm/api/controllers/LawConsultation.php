<?php namespace Sdm\Api\Controllers;

use Carbon\Carbon;

use Sdm\Api\Classes\ApiController;

use Sdm\Api\Transformers\LawConsultationTransformer;

class LawConsultation extends ApiController
{
    public function thread()
    {
        return response()->json([
            'result'    => true,
            'response'  => [
                'data' => \Sdm\Law\Models\Consultation::whereIsClose(0)->whereUserId(input('id'))->get()
            ],
        ]);
    }

    public function new()
    {
        $consultation = \Sdm\Law\Models\Consultation::create([
            'user_id' => post('user_id'),
            'title'   => post('title'),
        ]);

        $message      = \Sdm\Law\Models\ConsultationDetail::create([
            'user_id'         => post('user_id'),
            'consultation_id' => $consultation->id,
            'message'         => 'Saya ingin konsultasi terkait : '.post('title')
        ]);

        return response()->json([
            'result'   => true,
            'response' => [
                'data' => $consultation
            ],
        ]);
    }

    public function create()
    {
        $notification = new \Sdm\Core\Classes\Notification;
        $consultation = \Sdm\Law\Models\Consultation::find(post('consultation_id'));
        $message      = \Sdm\Law\Models\ConsultationDetail::create([
            'user_id'         => post('user_id'),
            'consultation_id' => $consultation->id,
            'message'         => post('message')
        ]);

        if($consultation->user_id == post('user_id')) {
            // Sent to admin
            $users   = \Sdm\User\Models\User::whereIsHealth(1)->get()->pluck('id');
            $tokens  = \Sdm\User\Models\Token::whereIn('user_id', $users)->get()->pluck('token');
            $body    = post('message');
            $content = [
                'title'       => $consultation->title,
                'body'        => $body,
                'application' => [
                    'page'      => 'LawChat',
                    'parameter' => 'lawId',
                    'key'       => $consultation->id
                ]
            ];
            $notification->pulse('admin', $tokens, $content);
        }
        else {
            // Sent to member
            $tokens  = \Sdm\User\Models\Token::whereUserId(post('user_id'))->get()->pluck('token');
            $body    = post('message');
            $content = [
                'title'       => $consultation->title,
                'body'        => $body,
                'application' => [
                    'page'      => 'LawDetail',
                    'parameter' => 'lawId',
                    'key'       => $consultation->id
                ]
            ];
            $notification->pulse('member', $tokens, $content);
        }

        return response()->json([
            'result' => true
        ]);
    }

    public function tag()
    {
        $rules = [
            'tag' => 'required',
        ];
        $attributeNames = [
            'tag' => 'tanda',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ]);
        }

        $arrInput     = [];
        $tagInput     = explode(',', post('tag'));
        foreach ($tagInput as $tag) {
            $tg = \Sdm\Law\Models\Tag::firstOrNew([
                'name' => $tag
            ]);
            $tg->save();
            array_push($arrInput, $tg->id);
        }
        $consultation = \Sdm\Law\Models\Consultation::find(post('consultation_id'));
        $consultation->tags()->sync($arrInput);

        return response()->json([
            'result' => true
        ]);
    }

    public function close()
    {
        $consultation = \Sdm\Law\Models\Consultation::whereId(post('consultation_id'))->update([
            'is_close'  => 1,
            'closed_at' => date('Y-m-d H:i:s')
        ]);
        return response()->json([
            'result' => true
        ]);
    }

    public function active()
    {
        $consultations = \Sdm\Law\Models\Consultation::whereIsClose(0)->orderBy('created_at', 'desc')->get();
        return response()->json([
            'result'        => true,
            'consultations' => $this->respondWithCollection($consultations, new LawConsultationTransformer)
        ]);
    }

    public function history()
    {
        $consultations = \Sdm\Law\Models\Consultation::whereIsClose(1)->orderBy('created_at', 'desc')->get();
        return response()->json([
            'result'        => true,
            'consultations' => $this->respondWithCollection($consultations, new LawConsultationTransformer)
        ]);
    }

    public function detail($id)
    {
        $consultations = \Sdm\Law\Models\Consultation::find($id);
        $chats         = \Sdm\Law\Models\ConsultationDetail::whereConsultationId($consultations->id)->orderBy('created_at', 'desc')->get();
        $messages      = [];

        foreach ($chats as $key => $message) {
            $data = [
                '_id'       => $key,
                'text'      => $message->message,
                'createdAt' => $message->created_at->format('d F Y H:i'),
                'user'      => [
                    '_id'    => (int) $message->user_id,
                ]
            ];
            if($message->user->is_law) {
                if($message->user->member->picture) {
                    $data['user']['avatar'] = $message->user->member->picture->getThumb('140', '140', 'crop');
                }
                else {
                    $data['user']['avatar'] = 'https://placeimg.com/140/140/any';
                }

                $data['user']['name'] = $message->user->member->name . ' (Konsultan)';
            }
            else {
                $data['user']['name'] = 'Anggota';
            }
            array_push($messages, $data);
        }

        return response()->json([
            'result'        => true,
            'consultation'  => $this->respondWithItem($consultations, new LawConsultationTransformer),
            'messages'      => $messages
        ]);
    }
}
