<?php namespace Sdm\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Sdm\Member\Models\Brivet as BrivetModels;

class MemberBrivetTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(BrivetModels $brivet)
    {
        return [
            'id'     => $brivet->id,
            'name'   => $brivet->name,
            'source' => $brivet->source,
            'status' => $brivet->status,
            'message'=> $brivet->message,
            'claimed_at' => [
                'ldFY'   => $brivet->claimed_at ? $brivet->claimed_at->format('l, d F Y') : '',
                'dFY'    => $brivet->claimed_at ? $brivet->claimed_at->format('d F Y') : '',
                'ymd'    => $brivet->claimed_at ? $brivet->claimed_at->format('Y-m-d') : '',
                'l'      => $brivet->claimed_at ? $brivet->claimed_at->format('l') : '',
                'd'      => $brivet->claimed_at ? $brivet->claimed_at->format('d') : '',
                'm'      => $brivet->claimed_at ? $brivet->claimed_at->format('m') : '',
                'F'      => $brivet->claimed_at ? $brivet->claimed_at->format('F') : '',
                'Y'      => $brivet->claimed_at ? $brivet->claimed_at->format('Y') : '',
            ],
            'document' => [
                'path' => $brivet->document ? $brivet->document->path : (bool) false,
                'name' => $brivet->document ? $brivet->document->file_name : (bool) false,
            ]
        ];
    }
}
