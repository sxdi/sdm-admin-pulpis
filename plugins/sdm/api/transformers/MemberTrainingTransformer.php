<?php namespace Sdm\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Sdm\Member\Models\Training as TrainingModels;

class MemberTrainingTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(TrainingModels $training)
    {
        return [
            'id'           => $training->id,
            'name'         => $training->name,
            'result'       => $training->result,
            'location'     => $training->location,
            'is_graduated' => $training->is_graduated,
            'is_now'       => (bool) $training->is_now,
            'status'       => $training->status,
            'message'      => $training->message,
            'started_at' => [
                'ldFY'   => $training->started_at ? $training->started_at->format('l, d F Y') : '',
                'dFY'    => $training->started_at ? $training->started_at->format('d F Y') : '',
                'ymd'    => $training->started_at ? $training->started_at->format('Y-m-d') : '',
                'l'      => $training->started_at ? $training->started_at->format('l') : '',
                'd'      => $training->started_at ? $training->started_at->format('d') : '',
                'm'      => $training->started_at ? $training->started_at->format('m') : '',
                'F'      => $training->started_at ? $training->started_at->format('F') : '',
                'Y'      => $training->started_at ? $training->started_at->format('Y') : '',
            ],
            'ended_at' => [
                'ldFY'   => $training->ended_at ? $training->ended_at->format('l, d F Y') : '',
                'dFY'    => $training->ended_at ? $training->ended_at->format('d F Y') : '',
                'ymd'    => $training->ended_at ? $training->ended_at->format('Y-m-d') : '',
                'l'      => $training->ended_at ? $training->ended_at->format('l') : '',
                'd'      => $training->ended_at ? $training->ended_at->format('d') : '',
                'm'      => $training->ended_at ? $training->ended_at->format('m') : '',
                'F'      => $training->ended_at ? $training->ended_at->format('F') : '',
                'Y'      => $training->ended_at ? $training->ended_at->format('Y') : '',
            ],
            'document' => [
                'path' => $training->document ? $training->document->path : (bool) false,
                'name' => $training->document ? $training->document->file_name : (bool) false,
            ]
        ];
    }
}
