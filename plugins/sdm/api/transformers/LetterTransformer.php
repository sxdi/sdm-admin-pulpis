<?php namespace Sdm\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Sdm\Letter\Models\Letter as LetterModels;

class LetterTransformer extends TransformerAbstract
{
    public $availableIncludes = [
        'items',
    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(LetterModels $letter)
    {
        return [
            'id'         => $letter->id,
            'name'       => $letter->name,
            'mode'       => $letter->parent ? $letter->parent->mode : '',
            'attachment' => $letter->document
        ];
    }

    public function includeItems(LetterModels $letter)
    {
        return $this->collection($letter->items, new \Sdm\Api\Transformers\LetterTransformer);
    }
}
