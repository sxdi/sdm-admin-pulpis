<?php namespace Sdm\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Sdm\Health\Models\Consultation as ConsultationModels;

class HealthConsultationTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(ConsultationModels $consultation)
    {
        return [
            'id'         => $consultation->id,
            'title'      => $consultation->title,
            'is_close'   => $consultation->is_close,
            'created_at' => [
                'ldFY'   => $consultation->created_at ? $consultation->created_at->format('l, d F Y') : '',
                'dFY'    => $consultation->created_at ? $consultation->created_at->format('d F Y') : '',
                'ymd'    => $consultation->created_at ? $consultation->created_at->format('Y-m-d') : '',
                'l'      => $consultation->created_at ? $consultation->created_at->format('l') : '',
                'd'      => $consultation->created_at ? $consultation->created_at->format('d') : '',
                'm'      => $consultation->created_at ? $consultation->created_at->format('m') : '',
                'F'      => $consultation->created_at ? $consultation->created_at->format('F') : '',
                'Y'      => $consultation->created_at ? $consultation->created_at->format('Y') : '',
            ],
            'user'       => [
                'id' => $consultation->user_id,
            ],
            'tags'   => $this->renderTags($consultation)
        ];
    }

    public function renderTags($consultation)
    {
        $str  = '';
        if(count($consultation->tags)) {
            $tags = $consultation->tags;
            foreach ($tags as $key => $tag) {
                $str .= $tag->name;
                if($key + 1 != count($tags)) {
                    $str .= ',';
                }
            }
        }

        return [
            'db'  => $consultation->tags,
            'str' => $str ? $str : (bool) false
        ];
    }
}
