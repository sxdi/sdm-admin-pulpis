<?php namespace Sdm\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Sdm\Member\Models\Grade as GradeModels;

class MemberGradeTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(GradeModels $grade)
    {
        return [
            'id'           => $grade->id,
            'name'         => $this->transformGrade($grade->parent),
            'number'       => $grade->number,
            'is_now'       => (bool) $grade->is_now,
            'status'       => $grade->status,
            'message'      => $grade->message,
            'started_at' => [
                'ldFY'   => $grade->started_at ? $grade->started_at->format('l, d F Y') : '',
                'dFY'    => $grade->started_at ? $grade->started_at->format('d F Y') : '',
                'ymd'    => $grade->started_at ? $grade->started_at->format('Y-m-d') : '',
                'l'      => $grade->started_at ? $grade->started_at->format('l') : '',
                'd'      => $grade->started_at ? $grade->started_at->format('d') : '',
                'm'      => $grade->started_at ? $grade->started_at->format('m') : '',
                'F'      => $grade->started_at ? $grade->started_at->format('F') : '',
                'Y'      => $grade->started_at ? $grade->started_at->format('Y') : '',
            ],
            'ended_at' => [
                'ldFY'   => $grade->ended_at ? $grade->ended_at->format('l, d F Y') : '',
                'dFY'    => $grade->ended_at ? $grade->ended_at->format('d F Y') : '',
                'ymd'    => $grade->ended_at ? $grade->ended_at->format('Y-m-d') : '',
                'l'      => $grade->ended_at ? $grade->ended_at->format('l') : '',
                'd'      => $grade->ended_at ? $grade->ended_at->format('d') : '',
                'm'      => $grade->ended_at ? $grade->ended_at->format('m') : '',
                'F'      => $grade->ended_at ? $grade->ended_at->format('F') : '',
                'Y'      => $grade->ended_at ? $grade->ended_at->format('Y') : '',
            ],
            'document' => [
                'path' => $grade->document ? $grade->document->path : (bool) false,
                'name' => $grade->document ? $grade->document->file_name : (bool) false,
            ]
        ];
    }

    public function transformGrade($grade)
    {
        return [
            'name'  => $grade->name,
            'code'  => strtoupper($grade->code),
            'grade' => $grade->grade,
            'value' => $grade->id
        ];
    }
}
