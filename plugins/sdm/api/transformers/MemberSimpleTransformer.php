<?php namespace Sdm\Api\Transformers;

use Carbon\Carbon;

use League\Fractal\TransformerAbstract;

use Sdm\Member\Models\Member as MemberModels;

class MemberSimpleTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(MemberModels $member)
    {
        return [
            'id'               => $member->id,
            'name'             => $member->name,
            'name'             => $member->name,
            'nrp'              => $member->nrp,
            'phone'            => $member->phone,
            'gender'           => $member->gender,
            'place'            => $member->place,
            'dob'              => [
                'ldFY'   => $member->dob ? $member->dob->format('l, d F Y') : '',
                'dFY'    => $member->dob ? $member->dob->format('d F Y') : '',
                'ymd'    => $member->dob ? $member->dob->format('Y-m-d') : '',
                'l'      => $member->dob ? $member->dob->format('l') : '',
                'd'      => $member->dob ? $member->dob->format('d') : '',
                'm'      => $member->dob ? $member->dob->format('m') : '',
                'F'      => $member->dob ? $member->dob->format('F') : '',
                'Y'      => $member->dob ? $member->dob->format('Y') : '',
                'age'    => $member->dob ? Carbon::parse($member->dob)->age : '',
            ],
            'child'            => $member->child,
            'blood'            => $member->blood,
            'religion'         => $member->religion,
            'marital'          => $member->marital,
            'nationality'      => $member->nationality,
            'skin_color'       => $member->skin_color,
            'hair_type'        => $member->hair_type,
            'hair_color'       => $member->hair_color,
            'height'           => $member->height,
            'weight'           => $member->weight,
            'address'          => $member->address,
            'uniform_head'     => $member->uniform_head,
            'uniform_trousers' => $member->uniform_trousers,
            'uniform_clothes'  => $member->uniform_clothes,
            'uniform_feet'     => $member->uniform_feet,
            'worked_at'        => $this->transformWorkTime($member),
            'position'         => [
                'name'       => $member->getCurrentPosition() ? $member->getCurrentPosition()->name : (bool) false,
                'department' => $member->getCurrentPosition() ? $member->getCurrentPosition()->department->name : (bool) false,
                'unit'       => $member->getCurrentPosition() ? $member->getCurrentPosition()->department->unit->name : (bool) false,
                'value'      => $member->getCurrentPosition() ? $member->getCurrentPosition()->id : (bool) false,
            ],
            'grade'             => [
                'name'  => $member->getCurrentGrade() ? $member->getCurrentGrade()->parent->name : (bool) false,
                'code'  => $member->getCurrentGrade() ? $member->getCurrentGrade()->parent->code : (bool) false,
                'value' => $member->getCurrentGrade() ? $member->getCurrentGrade()->parent->id : (bool) false,
            ],
            'attachment'       => [
                'picture' => [
                    'file'   => [
                        'path' => $member->picture ? $member->picture->path : (bool) false,
                        'name' => $member->picture ? $member->picture->file_name : (bool) false,
                    ],
                ],
                'ktp' => [
                    'status'  => $member->getType('ktp') ? $member->getType('ktp')->status : '',
                    'message' => $member->getType('ktp') ? $member->getType('ktp')->message : '',
                    'value'   => $member->getType('ktp') ? $member->getType('ktp')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('ktp') ? $member->getType('ktp')->ktp->path : (bool) false,
                        'name' => $member->getType('ktp') ? $member->getType('ktp')->ktp->file_name : (bool) false,
                    ],
                ],
                'npwp' => [
                    'status'  => $member->getType('npwp') ? $member->getType('npwp')->status : '',
                    'message' => $member->getType('npwp') ? $member->getType('npwp')->message : '',
                    'value'   => $member->getType('npwp') ? $member->getType('npwp')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('npwp') ? $member->getType('npwp')->npwp->path : (bool) false,
                        'name' => $member->getType('npwp') ? $member->getType('npwp')->npwp->file_name : (bool) false,
                    ],
                ],
                'kk' => [
                    'status'  => $member->getType('kk') ? $member->getType('kk')->status : '',
                    'message' => $member->getType('kk') ? $member->getType('kk')->message : '',
                    'value'   => $member->getType('kk') ? $member->getType('kk')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('kk') ? $member->getType('kk')->kk->path : (bool) false,
                        'name' => $member->getType('kk') ? $member->getType('kk')->kk->file_name : (bool) false,
                    ],
                ],
                'finger' => [
                    'status'  => $member->getType('finger') ? $member->getType('finger')->status : '',
                    'message' => $member->getType('finger') ? $member->getType('finger')->message : '',
                    'value'   => $member->getType('finger') ? $member->getType('finger')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('finger') ? $member->getType('finger')->finger->path : (bool) false,
                        'name' => $member->getType('finger') ? $member->getType('finger')->finger->file_name : (bool) false,
                    ],
                ],
                'asabri'       => [
                    'status'  => $member->getType('asabri') ? $member->getType('asabri')->status : '',
                    'message' => $member->getType('asabri') ? $member->getType('asabri')->message : '',
                    'value'   => $member->getType('asabri') ? $member->getType('asabri')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('asabri') ? $member->getType('asabri')->asabri->path : (bool) false,
                        'name' => $member->getType('asabri') ? $member->getType('asabri')->asabri->file_name : (bool) false,
                    ],
                ],
                'bpjs'       => [
                    'status'  => $member->getType('bpjs') ? $member->getType('bpjs')->status : '',
                    'message' => $member->getType('bpjs') ? $member->getType('bpjs')->message : '',
                    'value'   => $member->getType('bpjs') ? $member->getType('bpjs')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('bpjs') ? $member->getType('bpjs')->bpjs->path : (bool) false,
                        'name' => $member->getType('bpjs') ? $member->getType('bpjs')->bpjs->file_name : (bool) false,
                    ],
                ],
                'authority'       => [
                    'status'  => $member->getType('authority') ? $member->getType('authority')->status : '',
                    'message' => $member->getType('authority') ? $member->getType('authority')->message : '',
                    'value'   => $member->getType('authority') ? $member->getType('authority')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('authority') ? $member->getType('authority')->authority->path : (bool) false,
                        'name' => $member->getType('authority') ? $member->getType('authority')->authority->file_name : (bool) false,
                    ],
                ],
                'investigator'       => [
                    'status'  => $member->getType('investigator') ? $member->getType('investigator')->status : '',
                    'message' => $member->getType('investigator') ? $member->getType('investigator')->message : '',
                    'value'   => $member->getType('investigator') ? $member->getType('investigator')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('investigator') ? $member->getType('investigator')->investigator->path : (bool) false,
                        'name' => $member->getType('investigator') ? $member->getType('investigator')->investigator->file_name : (bool) false,
                    ],
                ],
                'member'       => [
                    'status'  => $member->getType('member') ? $member->getType('member')->status : '',
                    'message' => $member->getType('member') ? $member->getType('member')->message : '',
                    'value'   => $member->getType('member') ? $member->getType('member')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('member') ? $member->getType('member')->member->path : (bool) false,
                        'name' => $member->getType('member') ? $member->getType('member')->member->file_name : (bool) false,
                    ],
                ],
                'kps'       => [
                    'status'  => $member->getType('kps') ? $member->getType('kps')->status : '',
                    'message' => $member->getType('kps') ? $member->getType('kps')->message : '',
                    'value'   => $member->getType('kps') ? $member->getType('kps')->value : (bool) false,
                    'file'  => [
                        'path' => $member->getType('kps') ? $member->getType('kps')->kps->path : (bool) false,
                        'name' => $member->getType('kps') ? $member->getType('kps')->kps->file_name : (bool) false,
                    ],
                ],
                'simsa'       => [
                    'status'  => $member->getType('simsa') ? $member->getType('simsa')->status : '',
                    'message' => $member->getType('simsa') ? $member->getType('simsa')->message : '',
                    'value'   => $member->getType('simsa') ? $member->getType('simsa')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('simsa') ? $member->getType('simsa')->simsa->path : (bool) false,
                        'name' => $member->getType('simsa') ? $member->getType('simsa')->simsa->file_name : (bool) false,
                    ],
                ],
            ],
        ];
    }

    public function transformWorkTime($member)
    {
        return $member->getWorkTime();
    }
}
