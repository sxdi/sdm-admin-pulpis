<?php namespace Sdm\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Sdm\Member\Models\Health as HealthModels;

class HealthTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(HealthModels $health)
    {
        $memberTransformer = new \Sdm\Api\Transformers\MemberSimpleTransformer;
        return [
            'id'      => $health->id,
            'title'   => $health->title,
            'content' => $health->content,
            'member'  => $memberTransformer->transform($health->member),
            'admin'   => $memberTransformer->transform($health->admin),
            'date'    => [
                'ldFY'   => $health->date ? $health->date->format('l, d F Y') : '',
                'dFY'    => $health->date ? $health->date->format('d F Y') : '',
                'ymd'    => $health->date ? $health->date->format('Y-m-d') : '',
                'l'      => $health->date ? $health->date->format('l') : '',
                'd'      => $health->date ? $health->date->format('d') : '',
                'm'      => $health->date ? $health->date->format('m') : '',
                'F'      => $health->date ? $health->date->format('F') : '',
                'Y'      => $health->date ? $health->date->format('Y') : '',
            ],
            'file'    => [
                'path' => $health->document ? $health->document->path : (bool) false,
                'name' => $health->document ? $health->document->file_name : (bool) false,
            ],
            'tags'    => $this->renderTags($health)
        ];
    }

    public function renderTags($health)
    {
        $str  = '';
        if(count($health->tags)) {
            $tags = $health->tags;
            foreach ($tags as $key => $tag) {
                $str .= $tag->name;
                if($key + 1 != count($tags)) {
                    $str .= ',';
                }
            }
        }

        return [
            'db'  => $health->tags,
            'str' => $str ? $str : (bool) false
        ];
    }
}
