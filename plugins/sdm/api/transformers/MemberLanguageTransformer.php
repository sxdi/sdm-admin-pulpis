<?php namespace Sdm\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Sdm\Member\Models\Language as LanguageModels;

class MemberLanguageTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(LanguageModels $language)
    {
        return [
            'id'        => $language->id,
            'name'      => $language->name,
            'result'    => $this->transformResult($language->result)
        ];
    }

    public function transformResult($result)
    {
        if($result == 'active') {
            return [
                'name'  => 'Aktif',
                'value' => 'active'
            ];
        }

        return [
            'name'  => 'Pasif',
            'value' => 'passive'
        ];
    }
}
