<?php namespace Sdm\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Sdm\Member\Models\Sport as SportModels;

class MemberSportTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(SportModels $sport)
    {
        return [
            'id'      => $sport->id,
            'name'    => $sport->name,
            'content' => $sport->content,
        ];
    }
}
