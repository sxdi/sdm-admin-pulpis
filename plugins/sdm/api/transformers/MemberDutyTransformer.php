<?php namespace Sdm\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Sdm\Member\Models\Duty as DutyModels;

class MemberDutyTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(DutyModels $duty)
    {
        return [
            'id'           => $duty->id,
            'name'         => $duty->name,
            'origin'       => $duty->origin,
            'is_now'       => (bool) $duty->is_now,
            'status'       => $duty->status,
            'message'      => $duty->message,
            'started_at' => [
                'ldFY'   => $duty->started_at ? $duty->started_at->format('l, d F Y') : '',
                'dFY'    => $duty->started_at ? $duty->started_at->format('d F Y') : '',
                'ymd'    => $duty->started_at ? $duty->started_at->format('Y-m-d') : '',
                'l'      => $duty->started_at ? $duty->started_at->format('l') : '',
                'd'      => $duty->started_at ? $duty->started_at->format('d') : '',
                'm'      => $duty->started_at ? $duty->started_at->format('m') : '',
                'F'      => $duty->started_at ? $duty->started_at->format('F') : '',
                'Y'      => $duty->started_at ? $duty->started_at->format('Y') : '',
            ],
            'ended_at' => [
                'ldFY'   => $duty->ended_at ? $duty->ended_at->format('l, d F Y') : '',
                'dFY'    => $duty->ended_at ? $duty->ended_at->format('d F Y') : '',
                'ymd'    => $duty->ended_at ? $duty->ended_at->format('Y-m-d') : '',
                'l'      => $duty->ended_at ? $duty->ended_at->format('l') : '',
                'd'      => $duty->ended_at ? $duty->ended_at->format('d') : '',
                'm'      => $duty->ended_at ? $duty->ended_at->format('m') : '',
                'F'      => $duty->ended_at ? $duty->ended_at->format('F') : '',
                'Y'      => $duty->ended_at ? $duty->ended_at->format('Y') : '',
            ],
            'document' => [
                'path' => $duty->document ? $duty->document->path : (bool) false,
                'name' => $duty->document ? $duty->document->file_name : (bool) false,
            ]
        ];
    }
}
