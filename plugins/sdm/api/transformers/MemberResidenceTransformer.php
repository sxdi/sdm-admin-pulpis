<?php namespace Sdm\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Sdm\Member\Models\Residence as ResidenceModels;

class MemberResidenceTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(ResidenceModels $residence)
    {
        return [
            'id'      => $residence->id,
            'type'    => $this->renderType($residence),
            'province'=> $residence->province,
            'regency' => $residence->regency,
            'address' => $residence->address
        ];
    }

    public function renderType($residence)
    {
        if($residence->type == 'personal') {
            return [
                'name' => 'Rumah Pribadi',
                'value'=> $residence->type
            ];
        }

        if($residence->type == 'official') {
            return [
                'name' => 'Rumah Dinas',
                'value'=> $residence->type
            ];
        }

        if($residence->type == 'other') {
            return [
                'name' => 'Lainnya',
                'value'=> $residence->type
            ];
        }
    }
}
