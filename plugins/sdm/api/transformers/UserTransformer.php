<?php namespace Sdm\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Sdm\User\Models\User as UserModels;

use Sdm\Api\Transformers\MemberTransformer;

class UserTransformer extends TransformerAbstract
{
    // Related transformer that can be included
    public $availableIncludes = [
        'member',
    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(UserModels $user)
    {
        return [
            'id'     => $user->id,
            'code'   => $user->code,
            'auth'   => [
                'is_super'  => (bool) $user->is_super,
                'is_admin'  => (bool) $user->is_admin,
                'is_health' => (bool) $user->is_health,
                'is_law'    => (bool) $user->is_law,
            ]
        ];
    }

    public function includeMember(UserModels $user)
    {
        return $this->item($user->member, new MemberTransformer);
    }
}
