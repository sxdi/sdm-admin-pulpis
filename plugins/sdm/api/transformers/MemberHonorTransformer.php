<?php namespace Sdm\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Sdm\Member\Models\Honor as HonorModels;

class MemberHonorTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(HonorModels $honor)
    {
        return [
            'id'      => $honor->id,
            'name'    => $honor->name,
            'status'  => $honor->status,
            'message' => $honor->message,
            'honored' => [
                'ldFY'   => $honor->honored_at ? $honor->honored_at->format('l, d F Y') : '',
                'dFY'    => $honor->honored_at ? $honor->honored_at->format('d F Y') : '',
                'ymd'    => $honor->honored_at ? $honor->honored_at->format('Y-m-d') : '',
                'l'      => $honor->honored_at ? $honor->honored_at->format('l') : '',
                'd'      => $honor->honored_at ? $honor->honored_at->format('d') : '',
                'm'      => $honor->honored_at ? $honor->honored_at->format('m') : '',
                'F'      => $honor->honored_at ? $honor->honored_at->format('F') : '',
                'Y'      => $honor->honored_at ? $honor->honored_at->format('Y') : '',
            ],
            'document' => [
                'path' => $honor->document ? $honor->document->path : (bool) false,
                'name' => $honor->document ? $honor->document->file_name : (bool) false,
            ]
        ];
    }
}
