<?php namespace Sdm\Api\Transformers;

use Carbon\Carbon;

use League\Fractal\TransformerAbstract;

use Sdm\Member\Models\Member as MemberModels;

use Sdm\Core\Classes\MemberManager;

class MemberUpdateTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'languages',
        'languageforeigns',
        'sports',
        'educationformals',
        'educationnonformals',
        'educationpolries',
        'educationdikbangspes',
        'trainings',
        'duties',
        'positions',
        'grades',
        'works',
        'brivets',
        'honors',
        'salaries'
    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(MemberModels $member)
    {
        $memberUpdateManager = new \Sdm\Core\Classes\MemberUpdateManager;
        $transform = [
            'id'               => $member->id,
            'name'             => $member->name,
            'nrp'              => $member->nrp,
            'worked_at'        => $this->transformWorkTime($member),
            'position'         => [
                'name'       => $member->getCurrentPosition() ? $member->getCurrentPosition()->name : (bool) false,
                'department' => $member->getCurrentPosition() ? $member->getCurrentPosition()->department->name : (bool) false,
                'unit'       => $member->getCurrentPosition() ? $member->getCurrentPosition()->department->unit->name : (bool) false,
                'value'      => $member->getCurrentPosition() ? $member->getCurrentPosition()->id : (bool) false,
                'worked_at'  => $this->transformWorkPosition($member)
            ],
            'grade'             => [
                'name'       => $member->getCurrentGrade() ? $member->getCurrentGrade()->parent->name : (bool) false,
                'code'       => $member->getCurrentGrade() ? $member->getCurrentGrade()->parent->code : (bool) false,
                'value'      => $member->getCurrentGrade() ? $member->getCurrentGrade()->parent->id : (bool) false,
                'worked_at'  => $this->transformWorkGrade($member)
            ],
            'attachment'       => [
                'picture' => [
                    'file'   => [
                        'path' => $member->picture ? $member->picture->path : (bool) false,
                        'name' => $member->picture ? $member->picture->file_name : (bool) false,
                    ],
                ],
                'ktp' => [
                    'id'      => $member->getType('ktp') ? $member->getType('ktp')->id : '',
                    'status'  => $member->getType('ktp') ? $member->getType('ktp')->status : '',
                    'message' => $member->getType('ktp') ? $member->getType('ktp')->message : '',
                    'value'   => $member->getType('ktp') ? $member->getType('ktp')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('ktp') ? $member->getType('ktp')->ktp->path : (bool) false,
                        'name' => $member->getType('ktp') ? $member->getType('ktp')->ktp->file_name : (bool) false,
                    ],
                ],
                'npwp' => [
                    'id'      => $member->getType('npwp') ? $member->getType('npwp')->id : '',
                    'status'  => $member->getType('npwp') ? $member->getType('npwp')->status : '',
                    'message' => $member->getType('npwp') ? $member->getType('npwp')->message : '',
                    'value'   => $member->getType('npwp') ? $member->getType('npwp')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('npwp') ? $member->getType('npwp')->npwp->path : (bool) false,
                        'name' => $member->getType('npwp') ? $member->getType('npwp')->npwp->file_name : (bool) false,
                    ],
                ],
                'kk' => [
                    'id'      => $member->getType('kk') ? $member->getType('kk')->id : '',
                    'status'  => $member->getType('kk') ? $member->getType('kk')->status : '',
                    'message' => $member->getType('kk') ? $member->getType('kk')->message : '',
                    'value'   => $member->getType('kk') ? $member->getType('kk')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('kk') ? $member->getType('kk')->kk->path : (bool) false,
                        'name' => $member->getType('kk') ? $member->getType('kk')->kk->file_name : (bool) false,
                    ],
                ],
                'finger' => [
                    'id'      => $member->getType('finger') ? $member->getType('finger')->id : '',
                    'status'  => $member->getType('finger') ? $member->getType('finger')->status : '',
                    'message' => $member->getType('finger') ? $member->getType('finger')->message : '',
                    'value'   => $member->getType('finger') ? $member->getType('finger')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('finger') ? $member->getType('finger')->finger->path : (bool) false,
                        'name' => $member->getType('finger') ? $member->getType('finger')->finger->file_name : (bool) false,
                    ],
                ],
                'asabri'       => [
                    'id'      => $member->getType('asabri') ? $member->getType('asabri')->id : '',
                    'status'  => $member->getType('asabri') ? $member->getType('asabri')->status : '',
                    'message' => $member->getType('asabri') ? $member->getType('asabri')->message : '',
                    'value'   => $member->getType('asabri') ? $member->getType('asabri')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('asabri') ? $member->getType('asabri')->asabri->path : (bool) false,
                        'name' => $member->getType('asabri') ? $member->getType('asabri')->asabri->file_name : (bool) false,
                    ],
                ],
                'bpjs'       => [
                    'id'      => $member->getType('bpjs') ? $member->getType('bpjs')->id : '',
                    'status'  => $member->getType('bpjs') ? $member->getType('bpjs')->status : '',
                    'message' => $member->getType('bpjs') ? $member->getType('bpjs')->message : '',
                    'value'   => $member->getType('bpjs') ? $member->getType('bpjs')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('bpjs') ? $member->getType('bpjs')->bpjs->path : (bool) false,
                        'name' => $member->getType('bpjs') ? $member->getType('bpjs')->bpjs->file_name : (bool) false,
                    ],
                ],
                'authority'       => [
                    'id'      => $member->getType('authority') ? $member->getType('authority')->id : '',
                    'status'  => $member->getType('authority') ? $member->getType('authority')->status : '',
                    'message' => $member->getType('authority') ? $member->getType('authority')->message : '',
                    'value'   => $member->getType('authority') ? $member->getType('authority')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('authority') ? $member->getType('authority')->authority->path : (bool) false,
                        'name' => $member->getType('authority') ? $member->getType('authority')->authority->file_name : (bool) false,
                    ],
                ],
                'investigator'       => [
                    'id'      => $member->getType('investigator') ? $member->getType('investigator')->id : '',
                    'status'  => $member->getType('investigator') ? $member->getType('investigator')->status : '',
                    'message' => $member->getType('investigator') ? $member->getType('investigator')->message : '',
                    'value'   => $member->getType('investigator') ? $member->getType('investigator')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('investigator') ? $member->getType('investigator')->investigator->path : (bool) false,
                        'name' => $member->getType('investigator') ? $member->getType('investigator')->investigator->file_name : (bool) false,
                    ],
                ],
                'member'       => [
                    'id'      => $member->getType('member') ? $member->getType('member')->id : '',
                    'status'  => $member->getType('member') ? $member->getType('member')->status : '',
                    'message' => $member->getType('member') ? $member->getType('member')->message : '',
                    'value'   => $member->getType('member') ? $member->getType('member')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('member') ? $member->getType('member')->member->path : (bool) false,
                        'name' => $member->getType('member') ? $member->getType('member')->member->file_name : (bool) false,
                    ],
                ],
                'kps'       => [
                    'id'      => $member->getType('kps') ? $member->getType('kps')->id : '',
                    'status'  => $member->getType('kps') ? $member->getType('kps')->status : '',
                    'message' => $member->getType('kps') ? $member->getType('kps')->message : '',
                    'value'   => $member->getType('kps') ? $member->getType('kps')->value : (bool) false,
                    'file'  => [
                        'path' => $member->getType('kps') ? $member->getType('kps')->kps->path : (bool) false,
                        'name' => $member->getType('kps') ? $member->getType('kps')->kps->file_name : (bool) false,
                    ],
                ],
                'simsa'       => [
                    'id'      => $member->getType('simsa') ? $member->getType('simsa')->id : '',
                    'status'  => $member->getType('simsa') ? $member->getType('simsa')->status : '',
                    'message' => $member->getType('simsa') ? $member->getType('simsa')->message : '',
                    'value'   => $member->getType('simsa') ? $member->getType('simsa')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('simsa') ? $member->getType('simsa')->simsa->path : (bool) false,
                        'name' => $member->getType('simsa') ? $member->getType('simsa')->simsa->file_name : (bool) false,
                    ],
                ],
            ],
            'update'           => $memberUpdateManager->count($member)
        ];

        if($transform['attachment']['ktp']['status'] != 'process') {
            unset($transform['attachment']['ktp']);
        }
        if($transform['attachment']['npwp']['status'] != 'process') {
            unset($transform['attachment']['npwp']);
        }
        if($transform['attachment']['kk']['status'] != 'process') {
            unset($transform['attachment']['kk']);
        }
        if($transform['attachment']['finger']['status'] != 'process') {
            unset($transform['attachment']['finger']);
        }
        if($transform['attachment']['asabri']['status'] != 'process') {
            unset($transform['attachment']['asabri']);
        }
        if($transform['attachment']['bpjs']['status'] != 'process') {
            unset($transform['attachment']['bpjs']);
        }
        if($transform['attachment']['authority']['status'] != 'process') {
            unset($transform['attachment']['authority']);
        }
        if($transform['attachment']['investigator']['status'] != 'process') {
            unset($transform['attachment']['investigator']);
        }
        if($transform['attachment']['member']['status'] != 'process') {
            unset($transform['attachment']['member']);
        }
        if($transform['attachment']['kps']['status'] != 'process') {
            unset($transform['attachment']['kps']);
        }
        if($transform['attachment']['simsa']['status'] != 'process') {
            unset($transform['attachment']['simsa']);
        }

        return $transform;
    }

    /**
     * Include Language
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeLanguages(MemberModels $member)
    {
        $language = \Sdm\Member\Models\Language::whereMemberId($member->id)
            ->where('is_foreign', '=', 0)
            ->where('status', '=', 'process')
            ->get();
        return $this->collection($language, new \Sdm\Api\Transformers\MemberLanguageTransformer);
    }

    /**
     * Include Language Foreign
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeLanguageForeigns(MemberModels $member)
    {
        $language = \Sdm\Member\Models\Language::whereMemberId($member->id)
            ->where('is_foreign', '=', 1)
            ->where('status', '=', 'process')
            ->get();
        return $this->collection($language, new \Sdm\Api\Transformers\MemberLanguageTransformer);
    }

    /**
     * Include Sport
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeSports(MemberModels $member)
    {
        $sport = \Sdm\Member\Models\Sport::whereMemberId($member->id)
            ->where('status', '=', 'process')
            ->get();
        return $this->collection($sport, new \Sdm\Api\Transformers\MemberSportTransformer);
    }

    /**
     * Include Education
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeEducationFormals(MemberModels $member)
    {
        $education = \Sdm\Member\Models\Education::whereMemberId($member->id)
            ->where('type', '=', 'formal')
            ->where('status', '=', 'process')
            ->get();
        return $this->collection($education, new \Sdm\Api\Transformers\MemberEducationTransformer);
    }

    /**
     * Include Education
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeEducationNonFormals(MemberModels $member)
    {
        $education = \Sdm\Member\Models\Education::whereMemberId($member->id)
            ->where('type', '=', 'non')
            ->where('status', '=', 'process')
            ->get();
        return $this->collection($education, new \Sdm\Api\Transformers\MemberEducationTransformer);
    }

    /**
     * Include Education Polri
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeEducationPolries(MemberModels $member)
    {
        $education = \Sdm\Member\Models\EducationPolice::whereMemberId($member->id)
            ->where('type', '=', 'polri')
            ->where('status', '=', 'process')
            ->get();
        return $this->collection($education, new \Sdm\Api\Transformers\MemberEducationPolriTransformer);
    }

    /**
     * Include Education Dikbangspes
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeEducationDikbangspes(MemberModels $member)
    {
        $education = \Sdm\Member\Models\EducationPolice::whereMemberId($member->id)
            ->where('type', '=', 'dikbangspes')
            ->where('status', '=', 'process')
            ->get();
        return $this->collection($education, new \Sdm\Api\Transformers\MemberEducationPolriTransformer);
    }

     /**
     * Include Honor
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeHonors(MemberModels $member)
    {
        $honor = \Sdm\Member\Models\Honor::whereMemberId($member->id)
            ->where('status', '=', 'process')
            ->get();
        return $this->collection($honor, new \Sdm\Api\Transformers\MemberHonorTransformer);
    }

    /**
     * Include Training
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeTrainings(MemberModels $member)
    {
        $training = \Sdm\Member\Models\Training::whereMemberId($member->id)
            ->where('status', '=', 'process')
            ->get();
        return $this->collection($training, new \Sdm\Api\Transformers\MemberTrainingTransformer);
    }

    /**
     * Include Duties
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeDuties(MemberModels $member)
    {
        $duty = \Sdm\Member\Models\Duty::whereMemberId($member->id)
            ->where('status', '=', 'process')
            ->get();
        return $this->collection($duty, new \Sdm\Api\Transformers\MemberDutyTransformer);
    }

    /**
     * Include Positions
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includePositions(MemberModels $member)
    {
        $position = \Sdm\Member\Models\Position::whereMemberId($member->id)
            ->where('status', '=', 'process')
            ->get();
        return $this->collection($position, new \Sdm\Api\Transformers\MemberPositionTransformer);
    }

    /**
     * Include Grades
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeGrades(MemberModels $member)
    {
        $grade = \Sdm\Member\Models\Grade::whereMemberId($member->id)
            ->where('status', '=', 'process')
            ->get();
        return $this->collection($grade, new \Sdm\Api\Transformers\MemberGradeTransformer);
    }

    /**
     * Include Works
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeWorks(MemberModels $member)
    {
        $work = \Sdm\Member\Models\Work::whereMemberId($member->id)
            ->where('status', '=', 'process')
            ->get();
        return $this->collection($work, new \Sdm\Api\Transformers\MemberWorkTransformer);
    }

    /**
     * Include Brivet
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeBrivets(MemberModels $member)
    {
        $brivet = \Sdm\Member\Models\Brivet::whereMemberId($member->id)
            ->where('status', '=', 'process')
            ->get();
        return $this->collection($brivet, new \Sdm\Api\Transformers\MemberBrivetTransformer);
    }

    /**
     * Include Salary
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeSalaries(MemberModels $member)
    {
        $salary = \Sdm\Member\Models\Salary::whereMemberId($member->id)
            ->where('status', '=', 'process')
            ->get();
        return $this->collection($salary, new \Sdm\Api\Transformers\MemberSalaryTransformer);
    }

    public function transformWorkTime($member)
    {
        return $member->getWorkTime();
    }

    public function transformWorkGrade($member)
    {
        if($member->getCurrentGrade()) {
            $d1    = new \DateTime(date('Y-m-d'));
            $d2    = new \DateTime($member->getCurrentGrade()->started_at->format('Y-m-d'));
            $diff  = $d2->diff($d1);
            $total = [$diff->y, $diff->m];
            return [
                'date' => $member->getCurrentGrade()->started_at->format('d F Y'),
                'count'=> $total[0].' Tahun '.$total[1].' Bulan'
            ];
        }
    }

    public function transformWorkPosition($member)
    {
        if($member->getCurrentPosition()) {
            $d1    = new \DateTime(date('Y-m-d'));
            $d2    = new \DateTime($member->getCurrentPosition()->started_at->format('Y-m-d'));
            $diff  = $d2->diff($d1);
            $total = [$diff->y, $diff->m];
            return [
                'date' => $member->getCurrentPosition()->started_at->format('d F Y'),
                'count'=> $total[0].' Tahun '.$total[1].' Bulan'
            ];
        }
    }
}
