<?php namespace Sdm\Api\Transformers;

use Carbon\Carbon;

use League\Fractal\TransformerAbstract;

use Sdm\Member\Models\Member as MemberModels;

use Sdm\Core\Classes\MemberManager;

class MemberTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'partner',
        'childs',
        'father',
        'mother',
        'brother',
        'languages',
        'languageforeigns',
        'sports',
        'educationformals',
        'educationnonformals',
        'educationpolries',
        'educationdikbangspes',
        'trainings',
        'duties',
        'positions',
        'grades',
        'works',
        'brivets',
        'honors'
    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(MemberModels $member)
    {
        $memberUpdateManager = new \Sdm\Core\Classes\MemberUpdateManager;
        return [
            'id'               => $member->id,
            'name'             => $member->name,
            'name'             => $member->name,
            'nrp'              => $member->nrp,
            'phone'            => $member->phone,
            'gender'           => $member->gender,
            'place'            => $member->place,
            'dob'              => [
                'ldFY'   => $member->dob ? $member->dob->format('l, d F Y') : '',
                'dFY'    => $member->dob ? $member->dob->format('d F Y') : '',
                'ymd'    => $member->dob ? $member->dob->format('Y-m-d') : '',
                'l'      => $member->dob ? $member->dob->format('l') : '',
                'd'      => $member->dob ? $member->dob->format('d') : '',
                'm'      => $member->dob ? $member->dob->format('m') : '',
                'F'      => $member->dob ? $member->dob->format('F') : '',
                'Y'      => $member->dob ? $member->dob->format('Y') : '',
                'age'    => $member->dob ? Carbon::parse($member->dob)->age : '',
            ],
            'child'            => $member->child,
            'blood'            => $member->blood,
            'religion'         => $member->religion,
            'marital'          => $member->marital,
            'nationality'      => $member->nationality,
            'skin_color'       => $member->skin_color,
            'hair_type'        => $member->hair_type,
            'hair_color'       => $member->hair_color,
            'height'           => $member->height,
            'weight'           => $member->weight,
            'address'          => $member->address,
            'uniform_head'     => $member->uniform_head,
            'uniform_trousers' => $member->uniform_trousers,
            'uniform_clothes'  => $member->uniform_clothes,
            'uniform_feet'     => $member->uniform_feet,
            'worked_at'        => $this->transformWorkTime($member),
            'position'       => [
                'name'       => $member->getCurrentPosition() ? $member->getCurrentPosition()->name : (bool) false,
                'department' => $member->getCurrentPosition() ? $member->getCurrentPosition()->department->name : (bool) false,
                'unit'       => $member->getCurrentPosition() ? $member->getCurrentPosition()->department->unit->name : (bool) false,
                'value'      => $member->getCurrentPosition() ? $member->getCurrentPosition()->id : (bool) false,
                'worked_at'  => $this->transformWorkPosition($member),
            ],
            'grade'          => [
                'name'       => $member->getCurrentGrade() ? $member->getCurrentGrade()->parent->name : (bool) false,
                'code'       => $member->getCurrentGrade() ? $member->getCurrentGrade()->parent->code : (bool) false,
                'value'      => $member->getCurrentGrade() ? $member->getCurrentGrade()->parent->id : (bool) false,
                'worked_at'  => $this->transformWorkGrade($member),
                'salary'     => [
                    'started_at' => $member->getCurrentSalary() ? $member->getCurrentSalary()->started_at->format('d F Y') : '',
                    'ended_at'   => $member->getCurrentSalary() ? $member->getCurrentSalary()->ended_at->format('d F Y') : '',
                    'value'      => $member->getCurrentSalary() ? $member->getCurrentSalary()->value : '',
                ]
            ],
            'attachment'  => [
                'picture' => [
                    'file'   => [
                        'path' => $member->picture ? $member->picture->path : (bool) false,
                        'name' => $member->picture ? $member->picture->file_name : (bool) false,
                    ],
                ],
                'ktp' => [
                    'id'      => $member->getType('ktp') ? $member->getType('ktp')->id : '',
                    'status'  => $member->getType('ktp') ? $member->getType('ktp')->status : '',
                    'message' => $member->getType('ktp') ? $member->getType('ktp')->message : '',
                    'value'   => $member->getType('ktp') ? $member->getType('ktp')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('ktp') ? $member->getType('ktp')->ktp->path : (bool) false,
                        'name' => $member->getType('ktp') ? $member->getType('ktp')->ktp->file_name : (bool) false,
                    ],
                ],
                'npwp' => [
                    'id'      => $member->getType('npwp') ? $member->getType('npwp')->id : '',
                    'status'  => $member->getType('npwp') ? $member->getType('npwp')->status : '',
                    'message' => $member->getType('npwp') ? $member->getType('npwp')->message : '',
                    'value'   => $member->getType('npwp') ? $member->getType('npwp')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('npwp') ? $member->getType('npwp')->npwp->path : (bool) false,
                        'name' => $member->getType('npwp') ? $member->getType('npwp')->npwp->file_name : (bool) false,
                    ],
                ],
                'kk' => [
                    'id'      => $member->getType('kk') ? $member->getType('kk')->id : '',
                    'status'  => $member->getType('kk') ? $member->getType('kk')->status : '',
                    'message' => $member->getType('kk') ? $member->getType('kk')->message : '',
                    'value'   => $member->getType('kk') ? $member->getType('kk')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('kk') ? $member->getType('kk')->kk->path : (bool) false,
                        'name' => $member->getType('kk') ? $member->getType('kk')->kk->file_name : (bool) false,
                    ],
                ],
                'finger' => [
                    'id'      => $member->getType('finger') ? $member->getType('finger')->id : '',
                    'status'  => $member->getType('finger') ? $member->getType('finger')->status : '',
                    'message' => $member->getType('finger') ? $member->getType('finger')->message : '',
                    'value'   => $member->getType('finger') ? $member->getType('finger')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('finger') ? $member->getType('finger')->finger->path : (bool) false,
                        'name' => $member->getType('finger') ? $member->getType('finger')->finger->file_name : (bool) false,
                    ],
                ],
                'asabri'       => [
                    'id'      => $member->getType('asabri') ? $member->getType('asabri')->id : '',
                    'status'  => $member->getType('asabri') ? $member->getType('asabri')->status : '',
                    'message' => $member->getType('asabri') ? $member->getType('asabri')->message : '',
                    'value'   => $member->getType('asabri') ? $member->getType('asabri')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('asabri') ? $member->getType('asabri')->asabri->path : (bool) false,
                        'name' => $member->getType('asabri') ? $member->getType('asabri')->asabri->file_name : (bool) false,
                    ],
                ],
                'bpjs'       => [
                    'id'      => $member->getType('bpjs') ? $member->getType('bpjs')->id : '',
                    'status'  => $member->getType('bpjs') ? $member->getType('bpjs')->status : '',
                    'message' => $member->getType('bpjs') ? $member->getType('bpjs')->message : '',
                    'value'   => $member->getType('bpjs') ? $member->getType('bpjs')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('bpjs') ? $member->getType('bpjs')->bpjs->path : (bool) false,
                        'name' => $member->getType('bpjs') ? $member->getType('bpjs')->bpjs->file_name : (bool) false,
                    ],
                ],
                'authority'       => [
                    'id'      => $member->getType('authority') ? $member->getType('authority')->id : '',
                    'status'  => $member->getType('authority') ? $member->getType('authority')->status : '',
                    'message' => $member->getType('authority') ? $member->getType('authority')->message : '',
                    'value'   => $member->getType('authority') ? $member->getType('authority')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('authority') ? $member->getType('authority')->authority->path : (bool) false,
                        'name' => $member->getType('authority') ? $member->getType('authority')->authority->file_name : (bool) false,
                    ],
                ],
                'investigator'       => [
                    'id'      => $member->getType('investigator') ? $member->getType('investigator')->id : '',
                    'status'  => $member->getType('investigator') ? $member->getType('investigator')->status : '',
                    'message' => $member->getType('investigator') ? $member->getType('investigator')->message : '',
                    'value'   => $member->getType('investigator') ? $member->getType('investigator')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('investigator') ? $member->getType('investigator')->investigator->path : (bool) false,
                        'name' => $member->getType('investigator') ? $member->getType('investigator')->investigator->file_name : (bool) false,
                    ],
                ],
                'member'       => [
                    'id'      => $member->getType('member') ? $member->getType('member')->id : '',
                    'status'  => $member->getType('member') ? $member->getType('member')->status : '',
                    'message' => $member->getType('member') ? $member->getType('member')->message : '',
                    'value'   => $member->getType('member') ? $member->getType('member')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('member') ? $member->getType('member')->member->path : (bool) false,
                        'name' => $member->getType('member') ? $member->getType('member')->member->file_name : (bool) false,
                    ],
                ],
                'kps'       => [
                    'id'      => $member->getType('kps') ? $member->getType('kps')->id : '',
                    'status'  => $member->getType('kps') ? $member->getType('kps')->status : '',
                    'message' => $member->getType('kps') ? $member->getType('kps')->message : '',
                    'value'   => $member->getType('kps') ? $member->getType('kps')->value : (bool) false,
                    'file'  => [
                        'path' => $member->getType('kps') ? $member->getType('kps')->kps->path : (bool) false,
                        'name' => $member->getType('kps') ? $member->getType('kps')->kps->file_name : (bool) false,
                    ],
                ],
                'simsa'       => [
                    'id'      => $member->getType('simsa') ? $member->getType('simsa')->id : '',
                    'status'  => $member->getType('simsa') ? $member->getType('simsa')->status : '',
                    'message' => $member->getType('simsa') ? $member->getType('simsa')->message : '',
                    'value'   => $member->getType('simsa') ? $member->getType('simsa')->value : (bool) false,
                    'file'    => [
                        'path' => $member->getType('simsa') ? $member->getType('simsa')->simsa->path : (bool) false,
                        'name' => $member->getType('simsa') ? $member->getType('simsa')->simsa->file_name : (bool) false,
                    ],
                ],
            ],
            'information'      => [
                'personal' => [
                    'picture'    => $member->picture ? (bool) true : (bool) false,
                    'self'       => $member->name ? (bool) true : (bool) false,
                    'attachment' => $member->checkAttachment(['ktp', 'kk', 'asabri', 'bpjs']) ? (bool) true : (bool) false,
                ],
                'family' => [
                    'partner' => $member->checkFamily(['wife','husband'])->count() ? (bool) true : (bool) false,
                    'parent'  => $member->checkFamily(['father', 'mother'])->count() ? (bool) true : (bool) false,
                    'child'   => $member->checkFamily(['child'])->count() ? (bool) true : (bool) false,
                ],
                'residence' => $member->checkResidence()->count() ? (bool) true : (bool) false,
                'education' => [
                    'default'     => $member->checkEducation(['formal'])->count() ? (bool) true : (bool) false,
                    'foreign'     => $member->checkEducation(['non'])->count() ? (bool) true : (bool) false,
                    'polri'       => $member->checkEducationPolri(['polri'])->count() ? (bool) true : (bool) false,
                    'dikbangspes' => $member->checkEducationPolri(['dikbangspes'])->count() ? (bool) true : (bool) false,
                ],
                'language' => [
                    'default'   => $member->checkLanguage(0)->count() ? (bool) true : (bool) false,
                    'foreign'   => $member->checkLanguage(1)->count() ? (bool) true : (bool) false,
                ],
                'sport'    => $member->checkSport()->count() ? (bool) true : (bool) false,
                'honor'    => $member->checkHonor()->count() ? (bool) true : (bool) false,
                'training' => $member->checkTraining()->count() ? (bool) true : (bool) false,
                'duty'     => $member->checkDuty()->count() ? (bool) true : (bool) false,
                'position' => $member->checkPosition()->count() ? (bool) true : (bool) false,
                'grade'    => $member->checkGrade()->count() ? (bool) true : (bool) false,
                'work'     => $member->checkWork()->count() ? (bool) true : (bool) false,
                'brivet'   => $member->checkBrivet()->count() ? (bool) true : (bool) false,
                'salary'   => $member->checkSalary()->count() ? (bool) true : (bool) false,
            ],
            'update'           => $memberUpdateManager->count($member)
        ];
    }

    /**
     * Include Partner
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includePartner(MemberModels $member)
    {
        $memberManager = new MemberManager;
        $partner       = $memberManager->getMemberRelation($member, ['wife', 'husband']);
        if(count($partner)) {
            return $this->item($partner[0], new \Sdm\Api\Transformers\MemberFamilyTransformer);
        }
    }

    /**
     * Include Childs
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeChilds(MemberModels $member)
    {
        $memberManager = new MemberManager;
        $partner       = $memberManager->getMemberRelation($member, ['child']);
        if(count($partner)) {
            return $this->collection($partner, new \Sdm\Api\Transformers\MemberFamilyTransformer);
        }
    }

    /**
     * Include Father
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeFather(MemberModels $member)
    {
        $memberManager = new MemberManager;
        $partner       = $memberManager->getMemberRelation($member, ['father']);
        if(count($partner)) {
            // return $partner[0];
            return $this->item($partner[0], new \Sdm\Api\Transformers\MemberFamilyTransformer);
        }
    }

    /**
     * Include Mother
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeMother(MemberModels $member)
    {
        $memberManager = new MemberManager;
        $partner       = $memberManager->getMemberRelation($member, ['mother']);
        if(count($partner)) {
            return $this->item($partner[0], new \Sdm\Api\Transformers\MemberFamilyTransformer);
        }
    }

    /**
     * Include Brother
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeBrother(MemberModels $member)
    {
        $memberManager = new MemberManager;
        $partner       = $memberManager->getMemberRelation($member, ['brother']);
        if(count($partner)) {
            return $this->collection($partner, new \Sdm\Api\Transformers\MemberFamilyTransformer);
        }
    }

    /**
     * Include Language
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeLanguages(MemberModels $member)
    {
        $memberManager = new MemberManager;
        $language      = $memberManager->getLanguage($member);
        if($language) {
            return $this->collection($language, new \Sdm\Api\Transformers\MemberLanguageTransformer);
        }
    }

    /**
     * Include Language Foreign
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeLanguageForeigns(MemberModels $member)
    {
        $memberManager = new MemberManager;
        $language      = $memberManager->getLanguageForeign($member);
        if($language) {
            return $this->collection($language, new \Sdm\Api\Transformers\MemberLanguageTransformer);
        }
    }

    /**
     * Include Sport
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeSports(MemberModels $member)
    {
        $memberManager = new MemberManager;
        $sport         = $memberManager->getSport($member);
        if($sport) {
            return $this->collection($sport, new \Sdm\Api\Transformers\MemberSportTransformer);
        }
    }

    /**
     * Include Education
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeEducationFormals(MemberModels $member)
    {
        $memberManager = new MemberManager;
        $education     = $memberManager->getEducationFormal($member, ['column' => 'started_at', 'value' => 'desc']);
        if($education) {
            return $this->collection($education, new \Sdm\Api\Transformers\MemberEducationTransformer);
        }
    }

    /**
     * Include Education
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeEducationNonFormals(MemberModels $member)
    {
        $memberManager = new MemberManager;
        $education     = $memberManager->getEducationNonFormal($member, ['column' => 'started_at', 'value' => 'desc']);
        if($education) {
            return $this->collection($education, new \Sdm\Api\Transformers\MemberEducationTransformer);
        }
    }

    /**
     * Include Education Polri
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeEducationPolries(MemberModels $member)
    {
        $memberManager = new MemberManager;
        $education     = $memberManager->getEducationPolries($member, ['column' => 'started_at', 'value' => 'desc']);
        if($education) {
            return $this->collection($education, new \Sdm\Api\Transformers\MemberEducationPolriTransformer);
        }
    }

    /**
     * Include Education Dikbangspes
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeEducationDikbangspes(MemberModels $member)
    {
        $memberManager = new MemberManager;
        $education     = $memberManager->getEducationDikbangspes($member, ['column' => 'started_at', 'value' => 'desc']);
        if($education) {
            return $this->collection($education, new \Sdm\Api\Transformers\MemberEducationPolriTransformer);
        }
    }

    /**
     * Include Training
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeTrainings(MemberModels $member)
    {
        $memberManager = new MemberManager;
        $training      = $memberManager->getTraining($member, ['column' => 'started_at', 'value' => 'desc']);
        if($training) {
            return $this->collection($training, new \Sdm\Api\Transformers\MemberTrainingTransformer);
        }
    }

    /**
     * Include Duties
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeDuties(MemberModels $member)
    {
        $memberManager = new MemberManager;
        $duty         = $memberManager->getDuty($member, ['column' => 'started_at', 'value' => 'desc']);
        if($duty) {
            return $this->collection($duty, new \Sdm\Api\Transformers\MemberDutyTransformer);
        }
    }

    /**
     * Include Positions
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includePositions(MemberModels $member)
    {
        $memberManager = new MemberManager;
        $position      = $memberManager->getPosition($member, ['column' => 'started_at', 'value' => 'desc']);
        if($position) {
            return $this->collection($position, new \Sdm\Api\Transformers\MemberPositionTransformer);
        }
    }

    /**
     * Include Grades
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeGrades(MemberModels $member)
    {
        $memberManager = new MemberManager;
        $grade         = $memberManager->getGrade($member, ['column' => 'started_at', 'value' => 'desc']);
        if($grade) {
            return $this->collection($grade, new \Sdm\Api\Transformers\MemberGradeTransformer);
        }
    }

    /**
     * Include Honor
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeHonors(MemberModels $member)
    {
        $memberManager = new MemberManager;
        $honor        = $memberManager->getHonor($member, ['column' => 'honored_at', 'value' => 'desc']);
        if($honor) {
            return $this->collection($honor, new \Sdm\Api\Transformers\MemberHonorTransformer);
        }
    }

    /**
     * Include Works
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeWorks(MemberModels $member)
    {
        $memberManager = new MemberManager;
        $work          = $memberManager->getWork($member, ['column' => 'started_at', 'value' => 'desc']);
        if($work) {
            return $this->collection($work, new \Sdm\Api\Transformers\MemberWorkTransformer);
        }
    }

    /**
     * Include Brivet
     *
     * @return \League\Fractal\Resource\Collection
    */
    public function includeBrivets(MemberModels $member)
    {
        $memberManager = new MemberManager;
        $brivet        = $memberManager->getBrivet($member, ['column' => 'claimed_at', 'value' => 'desc']);
        if($brivet) {
            return $this->collection($brivet, new \Sdm\Api\Transformers\MemberBrivetTransformer);
        }
    }

    public function transformWorkTime($member)
    {
        return $member->getWorkTime();
    }

    public function transformWorkGrade($member)
    {
        if($member->getCurrentGrade()) {
            $d1    = new \DateTime(date('Y-m-d'));
            $d2    = new \DateTime($member->getCurrentGrade()->started_at->format('Y-m-d'));
            $diff  = $d2->diff($d1);
            $total = [$diff->y, $diff->m];
            return [
                'date' => $member->getCurrentGrade()->started_at->format('d F Y'),
                'count'=> $total[0].' Tahun '.$total[1].' Bulan'
            ];
        }
    }

    public function transformWorkPosition($member)
    {
        if($member->getCurrentPosition()) {
            $d1    = new \DateTime(date('Y-m-d'));
            $d2    = new \DateTime($member->getCurrentPosition()->started_at->format('Y-m-d'));
            $diff  = $d2->diff($d1);
            $total = [$diff->y, $diff->m];
            return [
                'date' => $member->getCurrentPosition()->started_at->format('d F Y'),
                'count'=> $total[0].' Tahun '.$total[1].' Bulan'
            ];
        }
    }
}
