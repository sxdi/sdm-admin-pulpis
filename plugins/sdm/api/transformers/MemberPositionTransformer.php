<?php namespace Sdm\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Sdm\Member\Models\Position as PositionModels;

class MemberPositionTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(PositionModels $position)
    {
        return [
            'id'           => $position->id,
            'name'         => $position->name,
            'number'       => $position->number,
            'is_now'       => (bool) $position->is_now,
            'status'       => $position->status,
            'message'      => $position->message,
            'started_at' => [
                'ldFY'   => $position->started_at ? $position->started_at->format('l, d F Y') : '',
                'dFY'    => $position->started_at ? $position->started_at->format('d F Y') : '',
                'ymd'    => $position->started_at ? $position->started_at->format('Y-m-d') : '',
                'l'      => $position->started_at ? $position->started_at->format('l') : '',
                'd'      => $position->started_at ? $position->started_at->format('d') : '',
                'm'      => $position->started_at ? $position->started_at->format('m') : '',
                'F'      => $position->started_at ? $position->started_at->format('F') : '',
                'Y'      => $position->started_at ? $position->started_at->format('Y') : '',
            ],
            'ended_at' => [
                'ldFY'   => $position->ended_at ? $position->ended_at->format('l, d F Y') : '',
                'dFY'    => $position->ended_at ? $position->ended_at->format('d F Y') : '',
                'ymd'    => $position->ended_at ? $position->ended_at->format('Y-m-d') : '',
                'l'      => $position->ended_at ? $position->ended_at->format('l') : '',
                'd'      => $position->ended_at ? $position->ended_at->format('d') : '',
                'm'      => $position->ended_at ? $position->ended_at->format('m') : '',
                'F'      => $position->ended_at ? $position->ended_at->format('F') : '',
                'Y'      => $position->ended_at ? $position->ended_at->format('Y') : '',
            ],
            'document' => [
                'path' => $position->document ? $position->document->path : (bool) false,
                'name' => $position->document ? $position->document->file_name : (bool) false,
            ],
            'department'   => [
                'id'       => $position->department->id,
                'name'     => $position->department->name
            ],
            'unit'        => [
                'id'        => $position->department->unit->id,
                'name'      => $position->department->unit->name,
                'is_system' => (bool) $position->department->unit->is_system
            ],
        ];
    }
}
