<?php namespace Sdm\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Sdm\Member\Models\Work as WorkModels;

class MemberWorkTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(WorkModels $work)
    {
        return [
            'id'       => $work->id,
            'number'   => $work->number,
            'name'     => $work->name,
            'location' => $work->location,
            'is_now'   => (bool) $work->is_now,
            'status'   => $work->status,
            'message'  => $work->message,
            'started_at' => [
                'ldFY'   => $work->started_at ? $work->started_at->format('l, d F Y') : '',
                'dFY'    => $work->started_at ? $work->started_at->format('d F Y') : '',
                'ymd'    => $work->started_at ? $work->started_at->format('Y-m-d') : '',
                'l'      => $work->started_at ? $work->started_at->format('l') : '',
                'd'      => $work->started_at ? $work->started_at->format('d') : '',
                'm'      => $work->started_at ? $work->started_at->format('m') : '',
                'F'      => $work->started_at ? $work->started_at->format('F') : '',
                'Y'      => $work->started_at ? $work->started_at->format('Y') : '',
            ],
            'ended_at' => [
                'ldFY'   => $work->ended_at ? $work->ended_at->format('l, d F Y') : '',
                'dFY'    => $work->ended_at ? $work->ended_at->format('d F Y') : '',
                'ymd'    => $work->ended_at ? $work->ended_at->format('Y-m-d') : '',
                'l'      => $work->ended_at ? $work->ended_at->format('l') : '',
                'd'      => $work->ended_at ? $work->ended_at->format('d') : '',
                'm'      => $work->ended_at ? $work->ended_at->format('m') : '',
                'F'      => $work->ended_at ? $work->ended_at->format('F') : '',
                'Y'      => $work->ended_at ? $work->ended_at->format('Y') : '',
            ],
            'document' => [
                'path' => $work->document ? $work->document->path : (bool) false,
                'name' => $work->document ? $work->document->file_name : (bool) false,
            ]
        ];
    }
}
