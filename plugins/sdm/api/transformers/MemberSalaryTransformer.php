<?php namespace Sdm\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Sdm\Member\Models\Salary as SalaryModels;

class MemberSalaryTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(SalaryModels $salary)
    {
        return [
            'id'      => $salary->id,
            'status'  => $salary->status,
            'message' => $salary->message,
            'value'   => [
                'text'  => number_format($salary->value),
                'value' => (int) $salary->value,
            ],
            'grade'   => [
                'id'   => $salary->grade->id,
                'name' => $salary->grade->parent->name,
                'code' => $salary->grade->parent->code
            ],
            'started_at' => [
                'ldFY'   => $salary->started_at ? $salary->started_at->format('l, d F Y') : '',
                'dFY'    => $salary->started_at ? $salary->started_at->format('d F Y') : '',
                'ymd'    => $salary->started_at ? $salary->started_at->format('Y-m-d') : '',
                'l'      => $salary->started_at ? $salary->started_at->format('l') : '',
                'd'      => $salary->started_at ? $salary->started_at->format('d') : '',
                'm'      => $salary->started_at ? $salary->started_at->format('m') : '',
                'F'      => $salary->started_at ? $salary->started_at->format('F') : '',
                'Y'      => $salary->started_at ? $salary->started_at->format('Y') : '',
            ],
            'ended_at' => [
                'ldFY'   => $salary->ended_at ? $salary->ended_at->format('l, d F Y') : '',
                'dFY'    => $salary->ended_at ? $salary->ended_at->format('d F Y') : '',
                'ymd'    => $salary->ended_at ? $salary->ended_at->format('Y-m-d') : '',
                'l'      => $salary->ended_at ? $salary->ended_at->format('l') : '',
                'd'      => $salary->ended_at ? $salary->ended_at->format('d') : '',
                'm'      => $salary->ended_at ? $salary->ended_at->format('m') : '',
                'F'      => $salary->ended_at ? $salary->ended_at->format('F') : '',
                'Y'      => $salary->ended_at ? $salary->ended_at->format('Y') : '',
            ],
            'document' => [
                'path' => $salary->document ? $salary->document->path : (bool) false,
                'name' => $salary->document ? $salary->document->file_name : (bool) false,
            ]
        ];
    }
}
