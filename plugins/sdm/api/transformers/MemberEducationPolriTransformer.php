<?php namespace Sdm\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Sdm\Member\Models\EducationPolice as EducationPolriModels;

class MemberEducationPolriTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(EducationPolriModels $education)
    {
        return [
            'id'           => $education->id,
            'name'         => $education->name,
            'type'         => $this->transformType($education->type),
            'location'     => $education->location,
            'result'       => $education->result,
            'is_now'       => (bool) $education->is_now,
            'status'       => $education->status,
            'message'      => $education->message,
            'started_at' => [
                'ldFY'   => $education->started_at ? $education->started_at->format('l, d F Y') : '',
                'dFY'    => $education->started_at ? $education->started_at->format('d F Y') : '',
                'ymd'    => $education->started_at ? $education->started_at->format('Y-m-d') : '',
                'l'      => $education->started_at ? $education->started_at->format('l') : '',
                'd'      => $education->started_at ? $education->started_at->format('d') : '',
                'm'      => $education->started_at ? $education->started_at->format('m') : '',
                'F'      => $education->started_at ? $education->started_at->format('F') : '',
                'Y'      => $education->started_at ? $education->started_at->format('Y') : '',
            ],
            'ended_at' => [
                'ldFY'   => $education->ended_at ? $education->ended_at->format('l, d F Y') : '',
                'dFY'    => $education->ended_at ? $education->ended_at->format('d F Y') : '',
                'ymd'    => $education->ended_at ? $education->ended_at->format('Y-m-d') : '',
                'l'      => $education->ended_at ? $education->ended_at->format('l') : '',
                'd'      => $education->ended_at ? $education->ended_at->format('d') : '',
                'm'      => $education->ended_at ? $education->ended_at->format('m') : '',
                'F'      => $education->ended_at ? $education->ended_at->format('F') : '',
                'Y'      => $education->ended_at ? $education->ended_at->format('Y') : '',
            ],
            'document' => [
                'path' => $education->document ? $education->document->path : (bool) false,
                'name' => $education->document ? $education->document->file_name : (bool) false,
            ]
        ];
    }

    public function transformEducation($education)
    {
        return [
            'name'  => $education->name,
            'value' => $education->id
        ];
    }

    public function transformType($type)
    {
        if($type == 'polri') {
            return [
                'name'  => 'Polri',
                'value' => 'polri'
            ];
        }

        if($type == 'dikbangspes') {
            return [
                'name'  => 'DIKBANGSPES',
                'value' => 'dikbangspes'
            ];
        }
    }
}
