<?php namespace Sdm\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Sdm\Member\Models\Education as EducationModels;

class MemberEducationTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(EducationModels $education)
    {
        return [
            'id'           => $education->id,
            'name'         => $education->name,
            'education'    => $education->education ? $this->transformEducation($education->education) : (bool) false,
            'type'         => $this->transformType($education->type),
            'study'        => $education->study,
            'location'     => $education->location,
            'is_graduated' => $education->is_graduated,
            'is_now'       => (bool) $education->is_now,
            'status'       => $education->status,
            'message'      => $education->message,
            'started_at' => [
                'ldFY'   => $education->started_at ? $education->started_at->format('l, d F Y') : '',
                'dFY'    => $education->started_at ? $education->started_at->format('d F Y') : '',
                'ymd'    => $education->started_at ? $education->started_at->format('Y-m-d') : '',
                'l'      => $education->started_at ? $education->started_at->format('l') : '',
                'd'      => $education->started_at ? $education->started_at->format('d') : '',
                'm'      => $education->started_at ? $education->started_at->format('m') : '',
                'F'      => $education->started_at ? $education->started_at->format('F') : '',
                'Y'      => $education->started_at ? $education->started_at->format('Y') : '',
            ],
            'ended_at' => [
                'ldFY'   => $education->ended_at ? $education->ended_at->format('l, d F Y') : '',
                'dFY'    => $education->ended_at ? $education->ended_at->format('d F Y') : '',
                'ymd'    => $education->ended_at ? $education->ended_at->format('Y-m-d') : '',
                'l'      => $education->ended_at ? $education->ended_at->format('l') : '',
                'd'      => $education->ended_at ? $education->ended_at->format('d') : '',
                'm'      => $education->ended_at ? $education->ended_at->format('m') : '',
                'F'      => $education->ended_at ? $education->ended_at->format('F') : '',
                'Y'      => $education->ended_at ? $education->ended_at->format('Y') : '',
            ],
            'document' => [
                'path' => $education->document ? $education->document->path : (bool) false,
                'name' => $education->document ? $education->document->file_name : (bool) false,
            ]
        ];
    }

    public function transformEducation($education)
    {
        return [
            'name'  => $education->name,
            'code'  => $education->code,
            'value' => (string)$education->id
        ];
    }

    public function transformType($type)
    {
        if($type == 'formal') {
            return [
                'name'  => 'Formal',
                'value' => 'formal'
            ];
        }

        if($type == 'non') {
            return [
                'name'  => 'Non Formal',
                'value' => 'non'
            ];
        }
    }
}
