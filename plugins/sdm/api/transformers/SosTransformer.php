<?php namespace Sdm\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Sdm\Sos\Models\Sos as SosModels;


class SosTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'member',
    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(SosModels $sos)
    {
        return [
            'id'         => $sos->id,
            'latitude'   => (float) $sos->latitude,
            'longitude'  => (float) $sos->longitude,
            'created_at' => [
                'complete' => $sos->created_at ? $sos->created_at->format('l, d F Y H:i') : '',
                'ldFY'     => $sos->created_at ? $sos->created_at->format('l, d F Y') : '',
                'dFY'      => $sos->created_at ? $sos->created_at->format('d F Y') : '',
                'ymd'      => $sos->created_at ? $sos->created_at->format('Y-m-d') : '',
                'l'        => $sos->created_at ? $sos->created_at->format('l') : '',
                'd'        => $sos->created_at ? $sos->created_at->format('d') : '',
                'm'        => $sos->created_at ? $sos->created_at->format('m') : '',
                'F'        => $sos->created_at ? $sos->created_at->format('F') : '',
                'Y'        => $sos->created_at ? $sos->created_at->format('Y') : '',
                'human'    => $sos->created_at ? $sos->created_at->diffForHumans() : '',
            ]
        ];
    }

    /**
     * Include Member
     *
     * @return \League\Fractal\Resource\Item
    */
    public function includeMember(SosModels $sos)
    {
        return $this->item($sos->member, new \Sdm\Api\Transformers\MemberSimpleTransformer);
    }
}
