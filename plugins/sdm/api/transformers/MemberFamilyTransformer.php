<?php namespace Sdm\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Sdm\Member\Models\Family as FamilyModels;

class MemberFamilyTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(FamilyModels $family)
    {
        return [
            'id'             => $family->id,
            'name'           => $family->name,
            'gender'         => $family->gender,
            'job'            => $family->job,
            'place'          => $family->place,
            'education'      => $family->education,
            'content'        => $family->content,
            'relation'       => $this->transformRelation($family->relation),
            'place_relation' => $family->relation_place ?: '',
            'place_date'     => $family->relation_date ? $this->transformDob($family->relation_date) : '',
            'dob'            => $this->transformDob($family->dob)
        ];
    }

    public function transformRelation($relation)
    {
        if($relation == 'father') {
            $relationName  = 'Ayah';
            $relationValue = 'father';
        }
        if($relation == 'mother') {
            $relationName  = 'Ibu';
            $relationValue = 'mother';
        }
        if($relation == 'wife') {
            $relationName  = 'Istri';
            $relationValue = 'wife';
        }
        if($relation == 'husband') {
            $relationName  = 'Suami';
            $relationValue = 'husband';
        }
        if($relation == 'child') {
            $relationName  = 'Anak Kandung';
            $relationValue = 'child';
        }
        if($relation == 'brother') {
            $relationName  = 'Saudara';
            $relationValue = 'borther';
        }

        return [
            'name'  => $relationName,
            'value' => $relationValue
        ];
    }

    public function transformDob($date)
    {
        return [
            'ldFY' => $date ? $date->format('l, d F Y') : '',
            'dFY'  => $date ? $date->format('d F Y') : '',
            'ymd'  => $date ? $date->format('Y-m-d') : '',
            'l'    => $date ? $date->format('l') : '',
            'd'    => $date ? $date->format('d') : '',
            'm'    => $date ? $date->format('m') : '',
            'F'    => $date ? $date->format('F') : '',
            'Y'    => $date ? $date->format('Y') : '',
            'age'  => $date ? (int) \Carbon\Carbon::parse($date)->age : ''
        ];
    }
}
