<?php namespace Sdm\Api\Transformers;

use League\Fractal\TransformerAbstract;

use Sdm\Member\Models\Law as LawModels;

class LawTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(LawModels $law)
    {
        $memberTransformer = new \Sdm\Api\Transformers\MemberSimpleTransformer;
        return [
            'id'      => $law->id,
            'title'   => $law->title,
            'content' => $law->content,
            'member'  => $memberTransformer->transform($law->member),
            'admin'   => $memberTransformer->transform($law->admin),
            'date'    => [
                'ldFY'   => $law->date ? $law->date->format('l, d F Y') : '',
                'dFY'    => $law->date ? $law->date->format('d F Y') : '',
                'ymd'    => $law->date ? $law->date->format('Y-m-d') : '',
                'l'      => $law->date ? $law->date->format('l') : '',
                'd'      => $law->date ? $law->date->format('d') : '',
                'm'      => $law->date ? $law->date->format('m') : '',
                'F'      => $law->date ? $law->date->format('F') : '',
                'Y'      => $law->date ? $law->date->format('Y') : '',
            ],
            'file'    => [
                'path' => $law->document ? $law->document->path : (bool) false,
                'name' => $law->document ? $law->document->file_name : (bool) false,
            ],
            'tags'    => $this->renderTags($law)
        ];
    }

    public function renderTags($law)
    {
        $str  = '';
        if(count($law->tags)) {
            $tags = $law->tags;
            foreach ($tags as $key => $tag) {
                $str .= $tag->name;
                if($key + 1 != count($tags)) {
                    $str .= ',';
                }
            }
        }

        return [
            'db'  => $law->tags,
            'str' => $str ? $str : (bool) false
        ];
    }
}
