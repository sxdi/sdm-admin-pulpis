<?php

Route::group([
    'prefix'     => 'api/v1',
    'namespace'  => 'Sdm\Api\Controllers',
], function() {

    // Version
    Route::get('/android', function(){
        return response()->json([
            'result' => true,
            'version'=> (int) env('APP_MEMBER'),
            'admin'  => (int) env('APP_ADMIN')
        ]);
    });

    // Sos
    Route::group(['prefix' => 'sos'], function () {
        Route::get('/', 'Sos@all');
        Route::get('/detail/{id}', 'Sos@get');
        Route::get('/active', 'Sos@active');
        Route::post('/create', 'Sos@create');
        Route::post('/stop', 'Sos@stop');
    });

    // Admin
    Route::group(['prefix' => 'admin'], function () {
        Route::get('dashboard', 'Admin@index');
    });

    // Auth
    Route::group(['prefix' => 'auth'], function () {
        Route::post('login', 'Auth@login');
    });

    // Member
    Route::group(['prefix' => 'member'], function () {
        Route::get('/', 'Member@index');
        Route::get('/detail/{id}', 'Member@detail');
        Route::get('print', 'Member@print');
        Route::get('find', 'Member@find');
        Route::post('search', 'Member@search');
        Route::post('download', 'Member@download');
        Route::get('report/{id}', 'Member@report');

        Route::group(['prefix' => 'update'], function () {
            Route::post('/', 'Member@updateStatus');

            Route::get('/', 'Member@update');
            Route::get('/detail/{id}', 'Member@updateDetail');
        });
    });

    // Urkes
    Route::group(['prefix' => 'health'], function () {
        Route::get('/', 'Health@index');
        Route::get('dashboard', 'Health@dashboard');
        Route::get('view/{id}', 'Health@detail');
        Route::get('delete/{id}', 'Health@delete');
        Route::get('report', 'Health@report');
        Route::post('save', 'Health@save');

        Route::group(['prefix' => 'consultation'], function () {
            Route::get('/', 'HealthConsultation@active');
            Route::get('history', 'HealthConsultation@history');
            Route::get('detail/{id}', 'HealthConsultation@detail');
            Route::post('create', 'HealthConsultation@create');
            Route::post('tag', 'HealthConsultation@tag');
            Route::post('new', 'HealthConsultation@new');
            Route::post('close', 'HealthConsultation@close');
        });
    });

    // Urkum
    Route::group(['prefix' => 'law'], function () {
        Route::get('/', 'Law@index');
        Route::get('dashboard', 'Law@dashboard');
        Route::get('view/{id}', 'Law@detail');
        Route::get('delete/{id}', 'Law@delete');
        Route::get('report', 'Law@report');
        Route::post('save', 'Law@save');

        Route::group(['prefix' => 'consultation'], function () {
            Route::get('/', 'LawConsultation@active');
            Route::get('history', 'LawConsultation@history');
            Route::get('detail/{id}', 'LawConsultation@detail');
            Route::post('create', 'LawConsultation@create');
            Route::post('tag', 'LawConsultation@tag');
            Route::post('new', 'LawConsultation@new');
            Route::post('close', 'LawConsultation@close');
        });
    });

    // Letter
    Route::group(['prefix' => 'letter'], function () {
        Route::get('/', function(){
            $letters    = \Sdm\Letter\Models\Letter::whereIsSingle(0)->get();
            $letterData = [];
            foreach ($letters as $key => $g) {
                $letterData[$key] = [
                    'name'  => $g->name,
                    'value' => $g->code
                ];
            }
            return response()->json([
                'result'   => true,
                'response' => [
                    'data' => $letterData
                ]
            ]);
        });

        Route::group(['prefix' => 'brief'], function () {
            Route::get('/', 'Letter@brief');
            Route::get('detail/{id}', 'Letter@briefDetail');
        });

        Route::group(['prefix' => 'generate'], function () {
            Route::post('zip', 'Letter@generateZip');
        });
    });

    // Grade
    Route::group(['prefix' => 'grade'], function () {
        Route::get('/', function(){
            $grades    = \Sdm\Master\Models\Grade::get();
            $gradeData = [];
            foreach ($grades as $key => $g) {
                $gradeData[$key] = [
                    'id'    => $g->id,
                    'name'  => $g->name,
                    'code'  => strtoupper($g->code),
                    'value' => $g->grade
                ];
            }
            return response()->json([
                'result'   => true,
                'response' => [
                    'data' => $gradeData
                ]
            ]);
        });
    });

    // Education
    Route::group(['prefix' => 'education'], function () {
        Route::get('/', function(){
            $educations = \Sdm\Master\Models\Education::get();
            return response()->json([
                'result'   => true,
                'response' => [
                    'data' => $educations
                ]
            ]);
        });
    });

    // Position
    Route::group(['prefix' => 'position'], function () {
        Route::get('/picker', function(){
            $positions = \Sdm\Master\Models\Position::whereNull('parent_id')->get();
            $children  = [];
            foreach ($positions as $keyParent => $position) {
                $children[$keyParent] = $position;
                foreach ($position->childs as $key => $pos) {
                    // $children[$keyParent][$key] = $pos;
                }
            }
            return response()->json([
                'result'   => true,
                'response' => [
                    'data'     => $children,
                ]
            ]);
        });

        Route::get('/unit', function(){
            $units        = \Sdm\Master\Models\Unit::whereIsSystem(1)->orderBy('sort', 'asc')->get();
            $inUnits      = [];
            foreach ($units as $unit) {
                array_push($inUnits, [
                    'id'    => (int) $unit->id,
                    'name'  => $unit->name
                ]);
            }
            array_push($inUnits, [
                'id'    =>  (string) '99',
                'name'  => 'LAINNYA'
            ]);
            return response()->json([
                'result'   => true,
                'response' => [
                    'data'   => $inUnits,
                ],
            ]);
        });

        Route::get('/department/{id}', function($id){
            $departments   = \Sdm\Master\Models\Department::whereUnitId($id)->orderBy('sort', 'asc')->get();
            $inDepartments = [];
            foreach ($departments as $departments) {
                array_push($inDepartments, [
                    'id'    => (int) $departments->id,
                    'name'  => $departments->name
                ]);
            }
            return response()->json([
                'result'   => true,
                'response' => [
                    'data'   => $inDepartments,
                ],
            ]);
        });
    });

    // User
    Route::group(['prefix' => 'user'], function () {
        Route::get('/', 'User@get');
        Route::post('/update-picture', 'User@updatePicture');
        Route::post('/update-profile', 'User@updateProfile');
        Route::post('/update-password', 'User@updatePassword');
        Route::post('/update-ktp', 'User@updateKtp');
        Route::post('/update-npwp', 'User@updateNpwp');
        Route::post('/update-kk', 'User@updateKk');
        Route::post('/update-finger', 'User@updateFinger');
        Route::post('/update-asabri', 'User@updateAsabri');
        Route::post('/update-bpjs', 'User@updateBpjs');
        Route::post('/update-authority', 'User@updateAuthority');
        Route::post('/update-investigator', 'User@updateInvestigator');
        Route::post('/update-member', 'User@updateMember');
        Route::post('/update-kps', 'User@updateKps');
        Route::post('/update-simsa', 'User@updatesimsa');

        Route::group(['prefix' => 'parent'], function () {
            Route::get('/', 'User@getParent');
            Route::get('/view/{id}', 'User@getParentDetail');
            Route::post('/save', 'User@saveParent');
            Route::get('/delete/{id}', 'User@deleteParent');
        });

        Route::group(['prefix' => 'partner'], function () {
            Route::get('/', 'User@getPartner');
            Route::post('/save', 'User@savePartner');
        });

        Route::group(['prefix' => 'family'], function () {
            Route::get('/', 'User@getFamily');
            Route::post('/save', 'User@saveParent');
            Route::get('/delete/{id}', 'User@deleteParent');
        });

        Route::group(['prefix' => 'residence'], function () {
            Route::get('/', 'User@getResidence');
            Route::get('/view/{id}', 'User@getResidenceDetail');
            Route::post('/save', 'User@saveResidence');
            Route::get('/delete/{id}', 'User@deleteResidence');
        });

        Route::group(['prefix' => 'language'], function () {
            Route::get('/', 'User@getLanguage');
            Route::post('/save', 'User@saveLanguage');
            Route::get('/view/{id}', 'User@getLanguageDetail');
            Route::get('/delete/{id}', 'User@deleteLanguage');
        });

        Route::group(['prefix' => 'language-foreign'], function () {
            Route::get('/', 'User@getLanguageForeign');
            Route::post('/save', 'User@saveLanguage');
            Route::get('/view/{id}', 'User@getLanguageDetail');
            Route::get('/delete/{id}', 'User@deleteLanguage');
        });

        Route::group(['prefix' => 'sport'], function () {
            Route::get('/', 'User@getSport');
            Route::get('/view/{id}', 'User@getSportDetail');
            Route::post('/save', 'User@saveSport');
            Route::get('/delete/{id}', 'User@deleteSport');
        });

        Route::group(['prefix' => 'honor'], function () {
            Route::get('/', 'User@getHonor');
            Route::get('/view/{id}', 'User@getHonorDetail');
            Route::post('/save', 'User@saveHonor');
            Route::get('/delete/{id}', 'User@deleteHonor');
        });

        Route::group(['prefix' => 'education'], function () {
            Route::get('/', 'User@getEducation');
            Route::get('/view/{id}', 'User@getEducationDetail');
            Route::post('/save', 'User@saveEducation');
            Route::get('/delete/{id}', 'User@deleteEducation');
        });

        Route::group(['prefix' => 'education-non'], function () {
            Route::get('/', 'User@getEducationNon');
            Route::get('/view/{id}', 'User@getEducationDetail');
            Route::post('/save', 'User@saveEducationNon');
            Route::get('/delete/{id}', 'User@deleteEducation');
        });

        Route::group(['prefix' => 'education-polri'], function () {
            Route::get('/', 'User@getEducationPolri');
            Route::get('/view/{id}', 'User@getEducationPolriDetail');
            Route::post('/save', 'User@saveEducationPolri');
            Route::get('/delete/{id}', 'User@deleteEducationPolri');
        });

        Route::group(['prefix' => 'education-dikbangspes'], function () {
            Route::get('/', 'User@getEducationDikbangspes');
            Route::get('/view/{id}', 'User@getEducationPolriDetail');
            Route::post('/save', 'User@saveEducationPolri');
            Route::get('/delete/{id}', 'User@deleteEducationPolri');
        });

        Route::group(['prefix' => 'training'], function () {
            Route::get('/', 'User@getTraining');
            Route::get('/view/{id}', 'User@getTrainingDetail');
            Route::post('/save', 'User@saveTraining');
            Route::get('/delete/{id}', 'User@deleteTraining');
        });

        Route::group(['prefix' => 'duty'], function () {
            Route::get('/', 'User@getDuty');
            Route::get('/view/{id}', 'User@getDutyDetail');
            Route::post('/save', 'User@saveDuty');
            Route::get('/delete/{id}', 'User@deleteDuty');
        });

        Route::group(['prefix' => 'position'], function () {
            Route::get('/', 'User@getPosition');
            Route::get('/view/{id}', 'User@getPositionDetail');
            Route::post('/save', 'User@savePosition');
            Route::get('/delete/{id}', 'User@deletePosition');
        });

        Route::group(['prefix' => 'grade'], function () {
            Route::get('/', 'User@getGrade');
            Route::get('/view/{id}', 'User@getGradeDetail');
            Route::post('/save', 'User@saveGrade');
            Route::get('/delete/{id}', 'User@deleteGrade');
        });

        Route::group(['prefix' => 'work'], function () {
            Route::get('/', 'User@getWork');
            Route::get('/view/{id}', 'User@getWorkDetail');
            Route::post('/save', 'User@saveWork');
            Route::get('/delete/{id}', 'User@deleteWork');
        });

        Route::group(['prefix' => 'brivet'], function () {
            Route::get('/', 'User@getBrivet');
            Route::get('/view/{id}', 'User@getBrivetDetail');
            Route::post('/save', 'User@saveBrivet');
            Route::get('/delete/{id}', 'User@deleteBrivet');
        });

        Route::group(['prefix' => 'salary'], function () {
            Route::get('/', 'User@getSalary');
            Route::get('/view/{id}', 'User@getSalaryDetail');
            Route::post('/save', 'User@saveSalary');
            Route::get('/delete/{id}', 'User@deleteSalary');
        });

        Route::group(['prefix' => 'health'], function () {
            Route::get('/', 'Health@thread');
        });

        Route::group(['prefix' => 'law'], function () {
            Route::get('/', 'Law@thread');
        });
    });
});
