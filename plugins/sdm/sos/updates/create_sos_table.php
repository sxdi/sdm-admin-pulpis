<?php namespace Sdm\Sos\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSosTable extends Migration
{
    public function up()
    {
        Schema::create('sdm_sos_sos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('member_id');
            $table->text('latitute');
            $table->text('longitude');
            $table->timestamps();
            $table->boolean('is_close')->nullable()->default(0);
        });
    }

    public function down()
    {
        Schema::dropIfExists('sdm_sos_sos');
    }
}
