<?php namespace Sdm\Health\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateConsultationTagsTable extends Migration
{
    public function up()
    {
        Schema::create('sdm_health_consultation_tags', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('consultation_id');
            $table->integer('tag_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sdm_health_consultation_tags');
    }
}
