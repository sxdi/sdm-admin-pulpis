<?php namespace Sdm\Health\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateConsultationDetailsTable extends Migration
{
    public function up()
    {
        Schema::create('sdm_health_consultation_details', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('consultation_id');
            $table->integer('user_id');
            $table->text('message')->nullable();
            $table->timestamps();
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('sdm_health_consultation_details');
    }
}
