<?php namespace Sdm\Health\Models;

use Model;
use Sdm\Core\Classes\Generator;

/**
 * Consultation Model
 */
class Consultation extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'sdm_health_consultations';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'user_id',
        'title',
        'is_close'
    ];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [
        'details' => [
            'Sdm\Health\Models\ConsultationDetail',
            'key'       => 'consultation_id',
            'otherKey'  => 'id'
        ]
    ];
    public $belongsTo     = [];
    public $belongsToMany = [
        'tags' => [
            'Sdm\Health\Models\Tag',
            'table'    => 'sdm_health_consultation_tags',
            'key'      => 'consultation_id',
            'otherKey' => 'tag_id'
        ]
    ];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [];
    public $attachMany    = [];

    public function beforeCreate()
    {
        $generator          = new Generator;
        $this->parameter    = $generator->make();
    }
}
