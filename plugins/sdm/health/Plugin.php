<?php namespace Sdm\Health;

use Backend;
use System\Classes\PluginBase;

/**
 * Health Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Health',
            'description' => 'No description provided yet...',
            'author'      => 'Sdm',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Sdm\Health\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'sdm.health.some_permission' => [
                'tab' => 'Health',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'health' => [
                'label'       => 'Health',
                'url'         => Backend::url('sdm/health/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['sdm.health.*'],
                'order'       => 500,
            ],
        ];
    }
}
