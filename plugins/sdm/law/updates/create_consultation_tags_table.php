<?php namespace Sdm\Law\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateConsultationTagsTable extends Migration
{
    public function up()
    {
        Schema::create('sdm_law_consultation_tags', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('consultation_id');
            $table->integer('tag_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('sdm_law_consultation_tags');
    }
}
