<?php namespace Sdm\Law\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateConsultationsTable extends Migration
{
    public function up()
    {
        Schema::create('sdm_law_consultations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->string('title');
            $table->timestamps();
            $table->boolean('is_closed')->nullable()->default(0);
            $table->string('parameter', 32);
        });
    }

    public function down()
    {
        Schema::dropIfExists('sdm_law_consultations');
    }
}
