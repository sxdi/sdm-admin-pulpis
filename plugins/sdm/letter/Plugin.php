<?php namespace Sdm\Letter;

use Backend;
use System\Classes\PluginBase;

/**
 * Letter Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Letter',
            'description' => 'No description provided yet...',
            'author'      => 'Sdm',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Sdm\Letter\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'sdm.letter.some_permission' => [
                'tab' => 'Letter',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'letter' => [
                'label'       => 'Letter',
                'url'         => Backend::url('sdm/letter/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['sdm.letter.*'],
                'order'       => 500,
            ],
        ];
    }
}
