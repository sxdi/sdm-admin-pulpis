<?php namespace Sdm\Letter\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateGeneratorsTable extends Migration
{
    public function up()
    {
        Schema::create('sdm_letter_generators', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('member_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sdm_letter_generators');
    }
}
