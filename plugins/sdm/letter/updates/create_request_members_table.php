<?php namespace Sdm\Letter\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRequestMembersTable extends Migration
{
    public function up()
    {
        Schema::create('sdm_letter_request_members', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('request_id');
            $table->integer('member_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sdm_letter_request_members');
    }
}
