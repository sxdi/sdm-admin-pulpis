<?php namespace Sdm\Core\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class BuildFile extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'sdm:build-file';

    /**
     * @var string The console command description.
     */
    protected $description = 'Does something cool.';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $t = storage_path('temporary/');
        if(!is_dir($t)) {
            mkdir($t);
        }

        $helper    = new \Sdm\Core\Classes\Generator;
        $generator = \Sdm\Letter\Models\Generator::whereIsFinish(0)->whereIsProcess(0)->first();

        if($generator) {
            $generator->is_process = 1;
            $generator->save();

            $filename = new \Sdm\Core\Classes\Generator;
            $zipName  = $filename->make();
            $zip      = \ZanySoft\Zip\Zip::create(storage_path('app/media/zip/'.$zipName.'.zip'));

            $data = [
                'letters' => \Sdm\Letter\Models\Request::whereGeneratorId($generator->id)->get(),
                'members' => \Sdm\Letter\Models\RequestMember::whereGeneratorId($generator->id)->get()
            ];

            foreach ($data['letters'] as $letter) {
                $letter = $letter->letter;
                $data   = [
                    'path'    => $letter->document->path,
                    'members' => $data['members'],
                ];

                /**
                 * KPI/KPS/KARSU
                 * 1) Formulir
                */
                if($letter->code == 'kps_form') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;

                    foreach ($data['members'] as $key => $member) {
                        $member = $member->member;
                        $tmp = storage_path('temporary/'.str_slug($member->name).'/');
                        if(!is_dir($tmp)) {
                            mkdir($tmp);
                        }

                        $zipPath = $letterManager->processPersonel([
                            'path'         => $data['path'],
                            'members'      => $member,
                            'temp'         => $tmp,
                            'letter'       => $letter,
                            'kps_form'     => true
                        ]);
                    }
                }

                /**
                 * Kenaikan Gaji Berkala
                 * @var [type]
                 *  1) Surat Keputusan
                 *  2) Surat Petikan
                 *  3) Lampiran
                */
                if($letter->code == 'kgb_decide') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;

                    $tmp = storage_path('temporary/');
                    if(!is_dir($tmp)) {
                        mkdir($tmp);
                    }

                    $zipPath = $letterManager->processPersonel([
                        'path'         => $data['path'],
                        'members'      => $member[0],
                        'temp'         => $tmp,
                        'letter'       => $letter,
                    ]);
                }
                if($letter->code == 'kgb_quote') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;

                    foreach ($data['members'] as $key => $member) {
                        $member = $member->member;
                        $tmp = storage_path('temporary/'.str_slug($member->name).'/');
                        if(!is_dir($tmp)) {
                            mkdir($tmp);
                        }

                        $zipPath = $letterManager->processPersonel([
                            'path'         => $data['path'],
                            'members'      => $member,
                            'temp'         => $tmp,
                            'letter'       => $letter,
                        ]);
                    }
                }
                if($letter->code == 'kgb_attachment') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;
                    $tmp = storage_path('temporary/');
                    if(!is_dir($tmp)) {
                        mkdir($tmp);
                    }

                    $letterManager->processKgbAttachment([
                        'path'           => $letter->document->getLocalPath(),
                        'members'        => $data['members'],
                        'temp'           => $tmp,
                        'letter'         => $letter,
                    ]);
                }

                /**
                 * BP4R
                 * @var [type]
                 *  1) Formulir Cerai
                 *  2) Formulir Kawin
                */
                if($letter->code == 'bp4r_divorce') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;

                    foreach ($data['members'] as $key => $member) {
                        $member = $member->member;
                        $tmp = storage_path('temporary/'.str_slug($member->name).'/');
                        if(!is_dir($tmp)) {
                            mkdir($tmp);
                        }

                        $zipPath = $letterManager->processPersonel([
                            'path'         => $data['path'],
                            'members'      => $member,
                            'temp'         => $tmp,
                            'letter'       => $letter,
                            'bp4r_divorce' => true
                        ]);
                    }
                }
                if($letter->code == 'bp4r_marriage') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;

                    foreach ($data['members'] as $key => $member) {
                        $member = $member->member;
                        $tmp = storage_path('temporary/'.str_slug($member->name).'/');
                        if(!is_dir($tmp)) {
                            mkdir($tmp);
                        }

                        $zipPath = $letterManager->processPersonel([
                            'path'         => $data['path'],
                            'members'      => $member,
                            'temp'         => $tmp,
                            'letter'       => $letter,
                            'bp4r_marriage'=> true
                        ]);
                    }
                }

                /**
                 * Pemegang Senjata Api
                 * @var [type]
                 *  1) Surat Permohonan
                 *  2) Surat LMPA
                 *  3)
                */
                if($letter->code == 'senpi_form') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;

                    foreach ($data['members'] as $key => $member) {
                        $member = $member->member;
                        $tmp = storage_path('temporary/'.str_slug($member->name).'/');
                        if(!is_dir($tmp)) {
                            mkdir($tmp);
                        }

                        $zipPath = $letterManager->processPersonel([
                            'path'    => $data['path'],
                            'members' => $member,
                            'temp'    => $tmp,
                            'letter'  => $letter
                        ]);
                    }
                }
                if($letter->code == 'senpi_lmpa') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;

                    foreach ($data['members'] as $key => $member) {
                        $member = $member->member;
                        $tmp = storage_path('temporary/'.str_slug($member->name).'/');
                        if(!is_dir($tmp)) {
                            mkdir($tmp);
                        }

                        $zipPath = $letterManager->processPersonel([
                            'path'    => $data['path'],
                            'members' => $member,
                            'temp'    => $tmp,
                            'letter'  => $letter
                        ]);
                    }
                }

                /**
                 * Riwayat Hidup
                 * @var [type]
                 * 1) Riwayat Hidup Singkat
                 * 2) Riwayat Hidup Lengkap
                 * 3) Riwayat Hidup Assesmen
                */
                if($letter->code == 'rh_simple') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;

                    foreach ($data['members'] as $key => $member) {
                        $member = $member->member;
                        $tmp = storage_path('temporary/'.str_slug($member->name).'/');
                        if(!is_dir($tmp)) {
                            mkdir($tmp);
                        }

                        $zipPath = $letterManager->processPersonel([
                            'path'        => $data['path'],
                            'members'     => $member,
                            'temp'        => $tmp,
                            'letter'      => $letter,
                            'rh_simple'   => true
                        ]);
                    }
                }
                if($letter->code == 'rh_complete') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;

                    foreach ($data['members'] as $key => $member) {
                        $member = $member->member;
                        $tmp = storage_path('temporary/'.str_slug($member->name).'/');
                        if(!is_dir($tmp)) {
                            mkdir($tmp);
                        }

                        $zipPath = $letterManager->processPersonel([
                            'path'        => $data['path'],
                            'members'     => $member,
                            'temp'        => $tmp,
                            'letter'      => $letter,
                            'rh_complete' => true
                        ]);
                    }
                }
                if($letter->code == 'rh_assesment') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;

                    foreach ($data['members'] as $key => $member) {
                        $member = $member->member;
                        $tmp = storage_path('temporary/'.str_slug($member->name).'/');
                        if(!is_dir($tmp)) {
                            mkdir($tmp);
                        }

                        $zipPath = $letterManager->processPersonel([
                            'path'        => $data['path'],
                            'members'     => $member,
                            'temp'        => $tmp,
                            'letter'      => $letter,
                            'rh_assesment'=> true
                        ]);
                    }
                }

                /**
                 * Pemberhentian Dengan Hormat
                 * @var [type]
                 * 1) Formulir Pengembalian Barang Negara
                 * 2) Formulir Permohonan
                 * 3) Formulir Alamat Pensiun
                 * 4) Formulir Asabri
                */
                if($letter->code == 'pdh_things') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;

                    foreach ($data['members'] as $key => $member) {
                        $member = $member->member;
                        $tmp = storage_path('temporary/'.str_slug($member->name).'/');
                        if(!is_dir($tmp)) {
                            mkdir($tmp);
                        }

                        $zipPath = $letterManager->processPersonel([
                            'path'    => $data['path'],
                            'members' => $member,
                            'temp'    => $tmp,
                            'letter'  => $letter,
                        ]);
                    }
                }
                if($letter->code == 'pdh_letter') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;

                    foreach ($data['members'] as $key => $member) {
                        $member = $member->member;
                        $tmp = storage_path('temporary/'.str_slug($member->name).'/');
                        if(!is_dir($tmp)) {
                            mkdir($tmp);
                        }

                        $zipPath = $letterManager->processPersonel([
                            'path'    => $data['path'],
                            'members' => $member,
                            'temp'    => $tmp,
                            'letter'  => $letter
                        ]);
                    }
                }
                if($letter->code == 'pdh_pension') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;

                    foreach ($data['members'] as $key => $member) {
                        $member = $member->member;
                        $tmp = storage_path('temporary/'.str_slug($member->name).'/');
                        if(!is_dir($tmp)) {
                            mkdir($tmp);
                        }

                        $zipPath = $letterManager->processPersonel([
                            'path'    => $data['path'],
                            'members' => $member,
                            'temp'    => $tmp,
                            'letter'  => $letter
                        ]);
                    }
                }
                if($letter->code == 'pdh_asabri') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;

                    foreach ($data['members'] as $key => $member) {
                        $member = $member->member;
                        $tmp = storage_path('temporary/'.str_slug($member->name).'/');
                        if(!is_dir($tmp)) {
                            mkdir($tmp);
                        }

                        $zipPath = $letterManager->processSarminAsabri([
                            'path'    => $letter->document->getLocalPath(),
                            'members' => $member,
                            'temp'    => $tmp,
                            'letter'  => $letter
                        ]);
                    }
                }

                /**
                 * Mutasi
                 * 1) Surat Keputusan
                 * 2) Surat Petikan
                 * 3) Surat Perintah Pelaksanaan
                 * 4) Surat Telegram
                 * 5) Lampiran (Rencana Mutasi)
                 * 6) Berita Acara
                */
                if($letter->code == 'mutation_information') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;

                    foreach ($data['members'] as $key => $member) {
                        $member = $member->member;
                        $tmp = storage_path('temporary/'.str_slug($member->name).'/');
                        if(!is_dir($tmp)) {
                            mkdir($tmp);
                        }

                        $zipPath = $letterManager->processPersonel([
                            'path'           => $data['path'],
                            'members'        => $member,
                            'temp'           => $tmp,
                            'letter'         => $letter,
                        ]);
                    }
                }
                if($letter->code == 'mutation_quote') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;

                    foreach ($data['members'] as $key => $member) {
                        $member = $member->member;
                        $tmp = storage_path('temporary/'.str_slug($member->name).'/');
                        if(!is_dir($tmp)) {
                            mkdir($tmp);
                        }

                        $zipPath = $letterManager->processPersonel([
                            'path'           => $data['path'],
                            'members'        => $member,
                            'temp'           => $tmp,
                            'letter'         => $letter,
                            'mutation_quote' => true
                        ]);
                    }
                }
                if($letter->code == 'mutation_order') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;

                    foreach ($data['members'] as $key => $member) {
                        $member = $member->member;
                        $tmp = storage_path('temporary/'.str_slug($member->name).'/');
                        if(!is_dir($tmp)) {
                            mkdir($tmp);
                        }

                        $zipPath = $letterManager->processPersonel([
                            'path'           => $data['path'],
                            'members'        => $member,
                            'temp'           => $tmp,
                            'letter'         => $letter,
                            'mutation_order' => true
                        ]);
                    }
                }
                if($letter->code == 'mutation_telegram') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;
                    $tmp = storage_path('temporary/');
                    if(!is_dir($tmp)) {
                        mkdir($tmp);
                    }

                    $letterManager->processMutationTelegram([
                        'path'           => $data['path'],
                        'members'        => $data['members'],
                        'temp'           => $tmp,
                        'letter'         => $letter,
                    ]);
                }
                if($letter->code == 'mutation_plan') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;
                    $tmp = storage_path('temporary/');
                    if(!is_dir($tmp)) {
                        mkdir($tmp);
                    }

                    $letterManager->processMutationPlan([
                        'path'           => $letter->document->getLocalPath(),
                        'members'        => $data['members'],
                        'temp'           => $tmp,
                        'letter'         => $letter,
                    ]);
                }
                if($letter->code == 'mutation_news') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;
                    $tmp = storage_path('temporary/');
                    if(!is_dir($tmp)) {
                        mkdir($tmp);
                    }

                    $letterManager->processMutationNews([
                        'path'           => $letter->document->getLocalPath(),
                        'members'        => $data['members'],
                        'temp'           => $tmp,
                        'letter'         => $letter,
                    ]);
                }

                /**
                 * Nominatif
                 * 1) Pencarian
                 * 2) Daftar Riwayat Penyakit
                 * 3) Daftar Riwayat Permasalah
                 * 4) Daftar Kepemilikan Rumah
                */
                if($letter->code == 'nominatif_primary') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;

                    $tmp = storage_path('temporary/');
                    if(!is_dir($tmp)) {
                        mkdir($tmp);
                    }

                    $zipPath = $letterManager->processNominatifSearch([
                        'path'   => $data['path'],
                        'members'=> $data['members'],
                        'temp'   => $tmp,
                        'letter' => $letter,
                    ]);
                }
                if($letter->code == 'nominatif_sick') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;

                    $tmp = storage_path('temporary/');
                    if(!is_dir($tmp)) {
                        mkdir($tmp);
                    }

                    $healths = \Sdm\Member\Models\Health::orderBy('created_at', 'desc')->get();
                    $zipPath = $letterManager->processPersonelSick([
                        'path'        => $data['path'],
                        'healths'     => $healths,
                        'temp'        => $tmp,
                        'letter'      => $letter,
                    ]);
                }
                if($letter->code == 'nominatif_trouble') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;

                    $tmp = storage_path('temporary/');
                    if(!is_dir($tmp)) {
                        mkdir($tmp);
                    }

                    $laws    = \Sdm\Member\Models\Law::orderBy('created_at', 'desc')->get();
                    $zipPath = $letterManager->processPersonelProblem([
                        'path'        => $data['path'],
                        'laws'        => $laws,
                        'temp'        => $tmp,
                        'letter'      => $letter,
                    ]);
                }
                if($letter->code == 'nominatif_house') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;

                    $tmp = storage_path('temporary/');
                    if(!is_dir($tmp)) {
                        mkdir($tmp);
                    }

                    $residences    = \Sdm\Member\Models\Residence::get();
                    $zipPath = $letterManager->processNominatifResidence([
                        'path'        => $data['path'],
                        'residences'  => $residences,
                        'temp'        => $tmp,
                        'letter'      => $letter,
                    ]);
                }

                /**
                 * Mutasi
                 * 1) Surat Telegram
                 * 2) Rencana Mutasi
                 */


                /**
                 * DSP
                 * 1) DSP Lengkap
                 */
                if($letter->code == 'dsp_complete') {
                    $letterManager = new \Sdm\Core\Classes\LetterManager;

                    $tmp = storage_path('temporary/');
                    if(!is_dir($tmp)) {
                        mkdir($tmp);
                    }

                    $zipPath = $letterManager->processDspComplete([
                        'path'    => $letter->document->getLocalPath(),
                        'temp'    => $tmp,
                        'letter'  => $letter
                    ]);
                }
            }

            $zip->add(storage_path('temporary'));
            $zip->close();

            $file = new \System\Models\File;
            $file->fromUrl('https://e-sdm.polrespulpis.com/storage/app/media/zip/'.$zipName.'.zip');
            $file->save();

            $generator->document()->add($file);

            $filename = new \Sdm\Core\Classes\Generator;
            $zipName  = $filename->make();
            $zip      = \ZanySoft\Zip\Zip::create(storage_path('app/media/zip/'.$zipName.'.zip'));

            $helper->delTree(storage_path('temporary/'));

            $notification = new \Sdm\Core\Classes\Notification;
            $member       = \Sdm\Member\Models\Member::find($generator->member_id);
            $tokens       = $member->getToken();
            $body         = 'File anda sudah selesai untuk di unduh';
            $content = [
                'title'       => 'Pemberitahuan',
                'body'        =>  $body,
                'application' => [
                    'page'      => 'Download',
                    'parameter' => 'searchId',
                    'key'       => $generator->id,
                ]
            ];

            $generator->is_finish = 1;
            $generator->save();

            $notification->pulse('admin', $tokens, $content);
            return;
        }
    }


    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}
