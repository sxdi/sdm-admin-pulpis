<?php namespace Sdm\Core\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SampleData extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'sdm:sample-data';

    /**
     * @var string The console command description.
     */
    protected $description = 'Does something cool.';

    /**
     * Execute the console command.
     * @return void
     */

    public function handle123()
    {
        $data = [
            '76110635',
            '68070099',
            '64120701',
            '62040755',
            '62060724',
            '65120863',
            '63110153',
            '66050405',
            '62070967',
            '67060446',
            '74100621',
            '78070820',
            '68030295',
            '91070239',
            '63040503',
            '62020827',
            '75020014',
            '68030104',
            '68060565',
            '73080509',
            '72080340',
            '72070677',
            '64080678',
            '79030521',
            '68080150',
            '85090225',
            '71100335',
            '73010249',
            '79070992',
            '72030362',
            '74100677',
            '71080270',
            '94101244',
            '80100662',
            '86020044',
            '75030355',
            '74040716',
            '76120318',
            '76080588',
            '74040399',
            '77080267',
            '76100030',
            '76030210',
            '71070450',
            '79080417',
            '80050954',
            '79041204',
            '79100915',
            '78050829',
            '80100105',
            '80030781',
            '80060682',
            '82020298',
            '80070558',
            '80050709',
            '79100911',
            '78120170',
            '79100767',
            '79081141',
            '80100907',
            '80020813',
            '80090911',
            '82040040',
            '79100767',
            '79080247',
            '78091229',
            '82110024',
            '79060998',
            '80040222',
            '80010679',
            '79060302',
            '80120136',
            '79041055',
            '79071141',
            '81030665',
            '84100006',
            '81010281',
            '80110045',
            '81070529',
            '80020835',
            '77010839',
            '80010868',
            '85020025',
            '80101013',
            '83120207',
            '86050378',
            '84071437',
            '85080840',
            '85090036',
            '84090035',
            '85080844',
            '81110382',
            '85080377',
            '82090153',
            '86070500  ',
            '85070970',
            '83050628',
            '86020341',
            '83030379',
            '86101035',
            '85081095',
            '86090487',
            '80050614',
            '77071202',
            '82010551',
            '82090198',
            '81120499',
            '84020595',
            '86050378',
            '83121447',
            '86040390',
            '83060092',
            '82120387',
            '84050348',
            '85101359',
            '86080315',
            '85060272',
            '82020886',
            '83070169',
            '87070285',
            '87070428',
            '84061097',
            '83020977',
            '86060334',
            '85090895',
            '86040392',
            '86030381',
            '83031185',
            '84041670',
            '85100257',
            '82060483',
            '85060286',
            '86040391',
            '85100736',
            '86081243',
            '82100346',
            '83090494',
            '86040522',
            '80030681',
            '86010468',
            '86060331',
            '81110959',
            '85041012',
            '81120066',
            '81020781',
            '87110193',
            '85081893',
            '88110092',
            '89070029',
            '88110315',
            '88060224',
            '87110382',
            '88040520',
            '89030193',
            '86031466',
            '88010967',
            '86031205',
            '86111649',
            '90020303',
            '88100197',
            '86101388',
            '87110732',
            '88120098',
            '89010330',
            '88080156',
            '85120604',
            '86031366',
            '88080185',
            '88090858',
            '87060700',
            '86031871',
            '86031205',
            '85121953',
            '88050100',
            '87041087',
            '88080138',
            '88040377',
            '88100381',
            '87120023',
            '86110703',
            '86081272',
            '87051153',
            '88090330',
            '88010268',
            '86051752',
            '87070907',
            '86051954',
            '89120036',
            '85111094',
            '77090699',
            '79030240',
            '86071207',
            '88020998',
            '88070508',
            '87071132',
            '86051238',
            '87020739',
            '88100065',
            '87051153',
            '88010130',
            '75040398',
            '85011396',
            '86090860',
            '86031858',
            '86101362',
            '87120567',
            '90010095',
            '86071718',
            '74090367',
            '87021246',
            '86041769',
            '85121615',
            '87080952',
            '87120477',
            '86121098',
            '87070752',
            '88060512',
            '87011492',
            '86011027',
            '88070738',
            '83030464',
            '88030311',
            '88040269',
            '80050066',
            '85062030',
            '87010881',
            '88090704',
            '85121911',
            '87120122',
            '86121500',
            '89090399',
            '90040330',
            '90040352',
            '93120568',
            '93120173',
            '94050207',
            '94070104',
            '94110440',
            '95010022',
            '93040213',
            '95050169',
            '95100044',
            '94040941',
            '92050284',
            '93060167',
            '90030230',
            '92080855',
            '94020166',
            '92030254',
            '94090524',
            '94070356',
            '94090595',
            '92100684',
            '95060045',
            '93040939',
            '92070313',
            '94010247',
            '94070515',
            '92100252',
            '95020292',
            '93040790',
            '94010719',
            '95020171',
            '92060474',
            '95040823',
            '94080925',
            '96080133',
            '95090687',
            '94120932',
            '96020197',
            '96110011',
            '94081029',
            '95110477',
            '96040160',
            '96050363',
            '96080448',
            '95120403',
            '95060419',
            '96120036',
            '95100578',
            '95120454',
            '93090960',
            '95070472',
            '96010145',
            '96020009',
            '96040621',
            '97060339',
            '97090221',
            '98070097',
            '95080510',
            '95040866',
            '96060320',
            '95040523',
            '96120506',
            '97060263',
            '96050023',
            '95070545',
            '94060929',
            '95030611',
            '95120303',
            '95090391',
            '96030099',
            '96110252',
            '96080226',
            '96060503',
            '19690308 201412 2 001',
            '197702102014121001',
            '19720604 201412 2 003',
        ];

        foreach ($data as $d) {
            $user = \Sdm\User\Models\User::firstOrNew([
                'code' => $d,
                'passcode' => \Hash::make($d)
            ]);
            $user->save();
        }
    }
    public function hansdle()
    {
        $content = [
            'title' => 'Pemberitahuan Pengingat',
            'body'  =>  'wew',
            // 'application' => [
            //     'page'      => 'ReminderDetail',
            //     'parameter' => 'reminderId',
            //     'key'       => $reminder->id
            // ]
        ];
        $notification = new \Sdm\Core\Classes\Notification;
        $notification->pulse(
            ['d2K4tzxeA-w:APA91bGKZTnFgKGOvxxFx4sQKffiAHYlVW0JUTL4lQg5V3IqpznsvNg53JSPdpSnJoE2qELQXLTP4UxLQKb_Zsvnt2hgM2uUtt7aoR-wNgomPZePX4UjiW_ymLEPRp24p2Y4R2gL3z4z'],
            $content
        );
    }
    public function handlex()
    {
        $grades = [
            [
                'name' => 'Jendral Polisi',
                'code' => 'Jendral Pol',
                'grade'=> 'IV G'
            ],
            [
                'name' => 'Komisaris Jendral Polisi',
                'code' => 'Komjen Pol',
                'grade'=> 'IV F'
            ],
            [
                'name' => 'Inspektur Jendral Polisi',
                'code' => 'Irjen Pol',
                'grade'=> 'IV E'
            ],
            [
                'name' => 'Brigadir Jendral Polisi',
                'code' => 'Brigjen Pol',
                'grade'=> 'IV D'
            ],
            [
                'name' => 'Komisaris Besar Polisi',
                'code' => 'Kombes Pol',
                'grade'=> 'IV C'
            ],
            [
                'name' => 'Ajun Komisaris Besar Polisi',
                'code' => 'AKBP',
                'grade'=> 'IV B'
            ],
            [
                'name' => 'Komisaris Polisi',
                'code' => 'Kompol',
                'grade'=> 'IV A'
            ],
            [
                'name' => 'Ajun Komisaris Polisi',
                'code' => 'AKP',
                'grade'=> 'III C'
            ],
            [
                'name' => 'Inspektur Polisi Satu',
                'code' => 'Iptu',
                'grade'=> 'III B'
            ],
            [
                'name' => 'Inspektur Polisi Dua',
                'code' => 'Ipda',
                'grade'=> 'III A'
            ],
            [
                'name' => 'Ajun Inspektur Polisi Satu',
                'code' => 'Aiptu',
                'grade'=> 'II F'
            ],
            [
                'name' => 'Ajun Inspektur Polisi Dua',
                'code' => 'Aipda',
                'grade'=> 'II E'
            ],
            [
                'name' => 'Bintara Brigadir Polisi Kepala',
                'code' => 'Bripka',
                'grade'=> 'II D'
            ],
            [
                'name' => 'Bintara Brigadir Polisi',
                'code' => 'Brigpol',
                'grade'=> 'II C'
            ],
            [
                'name' => 'Bintara Brigadir Polisi Satu',
                'code' => 'Briptu',
                'grade'=> 'II B'
            ],
            [
                'name' => 'Bintara Brigadir Polisi Dua',
                'code' => 'Bripda',
                'grade'=> 'II A'
            ],
            [
                'name' => 'Tamtama Ajun Brigadir Polisi',
                'code' => 'Abrip',
                'grade'=> 'I F'
            ],
            [
                'name' => 'Tamtama Ajun Brigadir Polisi Satu',
                'code' => 'Brigpol',
                'grade'=> 'I E'
            ],
            [
                'name' => 'Tamtama Ajun Brigadir Polisi Dua',
                'code' => 'Abripda',
                'grade'=> 'I D'
            ],
            [
                'name' => 'Tamtama Bhayangkara Kepala',
                'code' => 'Baraka',
                'grade'=> 'I C'
            ],
            [
                'name' => 'Tamtama Bhayangkara Satur',
                'code' => 'Bharatu',
                'grade'=> 'I B'
            ],
            [
                'name' => 'Tamtama Bhayangkara Dua',
                'code' => 'Bharada',
                'grade'=> 'I A'
            ],
        ];

        foreach ($grades as $g) {
            $grade = \Sdm\Master\Models\Grade::create([
                'name'  => $g['name'],
                'code'  => $g['code'],
                'grade' => $g['grade'],
            ]);
        }
    }

    public function handle()
    {
        $notification = new \Sdm\Core\Classes\Notification;
        // $tokens       = \Sdm\User\Models\Token::whereUserId(1)->get()->pluck('token');
        $body    = 'tes';
        // $token   = 'cqJ8yJOzIJU:APA91bFfzMiVDCpPPXUlE9NigxgBTHd87Ucf5RnvJ9AZVmhHjy9Yti_F-eAsJxPpMV77dcXLt7JnxX3KouIKi8Zj3PPAiyZSbBnj3pYRrJO1w43d3DWajA8HHuotanWGomSNGcolxqWP';
        $token   = 'dwIt0-LQaRM:APA91bFTNV5HNsinRDh5_SncVep4sk3VjuVEh2qY8RFdLQO44nN85kgAZobP7HPlm4mAUlDHsqh0TRa9mG_RyEVE-_u-MsVSQw4ashga7Hss3v5Mp8BnuJ4MpDJx52C575HTRkHSjzha';
        $content = [
            'title'       => 'Pemberitahuan',
            'body'        =>  $body,
            'application' => [
                'page' => 'MemberDetail',
                'parameter' => 'memberId',
                'key'   => 125,
            ]
            // 'application' => [
                // 'page'      => 'Profile',
            // ]
        ];

        $notification->pulse('admin', [$token], $content);
        $this->info('Terkirim');
        return;
        $data = [
            'PIMPINAN' => [
                'KAPOLRES',
                'WAKAPOLRES'
            ],
            'BAG OPS' => [
                'KABAGOPS',
                'PAURMIN BAGOPS',
                'KASUABBAGBINOPS BAGOPS',
                'PAUR SUBBAGBINOPS BAGOPS',
                'KASUBBAGDALOPS BAGOPS',
                'PAUR SUBBAGDALOPS BAGOPS',
                'KASUBBAGHUMAS BAGOPS',
                'PAUR SUBBAGHUMAS',
                'BANUM BAGOPS',
                'BAMIN BAGOPS',
                'PA SIAGA I BAGOPS',
                'PA SIAGA II BAGOPS',
                'PA SIAGA III BAGOPS',
            ],
            'BAG SUMDA' => [
                'KABAGSUMDA',
                'PAURMIN BAGSUMDA',
                'KASUBBAGPERS BAGSUMDA',
                'PAURMINPERS I BAGSUMDA',
                'PAURMINPERS II BAGSUMDA',
                'BAMINPERS BAGSUMDA',
                'PAURKES',
                'BAMIN BAGSUMDA',
                'BANUM BAGSUMDA',
                'KASUBBAGSARPRAS',
                'PAURLOG SUBBAGSARPRAS BAGSUMDA',
                'BAMIN SUBBAGSARPRAS BAGSUMDA',
                'BANUM SUBBAGSARPRAS BAGSUMDA',
                'KASUBBAGHUKUM BAGSUMDA',
                'PAUR BANKUM BAGSUMDA',
                'PAUR  RAPKUM BAGSUMDA',
            ],
            'BAG REN' => [
                'KABAGREN',
                'PAURMIN BAGREN',
                'KASUBBAGPROGAR BAGREN',
                'PAUR SUBBAGPROGAR BAGREN',
                'KASUBBAGDALGAR BAGREN',
                'PAUR SUBBAGDALGAR',
                'BAMIN BAGREN',
                'BANUM BAGREN',
            ],

            'SIUM' => [
                'KASIUM',
                'BASUBSIMINTU SIUM',
                'BASUBSIYANMA',
                'BAMIN SIUM',
                'BANUM SIUM',
            ],
            'SIKEU' => [
                'KASIKEU',
                'BAMIN SIKEU',
                'BANUM SIKEU',
            ],

            'SIPROPAM' => [
                'KASIPROPAM',
                'BAURPROVOS SIPROPAM',
                'BAURPAMINAL SIPROPAM',
            ],

            'SIWAS' => [
                'KASIWAS',
                'BAURBIDOPSNAL SIWAS',
                'BAURBIDMIN SIWAS',
                'BAMIN SIWAS',
                'BANUM SIWAS',
            ],

            'SPKT' => [
                'KA SPKT',
                'KANIT 1 SPKT',
                'KANIT 2 SPKT',
                'KANIT 3 SPKT',
                'BANIT SPKT',
            ],

            'SAT INTELKAM' => [
                'KASATINTELKAM',
                'KAURBINOPS SATINTELKAM',
                'KAURMINTU SATINTELKAM',
                'KANIT 1 SATINTELKAM',
                'KANIT 2 SATINTELKAM',
                'KANIT 3 SATINTELKAM',
                'KANIT 4 SATINTELKAM',
                'KANIT 5 SATINTELKAM',
                'KANIT 6 SATINTELKAM',
                'BANIT SATINTELKAM',
            ],

            'SAT RESKRIM' => [
                'KASATRESKRIM',
                'KAURBINOPS SATRESKRIM',
                'KAURMINTU SATRESKRIM',
                'KAURIDENT SATRESKRIM',
                'BAURIDENT SATRESKRIM',
                'KANIT IDIK 1 SATRESKRIM',
                'KANIT IDIK 2 SATRESKRIM',
                'KANIT IDIK 3 SATRESKRIM',
                'KANIT IDIK 4 SATRESKRIM',
                'KANITTIPIDKOR SATRESKRIM',
                'BANITTIPIDKOR SATRESKRIM',
            ],

            'SAT NARKOBA' => [
                'KASATNARKOBA ',
                'KAURBINOPS SATRESNARKOBA',
                'KAURMINTU SATRESNARKOBA',
                'BAMIN SATRESNARKOBA',
                'BANUM SATRESNARKOBA',
                'KANIT IDIK 1 SATRESNARKOBA',
                'KANIT IDIK 2 SATRESNARKOBA',
                'BANIT IDIK SATRESNARKOBA',
            ],

            'SAT BINMAS' => [
                'KASATBINMAS',
                'KAURBINOPS SATBINMAS',
                'KAURMINTU SATBINMAS',
                'BAMIN SATBINMAS',
                'BANUM SATBINMAS',
                'KANIT BIN POLMAS',
                'KANIT BINTIBMAS',
                'KANIT BINKAMSA',
                'BANIT SATBINMAS',
            ],

            'SAT SABHARA' => [
                'KASATSABHARA',
                'KAURBINOPS SATSABHARA',
                'KAURMINTU SATSABHARA',
                'BAMIN SATSABHARA',
                'BANUM SATSABHARA',
                'KANITTURJAWALI SATSABHARA',
                'KANITPAMOBVIT SATSABHARA',
                'KANITDALMAS 1 SATSABHARA',
                'KASUBNITDALMAS 1 SATSABHARA',
                'KANITDALMAS 2 SATSABHARA',
                'KASUBNIT DALMAS 2 SATSABHARA',
                'BANIT SATSABHARA',
            ],

            'SAT LANTAS' => [
                'KASATLANTAS',
                'KAURBINOPS',
                'KAUR MINTU',
                'BAMIN SATLANTAS',
                'BANUM SATLANTAS',
                'KANITDIKYAKSA SATLANTAS',
                'KANITTURJAWALI SATLANTAS',
                'KANITREGIDENT SATLANTAS',
                'KANITLAKA SATLANTAS',
                'BANIT SATLANTAS',
            ],

            'SAT POLAIR' => [
                'KASATPOLAIR',
                'KAURBINOPS SATPOLAIR',
                'KAURMINTU SATPOLAIR',
                'BAMIN SATPOLAIR',
                'BANUM SATPOLAIR',
                'KANIT GAKKUM SATPOLAIR',
                'KANIT PATROLI SATPOLAIR',
                'KANIT HARKAPAL SATPOLAIR',
                'KASUBNIT BINMAS SATPOLAIR',
                'KASUBNIT PATWAL SATPOLAIR',
                'KASUBNIT LIDIK SATPOLAIR',
                'KASUBNIT TINDAK SATPOLAIR',
                'BANIT SATPOLAIR',
            ],

            'SAT TAHTI' => [
                'KASATTAHTI ',
                'BAURMINTU SATTAHTI',
                'BANITWATTAH SATTAHTI',
                'BANITBARBUK SATTAHTI',
                'BANIT SATTAHTI',
            ],

            'SITIPOL' => [
                'KASITIPOL',
                'BAURMINTU SITIPOL',
                'BAMIN SITIPOL',
                'BAURTEKKOM SITIPOL',
                'BAURTEKINFO SITIPOL',
                'BANIT SITIPOL',
            ],

            'POLSEK KAHAYAN HILIR' => [
                'KAPOLSEK KAHAYAN HILIR',
                'WAKAPOLSEK KAHAYAN HILIR',
                'KANITPROVOS POLSEK KAHAYAN HILIR',
                'KASIUM POLSEK KAHAYAN HILIR',
                'KASIHUMAS POLSEK KAHAYAN HILIR',
                'KA SPKT 1 POLSEK KAHAYAN HILIR',
                'KA SPKT 2 POLSEK KAHAYAN HILIR',
                'KA SPKT 3 POLSEK KAHAYAN HILIR',
                'KANITINTELPOLSEK KAHAYAN HILIR',
                'KANITRESKRIM POLSEK KAHAYAN HILIR',
                'KANITBINMAS POLSEK KAHAYAN HILIR',
                'KANITSABHARA POLSEK KAHAYAN HILIR',
                'BANIT SABHARA POLSEK KAHAYAN HILIR (KAPOSPOL DS. MINTIN)',
                'BA POLSEK KAHAYAN HILIR ',
                'BHABINKAMTIBMAS KEL. KALAWA POLSEK KAHAYAN HILIR',
                'BHABINKAMTIBMAS DS. HANJAK MAJU POLSEK KAHAYAN HILIR',
                'BHABINKAMTIBMAS DS. ANJIR POLSEK KAHAYAN HILIR',
                'BHABINKAMTIBMAS DS. GOHONG POLSEK KAHAYAN HILIR',
                'BHABINKAMTIBMAS DS. MENTAREN II POLSEK KAHAYAN HILIR',
                'BHABINKAMTIBMASD KEL. BERENG POLSEK KAHAYAN HILIR',
                'BHABINKAMTIIBMAS DS. MINTIN POLSEK KAHAYAN HILIR',
                'BHABINKAMTIBMAS DS. BUNTOI POLSEK KAHAYAN HILIR',
            ],

            'POLSEK JEBIREN RAYA' => [
                'KAPOLSEK JABIREN RAYA',
                'KANITPROVOS POLSEK JABIREN RAYA',
                'KASIUM POLSEK JABIREN RAYA',
                'KANITINTEL POLSEK JABIREN RAYA',
                'KANITRESKRIM POLSEK JABIREN RAYA',
                'KANITBINMAS POLSEK JABIREN RAYA',
                'KANITSABHARA POLSEK JABIREN RAYA',
                'BA POLSEK KAHAYAN TENGAH',
                'BHABINKAMTIBMAS DS. PILANG POLSEK JABIREN RAYA',
                'BHABINKAMTIBMAS DS. SIMPUR POLSEK JABIREN RAYA',
                'BHABINKAMTIBMAS DS. TANJUNG TARUNA POLSEK JABIREN RAYA',
                'BHABINKAMTIBMASD DS. JABIREN POLSEK JABIREN RAYA',
                'BHABINKAMTIIBMAS DS. SAKA KAJANG POLSEK JABIREN RAYA',
                'BHABINKAMTIBMAS DS. TUMBANG NUSA POLSEK JABIREN RAYA',
            ],

            'POLSEK KAHAYAN TENGAH' => [
                'KAPOLSEK ',
                'WAKA POLSEK',
                'KANIT PROVOS',
                'KASIUM',
                'KASI HUMAS',
                'KA SPKT 1',
                'KA SPKT 2',
                'KA SPKT 3',
                'KANIT INTEL',
                'KANIT RESKRIM',
                'KANIT BINMAS',
                'KANIT SABHARA',
                'BA POLSEK KAHAYAN TENGAH',
                'BHABINKAMTIBMAS DS. BAHU PALAWA POLSEK KAHAYAN TENGAH',
                'BHABINKAMTIBMAS DS. TAHAWA POLSEK KAHAYAN TENGAH',
                'BHABINKAMTIBMAS DS. BUKIT LITI POLSEK KAHAYAN TENGAH',
                'BHABINKAMTIBMASD DS. PAMARUNAN POLSEK KAHAYAN TENGAH',
                'BHABINKAMTIIBMAS DS. TANJUNG SANGGALANG SEK KAH TENGAH',
                'BHABINKAMTIBMAS DS. TUWUNG POLSEK KAHAYAN TENGAH',
            ],

            'POLSEK KAHAYAN KUALA' => [
                'KAPOLSEK ',
                'WAKA POLSEK',
                'KANIT PROVOS',
                'KASIUM',
                'KASI HUMAS',
                'KA SPKT 1',
                'KA SPKT 2',
                'KA SPKT 3',
                'KANIT INTEL',
                'KANIT RESKRIM',
                'KANIT BINMAS',
                'KANIT SABHARA',
                'BA POLSEK KAHAYAN KUALA',
                'BHABINKAMTIBMAS DS. TANJUNG PERAWAN POLSEK KAH KUALA',
                'BHABINKAMTIBMAS DS. BAHAUR HILIR POLSEK KAHAYAN KUALA',
                'BHABINKAMTIBMAS DS. SEI RUNGUN POLSEK KAHAYAN TENGAH',
                'BHABINKAMTIBMASD DS. BAHAUR TENGAH POLSEK KAH TENGAH',
            ],

            'POLSEK PANDIH BATU' => [
                'KAPOLSEK ',
                'WAKA POLSEK',
                'KANIT PROVOS',
                'KASIUM',
                'KASI HUMAS',
                'KA SPKT 1',
                'KA SPKT 2',
                'KA SPKT 3',
                'KANIT INTEL',
                'KANIT RESKRIM',
                'KANIT BINMAS',
                'KANIT SABHARA',
                'BA POLSEK PANDIH BATU',
                'BANIT SABHARA POLSEK PANDIH BATU',
                'BHABINKAMTIBMAS DS. KARYA BERSAMA POLSEK PANDIH BATU',
                'BHABINKAMTIBMAS DS. BELANTI SIAM POLSEK PANDIH BATU',
                'BHABINKAMTIBMAS DS. PANGKOH SARI POLSEK PANDIH BATU',
                'BHABINKAMTIBMAS DS. DANDANG POLSEK PANDIH BATU',
                'BHABINKAMTIBMAS DS. PANGKOH HILIR POLSEK PANDIH BATU',
            ],

            'POLSEK BANAMA TINGANG' => [
                'KAPOLSEK ',
                'WAKA POLSEK',
                'KANIT PROVOS',
                'KASIUM',
                'KASI HUMAS',
                'KA SPKT 1',
                'KA SPKT 2',
                'KA SPKT 3',
                'KANIT INTEL',
                'KANIT RESKRIM',
                'KANIT BINMAS',
                'KANIT SABHARA',
                'BA POLSEK BANAMA TINGANG',
                'BHABINKAMTIBMAS DS. LAWANG URU POLSEK BANTING',
                'BHABINKAMTIBMAS DS. PANGI POLSEK BANTING',
                'BHABINKAMTIBMAS DS. GOHA POLSEK BANTING',
                'BHABINKAMTIBMAS DS. TANGKAHEN POLSEK BANTING',
                'BHABINKAMTIBMAS DS. TUMBANG TERUSAN POLSEK BANTING',
                'BHABINKAMTIBMAS DS. BAWAN POLSEK BANTING',
                'BHABINKAMTIBMAS DS. PAHAWAN POLSEK BANTING',
            ],

            'POLSEK SIAP MALIKU' => [
                'KAPOLSEK',
                'KANIT PROVOS',
                'KASIUM',
                'KANIT INTEL',
                'KANIT RESKRIM',
                'KANIT BINMAS',
                'KANIT SABHARA',
                'BA POLSEK MALIKU',
                'BHABINKAMTIBMAS DS. TAHAI JAYA POLSEK MALIKU (KAPOSPOL)',
                'BHABINKAMTIBMAS DS. KANAMIT POLSEK MALIKU',
                'BHABINKAMTIBMAS DS. GARANTUNG POLSEK MALIKU',
                'BHABINKAMTIBMAS DS. GANDANG POLSEK MALIKU',
                'BHABINKAMTIBMAS DS. KANAMIT JAYA POLSEK MALIKU',
                'BHABINKAMTIBMAS DS. WONO AGUNG POLSEK MALIKU',
                'BHABINKAMTIBMAS DS. PURWODADI POLSEK MALIKU',
                'BHABINKAMTIBMAS DS. TAHAI BARU POLSEK MALIKU',
                'BHABINKAMTIBMAS DS. BADIRIH POLSEK MALIKU',
            ],

            'POLSEK SIAP SEBANGAU' => [
                'KAPOLSEK',
                'KANIT PROVOS',
                'KASIUM',
                'KANIT INTEL',
                'KANIT RESKRIM',
                'KANIT BINMAS',
                'KANIT SABHARA',
                'BA POLSEK SEBANGAU KUALA',
                'BHABINKAMTIBMAS DS. MEKAR JAYA POLSEK SEBAKUL',
                'BHABINKAMTIBMAS DS. SEI BAKAU POLSEK SEBAKUL',
                'BHABINKAMTIBMAS DS. SEBANGAU MULYA POLSEK SEBAKUL',
                'BHABINKAMTIBMAS DS. SEBANAGAU PERMAI POLSEK SEBAKUL',
            ]
        ];

        $keyData = ['PIMPINAN', 'BAG OPS', 'BAG SUMDA', 'BAG REN', 'SIUM', 'SIKEU', 'SIPROPAM', 'SIWAS', 'SPKT', 'SAT INTELKAM', 'SAT RESKRIM', 'SAT NARKOBA', 'SAT BINMAS', 'SAT SABHARA', 'SAT LANTAS', 'SAT POLAIR', 'SAT TAHTI', 'SITIPOL', 'POLSEK KAHAYAN HILIR', 'POLSEK JEBIREN RAYA', 'POLSEK KAHAYAN TENGAH', 'POLSEK KAHAYAN KUALA', 'POLSEK PANDIH BATU', 'POLSEK BANAMA TINGANG', 'POLSEK SIAP MALIKU', 'POLSEK SIAP SEBANGAU'];
        $order = 1;
        foreach ($keyData as $k) {
            $position = \Sdm\Master\Models\Position::create([
                'name'  => $k,
                'order' => $order,
            ]);
            foreach ($data[$k] as $d) {
                $p = \Sdm\Master\Models\Position::create([
                    'parent_id' => $position->id,
                    'order'     => $order,
                    'name'      => $d,
                ]);
            }

            $order++;
        }
    }

    public function handles()
    {
        $educations = [
            [
                'name' => 'Strata Tiga',
                'code' => 'S3',
            ],
            [
                'name' => 'Strata Dua',
                'code' => 'S2',
            ],
            [
                'name' => 'Strata Satu',
                'code' => 'S1',
            ],
            [
                'name' => 'Diploma Empat',
                'code' => 'D-IV',
            ],
            [
                'name' => 'Diploma Tiga',
                'code' => 'D-III',
            ],
            [
                'name' => 'Diploma Dua',
                'code' => 'D-II',
            ],
            [
                'name' => 'Diploma Satu',
                'code' => 'D-I',
            ],
            [
                'name' => 'Sekolah Menengah Akhir',
                'code' => 'SMA',
            ],
            [
                'name' => 'Sekolah Menengah Pratama',
                'code' => 'SMP',
            ],
            [
                'name' => 'Sekolah Dasar',
                'code' => 'SD',
            ],
        ];

        foreach ($educations as $education) {
            \Sdm\Master\Models\Education::create([
                'name' => $education['name'],
                'code' => $education['code'],
            ]);
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}
