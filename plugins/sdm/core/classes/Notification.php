<?php namespace Sdm\Core\Classes;

class Notification
{
    public function pulse($app, $token, $body)
    {
        $fcmUrl  = 'https://fcm.googleapis.com/fcm/send';

        if($app == 'member') {
            $headers = [
                'Authorization: key=AAAASr5MpLM:APA91bFT3QZL8s0M9UQr6lNc6mxMmA15hi0Dhj7xTjxQX0LUtrgiJI_K6VnKm1I-KnB86tEeeJ0R5R3nxcrU859aDI4o86HAUtZra5fgzmzmuy-OoP6WlswZG5Lp-hCYrOPo1qRHjS5y',
                'Content-Type: application/json'
            ];
        }

        if($app == 'admin') {
            $headers = [
                'Authorization: key=AAAABcmr5gM:APA91bHa7WX0Un-QyPg7ONp7JZReHgeggmuuIUt7fQMaVUp0-NmxjJo9vYGq0LKeZpy_d7q7ydT3Dq_S-2Eb5fQyzPYJWa8vlX_uVyq2VTqpSNRHHlZk_VAbAyP3155FEmyc4GEZZT7l',
                'Content-Type: application/json'
            ];
        }

        $notification = [
            'title'              => $body['title'],
            'body'               => $body['body'],
            'sound'              => 'bell',
            'color'              => '#222',
            'priority'           => 'high',
            'icon'               => 'ic_launcher',
            'show_in_foreground' => true,
            'show_in_background' => true,
        ];

        if(isset($body['application'])) {
            $notification['application'] = $body['application'];
        }

        $fcmNotification = [
            'registration_ids' => $token,
            'notification'     => $notification,
            'data'             => $notification
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);

        return true;
    }
}
