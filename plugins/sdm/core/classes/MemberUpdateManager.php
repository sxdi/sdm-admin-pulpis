<?php namespace Sdm\Core\Classes;

use Carbon\Carbon;

/**
 * Core Plugin Information File
 */
class MemberUpdateManager
{
    public function count($member)
    {
        $counter = [
            'attachments' => [
                'items' => [
                    'ktp'          => (int) count($this->countAttachmentByType($member, 'ktp')),
                    'npwp'         => (int) count($this->countAttachmentByType($member, 'npwp')),
                    'kk'           => (int) count($this->countAttachmentByType($member, 'kk')),
                    'finger'       => (int) count($this->countAttachmentByType($member, 'finger')),
                    'asabri'       => (int) count($this->countAttachmentByType($member, 'asabri')),
                    'bpjs'         => (int) count($this->countAttachmentByType($member, 'bpjs')),
                    'authority'    => (int) count($this->countAttachmentByType($member, 'authority')),
                    'investigator' => (int) count($this->countAttachmentByType($member, 'investigator')),
                    'member'       => (int) count($this->countAttachmentByType($member, 'member')),
                    'kps'          => (int) count($this->countAttachmentByType($member, 'kps')),
                    'simsa'        => (int) count($this->countAttachmentByType($member, 'simsa')),
                ]
            ],
            'primaries' => [
                'items' => [
                    'language'             => (int) count($this->counterPrimaryLanguage($member, '0')),
                    'languageForeign'      => (int) count($this->counterPrimaryLanguage($member, '1')),
                    'sport'                => (int) count($this->counterPrimarySport($member)),
                    'honor'                => (int) count($this->counterPrimaryHonor($member)),
                    'educationFormal'      => (int) count($this->counterEducation($member, 'formal')),
                    'educationNonFormal'   => (int) count($this->counterEducation($member, 'non')),
                    'educationPolri'       => (int) count($this->counterEducationPolice($member, 'polri')),
                    'educationDikbangspes' => (int) count($this->counterEducationPolice($member, 'dikbangspes')),
                    'training'             => (int) count($this->counterTraining($member)),
                    'duty'                 => (int) count($this->counterDuty($member)),
                    'position'             => (int) count($this->counterPosition($member)),
                    'grade'                => (int) count($this->counterGrade($member)),
                    'work'                 => (int) count($this->counterWork($member)),
                    'brivet'               => (int) count($this->counterBrivet($member)),
                    'salary'               => (int) count($this->countSalary($member))
                ]
            ]
        ];

        $counter['attachments']['total'] = (int) array_sum($counter['attachments']['items']);
        $counter['primaries']['total']   = (int) array_sum($counter['primaries']['items']);
        $counter['total']                = (int) array_sum($counter['attachments']['items']) + array_sum($counter['primaries']['items']);

        return $counter;
    }

    public function countAttachmentByType($member, $type)
    {
        return \Sdm\Member\Models\Attachment::whereMemberId($member->id)
            ->where('type', '=', $type)
            ->where('status', '=', 'process')
            ->get();
    }

    public function counterPrimaryLanguage($member, $isForeign)
    {
        return \Sdm\Member\Models\Language::whereMemberId($member->id)
            ->where('is_foreign', '=', $isForeign)
            ->where('status', '=', 'process')
            ->get();
    }

    public function counterPrimarySport($member)
    {
        return \Sdm\Member\Models\Sport::whereMemberId($member->id)
            ->where('status', '=', 'process')
            ->get();
    }

    public function counterPrimaryHonor($member)
    {
        return \Sdm\Member\Models\Honor::whereMemberId($member->id)
            ->where('status', '=', 'process')
            ->get();
    }

    public function counterEducation($member, $type)
    {
        return \Sdm\Member\Models\Education::whereMemberId($member->id)
            ->where('type', '=', $type)
            ->where('status', '=', 'process')
            ->get();
    }

    public function counterEducationPolice($member, $type)
    {
        return \Sdm\Member\Models\EducationPolice::whereMemberId($member->id)
            ->where('type', '=', $type)
            ->where('status', '=', 'process')
            ->get();
    }

    public function counterTraining($member)
    {
        return \Sdm\Member\Models\Training::whereMemberId($member->id)
            ->where('status', '=', 'process')
            ->get();
    }

    public function counterDuty($member)
    {
        return \Sdm\Member\Models\Duty::whereMemberId($member->id)
            ->where('status', '=', 'process')
            ->get();
    }

    public function counterPosition($member)
    {
        return \Sdm\Member\Models\Position::whereMemberId($member->id)
            ->where('status', '=', 'process')
            ->get();
    }

    public function counterGrade($member)
    {
        return \Sdm\Member\Models\Grade::whereMemberId($member->id)
            ->where('status', '=', 'process')
            ->get();
    }

    public function counterWork($member)
    {
        return \Sdm\Member\Models\Work::whereMemberId($member->id)
            ->where('status', '=', 'process')
            ->get();
    }

    public function counterBrivet($member)
    {
        return \Sdm\Member\Models\Brivet::whereMemberId($member->id)
            ->where('status', '=', 'process')
            ->get();
    }

    public function countSalary($member)
    {
        return \Sdm\Member\Models\Salary::whereMemberId($member->id)
            ->where('status', '=', 'process')
            ->get();
    }
}
