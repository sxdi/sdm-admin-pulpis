<?php namespace Sdm\Core\Classes;

use Carbon\Carbon;

/**
 * Core Plugin Information File
 */
class MemberManager
{
    public function getUrminToken()
    {
        $users = \Sdm\User\Models\User::whereIsAdmin(1)->get()->pluck('id');
        $tokens= \Sdm\User\Models\Token::whereIn('user_id', $users)->get()->pluck('token');
        return $tokens;
    }

    public function getKapolres()
    {
        $position = \Sdm\Member\Models\Position::whereName('KAPOLRES')->whereIsNow(1)->first();
        if($position) {
            return \Sdm\Member\Models\Member::find($position->member_id);
        }
    }

    public function getWakaPolres()
    {
        $position = \Sdm\Member\Models\Position::where('name', 'like' ,'%WAKA POLRES%')->whereIsNow(1)->first();
        if($position) {
            return \Sdm\Member\Models\Member::find($position->member_id);
        }
    }

    public function getKabagsumda()
    {
        $position = \Sdm\Member\Models\Position::where('name', 'like' ,'%KABAG SUMDA%')->whereIsNow(1)->first();
        if($position) {
            return \Sdm\Member\Models\Member::find($position->member_id);
        }
    }

    public function getKasipropam()
    {
        $position = \Sdm\Member\Models\Position::where('name', 'like' ,'%KASI PROPAM%')->whereIsNow(1)->first();
        if($position) {
            return \Sdm\Member\Models\Member::find($position->member_id);
        }
    }

    public function getKasiwas()
    {
        $position = \Sdm\Member\Models\Position::where('name', 'like' ,'%KASIWAS%')->whereIsNow(1)->first();
        if($position) {
            return \Sdm\Member\Models\Member::find($position->member_id);
        }
    }

    public function getPaurminpers()
    {
        $position = \Sdm\Member\Models\Position::where('name', 'like' ,'%PAURMINPERS%')->whereIsNow(1)->first();
        if($position) {
            return \Sdm\Member\Models\Member::find($position->member_id);
        }
    }

    public function getWorkTime($member)
    {
        if($member->getWorkTime()) {
            $month = number_format(Carbon::parse($member->getWorkTime()->started_at)->diffInMonths() / 12, 2, '.', '');
            $total = explode('.', $month);
            return [
                'date'  => Carbon::parse($member->getWorkTime()->started_at)->format('d F Y'),
                'count' => $total[0].' Tahun '.$total[1].' Bulan'
            ];
        }

        return false;
    }

    public function getMothRange($start, $end)
    {
        $started = Carbon::parse($start);
        $ended   = Carbon::parse($end);
        return $started->diffInMonths($ended);
    }

    public function getMemberRelation($member, $type)
    {
        return \Sdm\Member\Models\Family::whereIn('relation', $type)->orderBy('dob', 'asc')->whereMemberId($member->id)->get();
        // return \Sdm\Member\Models\Family::whereIn('relation', $type)->whereMemberId($member->id)->get();
    }

    public function getAttachment($member, $type)
    {
        return \Sdm\Member\Models\Attachment::whereMemberId($member->id)->where('type', '=', $type)->first();
    }

    public function getLanguage($member)
    {
        return \Sdm\Member\Models\Language::whereMemberId($member->id)->whereIsForeign(0)->get();
    }

    public function getSport($member)
    {
        return \Sdm\Member\Models\Sport::whereMemberId($member->id)->get();
    }

    public function getLanguageForeign($member)
    {
        return \Sdm\Member\Models\Language::whereMemberId($member->id)->whereIsForeign(1)->get();
    }

    public function getEducationFormal($member, $order)
    {
        return \Sdm\Member\Models\Education::whereMemberId($member->id)->whereIn('type', ['formal'])->orderBy($order['column'], $order['value'])->get();
    }

    public function getEducationNonFormal($member, $order)
    {
        return \Sdm\Member\Models\Education::whereMemberId($member->id)->whereIn('type', ['non'])->orderBy($order['column'], $order['value'])->get();
    }

    public function getEducationPolries($member, $order)
    {
        return \Sdm\Member\Models\EducationPolice::whereMemberId($member->id)->whereIn('type', ['polri'])->orderBy($order['column'], $order['value'])->get();
    }

    public function getEducationDikbangspes($member, $order)
    {
        return \Sdm\Member\Models\EducationPolice::whereMemberId($member->id)->whereIn('type', ['dikbangspes'])->orderBy($order['column'], $order['value'])->get();
    }

    public function getTraining($member, $order)
    {
        return \Sdm\Member\Models\Training::whereMemberId($member->id)->orderBy($order['column'], $order['value'])->get();
    }

    public function getDuty($member, $order)
    {
        return \Sdm\Member\Models\Duty::whereMemberId($member->id)->orderBy($order['column'], $order['value'])->get();
    }

    public function getPosition($member, $order)
    {
        return \Sdm\Member\Models\Position::whereMemberId($member->id)->orderBy($order['column'], $order['value'])->get();
    }

    public function getGrade($member, $order)
    {
        return \Sdm\Member\Models\Grade::whereMemberId($member->id)->orderBy($order['column'], $order['value'])->get();
    }

    public function getHonor($member, $order)
    {
        return \Sdm\Member\Models\Honor::whereMemberId($member->id)->orderBy($order['column'], $order['value'])->get();
    }

    public function getWork($member, $order)
    {
        return \Sdm\Member\Models\Work::whereMemberId($member->id)->orderBy($order['column'], $order['value'])->get();
    }

    public function getBrivet($member, $order)
    {
        return \Sdm\Member\Models\Brivet::whereMemberId($member->id)->orderBy($order['column'], $order['value'])->get();
    }
}
