<?php namespace Sdm\Core\Classes;

/**
 * Core Plugin Information File
 */
class UserManager
{
	public function getUser()
	{
		$user = \Session::get('userLogin');
		if($user) {
			$user = \Sdm\User\Models\User::find($user);
			return $user;
		}

		return false;
	}

	public function getMember()
	{
		$user = $this->getUser();
		if($user) {
			$member = \Sdm\Member\Models\Member::find($user->id);
			return $member;
		}
	}

	public function removeUser()
	{
		\Session::forget('userLogin');
	}
}
