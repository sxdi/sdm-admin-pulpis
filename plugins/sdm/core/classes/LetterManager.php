<?php namespace Sdm\Core\Classes;

/**
 * Core Plugin Information File
 */
class LetterManager
{
    public function processNominatifResidence($data)
    {
        // Create File
        $filename          = new \Sdm\Core\Classes\Generator;
        $filename          = $filename->make();

        $memberManager     = new \Sdm\Core\Classes\MemberManager;
        $kabagsumda        = $memberManager->getKabagsumda();
        if($kabagsumda) {
            $templateProcessor->setValues([
                'kabagsumda_name'  => $kabagsumda->name,
                'kabagsumda_grade' => $kabagsumda->getCurrentGrade() ? $kabagsumda->getCurrentGrade()->parent->code : '',
                'kabagsumda_nrp'   => $kabagsumda->nrp
            ]);
        }

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($data['path']);

        $residenceTemplate = [];
        foreach ($data['residences'] as $key => $residence) {
            $member   = $residence->member;
            $grade    = $member->getCurrentGrade();
            $position = $member->getCurrentPosition();

            switch ($residence->type) {
                case 'personal':
                    $type = 'Pribadi';
                    break;

                case 'official':
                    $type = 'Dinas';
                    break;

                case 'other':
                    $type = 'Lainnya';
                    break;
            }

            array_push($residenceTemplate, [
                'mapping_no'          => $key+1,
                'mapping_name'        => strtoupper($member->name),
                'mapping_nrp'         => $member->nrp,
                'mapping_grade'       => $grade ? strtoupper($grade->parent->code) : '',
                'mapping_position'    => $position ? strtoupper($position->name.' '.$position->department->name. ' '.$position->department->unit->name) : '',
                'mapping_type'        => $type,
                'mapping_address'     => $residence->address. ', '.$residence->regency.', '.$residence->province,
                'mapping_description' => '',
            ]);
        }
        $tmp = $data['temp'].'['.$data['letter']->name.'] '.$filename.'.docx';
        $templateProcessor->cloneRowAndSetValues('mapping_no', $residenceTemplate);
        $templateProcessor->saveAs($tmp);
        return $tmp;
    }

    public function processNominatifSearch($data)
    {
        // Create File
        $filename          = new \Sdm\Core\Classes\Generator;
        $filename          = $filename->make();

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($data['path']);

        $memberTemplate = [];
        foreach ($data['members'] as $key => $member) {
            $member = $member->member;
            $grade    = $member->getCurrentGrade();
            $position = $member->getCurrentPosition();

            array_push($memberTemplate, [
                'mapping_no'              => $key+1,
                'mapping_name'            => strtoupper($member->name),
                'mapping_nrp'             => $member->nrp,
                'mapping_grade'           => $grade ? strtoupper($grade->parent->code) : '',
                'mapping_position'        => $position ? strtoupper($position->name.' '.$position->department->name. ' '.$position->department->unit->name) : '',
                'mapping_position_skep'   => $position ? $position->number : '',
            ]);
        }
        $tmp = $data['temp'].'['.$data['letter']->name.'] '.$filename.'.docx';
        $templateProcessor->cloneRowAndSetValues('mapping_no', $memberTemplate);
        $templateProcessor->saveAs($tmp);
        return $tmp;
    }

    public function processPersonelProblem($data)
    {
        // Create File
        $filename          = new \Sdm\Core\Classes\Generator;
        $filename          = $filename->make();

        $memberManager     = new \Sdm\Core\Classes\MemberManager;
        $kabagsumda        = $memberManager->getKabagsumda();

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($data['path']);

        if($kabagsumda) {
            $templateProcessor->setValues([
                'kabagsumda_name'  => $kabagsumda->name,
                'kabagsumda_grade' => $kabagsumda->getCurrentGrade() ? $kabagsumda->getCurrentGrade()->parent->code : '',
                'kabagsumda_nrp'   => $kabagsumda->nrp
            ]);
        }

        $lawTemplate = [];
        foreach ($data['laws'] as $key => $law) {
            $member   = $law->member;
            $grade    = $member->getCurrentGrade();
            $position = $member->getCurrentPosition();

            array_push($lawTemplate, [
                'mapping_no'       => $key+1,
                'mapping_name'     => strtoupper($member->name),
                'mapping_nrp'      => $member->nrp,
                'mapping_grade'    => $grade ? strtoupper($grade->parent->code) : '',
                'mapping_position' => $position ? strtoupper($position->name.' '.$position->department->name. ' '.$position->department->unit->name) : '',
                'mapping_content'  => $law->content,
                'mapping_title'    => $law->title,
            ]);
        }
        $tmp = $data['temp'].'['.$data['letter']->name.'] '.$filename.'.docx';
        $templateProcessor->cloneRowAndSetValues('mapping_no', $lawTemplate);
        $templateProcessor->saveAs($tmp);
        return $tmp;
    }

    public function processPersonelSick($data)
    {
        // Create File
        $filename          = new \Sdm\Core\Classes\Generator;
        $filename          = $filename->make();

        $memberManager     = new \Sdm\Core\Classes\MemberManager;
        $kabagsumda        = $memberManager->getKabagsumda();

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($data['path']);

        if($kabagsumda) {
            $templateProcessor->setValues([
                'kabagsumda_name'  => $kabagsumda->name,
                'kabagsumda_grade' => $kabagsumda->getCurrentGrade() ? $kabagsumda->getCurrentGrade()->parent->code : '',
                'kabagsumda_nrp'   => $kabagsumda->nrp
            ]);
        }

        $healthTemplate = [];
        foreach ($data['healths'] as $key => $health) {
            $member   = $health->member;
            $grade    = $member->getCurrentGrade();
            $position = $member->getCurrentPosition();

            array_push($healthTemplate, [
                'mapping_no'                => $key+1,
                'mapping_name'              => strtoupper($member->name),
                'mapping_nrp'               => $member->nrp,
                'mapping_grade'             => $grade ? strtoupper($grade->parent->code) : '',
                'mapping_position'          => $position ? strtoupper($position->name.' '.$position->department->name. ' '.$position->department->unit->name) : '',
                'mapping_skep'              => $position ? $position->number : '',
                'mapping_description'       => $health->date->format('d F Y').' '.$health->title.' : '.$health->content
            ]);
        }
        $tmp = $data['temp'].'['.$data['letter']->name.'] '.$filename.'.docx';
        $templateProcessor->cloneRowAndSetValues('mapping_no', $healthTemplate);
        $templateProcessor->saveAs($tmp);
        return $tmp;
    }

    public function processKgbAttachment($data)
    {
        // Create File
        $filename    = new \Sdm\Core\Classes\Generator;
        $filename    = $filename->make();

        $reader      = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet = $reader->load($data['path']);
        $kgbSheet    = $spreadsheet->getActiveSheet('KGB');

        $index       = 18;
        $index2      = 19;
        $index3      = 20;
        $increase    = 3;
        foreach ($data['members'] as $member) {
            $member   = $member->member;
            $grade    = $member->getCurrentGrade();
            $position = $member->getCurrentPosition();
            $workTime = $member->getWorkTime();

            $kgbSheet->getCell('B'.$index)->setValue(strtoupper($member->name));
            $kgbSheet->getCell('B'.$index2)->setValue($member->dob->format('d-m-Y'));
            $kgbSheet->getCell('C'.$index)->setValue($grade ? $grade->parent->code . ' / '. $member->nrp : '');
            $kgbSheet->getCell('D'.$index)->setValue($grade ? $grade->parent->grade : '');
            $kgbSheet->getCell('E'.$index2)->setValue($workTime ? $workTime['date'] : '');
            $kgbSheet->getCell('E'.$index3)->setValue($workTime ? $workTime['count'] : '');

            $index  = $index + $increase;
            $index2 = $index2 + $increase;
            $index3 = $index3 + $increase;
        }

        $tmp    = $data['temp'].'['.$data['letter']->name.'] '.$filename.'.xls';
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $writer->save($tmp);
    }

    public function processDspComplete($data)
    {
        // Create File
        $filename      = new \Sdm\Core\Classes\Generator;
        $filename      = $filename->make();

        $reader        = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet   = $reader->load($data['path']);

        // Sheet1
        $cells       = 15;
        $perkapSheet = $spreadsheet->setActiveSheetIndexByName('PERKAP23');
        $unitPolres  = \Sdm\Master\Models\Unit::find(1);
        $departments = \Sdm\Master\Models\Department::whereUnitId($unitPolres->id)->orderBy('sort', 'asc')->get();
        foreach ($departments as $department) {
            $perkapSheet->getCell('C'.$cells)->setValue(strtoupper($department->name));

            // AKBP
            $countAkpb = \Sdm\Member\Models\Member::whereHas('positions', function($q) use($department){
                $q->whereIsNow(1);
                $q->whereDepartmentId($department->id);
            })->whereHas('grades', function($q) {
                $q->whereIsNow(1);
                $q->whereHas('parent', function($r){
                    $r->whereIn('code', ['AKBP']);
                });
            })->count();
            $perkapSheet->getCell('K'.$cells)->setValue($countAkpb);

            // KOMPOL
            $countKompol = \Sdm\Member\Models\Member::whereHas('positions', function($q) use($department){
                $q->whereIsNow(1);
                $q->whereDepartmentId($department->id);
            })->whereHas('grades', function($q) {
                $q->whereIsNow(1);
                $q->whereHas('parent', function($r){
                    $r->whereIn('code', ['KOMPOL']);
                });
            })->count();
            $perkapSheet->getCell('L'.$cells)->setValue($countKompol);

            // AKP
            $countAkp = \Sdm\Member\Models\Member::whereHas('positions', function($q) use($department){
                $q->whereIsNow(1);
                $q->whereDepartmentId($department->id);
            })->whereHas('grades', function($q) {
                $q->whereIsNow(1);
                $q->whereHas('parent', function($r){
                    $r->whereIn('code', ['AKP']);
                });
            })->count();
            $perkapSheet->getCell('M'.$cells)->setValue($countAkp);

            // IP
            $countIp = \Sdm\Member\Models\Member::whereHas('positions', function($q) use($department){
                $q->whereIsNow(1);
                $q->whereDepartmentId($department->id);
            })->whereHas('grades', function($q) {
                $q->whereIsNow(1);
                $q->whereHas('parent', function($r){
                    $r->whereIn('code', ['IPDA', 'IPTU']);
                });
            })->count();
            $perkapSheet->getCell('N'.$cells)->setValue($countIp);

            // BA
            $countBa = \Sdm\Member\Models\Member::whereHas('positions', function($q) use($department){
                $q->whereIsNow(1);
                $q->whereDepartmentId($department->id);
            })->whereHas('grades', function($q) {
                $q->whereIsNow(1);
                $q->whereHas('parent', function($r){
                    $r->whereIn('code', ['AIPTU', 'AIPDA', 'BRIPKA', 'BRIGPOL', 'BRIPTU', 'BRIPDA']);
                });
            })->count();
            $perkapSheet->getCell('O'.$cells)->setValue($countBa);

            // PNS
            $countPns = \Sdm\Member\Models\Member::whereHas('positions', function($q) use($department){
                $q->whereIsNow(1);
                $q->whereDepartmentId($department->id);
            })->whereHas('grades', function($q) {
                $q->whereIsNow(1);
                $q->whereHas('parent', function($r){
                    $r->whereIn('code', ['Pembina Utama', 'Pembina Utama Madya', 'Pembina Utama Muda', 'Pembina Tingkat I', 'Pembina', 'Penata Tingkat I', 'Penata', 'Penata Muda Tingkat I',  'Penata Muda', 'Pengatur Tingkat I', 'Pengatur', 'Pengatur Muda Tingkat I', 'Pengatur Muda', 'Juru Tingkat I',  'Juru', 'Juru Muda Tingkat I', 'Juru Muda']);
                });
            })->count();
            $perkapSheet->getCell('P'.$cells)->setValue($countPns);

            $cells++;
        }
        $cells       = 59;
        $unitPolsek  = \Sdm\Master\Models\Unit::whereNotIn('id', [1])->whereIsSystem(1)->orderBy('sort', 'asc')->get();
        foreach ($unitPolsek as $polsek) {
            $perkapSheet->getCell('C'.$cells)->setValue(strtoupper($polsek->name));

            // AKBP
            $countAkpb = \Sdm\Member\Models\Member::whereHas('positions', function($q) use($polsek){
                $q->whereIsNow(1);
                $q->whereHas('department', function($r) use($polsek) {
                    $r->whereUnitId($polsek->id);
                });
            })->whereHas('grades', function($q) {
                $q->whereIsNow(1);
                $q->whereHas('parent', function($r){
                    $r->whereIn('code', ['AKBP']);
                });
            })->count();
            $perkapSheet->getCell('K'.$cells)->setValue($countAkpb);

            // KOMPOL
            $countKompol = \Sdm\Member\Models\Member::whereHas('positions', function($q) use($polsek){
                $q->whereIsNow(1);
                $q->whereHas('department', function($r) use($polsek) {
                    $r->whereUnitId($polsek->id);
                });
            })->whereHas('grades', function($q) {
                $q->whereIsNow(1);
                $q->whereHas('parent', function($r){
                    $r->whereIn('code', ['KOMPOL']);
                });
            })->count();
            $perkapSheet->getCell('L'.$cells)->setValue($countKompol);

            // AKP
            $countAkp = \Sdm\Member\Models\Member::whereHas('positions', function($q) use($polsek){
                $q->whereIsNow(1);
                $q->whereHas('department', function($r) use($polsek) {
                    $r->whereUnitId($polsek->id);
                });
            })->whereHas('grades', function($q) {
                $q->whereIsNow(1);
                $q->whereHas('parent', function($r){
                    $r->whereIn('code', ['AKP']);
                });
            })->count();
            $perkapSheet->getCell('M'.$cells)->setValue($countAkp);

            // IP
            $countIp = \Sdm\Member\Models\Member::whereHas('positions', function($q) use($polsek){
                $q->whereIsNow(1);
                $q->whereHas('department', function($r) use($polsek) {
                    $r->whereUnitId($polsek->id);
                });
            })->whereHas('grades', function($q) {
                $q->whereIsNow(1);
                $q->whereHas('parent', function($r){
                    $r->whereIn('code', ['IPDA', 'IPTU']);
                });
            })->count();
            $perkapSheet->getCell('N'.$cells)->setValue($countIp);

            // BA
            $countBa = \Sdm\Member\Models\Member::whereHas('positions', function($q) use($polsek){
                $q->whereIsNow(1);
                $q->whereHas('department', function($r) use($polsek) {
                    $r->whereUnitId($polsek->id);
                });
            })->whereHas('grades', function($q) {
                $q->whereIsNow(1);
                $q->whereHas('parent', function($r){
                    $r->whereIn('code', ['AIPTU', 'AIPDA', 'BRIPKA', 'BRIGPOL', 'BRIPTU', 'BRIPDA']);
                });
            })->count();
            $perkapSheet->getCell('O'.$cells)->setValue($countBa);

            // PNS
            $countPns = \Sdm\Member\Models\Member::whereHas('positions', function($q) use($polsek){
                $q->whereIsNow(1);
                $q->whereHas('department', function($r) use($polsek) {
                    $r->whereUnitId($polsek->id);
                });
            })->whereHas('grades', function($q) {
                $q->whereIsNow(1);
                $q->whereHas('parent', function($r){
                    $r->whereIn('code', ['Pembina Utama', 'Pembina Utama Madya', 'Pembina Utama Muda', 'Pembina Tingkat I', 'Pembina', 'Penata Tingkat I', 'Penata', 'Penata Muda Tingkat I',  'Penata Muda', 'Pengatur Tingkat I', 'Pengatur', 'Pengatur Muda Tingkat I', 'Pengatur Muda', 'Juru Tingkat I',  'Juru', 'Juru Muda Tingkat I', 'Juru Muda']);
                });
            })->count();
            $perkapSheet->getCell('P'.$cells)->setValue($countPns);

            $cells++;
        }

        // Sheet2
        $cells    = 11;
        $golSheet = $spreadsheet->setActiveSheetIndexByName('GOL');
        foreach ($departments as $department) {
            $golSheet->getCell('B'.$cells)->setValue(strtoupper($department->name));
            $grades = ['AKBP', 'KOMPOL', 'AKP', 'IPTU', 'IPDA', 'AIPTU', 'AIPDA', 'BRIPKA', 'BRIGPOL', 'BRIPTU', 'BRIPDA'];
            $column = ['C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'];

            foreach ($grades as $key => $grade) {
                $counter = \Sdm\Member\Models\Member::whereHas('positions', function($q) use($department){
                    $q->whereIsNow(1);
                    $q->whereDepartmentId($department->id);
                })->whereHas('grades', function($q) use($grade) {
                    $q->whereIsNow(1);
                    $q->whereHas('parent', function($r) use($grade){
                        $r->whereIn('code', [$grade]);
                    });
                })->count();
                $golSheet->getCell($column[$key].$cells)->setValue($counter);
            }

            $cells++;
        }
        foreach ($unitPolsek as $polsek) {
            $golSheet->getCell('B'.$cells)->setValue(strtoupper($polsek->name));
            $grades = ['AKBP', 'KOMPOL', 'AKP', 'IPTU', 'IPDA', 'AIPTU', 'AIPDA', 'BRIPKA', 'BRIGPOL', 'BRIPTU', 'BRIPDA'];
            $column = ['C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'];

            foreach ($grades as $key => $grade) {
                $counter = \Sdm\Member\Models\Member::whereHas('positions', function($q) use($polsek){
                    $q->whereIsNow(1);
                    $q->whereDepartmentId($polsek->id);
                })->whereHas('grades', function($q) use($grade) {
                    $q->whereIsNow(1);
                    $q->whereHas('parent', function($r) use($grade){
                        $r->whereIn('code', [$grade]);
                    });
                })->count();
                $golSheet->getCell($column[$key].$cells)->setValue($counter);
            }

            $cells++;
        }

        $tmp    = $data['temp'].'['.$data['letter']->name.'] '.$filename.'.xls';
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $writer->save($tmp);
    }

    public function processMutationNews($data)
    {
        // Create File
        $filename   = new \Sdm\Core\Classes\Generator;
        $filename   = $filename->make();

        $memberManager     = new \Sdm\Core\Classes\MemberManager;
        $wakapolres        = $memberManager->getWakaPolres();
        $kabagsumda        = $memberManager->getKabagsumda();
        $kasipropam        = $memberManager->getKasipropam();
        $kasiwas           = $memberManager->getKasiwas();
        $paurminpers       = $memberManager->getPaurminpers();

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($data['path']);

        if($wakapolres) {
            $templateProcessor->setValues([
                'wakapolres_name'  => $wakapolres->name,
                'wakapolres_grade' => $wakapolres->getCurrentGrade() ? $wakapolres->getCurrentGrade()->parent->code : '',
                'wakapolres_nrp'   => $wakapolres->nrp
            ]);
        }

        if($kabagsumda) {
            $templateProcessor->setValues([
                'kabagsumda_name'  => $kabagsumda->name,
                'kabagsumda_grade' => $kabagsumda->getCurrentGrade() ? $kabagsumda->getCurrentGrade()->parent->code : '',
                'kabagsumda_nrp'   => $kabagsumda->nrp
            ]);
        }

        if($kasipropam) {
            $templateProcessor->setValues([
                'kasipropam_name'  => $kasipropam->name,
                'kasipropam_grade' => $kasipropam->getCurrentGrade() ? $kasipropam->getCurrentGrade()->parent->code : '',
                'kasipropam_nrp'   => $kasipropam->nrp
            ]);
        }

        if($kasiwas) {
            $templateProcessor->setValues([
                'kasiwas_name'  => $kasiwas->name,
                'kasiwas_grade' => $kasiwas->getCurrentGrade() ? $kasiwas->getCurrentGrade()->parent->code : '',
                'kasiwas_nrp'   => $kasiwas->nrp
            ]);
        }

        if($paurminpers) {
            $templateProcessor->setValues([
                'paurminpers_name'  => $paurminpers->name,
                'paurminpers_grade' => $paurminpers->getCurrentGrade() ? $paurminpers->getCurrentGrade()->parent->code : '',
                'paurminpers_nrp'   => $paurminpers->nrp
            ]);
        }

        $memberTemplate = [];
        foreach ($data['members'] as $key => $member) {
            $member = $member->member;
            $grade    = $member->getCurrentGrade();
            $position = $member->getCurrentPosition();

            array_push($memberTemplate, [
                'mutation_no'                => $key+1,
                'mutation_name'              => strtoupper($member->name),
                'mutation_nrp'               => $member->nrp,
                'mutation_grade'             => $grade ? strtoupper($grade->parent->code) : '',
                'mutation_position_old_name' => $position ? strtoupper($position->name.' '.$position->department->unit->name) : '',
                'mutation_position_started_at'=> $position ? $position->started_at->format('d-m-Y') : '',
            ]);
        }
        $tmp = $data['temp'].'['.$data['letter']->name.'] '.$filename.'.docx';
        $templateProcessor->cloneRowAndSetValues('mutation_no', $memberTemplate);
        $templateProcessor->saveAs($tmp);
        return $tmp;
    }

    public function processMutationTelegram($data)
    {
        // Create File
        $filename   = new \Sdm\Core\Classes\Generator;
        $filename   = $filename->make();

        $memberManager     = new \Sdm\Core\Classes\MemberManager;
        $kapolres          = $memberManager->getKapolres();

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($data['path']);

        if($kapolres) {
            $templateProcessor->setValues([
                'kapolres_name'  => $kapolres->name,
                'kapolres_grade' => $kapolres->getCurrentGrade() ? $kapolres->getCurrentGrade()->parent->code : '',
                'kapolres_nrp'   => $kapolres->nrp
            ]);
        }

        $memberTemplate = [];
        foreach ($data['members'] as $key => $member) {
            $member = $member->member;
            $grade    = $member->getCurrentGrade();
            $position = $member->getCurrentPosition();

            array_push($memberTemplate, [
                'mutation_no'                => $this->renderLatin($key+1),
                'mutation_name'              => strtoupper($member->name),
                'mutation_nrp'               => $member->nrp,
                'mutation_grade'             => $grade ? strtoupper($grade->parent->code) : '',
                'mutation_position_old_name' => $position ? strtoupper($position->name.' '.$position->department->unit->name) : '',
            ]);
        }
        $tmp = $data['temp'].'['.$data['letter']->name.'] '.$filename.'.docx';
        $templateProcessor->cloneRowAndSetValues('mutation_no', $memberTemplate);
        $templateProcessor->saveAs($tmp);
        return $tmp;
    }

    public function processMutationPlan($data)
    {
        // Create File
        $filename      = new \Sdm\Core\Classes\Generator;
        $filename      = $filename->make();

        $reader        = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet   = $reader->load($data['path']);

        $planSheet   = $spreadsheet->getActiveSheet('Sheet');
        $start       = 14;
        foreach ($data['members'] as $member) {
            $member = $member->member;
            $memberManager = new \Sdm\Core\Classes\MemberManager;
            $grade         = $member->getCurrentGrade();
            $position      = $member->getCurrentPosition();

            // Name
            $planSheet->getCell('B'.$start)->setValue(strtoupper($member->name));
            $planSheet->getCell('C'.$start)->setValue(strtoupper($grade ? $grade->parent->code.' / '.$member->nrp : ''));
            $planSheet->getCell('E'.$start)->setValue(strtoupper($position ? $position->name.' '.$position->department->unit->name : ''));
            $planSheet->getCell('F'.$start)->setValue(strtoupper($position ? $position->started_at->format('d-m-Y') : ''));

            // Positions
            $positions = $memberManager->getPosition($member, ['column' => 'started_at', 'value' => 'desc']);
            if(count($positions)) {
                $positionIndex = $start;
                foreach ($positions as $position) {
                    $planSheet->getCell('J'.$positionIndex)->setValue('- '. strtoupper($position->name.' '.$position->department->unit->name));
                    $positionIndex++;
                }
            }

            // Education Polries
            $educationPolries = $memberManager->getEducationPolries($member, ['column' => 'started_at', 'value' => 'desc']);
            if(count($educationPolries)) {
                $educationPolriesIndex = $start;
                foreach ($educationPolries as $education) {
                    $planSheet->getCell('H'.$educationPolriesIndex)->setValue('- '.strtoupper($education->name));
                    $educationPolriesIndex++;
                }
            }

            // Education Dikbangspes
            $educationDikbangspeses = $memberManager->getEducationDikbangspes($member, ['column' => 'started_at', 'value' => 'desc']);
            if(count($educationDikbangspeses)) {
                $educationDikbangspesesIndex = $start;
                foreach ($educationDikbangspeses as $education) {
                    $planSheet->getCell('I'.$educationDikbangspesesIndex)->setValue('- '.strtoupper($education->name));
                    $educationDikbangspesesIndex++;
                }
            }

            $nextIndex = [count($positions), count($educationPolries), count($educationDikbangspeses)];
            $start     = $start + max($nextIndex) + 1;
        }

        $tmp    = $data['temp'].'['.$data['letter']->name.'] '.$filename.'.xls';
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $writer->save($tmp);
    }

    public function processRequestKpi($data)
    {
        foreach ($data['members'] as $key => $member) {
            $member = $member->member;
            $memberManager        = new \Sdm\Core\Classes\MemberManager;
            $kabagsumda           = $memberManager->getKabagsumda();

            $filename             = new \Sdm\Core\Classes\Generator;
            $filename             = $filename->make();

            $kabagsumda          = $memberManager->getKabagsumda();

            $partners            = $memberManager->getMemberRelation($member, ['wife','husband']);
            $positions           = $memberManager->getPosition($member, ['column' => 'started_at', 'value' => 'desc']);
            $grades              = $memberManager->getGrade($member, ['column' => 'started_at', 'value' => 'desc']);

            $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($data['path']);
            $templateProcessor->setValues([
                'name'             => strtoupper($member->name),
                'nrp'              => $member->nrp,
                'place'            => $member->place,
                'dob'              => $member->dob->format('d F Y'),
                'religion'         => $member->religion,
            ]);

            if($kabagsumda) {
                $templateProcessor->setValues([
                    'kabagsumda_name'  => strtoupper($kabagsumda->name),
                    'kabagsumda_nrp'   => $kabagsumda->nrp,
                    'kabagsumda_grade' => $kabagsumda->getCurrentGrade() ? $kabagsumda->getCurrentGrade()->parent->code : '',
                ]);
            }

            if(count($partners)) {
                $partner = $partners[0];
                $templateProcessor->setValues([
                    'partner_name'     => $partner->name,
                    'partner_dob'      => $partner->dob->format('d F Y'),
                    'partner_place'    => $partner->place,
                    'partner_job'      => $partner->job,
                    'partner_religion' => $partner->religion,

                    'relation_place'   => $partner->relation_place,
                    'relation_date'    => $partner->relation_date,
                ]);
            }

            if(count($positions)) {
                $templateProcessor->setValues([
                    'grade' => $grades[0]->parent->code,
                ]);
            }

            if(count($grades)) {
                $templateProcessor->setValues([
                    'position' => $positions[0]->name,
                ]);
            }

            // Include Child
            $this->includeChild($member, $templateProcessor);

            // Include Grade
            $this->includeGrade($member, $templateProcessor);


            $templateProcessor->saveAs(storage_path('app/media/'.$filename.'.docx'));

            $requestMember             = new \Sdm\Letter\Models\RequestMember;
            $requestMember->request_id = $data['request']->id;
            $requestMember->member_id  = $member->id;
            $requestMember->save();

            $file = new \System\Models\File;
            $file->fromUrl('https://e-sdm.polrespulpis.com/storage/app/media/'.$filename.'.docx');
            $file->save();
            $requestMember->document()->add($file);
        }
    }

    public function processSarminAsabri($data)
    {
        $member        = $data['members'];
        $filename      = new \Sdm\Core\Classes\Generator;
        $filename      = $filename->make();

        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $kapolres      = $memberManager->getKapolres();
        $reader        = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        $spreadsheet   = $reader->load($data['path']);

        // Asabri
        $asabri     = $memberManager->getAttachment($member, 'asabri');
        $firstGrade = $memberManager->getGrade($member, ['column' => 'started_at', 'value' => 'asc']);
        $lastGrade  = $memberManager->getGrade($member, ['column' => 'started_at', 'value' => 'desc']);

        $sppSheet   = $spreadsheet->setActiveSheetIndexByName('SPP');
        $sppSheet->getCell('L16')->setValue($asabri->value);
        $sppSheet->getCell('L18')->setValue($member->name);
        $sppSheet->getCell('L20')->setValue($member->nrp);
        $sppSheet->getCell('L22')->setValue($member->dob->format('d'));
        $sppSheet->getCell('O22')->setValue($member->dob->format('m'));
        $sppSheet->getCell('R22')->setValue($member->dob->format('Y'));
        $sppSheet->getCell('B87')->setValue($kapolres->name);
        $sppSheet->getCell('U87')->setValue($member->name);

        $asSheet   = $spreadsheet->setActiveSheetIndexByName('43AS');
        $asSheet->getCell('A16')->setValue($asabri->value);
        $asSheet->getCell('J19')->setValue($member->name);
        $asSheet->getCell('J20')->setValue($lastGrade ? $lastGrade[0]->parent->code : '');
        $asSheet->getCell('J21')->setValue($member->dob->format('d'));
        $asSheet->getCell('M21')->setValue($member->dob->format('m'));
        $asSheet->getCell('P21')->setValue($member->dob->format('Y'));
        if($member->gender == 'male') {
            $asSheet->getCell('AA21')->setValue('v');
        }
        else {
            $asSheet->getCell('M21')->setValue('v');
        }
        $asSheet->getCell('W20')->setValue($lastGrade ? $lastGrade[0]->parent->grade : '');
        $asSheet->getCell('U64')->setValue($member->name);
        $asSheet->getCell('B64')->setValue($kapolres->name);
        $asSheet->getCell('B65')->setValue($kapolres->nrp);

        $rhSheet   = $spreadsheet->setActiveSheetIndexByName('DRHSINGKAT');
        $rhSheet->getCell('F8')->setValue($member->name);
        $rhSheet->getCell('F9')->setValue($member->nrp);
        $rhSheet->getCell('F10')->setValue($asabri ? $asabri->value : '');
        $rhSheet->getCell('F11')->setValue($lastGrade ? $lastGrade[0]->parent->code.' / '.$lastGrade[0]->parent->grade : '');
        $rhSheet->getCell('F13')->setValue($member->dob->format('d F Y'));
        $rhSheet->getCell('F14')->setValue($firstGrade ? $firstGrade[0]->started_at->format('d F Y') : '');
        $rhSheet->getCell('F15')->setValue($firstGrade ? $firstGrade[0]->number : '');
        $rhSheet->getCell('F20')->setValue($member->marital);
        $rhSheet->getCell('J60')->setValue($member->name);
        $rhSheet->getCell('B60')->setValue($kapolres->name);
        $rhSheet->getCell('B61')->setValue($kapolres->getCurrentGrade() ? $kapolres->getCurrentGrade()->code : '');

        $partner = $memberManager->getMemberRelation($member, ['wife', 'husband']);
        if(count($partner)) {
            $part = $partner[0];
            $rhSheet->getCell('A27')->setValue('1');
            $rhSheet->getCell('B27')->setValue($part->name);
            $rhSheet->getCell('C27')->setValue($part->dob->format('d F Y'));
            $rhSheet->getCell('F27')->setValue($part->relation_place);
            $rhSheet->getCell('G27')->setValue($part->relation_date->format('d F Y'));
        }

        $childs = $memberManager->getMemberRelation($member, ['child']);
        if(count($childs)) {
            $field = 34;
            $no    = 1;
            foreach ($childs as $child) {
                $rhSheet->getCell('A'.($field+$no))->setValue($no);
                $rhSheet->getCell('B'.($field+$no))->setValue($child->name);
                $rhSheet->getCell('C'.($field+$no))->setValue($child->dob->format('d F Y'));
                $rhSheet->getCell('F'.($field+$no))->setValue($child->education);
                $no++;
            }
        }

        $grades = $memberManager->getGrade($member, ['column' => 'started_at', 'value' => 'asc']);
        if(count($grades)) {
            $field = 42;
            $no    = 1;
            foreach ($grades as $grade) {
                $rhSheet->getCell('A'.($field+$no))->setValue($no);
                $rhSheet->getCell('B'.($field+$no))->setValue($grade->parent->code.'/'.$grade->parent->grade);
                if($grade->is_now) {
                    $rhSheet->getCell('C'.($field+$no))->setValue($grade->started_at->format('d F Y').' s/d Sekarang');
                }
                else {
                    $rhSheet->getCell('C'.($field+$no))->setValue($grade->started_at->format('d F Y').' s/d '.$grade->ended_at->format('d F Y'));
                }
                $no++;
            }
        }

        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $writer->save($data['temp'].$filename.'.xls');
        return $data['temp'].$filename.'.xls';
    }

    public function processAsabriPtdh($data)
    {
        $filename      = new \Sdm\Core\Classes\Generator;
        $filename      = $filename->make();

        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $kapolres      = $memberManager->getKapolres();

        foreach ($data['members'] as $key => $member) {
            $member = $member->member;
            $workTime          = $member->getWorkTime();
            $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($data['path']);
            $templateProcessor->setValues([
                'name'           => strtoupper($member->name),
                'nrp'            => $member->nrp,
                'position'       => $member->getCurrentPosition()->name,
                'grade'          => $member->getCurrentGrade()->parent->code,
                'age'            => $member->dob->age.' Tahun',
                'dob'            => $member->dob->format('d F Y'),
                'worked_at'      => $workTime['date'].' ('.$workTime['count'].')',

                'kapolres_name'  => strtoupper($kapolres->name),
                'kapolres_grade' => $kapolres->getCurrentGrade()->parent->code,
                'kapolres_nrp'   => $kapolres->nrp,
            ]);
            $templateProcessor->saveAs(storage_path('app/media/'.$filename.'.docx'));

            $requestMember             = new \Sdm\Letter\Models\RequestMember;
            $requestMember->request_id = $data['request']->id;
            $requestMember->member_id  = $member->id;
            $requestMember->save();

            $file = new \System\Models\File;
            $file->fromUrl('https://e-sdm.polrespulpis.com/storage/app/media/'.$filename.'.docx');
            $file->save();
            $requestMember->document()->add($file);
        }
    }

    public function processPersonel($data)
    {
        $member               = $data['members'];
        $memberManager        = new \Sdm\Core\Classes\MemberManager;

        $filename             = new \Sdm\Core\Classes\Generator;
        $filename             = $filename->make();

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($data['path']);
        if($member->picture) {
            $templateProcessor->setImageValue('picture', $member->picture->path);
        }
        $templateProcessor->setValues([
            'name'                 => strtoupper($member->name),
            'phone'                => $member->phone,
            'nrp'                  => $member->nrp,
            'place'                => $member->place,
            'gender'               => $member->gender == 'male' ? 'Pria' : 'Wanita',
            'dob'                  => $member->dob ? $member->dob->format('d F Y') : '',
            'blood'                => $member->blood,
            'religion'             => $member->religion,
            'marital'              => $member->marital,
            'nationality'          => $member->nationality,
            'skin_color'           => $member->skin_color,
            'hair_type'            => $member->nationality,
            'hair_color'           => $member->hair_color,
            'height'               => $member->height,
            'weight'               => $member->weight,
            'address'              => $member->address,
            'uh'                   => $member->uniform_head,
            'uf'                   => $member->uniform_feet,
            'ut'                   => $member->uniform_trousers,
            'uc'                   => $member->uniform_clothes,
            'child'                => $member->child,

            'ktp'                  => $member->getType('ktp') ? $member->getType('ktp')->value : '',
            'npwp'                 => $member->getType('npwp') ? $member->getType('npwp')->value : '',
            'kk'                   => $member->getType('kk') ? $member->getType('kk')->value : '',
            'finger'               => $member->getType('finger') ? $member->getType('finger')->value : '',
            'bpjs'                 => $member->getType('bpjs') ? $member->getType('bpjs')->value : '',
            'asabri'               => $member->getType('asabri') ? $member->getType('asabri')->value : '',
            'authority'            => $member->getType('authority') ? $member->getType('authority')->value : '',
            'investigator'         => $member->getType('investigator') ? $member->getType('investigator')->value : '',
            'member'               => $member->getType('member') ? $member->getType('member')->value : '',
            'kps'                  => $member->getType('kps') ? $member->getType('kps')->value : '',
            'simsa'                => $member->getType('simsa') ? $member->getType('simsa')->value : '',
        ]);

        // Partners
        if($memberManager->getMemberRelation($member, ['wife', 'husband'])) {
            $partners = $memberManager->getMemberRelation($member, ['wife', 'husband']);
            if(count($partners)) {
                $partner = $partners[0];
                $templateProcessor->setValues([
                    'partner_name'      => $partner->name,
                    'partner_gender'    => $partner->gender == 'male' ? 'Pria' : 'Wanita',
                    'partner_place'     => $partner->place,
                    'partner_dob'       => $partner->dob->format('d F Y'),
                    'partner_education' => $partner->education,
                    'partner_job'       => $partner->job,
                    'relation_place'    => $member->relation_place,
                    'relation_date'     => $member->relation_date
                ]);
            }
        }

        // Childs
        if($memberManager->getMemberRelation($member, ['child'])) {
            $childs = $memberManager->getMemberRelation($member, ['child']);
            $templateProcessor->setValues([
                'partner_child' => count($childs)
            ]);
        }

        // Worktime
        if($member->getWorkTime()) {
            $time = $member->getWorkTime();
            $templateProcessor->setValues([
                'worked_at' => $time['date']
            ]);
        }

        // Grade
        if($member->getCurrentGrade()) {
            $grade = $member->getCurrentGrade();
            $templateProcessor->setValues([
                'grade'     => $grade->parent->code,
                'grade_at'  => $grade->started_at->format('d F Y'),
                'grade_type'=> $grade->parent->grade,
            ]);
        }

        // Position
        if($member->getCurrentPosition()) {
            $position = $member->getCurrentPosition();
            $templateProcessor->setValues([
                'position'    => $position->name,
                'position_at' => $position->started_at->format('d F Y'),
                'unit'        => $position->department->unit->name
            ]);
        }

        // Education
        if($member->checkEducation(['formal'])->count()) {
            $education = $member->checkEducation(['formal'])->orderBy('started_at', 'desc')->first();
            $templateProcessor->setValues([
                'education_last_name'  => $education->education->name,
                'education_last_study' => $education->study,
            ]);
        }

        // Education Polri
        if($member->checkEducationPolri(['polri'])->count()) {
            $education = $member->checkEducationPolri(['polri'])->orderBy('started_at', 'desc')->first();
            $templateProcessor->setValues([
                'education_polri_last' => $education->name,
            ]);
        }

        // Education Dikbangspes
        if($member->checkEducationPolri(['dikbangspes'])->count()) {
            $education = $member->checkEducationPolri(['dikbangspes'])->orderBy('started_at', 'desc')->first();
            $templateProcessor->setValues([
                'education_dikbangspes_last' => $education->name,
            ]);
        }

        // Kapolres
        if($memberManager->getKapolres()) {
            $kapolres = $memberManager->getKapolres();
            $templateProcessor->setValues([
                'kapolres_name'     => strtoupper($kapolres->name),
                'kapolres_grade'    => $kapolres->getCurrentGrade() ? $kapolres->getCurrentGrade()->parent->code : '',
                'kapolres_nrp'      => $kapolres->nrp,
            ]);
        }

        // Kabagsumda
        if($memberManager->getKabagsumda()) {
            $kabagsumda = $memberManager->getKabagsumda();
            $templateProcessor->setValues([
                'kabagsumda_name'   => strtoupper($kabagsumda->name),
                'kabagsumda_grade'  => $kabagsumda->getCurrentGrade() ? $kabagsumda->getCurrentGrade()->parent->code : '',
                'kabagsumda_nrp'    => $kabagsumda->nrp,
            ]);
        }

        /**
         * KPI/KPS/KARSU
        */
        if(isset($data['kps_form'])) {
            $this->includeKpsForm($member, $templateProcessor);
        }

        /**
         * Riwayat Hidup
        */
        if(isset($data['rh_complete'])) {
            $this->includeRhComplete($member, $templateProcessor);
        }
        if(isset($data['rh_simple'])) {
            $this->includeRhSimple($member, $templateProcessor);
        }
        if(isset($data['rh_assesment'])) {
            $this->includeRhAssesment($member, $templateProcessor);
        }

        /**
         * BP4R
         */
        if(isset($data['bp4r_divorce'])) {
            $this->includeDivorce($member, $templateProcessor);
        }
        if(isset($data['bp4r_marriage'])) {
            $this->includeDivorce($member, $templateProcessor);
        }

        /**
         * Mutasi
        */
        if(isset($data['mutation_quote'])) {
            $this->includeMutationQuote($member, $templateProcessor);
        }
        if(isset($data['mutation_order'])) {
            $this->includeMutationOrder($member, $templateProcessor);
        }

        $tmp = $data['temp'].'['.$data['letter']->name.'] '.$filename.'.docx';
        $templateProcessor->saveAs($tmp);
        return $tmp;
    }

    public function processMapingPersonel($data)
    {
        $personelTemplate = [];
        $filename          = new \Sdm\Core\Classes\Generator;
        $filename          = $filename->make();

        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $kabagsumda    = $memberManager->getKabagsumda();

        // Data Looping
        foreach ($data['members'] as $key => $m) {
            $member = $m->member;
            $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($data['path']);
            array_push($personelTemplate,
                [
                    'mapping_no'          => ($key+1),
                    'mapping_name'        => $m->name,
                    'mapping_nrp'         => $m->nrp,
                    'mapping_position'    => $m->getCurrentPosition()->name,
                    'mapping_grade'       => $m->getCurrentGrade()->parent->code,
                    'mapping_skep'        => $m->getCurrentPosition()->number,
                    'mapping_description' => '',

                    'kabagsumda_name'     => $kabagsumda->name,
                    'kabagsumda_grade'    => $kabagsumda->grade,
                    'kabagsumda_nrp'      => $kabagsumda->nrp,
                ]
            );
            $templateProcessor->cloneRowAndSetValues('mapping_name', $personelTemplate);
        }

        $templateProcessor->saveAs(storage_path('app/media/'.$filename.'.docx'));

        // File Looping
        foreach ($data['members'] as $m) {
            $member = $b->member;
            $requestMember             = new \Sdm\Letter\Models\RequestMember;
            $requestMember->request_id = $data['request']->id;
            $requestMember->member_id  = $m->id;
            $requestMember->save();

            $file = new \System\Models\File;
            $file->fromUrl('https://e-sdm.polrespulpis.com/storage/app/media/'.$filename.'.docx');
            $file->save();
            $requestMember->document()->add($file);
        }
    }

    public function includeKpsForm($member, $templateProcessor)
    {
        $this->includeChild($member, $templateProcessor);
        $this->includeGrade($member, $templateProcessor);
    }

    public function includeRhComplete($member, $templateProcessor)
    {
        $this->includeFather($member, $templateProcessor);
        $this->includeMother($member, $templateProcessor);
        $this->includePartner($member, $templateProcessor);
        $this->includeChild($member, $templateProcessor);
        $this->includeLanguage($member, $templateProcessor);
        $this->includeLanguageForeign($member, $templateProcessor);
        $this->includeSport($member, $templateProcessor);
        $this->includeHonor($member, $templateProcessor);
        $this->includeEducation($member, $templateProcessor);
        $this->includeEducationNon($member, $templateProcessor);
        $this->includeEducationPolri($member, $templateProcessor);
        $this->includeEducationDikbangspes($member, $templateProcessor);
        $this->includeTraining($member, $templateProcessor);
        $this->includeDuty($member, $templateProcessor);
        $this->includePosition($member, $templateProcessor);
        $this->includeGrade($member, $templateProcessor);
        $this->includeWork($member, $templateProcessor);
        $this->includeBrivet($member, $templateProcessor);
    }

    public function includeRhSimple($member, $templateProcessor)
    {
        $memberManager = new \Sdm\Core\Classes\MemberManager;

        // Languages
        $languages = $memberManager->getLanguage($member);
        if(count($languages)) {
            $lang = '';
            foreach ($languages as $language) {
                $lang .= $language->name . ', ';
            }
            $templateProcessor->setValues([
                'languages' => $lang
            ]);
        }
        $languageForeigns = $memberManager->getLanguageForeign($member);
        if(count($languageForeigns)) {
            $lang = '';
            foreach ($languageForeigns as $language) {
                $lang .= $language->name . ', ';
           }
           $templateProcessor->setValues([
                'language_foreigns' => $lang
           ]);
        }

        // Educations
        $educations = $memberManager->getEducationFormal($member, ['column' => 'started_at', 'value' => 'desc']);
        if(count($educations)) {
            $edu           = '';
            $eduDate       = '';
            foreach ($educations as $education) {
                $edu     .= $education->education->code . '</w:t><w:br/><w:t>';
                $eduDate .= $education->started_at->format('Y') . '</w:t><w:br/><w:t>';
            }
            $templateProcessor->setValues([
                'education_dikums'      => $edu,
                'education_dikums_date' => $eduDate
            ]);
        }
        $educationPolries = $memberManager->getEducationPolries($member, ['column' => 'started_at', 'value' => 'desc']);
        if(count($educationPolries)) {
            $edu           = '';
            $eduDate       = '';
            foreach ($educationPolries as $education) {
                $edu     .= $education->name . '</w:t><w:br/><w:t>';
                $eduDate .= $education->started_at->format('Y') . '</w:t><w:br/><w:t>';
            }
            $templateProcessor->setValues([
                'education_polries'      => $edu,
                'education_polries_date' => $eduDate
            ]);
        }
        $educationDikbangspeses = $memberManager->getEducationDikbangspes($member, ['column' => 'started_at', 'value' => 'desc']);
        if(count($educationDikbangspeses)) {
            $edu           = '';
            $eduDate       = '';
            foreach ($educationDikbangspeses as $education) {
                $edu     .= $education->name . '</w:t><w:br/><w:t>';
                $eduDate .= $education->started_at->format('Y') . '</w:t><w:br/><w:t>';
            }
            $templateProcessor->setValues([
                'education_dikjurs'      => $edu,
                'education_dikjurs_date' => $eduDate
            ]);
        }

        // Duties
        $duties = $memberManager->getDuty($member, ['column' => 'started_at', 'value' => 'desc']);
        if(count($duties)) {
            $dut     = '';
            $dutDate = '';
            foreach ($duties as $duty) {
                $dut     .= $duty->name . '</w:t><w:br/><w:t>';
                $dutDate .= $duty->started_at->format('Y') . '</w:t><w:br/><w:t>';
            }
            $templateProcessor->setValues([
                'duties'      => $dut,
                'duties_date' => $dutDate
            ]);
        }

        // Positions
        $positions = $memberManager->getPosition($member, ['column' => 'started_at', 'value' => 'desc']);
        if(count($positions)) {
            $pos     = '';
            $posDate = '';
            foreach ($positions as $position) {
                $pos     .= $position->name. ' '.$position->department->unit->name. '</w:t><w:br/><w:t>';
                $posDate .= $position->started_at->format('Y') . '</w:t><w:br/><w:t>';
            }
            $templateProcessor->setValues([
                'positions'      => $pos,
                'positions_date' => $posDate
            ]);
        }

        // Grades
        $grades = $memberManager->getGrade($member, ['column' => 'started_at', 'value' => 'desc']);
        if(count($grades)) {
            $gr     = '';
            $grDate = '';
            foreach ($grades as $grade) {
                $gr     .= $grade->parent->code . '</w:t><w:br/><w:t>';
                $grDate .= $grade->started_at->format('Y') . '</w:t><w:br/><w:t>';
            }
            $templateProcessor->setValues([
                'grades'      => $gr,
                'grades_date' => $grDate
            ]);
        }

        // Honors
        $honors = $memberManager->getHonor($member, ['column' => 'honored_at', 'value' => 'desc']);
        if(count($honors)) {
            $hon     = '';
            $honDate = '';
            foreach ($honors as $honor) {
                $hon     .= $honor->name . '</w:t><w:br/><w:t>';
                $honDate .= $honor->honored_at->format('Y') . '</w:t><w:br/><w:t>';
            }
            $templateProcessor->setValues([
                'honors'      => $hon,
                'honors_date' => $honDate
            ]);
        }
    }

    public function includeRhAssesment($member, $templateProcessor)
    {
        $this->includePartner($member, $templateProcessor);
        $this->includeChild($member, $templateProcessor);
        $this->includeEducation($member, $templateProcessor);
        $this->includeEducationDikbangspes($member, $templateProcessor);
        $this->includeTraining($member, $templateProcessor);
        $this->includePosition($member, $templateProcessor);
    }

    public function includeDivorce($member, $templateProcessor)
    {
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $father        = $memberManager->getMemberRelation($member, ['father']);
        $mother        = $memberManager->getMemberRelation($member, ['mother']);
        $partner       = $memberManager->getMemberRelation($member, ['wife', 'husband']);

        if(count($father)) {
            $father = $father[0];
        }

        if(count($mother)) {
            $mother = $mother[0];
        }

        if(count($partner)) {
            $partner = $partner[0];
        }

        $templateProcessor->setValues([
            'father_name'      => $father->name,
            'father_place'     => $father->place,
            'father_dob'       => $father->dob->format('d F Y'),
            'father_education' => $father->education,
            'father_job'       => $father->job,
            'father_religion'  => $father->religion,
            'father_address'   => '',
        ]);

        $templateProcessor->setValues([
            'mother_name'      => $mother->name,
            'mother_place'     => $mother->place,
            'mother_dob'       => $mother->dob->format('d F Y'),
            'mother_education' => $mother->education,
            'mother_job'       => $mother->job,
            'mother_religion'  => $mother->religion,
            'mother_address'   => ''
        ]);

        $templateProcessor->setValues([
            'partner_name'      => $partner->name,
            'partner_place'     => $partner->place,
            'partner_dob'       => $partner->dob->format('d F Y'),
            'partner_education' => $partner->education,
            'partner_job'       => $partner->job,
            'partner_religion'  => $partner->religion,
            'partner_address'   => ''
        ]);
    }

    public function includeFather($member, $templateProcessor)
    {
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $fathers       = $memberManager->getMemberRelation($member, ['father']);
        if(count($fathers)) {
            $fatherTemplate = [];
            foreach ($fathers as $key => $father) {
                array_push($fatherTemplate, [
                    'father_no'        => '',
                    'father_name'      => $father->name,
                    'father_place'     => $father->place,
                    'father_dob'       => $father->dob->format('d F Y'),
                    'father_education' => $father->education,
                    'father_job'       => $father->job,
                ]);
            }
            $templateProcessor->cloneRowAndSetValues('father_name', $fatherTemplate);
        }
    }

    public function includeMother($member, $templateProcessor)
    {
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $mothers       = $memberManager->getMemberRelation($member, ['mother']);
        if(count($mothers)) {
            $motherTemplate = [];
            foreach ($mothers as $key => $mother) {
                array_push($motherTemplate, [
                    'mother_no'        => '',
                    'mother_name'      => $mother->name,
                    'mother_place'     => $mother->place,
                    'mother_dob'       => $mother->dob->format('d F Y'),
                    'mother_education' => $mother->education,
                    'mother_job'       => $mother->job,
                ]);
            }
            $templateProcessor->cloneRowAndSetValues('mother_name', $motherTemplate);
        }
    }

    public function includePartner($member, $templateProcessor)
    {
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $partners      = $memberManager->getMemberRelation($member, ['wife','husband']);
        if(count($partners)) {
            $partnerTemplate = [];
            foreach ($partners as $key => $partner) {
                array_push($partnerTemplate, [
                    'partner_no'        => ($key+1),
                    'partner_name'      => $partner->name,
                    'partner_gender'    => $partner->gender == 'male' ? 'Pria' : 'Wanita',
                    'partner_place'     => $partner->place,
                    'partner_dob'       => $partner->dob->format('d F Y'),
                    'partner_education' => $partner->education,
                    'partner_job'       => $partner->job,

                    'relation_place'    => $member->relation_place,
                    'relation_date'     => $member->relation_date
                ]);
            }
            $templateProcessor->cloneRowAndSetValues('partner_no', $partnerTemplate);
        }
    }

    public function includeChild($member, $templateProcessor)
    {
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $childs        = $memberManager->getMemberRelation($member, ['child']);
        if(count($childs)) {
            $childTemplate = [];
            foreach ($childs as $key => $child) {
                array_push($childTemplate, [
                    'child_no'          => ($key+1),
                    'child_name'        => $child->name,
                    'child_gender'      => $child->gender == 'male' ? 'Pria' : 'Wanita',
                    'child_place'       => $child->place,
                    'child_dob'         => $child->dob->format('d F Y'),
                    'child_education'   => $child->education ?: '',
                    'child_job'         => $child->job ?: '-',
                    'child_description' => $child->content,
                ]);
            }
            $templateProcessor->cloneRowAndSetValues('child_name', $childTemplate);
        }
    }

    public function includeLanguage($member, $templateProcessor)
    {
        $languages = \Sdm\Member\Models\Language::whereMemberId($member->id)->whereIsForeign(0)->get();
        if(count($languages)) {
            $languageTemplate = [];
            foreach ($languages as $key => $language) {
                array_push($languageTemplate, [
                    'language_no'     => ($key +1),
                    'language_name'   => $language->name,
                    'language_status' => $language->result == 'active' ? 'Aktif' : 'Pasif',
                ]);
            }
            $templateProcessor->cloneRowAndSetValues('language_no', $languageTemplate);
        }
    }

    public function includeLanguageForeign($member, $templateProcessor)
    {
        $languageForeigns = \Sdm\Member\Models\Language::whereMemberId($member->id)->whereIsForeign(1)->get();
        if(count($languageForeigns)) {
            $languageForeignTemplate = [];
            foreach ($languageForeigns as $key => $language) {
                array_push($languageForeignTemplate, [
                    'language_foreign_no'     => ($key +1),
                    'language_foreign_name'   => $language->name,
                    'language_foreign_status' => $language->result == 'active' ? 'Aktif' : 'Pasif',
                ]);
            }
            $templateProcessor->cloneRowAndSetValues('language_foreign_no', $languageForeignTemplate);
        }
    }

    public function includeSport($member, $templateProcessor)
    {
        $sports = \Sdm\Member\Models\Sport::whereMemberId($member->id)->get();
        if(count($sports)) {
            $sportTemplates = [];
            foreach ($sports as $key => $sport) {
                array_push($sportTemplates, [
                    'sport_no'          => ($key +1),
                    'sport_name'        => $sport->name,
                    'sport_description' => $sport->content,
                ]);
            }
            $templateProcessor->cloneRowAndSetValues('sport_no', $sportTemplates);
        }
    }

    public function includeHonor($member, $templateProcessor)
    {
        $honors = \Sdm\Member\Models\Honor::whereMemberId($member->id)->orderBy('honored_at', 'desc')->get();
        if(count($honors)) {
            $honorTemplates = [];
            foreach ($honors as $key => $honor) {
                array_push($honorTemplates, [
                    'honor_no'          => ($key +1),
                    'honor_name'        => $honor->name,
                    'honor_claimed_at'  => $honor->honored_at->format('d F Y'),
                ]);
            }
            $templateProcessor->cloneRowAndSetValues('honor_no', $honorTemplates);
        }
    }

    public function includeEducation($member, $templateProcessor)
    {
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $educations    = \Sdm\Member\Models\Education::whereType('formal')->whereMemberId($member->id)->orderBy('started_at', 'desc')->get();
        if(count($educations)) {
            $educationTemplate = [];
            foreach ($educations as $key => $education) {
                array_push($educationTemplate, [
                    'education_no'           => ($key +1),
                    'education_name'         => $education->name,
                    'education_code'         => $education->education->code,
                    'education_study'        => $education->study,
                    'education_location'     => $education->location,
                    'education_is_graduated' => $education->is_now ? '-' : $education->is_graduated ? 'Lulus' : 'Tidak Lulus',
                    'education_started_at'   => $education->started_at->format('d F Y'),
                    'education_ended_at'     => $education->is_now ? 'Sekarang' : $education->ended_at->format('d F Y'),
                ]);

                if(!$education->is_now) {
                    $educationTemplate[$key]['education_range'] = $memberManager->getMothRange($education->started_at, $education->ended_at);
                }
                else {
                    $educationTemplate[$key]['education_range'] = $memberManager->getMothRange($education->started_at, date('Y-m-d'));
                }
            }
            $templateProcessor->cloneRowAndSetValues('education_no', $educationTemplate);
        }
    }

    public function includeEducationNon($member, $templateProcessor)
    {
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $educationNons = \Sdm\Member\Models\Education::whereType('non')->whereMemberId($member->id)->orderBy('started_at', 'desc')->get();
        if(count($educationNons)) {
            $educationNonTemplate = [];
            foreach ($educationNons as $key => $education) {
                array_push($educationNonTemplate, [
                    'education_non_no'           => ($key +1),
                    'education_non_name'         => $education->name,
                    'education_non_rank'         => $education->result,
                    'education_non_location'     => $education->location,
                    'education_non_is_graduated' => $education->is_now ? '-' : $education->is_graduated ? 'Lulus' : 'Tidak Lulus',
                    'education_non_started_at'   => $education->started_at->format('d F Y'),
                    'education_non_ended_at'     => $education->is_now ? 'Sekarang' : $education->ended_at->format('d F Y'),
                ]);
                if(!$education->is_now) {
                    $educationNonTemplate[$key]['education_non_range'] = $memberManager->getMothRange($education->started_at, $education->ended_at);
                }
                else {
                    $educationNonTemplate[$key]['education_non_range'] = $memberManager->getMothRange($education->started_at, date('Y-m-d'));
                }
            }
            $templateProcessor->cloneRowAndSetValues('education_non_no', $educationNonTemplate);
        }
    }

    public function includeEducationPolri($member, $templateProcessor)
    {
        $memberManager    = new \Sdm\Core\Classes\MemberManager;
        $educationPolries = \Sdm\Member\Models\EducationPolice::whereType('polri')->whereMemberId($member->id)->orderBy('started_at', 'desc')->get();
        if(count($educationPolries)) {
            $educationPolriTemplate = [];
            foreach ($educationPolries as $key => $education) {
                array_push($educationPolriTemplate, [
                    'education_polri_no'           => ($key +1),
                    'education_polri_name'         => $education->name,
                    'education_polri_rank'         => $education->result,
                    'education_polri_place'        => $education->location,
                    'education_polri_is_graduated' => $education->is_now ? '-' : $education->is_graduated ? 'Lulus' : 'Tidak Lulus',
                    'education_polri_started_at'   => $education->started_at->format('d F Y'),
                    'education_polri_ended_at'     => $education->is_now ? 'Sekarang' : $education->ended_at->format('d F Y'),
                ]);
                if(!$education->is_now) {
                    $educationPolriTemplate[$key]['education_polri_range'] = $memberManager->getMothRange($education->started_at, $education->ended_at);
                }
                else {
                    $educationPolriTemplate[$key]['education_polri_range'] = $memberManager->getMothRange($education->started_at, date('Y-m-d'));
                }
            }
            $templateProcessor->cloneRowAndSetValues('education_polri_no', $educationPolriTemplate);
        }
    }

    public function includeEducationDikbangspes($member, $templateProcessor)
    {
        $memberManager          = new \Sdm\Core\Classes\MemberManager;
        $educationDikbangspeses = \Sdm\Member\Models\EducationPolice::whereType('dikbangspes')->whereMemberId($member->id)->orderBy('started_at', 'desc')->get();
        if(count($educationDikbangspeses)) {
            $educationDikbangspesTemplate = [];
            foreach ($educationDikbangspeses as $key => $education) {
                array_push($educationDikbangspesTemplate, [
                    'education_dikbangspes_no'           => ($key +1),
                    'education_dikbangspes_name'         => $education->name,
                    'education_dikbangspes_rank'         => $education->result,
                    'education_dikbangspes_location'     => $education->location,
                    'education_dikbangspes_is_graduated' => $education->is_now ? '-' : $education->is_graduated ? 'Lulus' : 'Tidak Lulus',
                    'education_dikbangspes_started_at'   => $education->started_at->format('d F Y'),
                    'education_dikbangspes_ended_at'     => $education->is_now ? 'Sekarang' : $education->ended_at->format('d F Y'),
                ]);
                if(!$education->is_now) {
                    $educationDikbangspesTemplate[$key]['education_dikbangspes_range'] = $memberManager->getMothRange($education->started_at, $education->ended_at);
                }
                else {
                    $educationDikbangspesTemplate[$key]['education_dikbangspes_range'] = $memberManager->getMothRange($education->started_at, date('Y-m-d'));
                }
            }
            $templateProcessor->cloneRowAndSetValues('education_dikbangspes_no', $educationDikbangspesTemplate);
        }
    }

    public function includeTraining($member, $templateProcessor)
    {
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $trainings     = \Sdm\Member\Models\Training::whereMemberId($member->id)->orderBy('started_at', 'desc')->get();
        if(count($trainings)) {
            $trainingTemplate = [];
            foreach ($trainings as $key => $training) {
                array_push($trainingTemplate, [
                    'training_no'           => ($key +1),
                    'training_name'         => $training->name,
                    'training_rank'         => $training->result,
                    'training_location'     => $training->location,
                    'training_is_graduated' => $training->is_now ? '-' : $training->is_graduated ? 'Lulus' : 'Tidak Lulus',
                    'training_started_at'   => $training->started_at->format('d F Y'),
                    'training_ended_at'     => $training->is_now ? 'Sekarang' : $training->ended_at->format('d F Y'),
                ]);
                if(!$training->is_now) {
                    $trainingTemplate[$key]['training_range'] = $memberManager->getMothRange($training->started_at, $training->ended_at);
                }
                else {
                    $trainingTemplate[$key]['training_range'] = $memberManager->getMothRange($training->started_at, date('Y-m-d'));
                }
            }
            $templateProcessor->cloneRowAndSetValues('training_no', $trainingTemplate);
        }
    }

    public function includeDuty($member, $templateProcessor)
    {
        $duties        = \Sdm\Member\Models\Duty::whereMemberId($member->id)->orderBy('started_at', 'desc')->get();
        if(count($duties)) {
            $dutyTemplate = [];
            foreach ($duties as $key => $duty) {
                array_push($dutyTemplate, [
                    'duty_no'         => ($key + 1),
                    'duty_origin'     => $duty->origin,
                    'duty_name'       => $duty->name,
                    'duty_started_at' => $duty->started_at->format('d F Y'),
                    'duty_ended_at'   => $duty->is_now ? 'Sekarang' : $duty->ended_at->format('d F Y'),
                ]);
            }
            $templateProcessor->cloneRowAndSetValues('duty_no', $dutyTemplate);
        }
    }

    public function includePosition($member, $templateProcessor)
    {
        $positions = \Sdm\Member\Models\Position::whereMemberId($member->id)->orderBy('started_at', 'desc')->get();
        if(count($positions)) {
            $positionTemplate = [];
            foreach ($positions as $key => $position) {
                array_push($positionTemplate, [
                    'position_no'         => ($key + 1),
                    'position_department' => $position->department->name,
                    'position_unit'       => $position->department->unit->name,
                    'position_name'       => $position->name,
                    'position_skep'       => $position->number,
                    'position_started_at' => $position->started_at->format('d F Y'),
                    'position_ended_at'   => $position->is_now ? 'Sekarang' : $position->ended_at->format('d F Y'),
                ]);
            }
            $templateProcessor->cloneRowAndSetValues('position_no', $positionTemplate);
        }
    }

    public function includeGrade($member, $templateProcessor)
    {
        $memberManager = new \Sdm\Core\Classes\MemberManager;
        $grades        = $memberManager->getGrade($member, ['column' => 'started_at', 'value' => 'desc']);
        if(count($grades)) {
            $gradeTemplate = [];
            foreach ($grades as $key => $grade) {
                array_push($gradeTemplate, [
                    'grade_no'         => ($key + 1),
                    'grade_name'       => $grade->parent->code,
                    'grade_skep'       => $grade->number,
                    'grade_started_at' => $grade->started_at->format('d F Y'),
                    'grade_ended_at'   => $grade->is_now ? 'Sekarang' : $grade->ended_at->format('d F Y'),
                ]);
            }
            $templateProcessor->cloneRowAndSetValues('grade_no', $gradeTemplate);
        }
    }

    public function includeWork($member, $templateProcessor)
    {
        $works = \Sdm\Member\Models\Work::whereMemberId($member->id)->orderBy('started_at', 'desc')->get();
        if(count($works)) {
            $workTemplate = [];
            foreach ($works as $key => $work) {
                array_push($workTemplate, [
                    'work_no'         => ($key + 1),
                    'work_name'       => $work->name,
                    'work_skep'       => $work->number,
                    'work_location'   => $work->location,
                    'work_started_at' => $work->started_at->format('d F Y'),
                    'work_ended_at'   => $work->is_now ? 'Sekarang' : $work->ended_at->format('d F Y'),
                ]);
            }
            $templateProcessor->cloneRowAndSetValues('work_no', $workTemplate);
        }
    }

    public function includeBrivet($member, $templateProcessor)
    {
        $brivets = \Sdm\Member\Models\Brivet::whereMemberId($member->id)->orderBy('claimed_at', 'desc')->get();
        if(count($brivets)) {
            $brivetTemplate = [];
            foreach ($brivets as $key => $brivet) {
                array_push($brivetTemplate, [
                    'brivet_no'         => ($key + 1),
                    'brivet_name'       => $brivet->name,
                    'brivet_source'     => $brivet->source,
                    'brivet_claimed_at' => $brivet->claimed_at->format('d F Y'),
                ]);
            }
            $templateProcessor->cloneRowAndSetValues('brivet_no', $brivetTemplate);
        }
    }

    public function includeMutationQuote($member, $templateProcessor)
    {
        $position = $member->getCurrentPosition();
        if($position) {
            $templateProcessor->setValues([
                'position_old_name'       => $position->name.' '.$position->department->unit->name,
                'position_old_started_at' => $position->started_at->format('d-m-Y')
            ]);
        }
    }

    public function includeMutationOrder($member, $templateProcessor)
    {
        $position = $member->getCurrentPosition();
        if($position) {
            $templateProcessor->setValues([
                'position_name' => $position->name.' '.$position->department->unit->name,
            ]);
        }
    }

    public function renderLatin($number)
    {
        switch ($number) {
            case 1:
                return 'SATU';
                break;

            case 2:
                return 'DUA';
                break;

            case 3:
                return 'TIGA';
                break;

            case 4:
                return 'EMPAT';
                break;

            case 5:
                return 'LIMA';
                break;

            case 6:
                return 'ENAM';
                break;

            case 7:
                return 'TUJUH';
                break;

            case 8:
                return 'DELAPAN';
                break;

            case 9:
                return 'SEMBILAN';
                break;

            case 10:
                return 'SEPULUH';
                break;

            default:
                return '';
                break;
        }
    }
}
