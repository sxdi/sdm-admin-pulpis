<?php namespace Sdm\Core\Classes;

/**
 * Core Plugin Information File
 */
class Generator
{
    public function make()
    {
        return md5(\Hash::make(date('Y-m-d H:i:s').rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9).rand(0,9)));
    }

    public function delTree($dir) {
       $files = array_diff(scandir($dir), array('.','..'));
        foreach ($files as $file) {
          (is_dir("$dir/$file")) ? $this->delTree("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
      }
}
