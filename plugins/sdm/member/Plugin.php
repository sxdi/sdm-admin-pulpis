<?php namespace Sdm\Member;

use Backend;
use System\Classes\PluginBase;

/**
 * Member Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Member',
            'description' => 'No description provided yet...',
            'author'      => 'Sdm',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Sdm\Member\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'sdm.member.some_permission' => [
                'tab' => 'Member',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'member' => [
                'label'       => 'Member',
                'url'         => Backend::url('sdm/member/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['sdm.member.*'],
                'order'       => 500,
            ],
        ];
    }
}
