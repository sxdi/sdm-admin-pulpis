<?php namespace Sdm\Member\Models;

use Model;
use Carbon\Carbon;

/**
 * Member Model
 */
class Member extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'sdm_member_members';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'name',
        'nrp',
        'phone',
        'gender',
        'child',
        'blood',
        'religion',
        'place',
        'dob',
        'marital',
        'nationality',
        'skin_color',
        'hair_type',
        'hair_color',
        'height',
        'weight',
        'address',
        'uniform_head',
        'uniform_trousers',
        'uniform_clothes',
        'uniform_feet',
    ];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'dob'
    ];

    /**
     * @var array Relations
    */
    public $hasOne        = [];
    public $hasMany       = [
        'residences'    => [
            'Sdm\Member\Models\Residence',
            'key'     => 'member_id',
            'otherKey'=> 'id'
        ],
        'families'    => [
            'Sdm\Member\Models\Family',
            'key'     => 'member_id',
            'otherKey'=> 'id'
        ],
        'attachments' => [
            'Sdm\Member\Models\Attachment',
            'key'      => 'member_id',
            'otherKey' => 'id'
        ],
        'languages' => [
            'Sdm\Member\Models\Language',
            'key'      => 'member_id',
            'otherKey' => 'id'
        ],
        'sports' => [
            'Sdm\Member\Models\Sport',
            'key'      => 'member_id',
            'otherKey' => 'id'
        ],
        'honors' => [
            'Sdm\Member\Models\Honor',
            'key'      => 'member_id',
            'otherKey' => 'id'
        ],
        'educations' => [
            'Sdm\Member\Models\Education',
            'key'      => 'member_id',
            'otherKey' => 'id'
        ],
        'educationPolries' => [
            'Sdm\Member\Models\EducationPolice',
            'key'      => 'member_id',
            'otherKey' => 'id'
        ],
        'trainings' => [
            'Sdm\Member\Models\Training',
            'key'      => 'member_id',
            'otherKey' => 'id'
        ],
        'duties' => [
            'Sdm\Member\Models\Duty',
            'key'      => 'member_id',
            'otherKey' => 'id'
        ],
        'positions' => [
            'Sdm\Member\Models\Position',
            'key'      => 'member_id',
            'otherKey' => 'id'
        ],
        'grades' => [
            'Sdm\Member\Models\Grade',
            'key'      => 'member_id',
            'otherKey' => 'id'
        ],
        'works' => [
            'Sdm\Member\Models\Work',
            'key'      => 'member_id',
            'otherKey' => 'id'
        ],
        'brivets' => [
            'Sdm\Member\Models\Brivet',
            'key'      => 'member_id',
            'otherKey' => 'id'
        ],
        'salaries' => [
            'Sdm\Member\Models\Salary',
            'key'      => 'member_id',
            'otherKey' => 'id'
        ]
    ];
    public $belongsTo     = [
        'user' => [
            'Sdm\User\Models\User',
            'key'      => 'id',
            'otherKey' => 'member_id'
        ]
    ];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [
        'picture' => ['System\Models\File']
    ];
    public $attachMany    = [];

    public function getToken()
    {
        return \Sdm\User\Models\Token::whereUserId($this->user->id)->get()->pluck('token');
    }

    public function getEducationBy($type)
    {
        return \Sdm\Member\Models\Education::whereMemberId($this->id)->whereType('type')->get();
    }

    public function getType($type)
    {
        $attachment = \Sdm\Member\Models\Attachment::whereType($type)->whereMemberId($this->id)->first();
        if($attachment) {
            return $attachment;
        }
    }

    public function getWorkTime()
    {
        $work = \Sdm\Member\Models\Grade::whereMemberId($this->id)->orderBy('started_at', 'asc')->first();
        if($work) {
            $month = number_format(Carbon::parse($work->started_at)->diffInMonths() / 12, 2, '.', '');
            $total = explode('.', $month);
            return [
                'date'  => Carbon::parse($work->started_at)->format('d F Y'),
                'count' => $total[0].' Tahun '.$total[1].' Bulan'
            ];
        }

        return false;
    }

    public function getCurrentGrade()
    {
        $grade = \Sdm\Member\Models\Grade::whereMemberId($this->id)->whereIsNow(1)->first();
        return $grade;
    }

    public function getCurrentPosition()
    {
        $position = \Sdm\Member\Models\Position::whereMemberId($this->id)->whereIsNow(1)->first();
        return $position;
    }

    public function getCurrentSalary()
    {
        $grade    = $this->getCurrentGrade();
        if($grade) {
            $salary   = \Sdm\Member\Models\Salary::whereMemberId($this->id)->whereGradeId($grade->id)->orderBy('started_at', 'desc')->first();
            return $salary;
        }
    }

    public function checkAttachment($type)
    {
        $result = [];
        foreach ($type as $t) {
            $query = \Sdm\Member\Models\Attachment::whereMemberId($this->id)->whereIn('type', [$t])->count();
            if($query) {
                array_push($result, $t);
            }
        }

        if(count($type) == count($result)) {
            return true;
        }

        return false;
    }

    public function checkFamily($relation)
    {
        return \Sdm\Member\Models\Family::whereMemberId($this->id)->whereIn('relation', $relation);
    }

    public function checkResidence()
    {
        return \Sdm\Member\Models\Residence::whereMemberId($this->id);
    }

    public function checkEducation($type)
    {
        return \Sdm\Member\Models\Education::whereMemberId($this->id)->whereIn('type', $type);
    }

    public function checkEducationPolri($type)
    {
        return \Sdm\Member\Models\EducationPolice::whereMemberId($this->id)->whereIn('type', $type);
    }

    public function checkLanguage($type)
    {
        return \Sdm\Member\Models\Language::whereMemberId($this->id)->whereIsForeign($type);
    }

    public function checkSport()
    {
        return \Sdm\Member\Models\Sport::whereMemberId($this->id);
    }

    public function checkHonor()
    {
        return \Sdm\Member\Models\Honor::whereMemberId($this->id);
    }

    public function checkTraining()
    {
        return \Sdm\Member\Models\Training::whereMemberId($this->id);
    }

    public function checkDuty()
    {
        return \Sdm\Member\Models\Duty::whereMemberId($this->id);
    }

    public function checkPosition()
    {
        return \Sdm\Member\Models\Position::whereMemberId($this->id);
    }

    public function checkGrade()
    {
        return \Sdm\Member\Models\Grade::whereMemberId($this->id);
    }

    public function checkWork()
    {
        return \Sdm\Member\Models\Work::whereMemberId($this->id);
    }

    public function checkBrivet()
    {
        return \Sdm\Member\Models\Brivet::whereMemberId($this->id);
    }

    public function checkSalary()
    {
        return \Sdm\Member\Models\Salary::whereMemberId($this->id);
    }
}
