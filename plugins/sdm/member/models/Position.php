<?php namespace Sdm\Member\Models;

use Model;

/**
 * Position Model
 */
class Position extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'sdm_member_positions';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'member_id',
        'status',
        'message'
    ];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'started_at',
        'ended_at'
    ];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [
        // 'position' => [
        //     'Sdm\Master\Models\Position',
        //     'key'      => 'position_id',
        //     'otherKey' => 'id'
        // ],
        'department' => [
            'Sdm\Master\Models\Department',
            'key'      => 'department_id',
            'otherKey' => 'id'
        ],
        'member' => [
            'Sdm\Member\Models\Member',
            'key'      => 'member_id',
            'otherKey' => 'id'
        ],
    ];
    public $belongsToMany = [];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [
        'document' => ['System\Models\File']
    ];
    public $attachMany    = [];
}
