<?php namespace Sdm\Member\Models;

use Model;

/**
 * Health Model
 */
class Health extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'sdm_member_healths';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'member_id',
        'admin_id'
    ];

    /**
     * @var array Validation rules for attributes
     */
    public $rules = [];

    /**
     * @var array Attributes to be cast to native types
     */
    protected $casts = [];

    /**
     * @var array Attributes to be cast to JSON
     */
    protected $jsonable = [];

    /**
     * @var array Attributes to be appended to the API representation of the model (ex. toArray())
     */
    protected $appends = [];

    /**
     * @var array Attributes to be removed from the API representation of the model (ex. toArray())
     */
    protected $hidden = [];

    /**
     * @var array Attributes to be cast to Argon (Carbon) instances
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'date'
    ];

    /**
     * @var array Relations
     */
    public $hasOne        = [];
    public $hasMany       = [];
    public $belongsTo     = [
        'member' => [
            'Sdm\Member\Models\Member',
            'key'      => 'member_id',
            'otherKey' => 'id'
        ],
        'admin' => [
            'Sdm\Member\Models\Member',
            'key'      => 'admin_id',
            'otherKey' => 'id'
        ],
    ];
    public $belongsToMany = [
        'tags' => [
            'Sdm\Health\Models\Tag',
            'table'    => 'sdm_member_health_tags',
            'key'      => 'health_id',
            'otherKey' => 'tag_id'
        ]
    ];
    public $morphTo       = [];
    public $morphOne      = [];
    public $morphMany     = [];
    public $attachOne     = [
        'document' => ['System\Models\File']
    ];
    public $attachMany    = [];
}
