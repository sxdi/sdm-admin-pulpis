<?php namespace Sdm\Member\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateGradesTable extends Migration
{
    public function up()
    {
        Schema::create('sdm_member_grades', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('member_id');
            $table->string('name');
            $table->string('number');
            $table->date('started_at');
            $table->date('ended_at');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sdm_member_grades');
    }
}
