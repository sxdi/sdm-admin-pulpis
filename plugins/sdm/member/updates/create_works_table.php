<?php namespace Sdm\Member\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateWorksTable extends Migration
{
    public function up()
    {
        Schema::create('sdm_member_works', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('member_id');
            $table->string('number');
            $table->string('name');
            $table->string('location');
            $table->date('started_at');
            $table->date('ended_at');
            $table->boolean('is_now')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sdm_member_works');
    }
}
