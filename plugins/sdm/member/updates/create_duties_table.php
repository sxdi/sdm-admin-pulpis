<?php namespace Sdm\Member\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateDutiesTable extends Migration
{
    public function up()
    {
        Schema::create('sdm_member_duties', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('member_id');
            $table->string('name');
            $table->text('content');
            $table->date('started_at');
            $table->date('ended_at');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sdm_member_duties');
    }
}
