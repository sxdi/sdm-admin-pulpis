<?php namespace Sdm\Member\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateFamiliesTable extends Migration
{
    public function up()
    {
        Schema::create('sdm_member_families', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('member_id');
            $table->string('name');
            $table->enum('relation', ['wife','husban','father','mother','brother'])->nullable();
            $table->enum('gender', ['male', 'female']);
            $table->string('job')->nullable();
            $table->string('place');
            $table->date('dob');
            $table->string('education')->nullable();
            $table->text('content')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sdm_member_families');
    }
}
