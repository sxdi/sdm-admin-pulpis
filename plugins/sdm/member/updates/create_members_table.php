<?php namespace Sdm\Member\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateMembersTable extends Migration
{
    public function up()
    {
        Schema::create('sdm_member_members', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('place');
            $table->date('dob');
            $table->string('blood');
            $table->string('religion');
            $table->string('marital');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sdm_member_members');
    }
}
