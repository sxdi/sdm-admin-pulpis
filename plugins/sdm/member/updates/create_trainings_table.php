<?php namespace Sdm\Member\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateTrainingsTable extends Migration
{
    public function up()
    {
        Schema::create('sdm_member_trainings', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('member_id');
            $table->string('name');
            $table->string('result');
            $table->string('place');
            $table->date('started_at');
            $table->date('ended_at');
            $table->boolean('is_graduated');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sdm_member_trainings');
    }
}
