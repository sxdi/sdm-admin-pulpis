<?php namespace Sdm\Member\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateResidencesTable extends Migration
{
    public function up()
    {
        Schema::create('sdm_member_residences', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('member_id');
            $table->enum('type', ['personal', 'offical', 'other'])->default('other');
            $table->string('province');
            $table->string('regency');
            $table->text('address');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sdm_member_residences');
    }
}
