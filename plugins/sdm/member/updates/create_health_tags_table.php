<?php namespace Sdm\Member\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateHealthTagsTable extends Migration
{
    public function up()
    {
        Schema::create('sdm_member_health_tags', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('health_id');
            $table->integer('tag_id');
        });
    }

    public function down()
    {
        Schema::dropIfExists('sdm_member_health_tags');
    }
}
