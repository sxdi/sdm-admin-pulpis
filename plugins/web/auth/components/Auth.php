<?php namespace Web\Auth\Components;

use Cms\Classes\ComponentBase;

class Auth extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Auth Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $userManager = new \Sdm\Core\Classes\UserManager;
        $user        = $userManager->getUser();

        if($user) {
            $this->page['userLogin'] = $userManager->getMember();
        }
    }

    public function onLogin()
    {
        $rules = [
            'nrp'      => 'required',
            'password' => 'required',
        ];
        $attributeNames = [
            'nrp'      => 'nomor pokok',
            'password' => 'password',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            \Flash::error($validator->messages()->first());
            return;
        }

        $user = \Sdm\User\Models\User::whereCode(post('nrp'))->first();
        if(!$user) {
            \Flash::error('Pengguna tidak ditemukan');
            return;
        }

        if(!\Hash::check(strtolower(post('password')), $user->passcode)) {
            \Flash::error('Password tidak sesuai');
            return;
        }

        $putSession = \Session::put('userLogin', $user->id);
        return \Redirect::to('/dashboard');
    }

    public function onLogout()
    {
        $userManager = new \Sdm\Core\Classes\UserManager;
        $userManager->removeUser();

        \Flash::success('Berhasil keluar');
        return \Redirect::to('/');
    }
}
