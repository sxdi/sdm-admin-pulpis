<?php namespace Web\Base\Components;

use Cms\Classes\ComponentBase;

class BaseStatistic extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'BaseStatistic Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->page['allUser']          = $this->getAllUser();
        $this->page['allMember']        = $this->getAllMember();
        $this->page['allNonMember']     = $this->getAllNonMember();

        $this->page['memberSelf']             = \Sdm\Member\Models\Member::orderBy('name', 'asc')->where('name', '!=', '')->get();
        $this->page['memberNonSelf']          = \Sdm\Member\Models\Member::orderBy('name', 'asc')->where('name', '=', '')->get();

        $this->page['memberAttachPicture']    = \Sdm\Member\Models\Member::orderBy('name', 'asc')->has('picture')->get();
        $this->page['memberNonAttachPicture'] = \Sdm\Member\Models\Member::orderBy('name', 'asc')->whereDoesntHave('picture')->get();

        $this->page['memberAttachKtp']        = \Sdm\Member\Models\Member::orderBy('name', 'asc')->whereHas('attachments', function($q) {
            return $q->where('type', '=', 'ktp');
        })->get();
        $this->page['memberNonAttachKtp']     = \Sdm\Member\Models\Member::orderBy('name', 'asc')->whereDoesntHave('attachments', function($q) {
            return $q->where('type', '=', 'ktp');
        })->get();

        $this->page['memberAttachNpwp']        = \Sdm\Member\Models\Member::orderBy('name', 'asc')->whereHas('attachments', function($q) {
            return $q->where('type', '=', 'npwp');
        })->get();
        $this->page['memberNonAttachNpwp']     = \Sdm\Member\Models\Member::orderBy('name', 'asc')->whereDoesntHave('attachments', function($q) {
            return $q->where('type', '=', 'npwp');
        })->get();

        $this->page['memberAttachKk']        = \Sdm\Member\Models\Member::orderBy('name', 'asc')->whereHas('attachments', function($q) {
            return $q->where('type', '=', 'kk');
        })->get();
        $this->page['memberNonAttachKk']     = \Sdm\Member\Models\Member::orderBy('name', 'asc')->whereDoesntHave('attachments', function($q) {
            return $q->where('type', '=', 'kk');
        })->get();

        $this->page['memberAttachFinger']        = \Sdm\Member\Models\Member::orderBy('name', 'asc')->whereHas('attachments', function($q) {
            return $q->where('type', '=', 'finger');
        })->get();
        $this->page['memberNonAttachFinger']     = \Sdm\Member\Models\Member::orderBy('name', 'asc')->whereDoesntHave('attachments', function($q) {
            return $q->where('type', '=', 'finger');
        })->get();

        $this->page['memberAttachAsabri']        = \Sdm\Member\Models\Member::orderBy('name', 'asc')->whereHas('attachments', function($q) {
            return $q->where('type', '=', 'asabri');
        })->get();
        $this->page['memberNonAttachAsabri']     = \Sdm\Member\Models\Member::orderBy('name', 'asc')->whereDoesntHave('attachments', function($q) {
            return $q->where('type', '=', 'asabri');
        })->get();

        $this->page['memberAttachBpjs']        = \Sdm\Member\Models\Member::orderBy('name', 'asc')->whereHas('attachments', function($q) {
            return $q->where('type', '=', 'bpjs');
        })->get();
        $this->page['memberNonAttachBpjs']     = \Sdm\Member\Models\Member::orderBy('name', 'asc')->whereDoesntHave('attachments', function($q) {
            return $q->where('type', '=', 'bpjs');
        })->get();

        $this->page['memberHaveLanguage']        = \Sdm\Member\Models\Member::orderBy('name', 'asc')->has('languages')->get();
        $this->page['memberNonHaveLanguage']     = \Sdm\Member\Models\Member::orderBy('name', 'asc')->whereDoesntHave('languages')->get();

        $this->page['memberHaveSport']        = \Sdm\Member\Models\Member::orderBy('name', 'asc')->has('sports')->get();
        $this->page['memberNonHaveSport']     = \Sdm\Member\Models\Member::orderBy('name', 'asc')->whereDoesntHave('sports')->get();

        $this->page['memberHaveEducation']        = \Sdm\Member\Models\Member::orderBy('name', 'asc')->has('educations')->get();
        $this->page['memberNonHaveEducation']     = \Sdm\Member\Models\Member::orderBy('name', 'asc')->whereDoesntHave('educations')->get();

        $this->page['memberHaveEducationPolri']        = \Sdm\Member\Models\Member::orderBy('name', 'asc')->whereHas('educationPolries', function($q){
            return $q->where('type', '=', 'polri');
        })->get();
        $this->page['memberNonHaveEducationPolri']     = \Sdm\Member\Models\Member::orderBy('name', 'asc')->whereDoesntHave('educationPolries', function($q){
            return $q->where('type', '=', 'polri');
        })->get();

        $this->page['memberHaveEducationDikbangspes']        = \Sdm\Member\Models\Member::orderBy('name', 'asc')->whereHas('educationPolries', function($q){
            return $q->where('type', '=', 'dikbangspes');
        })->get();
        $this->page['memberNonHaveEducationDikbangspes']     = \Sdm\Member\Models\Member::orderBy('name', 'asc')->whereDoesntHave('educationPolries', function($q){
            return $q->where('type', '=', 'dikbangspes');
        })->get();

        $this->page['memberHaveGrade']        = \Sdm\Member\Models\Member::orderBy('name', 'asc')->has('grades')->get();
        $this->page['memberNonHaveGrade']     = \Sdm\Member\Models\Member::orderBy('name', 'asc')->whereDoesntHave('grades')->get();

        $this->page['positions']              = \Sdm\Master\Models\Department::whereUnitId(1)->orderBy('sort', 'asc')->get();
    }

    public function getMemberSection($positionId)
    {
        return \Sdm\Member\Models\Position::where('department_id', '=', $positionId)->whereIsNow(1)->get();
    }

    public function getAllUser()
    {
        return \Sdm\User\Models\User::get();
    }

    public function getAllNonMember()
    {
        return \Sdm\User\Models\User::whereDoesntHave('member')->get();
    }

    public function getAllMember()
    {
        return \Sdm\Member\Models\Member::get();
    }
}
