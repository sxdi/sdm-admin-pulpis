<?php namespace Web\Admin\Components;

use Cms\Classes\ComponentBase;

class AdminUser extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminUser Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getUser()
    {
        $userManager = new \Sdm\Core\Classes\UserManager;
        return $userManager->getUser();
    }

    public function onUpdatePassword()
    {
        $rules = [
            'password_old'    => 'required',
            'password'        => 'required|min:6',
            'password_retype' => 'required|same:password',
        ];
        $attributeNames = [
            'password_old'    => 'password lama',
            'password'        => 'password baru',
            'password_retype' => 'ulangi password',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            \Flash::error($validator->messages()->first());
            return;
        }

        $user = $this->getUser();
        if(!\Hash::check(post('password_old'), $user->passcode)) {
            \Flash::error('Password lama tidak sesuai');
            return;
        }

        $user->passcode = \Hash::make(strtolower(post('password')));
        $user->save();

        \Session::forget('userLogin');
        \Flash::success('Password berhasil diubah');
        return \Redirect::to('/');
    }
}
