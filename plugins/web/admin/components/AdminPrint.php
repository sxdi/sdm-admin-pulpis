<?php namespace Web\Admin\Components;

use Carbon\Carbon;

use Cms\Classes\ComponentBase;

class AdminPrint extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminPrint Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getMember()
    {
        return \Sdm\Member\Models\Member::orderBy('name')->get();
    }

    public function getLetter()
    {
        return \Sdm\Letter\Models\Letter::orderBy('name')->get();
    }

    public function onPutFilter()
    {
        if(post('component_personal')) {
            $primary = post('component_personal');
            $forms  = [];
            foreach ($primary as $p) {
                $split = explode(',', $p);
                array_push($forms, $split[0]);
            }

            $this->page['component_personal'] = post('component_personal');
        }

        if(post('component_attachment')) {
            $primary = post('component_attachment');
            $forms  = [];
            foreach ($primary as $p) {
                $split = explode(',', $p);
                array_push($forms, $split[0]);
            }

            $this->page['component_attachment'] = post('component_attachment');
        }

        if(post('component_primary')) {
            $primary = post('component_primary');
            $forms  = [];
            foreach ($primary as $p) {
                $split = explode(',', $p);
                array_push($forms, $split[0]);
            }

            if(in_array('language', $forms)) {
                $this->page['languages']     = \Sdm\Member\Models\Language::whereNotNull('name')->orderBy('name')->groupBy('name')->get();
            }

            if(in_array('sport', $forms)) {
                $this->page['sports']     = \Sdm\Member\Models\Sport::whereNotNull('name')->orderBy('name')->groupBy('name')->get();
            }

            if(in_array('honor', $forms)) {
                $this->page['honors']     = \Sdm\Member\Models\Honor::whereNotNull('name')->orderBy('name')->groupBy('name')->get();
            }

            if(in_array('education', $forms)) {
                $this->page['educations']           = \Sdm\Master\Models\Education::get();
                $this->page['educationStudies']     = \Sdm\Member\Models\Education::whereType('formal')->whereNotNull('study')->orderBy('study')->groupBy('study')->get();
            }

            if(in_array('education_polri', $forms)) {
                $this->page['educationPolries']     = \Sdm\Member\Models\EducationPolice::whereType('polri')->orderBy('name')->groupBy('name')->get();
            }

            if(in_array('education_dikbangspes', $forms)) {
                $this->page['dikbangspeses']  = \Sdm\Member\Models\EducationPolice::whereType('dikbangspes')->orderBy('name')->groupBy('name')->get();
            }

            if(in_array('training', $forms)) {
                $this->page['trainings']  = \Sdm\Member\Models\Training::orderBy('name')->groupBy('name')->get();
            }

            if(in_array('duty', $forms)) {
                $this->page['duties']  = \Sdm\Member\Models\Duty::orderBy('name')->groupBy('name')->get();
            }

            if(in_array('position', $forms)) {
                $this->page['positions']  = \Sdm\Master\Models\Position::whereNotNull('parent_id')->get();
            }

            if(in_array('grade', $forms)) {
                $this->page['grades']  = \Sdm\Master\Models\Grade::get();
            }

            if(in_array('work', $forms)) {
                $this->page['works']  = \Sdm\Member\Models\Work::orderBy('name')->groupBy('name')->get();
            }

            if(in_array('brivet', $forms)) {
                $this->page['brivets']  = \Sdm\Member\Models\Brivet::orderBy('name')->groupBy('name')->get();
            }

            $this->page['component_personal']   = post('component_personal');
        }
        return;
    }

    public function onFilter()
    {
        $member = new \Sdm\Member\Models\Member;


        // Form Personal
        if(post('name')) {
            $name = post('name');
            if(post('name_logic') == 'same') {
                $member = $member->where('name', 'like', '%'.$name.'%');
            }

            if(post('name_logic') == 'in') {
                $names  = explode(',', $name);
                foreach ($names as $name) {
                    $member = $member->OrWhere('name', 'like', '%'.$name.'%');
                }
            }
        }

        if(post('nrp')) {
            $nrp = post('nrp');
            if(post('nrp_logic') == 'same') {
                $member = $member->where('nrp', 'like', '%'.$nrp.'%');
            }

            if(post('nrp_logic') == 'in') {
                $nrps  = explode(',', $nrp);
                foreach ($nrps as $nrp) {
                    $member = $member->OrWhere('nrp', 'like', '%'.$nrp.'%');
                }
            }
        }

        if(post('place')) {
            $place = post('place');
            if(post('place_logic') == 'same') {
                $member = $member->where('place', 'like', '%'.$place.'%');
            }

            if(post('place_logic') == 'in') {
                $places  = explode(',', $place);
                foreach ($places as $place) {
                    $member = $member->OrWhere('place', 'like', '%'.$place.'%');
                }
            }
        }

        if(post('dob')) {
            $dob = post('dob');
            if(post('dob_logic') == 'same') {
                $member = $member->where('dob', 'like', '%'.$dob.'%');
            }

            if(post('dob_logic') == 'in') {
                $dobs  = explode(',', $dob);
                foreach ($dobs as $dob) {
                    $member = $member->OrWhere('dob', 'like', '%'.$dob.'%');
                }
            }
        }

        if(post('blood')) {
            $blood = post('blood');
            if(post('blood_logic') == 'same') {
                $member = $member->where('blood', 'like', '%'.$blood.'%');
            }

            if(post('blood_logic') == 'in') {
                $bloods  = explode(',', $blood);
                foreach ($bloods as $blood) {
                    $member = $member->OrWhere('blood', 'like', '%'.$blood.'%');
                }
            }
        }

        if(post('religion')) {
            $religion = post('religion');
            if(post('religion_logic') == 'same') {
                $member = $member->where('religion', 'like', '%'.$religion.'%');
            }

            if(post('religion_logic') == 'in') {
                $religions  = explode(',', $religion);
                foreach ($religions as $religion) {
                    $member = $member->OrWhere('religion', 'like', '%'.$religion.'%');
                }
            }
        }

        if(post('nationality')) {
            $nationality = post('nationality');
            if(post('nationality_logic') == 'same') {
                $member = $member->where('nationality', 'like', '%'.$nationality.'%');
            }

            if(post('nationality_logic') == 'in') {
                $nationalitys  = explode(',', $nationality);
                foreach ($nationalitys as $nationality) {
                    $member = $member->OrWhere('nationality', 'like', '%'.$nationality.'%');
                }
            }
        }

        if(post('skin_color')) {
            $skin_color = post('skin_color');
            if(post('skin_color_logic') == 'same') {
                $member = $member->where('skin_color', 'like', '%'.$skin_color.'%');
            }

            if(post('skin_color_logic') == 'in') {
                $skin_colors  = explode(',', $skin_color);
                foreach ($skin_colors as $skin_color) {
                    $member = $member->OrWhere('skin_color', 'like', '%'.$skin_color.'%');
                }
            }
        }

        if(post('hair_color')) {
            $hair_color = post('hair_color');
            if(post('hair_color_logic') == 'same') {
                $member = $member->where('hair_color', 'like', '%'.$hair_color.'%');
            }

            if(post('hair_color_logic') == 'in') {
                $hair_colors  = explode(',', $hair_color);
                foreach ($hair_colors as $hair_color) {
                    $member = $member->OrWhere('hair_color', 'like', '%'.$hair_color.'%');
                }
            }
        }

        if(post('hair_type')) {
            $hair_type = post('hair_type');
            if(post('hair_type_logic') == 'same') {
                $member = $member->where('hair_type', 'like', '%'.$hair_type.'%');
            }

            if(post('hair_type_logic') == 'in') {
                $hair_types  = explode(',', $hair_type);
                foreach ($hair_types as $hair_type) {
                    $member = $member->OrWhere('hair_type', 'like', '%'.$hair_type.'%');
                }
            }
        }

        if(post('height')) {
            $height = post('height');
            if(post('height_logic') == 'same') {
                $member = $member->where('height', '=', $height);
            }

            if(post('height_logic') == 'grather') {
                $member = $member->where('height', '>', $height);
            }

            if(post('height_logic') == 'lower') {
                $member = $member->where('height', '<', $height);
            }
        }

        if(post('weight')) {
            $weight = post('weight');
            if(post('weight_logic') == 'same') {
                $member = $member->where('weight', '=', $weight);
            }

            if(post('weight_logic') == 'grather') {
                $member = $member->where('weight', '>', $weight);
            }

            if(post('weight_logic') == 'lower') {
                $member = $member->where('weight', '<', $weight);
            }
        }

        if(post('uniform_head')) {
            $uniform_head = post('uniform_head');
            if(post('uniform_head_logic') == 'same') {
                $member = $member->where('uniform_head', '=', $uniform_head);
            }

            if(post('uniform_head_logic') == 'grather') {
                $member = $member->where('uniform_head', '>', $uniform_head);
            }

            if(post('uniform_head_logic') == 'lower') {
                $member = $member->where('uniform_head', '<', $uniform_head);
            }
        }

        if(post('uniform_feet')) {
            $uniform_feet = post('uniform_feet');
            if(post('uniform_feet_logic') == 'same') {
                $member = $member->where('uniform_feet', '=', $uniform_feet);
            }

            if(post('uniform_feet_logic') == 'grather') {
                $member = $member->where('uniform_feet', '>', $uniform_feet);
            }

            if(post('uniform_feet_logic') == 'lower') {
                $member = $member->where('uniform_feet', '<', $uniform_feet);
            }
        }

        if(post('uniform_trousers')) {
            $uniform_trousers = post('uniform_trousers');
            if(post('uniform_trousers_logic') == 'same') {
                $member = $member->where('uniform_trousers', '=', $uniform_trousers);
            }

            if(post('uniform_trousers_logic') == 'grather') {
                $member = $member->where('uniform_trousers', '>', $uniform_trousers);
            }

            if(post('uniform_trousers_logic') == 'lower') {
                $member = $member->where('uniform_trousers', '<', $uniform_trousers);
            }
        }

        if(post('uniform_clothes')) {
            $uniform_clothes = post('uniform_clothes');
            if(post('uniform_clothes_logic') == 'same') {
                $member = $member->where('uniform_clothes', '=', $uniform_clothes);
            }

            if(post('uniform_clothes_logic') == 'grather') {
                $member = $member->where('uniform_clothes', '>', $uniform_clothes);
            }

            if(post('uniform_clothes_logic') == 'lower') {
                $member = $member->where('uniform_clothes', '<', $uniform_clothes);
            }
        }

        // Form Attachment
        if(post('ktp')) {
            $ktp = post('ktp');
            if(post('ktp_logic') == 'same') {
                $member = $member->whereHas('attachments', function($q) use($ktp){
                    $q->where('type', '=', 'ktp')->where('value', 'like', '%'.$ktp.'%');
                });
            }

            if(post('ktp_logic') == 'in') {
                $ktps   = explode(',', $ktp);
                $member = $member->whereHas('attachments', function($q) use($ktps){
                    return $q->where('type', '=', 'ktp')->whereIn('value', $ktps);
                });
            }
        }

        if(post('kk')) {
            $kk = post('kk');
            if(post('kk_logic') == 'same') {
                $member = $member->whereHas('attachments', function($q) use($kk){
                    $q->where('type', '=', 'kk')->where('value', 'like', '%'.$kk.'%');
                });
            }

            if(post('kk_logic') == 'in') {
                $kks   = explode(',', $kk);
                $member = $member->whereHas('attachments', function($q) use($kks){
                    return $q->where('type', '=', 'kk')->whereIn('value', $kks);
                });
            }
        }

        if(post('finger')) {
            $finger = post('finger');
            if(post('finger_logic') == 'same') {
                $member = $member->whereHas('attachments', function($q) use($finger){
                    $q->where('type', '=', 'finger')->where('value', 'like', '%'.$finger.'%');
                });
            }

            if(post('finger_logic') == 'in') {
                $fingers   = explode(',', $finger);
                $member = $member->whereHas('attachments', function($q) use($fingers){
                    return $q->where('type', '=', 'finger')->whereIn('value', $fingers);
                });
            }
        }

        if(post('asabri')) {
            $asabri = post('asabri');
            if(post('asabri_logic') == 'same') {
                $member = $member->whereHas('attachments', function($q) use($asabri){
                    $q->where('type', '=', 'asabri')->where('value', 'like', '%'.$asabri.'%');
                });
            }

            if(post('asabri_logic') == 'in') {
                $asabris   = explode(',', $asabri);
                $member = $member->whereHas('attachments', function($q) use($asabris){
                    return $q->where('type', '=', 'asabri')->whereIn('value', $asabris);
                });
            }
        }

        if(post('authority')) {
            $authority = post('authority');
            if(post('authority_logic') == 'same') {
                $member = $member->whereHas('attachments', function($q) use($authority){
                    $q->where('type', '=', 'authority')->where('value', 'like', '%'.$authority.'%');
                });
            }

            if(post('authority_logic') == 'in') {
                $authoritys   = explode(',', $authority);
                $member = $member->whereHas('attachments', function($q) use($authoritys){
                    return $q->where('type', '=', 'authority')->whereIn('value', $authoritys);
                });
            }
        }

        if(post('investigator')) {
            $investigator = post('investigator');
            if(post('investigator_logic') == 'same') {
                $member = $member->whereHas('attachments', function($q) use($investigator){
                    $q->where('type', '=', 'investigator')->where('value', 'like', '%'.$investigator.'%');
                });
            }

            if(post('investigator_logic') == 'in') {
                $investigators   = explode(',', $investigator);
                $member = $member->whereHas('attachments', function($q) use($investigators){
                    return $q->where('type', '=', 'investigator')->whereIn('value', $investigators);
                });
            }
        }

        if(post('member')) {
            $member = post('member');
            if(post('member_logic') == 'same') {
                $member = $member->whereHas('attachments', function($q) use($member){
                    $q->where('type', '=', 'member')->where('value', 'like', '%'.$member.'%');
                });
            }

            if(post('member_logic') == 'in') {
                $members   = explode(',', $member);
                $member = $member->whereHas('attachments', function($q) use($members){
                    return $q->where('type', '=', 'member')->whereIn('value', $members);
                });
            }
        }

        if(post('bpjs')) {
            $bpjs = post('bpjs');
            if(post('bpjs_logic') == 'same') {
                $member = $member->whereHas('attachments', function($q) use($bpjs){
                    $q->where('type', '=', 'bpjs')->where('value', 'like', '%'.$bpjs.'%');
                });
            }

            if(post('bpjs_logic') == 'in') {
                $bpjss   = explode(',', $bpjs);
                $member = $member->whereHas('attachments', function($q) use($bpjss){
                    return $q->where('type', '=', 'bpjs')->whereIn('value', $bpjss);
                });
            }
        }

        if(post('kps')) {
            $kps = post('kps');
            if(post('kps_logic') == 'same') {
                $member = $member->whereHas('attachments', function($q) use($kps){
                    $q->where('type', '=', 'kps')->where('value', 'like', '%'.$kps.'%');
                });
            }

            if(post('kps_logic') == 'in') {
                $kpss   = explode(',', $kps);
                $member = $member->whereHas('attachments', function($q) use($kpss){
                    return $q->where('type', '=', 'kps')->whereIn('value', $kpss);
                });
            }
        }

        if(post('simsa')) {
            $simsa = post('simsa');
            if(post('simsa_logic') == 'same') {
                $member = $member->whereHas('attachments', function($q) use($simsa){
                    $q->where('type', '=', 'simsa')->where('value', 'like', '%'.$simsa.'%');
                });
            }

            if(post('simsa_logic') == 'in') {
                $simsas   = explode(',', $simsa);
                $member = $member->whereHas('attachments', function($q) use($simsas){
                    return $q->where('type', '=', 'simsa')->whereIn('value', $simsas);
                });
            }
        }

        // Form Primary
        if(post('language') || post('language_result')) {
            $language = [
                'name'  => post('language'),
                'value' => post('language_result')
            ];
            $member   = $member->whereHas('languages', function($q) use($language) {
                $q->whereIn('name', $language['name'])
                  ->where('result', 'like', '%'.$language['value'].'%');
            });
        }

        if(post('sport')) {
            $sport = post('sport');
            $member = $member->whereHas('sports', function($q) use($sport) {
                return $q->whereIn('name', $sport);
            });
        }

        if(post('honor')) {
            $honor = post('honor');
            $member = $member->whereHas('honors', function($q) use($honor){
                return $q->whereIn('name', $honor);
            });
        }

        if(post('education') || post('education_study')) {
            if(post('education')) {
                $education  = explode(',', post('education'));
                $member     = $member->whereHas('educations', function($q) use($education){
                    return $q->whereType('formal')->whereIn('education_id', $education);
                });
            }

            if(post('education_study')) {
                $educationStudy = post('education_study');
                $member         = $member->whereHas('educations', function($q) use($educationStudy){
                    return $q->where('study', 'like', '%'.$educationStudy.'%');
                });
            }
        }

        if(post('education_polri') || post('education_polri_place') || post('education_polri_result')) {
            $member = $member->whereHas('educationPolries', function($q) {
                return $q->whereType('polri');
            });

            if(post('education_polri')) {
                $education = post('education_polri');
                $member    = $member->whereHas('educationPolries', function($q) use($education){
                    return $q->whereIn('name', $education);
                });
            }

            if(post('education_polri_place')) {
                $place = post('education_polri_place');
                $member= $member->whereHas('educationPolries', function($q) use($place){
                    return $q->where('place', 'like', '%'.$place.'%');
                });
            }

            if(post('education_polri_result')) {
                $reslt = post('education_polri_result');
                $member= $member->whereHas('educationPolries', function($q) use($reslt){
                    return $q->where('result', 'like', '%'.$reslt.'%');
                });
            }
        }

        if(post('education_dikbangspes') || post('education_dikbangspes_place') || post('education_dikbangspes_result')) {
            $member = $member->whereHas('educationPolries', function($q) {
                return $q->whereType('dikbangspes');
            });

            if(post('education_dikbangspes')) {
                $education = post('education_dikbangspes');
                $member    = $member->whereHas('educationPolries', function($q) use($education){
                    return $q->whereIn('name', $education);
                });
            }

            if(post('education_dikbangspes_place')) {
                $place = post('education_dikbangspes_place');
                $member= $member->whereHas('educationPolries', function($q) use($place){
                    return $q->where('place', 'like', '%'.$place.'%');
                });
            }

            if(post('education_dikbangspes_result')) {
                $reslt = post('education_polri_result');
                $member= $member->whereHas('educationPolries', function($q) use($reslt){
                    return $q->where('result', 'like', '%'.$reslt.'%');
                });
            }
        }

        if(post('training') || post('training_result')) {
            if(post('training')) {
                $training = post('training');
                $member   = $member->whereHas('trainings', function($q) use($training){
                    return $q->whereIn('name', $training);
                });
            }

            if(post('training_result')) {
                $result = post('training_result');
                $member = $member->whereHas('trainings', function($q) use($result){
                    return $q->where('result', 'like', '%'.$result.'%');
                });
            }
        }

        if(post('duty')) {
            $duty   = post('duty');
            $member = $member->whereHas('duties', function($q) use($duty){
                return $q->whereIn('name', $duty);
            });
        }

        if(post('position')) {
            $position = post('position');
            $member   = $member->whereHas('positions', function($q) use($position){
                return $q->whereIn('position_id', $position)->whereIsNow(1);
            });
        }

        if(post('grade')) {
            $grade  = post('grade');
            $member = $member->whereHas('grades', function($q) use($grade){
                return $q->whereIn('grade_id', $grade)->whereIsNow(1);
            });
        }

        if(post('work')) {
            $work   = post('work');
            $member = $member->whereHas('works', function($q) use($work){
                return $q->whereIn('name', $work);
            });
        }

        if(post('brivet')) {
            $brivet = post('brivet');
            $member = $member->whereHas('brivets', function($q) use($brivet){
                return $q->whereIn('name', $brivet);
            });
        }

        $member                = $member->get();
        $this->page['members'] = $member;
    }

    public function onViewRH()
    {
        $member = \Sdm\Member\Models\Member::find(post('id'));
        if($member) {
            $this->page['member'] = $member;
        }
    }

    public function onInsertList()
    {
        if(!post('member')) {
            throw new \Exception('Pilih anggota terlebih dahulu');;
            return;
        }

        $member = \Sdm\Member\Models\Member::whereIn('id', post('member'))->get();
        $this->page['members'] = $member;
    }

    public function onRemoveList()
    {
        return true;
    }

    public function onGenerate()
    {
        $letterManager      = new \Sdm\Core\Classes\LetterManager;
        $letter             = \Sdm\Letter\Models\Letter::find(post('letter_id'));
        $members            = \Sdm\Member\Models\Member::whereIn('id', post('member'))->get();
        $path               = $letter->document->path;

        $request            = new \Sdm\Letter\Models\Request;
        $request->letter_id = $letter->id;
        $request->name      = $letter->name;
        $request->save();

        if(!$letter->is_single) {
            if($letter->code == 'urmin_senpi') {
                $letterManager->processPersonel([
                    'path'    => $path,
                    'request' => $request,
                    'members' => $members,
                ]);
            }

            if($letter->code == 'urmin_things_pdh') {
                $letterManager->processPersonel([
                    'path'    => $path,
                    'request' => $request,
                    'members' => $members,
                ]);
            }

            if($letter->code == 'urmin_rh_polri') {
                $letterManager->processPersonel([
                    'path'    => $path,
                    'request' => $request,
                    'members' => $members,
                    'rh_polri'=> true,
                ]);
            }

            if($letter->code == 'urmin_rh_complete') {
                $letterManager->processPersonel([
                    'path'        => $path,
                    'request'     => $request,
                    'members'     => $members,
                    'rh_complete' => true,
                ]);
            }

            if($letter->code == 'urmin_asabri_ptdh') {
                $letterManager->processAsabriPtdh([
                    'path'    => $path,
                    'request' => $request,
                    'members' => $members
                ]);
            }

            if($letter->code == 'urmin_letter_pdh') {
                $letterManager->processPersonel([
                    'path'    => $path,
                    'request' => $request,
                    'members' => $members
                ]);
            }

            if($letter->code == 'urmin_address_pension') {
                $letterManager->processPersonel([
                    'path'    => $path,
                    'request' => $request,
                    'members' => $members
                ]);
            }

            if($letter->code == 'urmin_lmpa') {
                $letterManager->processPersonel([
                    'path'    => $path,
                    'request' => $request,
                    'members' => $members
                ]);
            }

            if($letter->code == 'urmin_divorce') {
                $letterManager->processPersonel([
                    'path'          => $path,
                    'request'       => $request,
                    'members'       => $members,
                    'urmin_divorce' => true
                ]);
            }

            if($letter->code == 'urmin_marriage') {
                $letterManager->processPersonel([
                    'path'          => $path,
                    'request'       => $request,
                    'members'       => $members,
                    'urmin_marriage' => true
                ]);
            }
        }

        // Group Print
        if($letter->code == 'urmin_mapping_personel') {
            $letterManager->processMapingPersonel([
                'path'    => $path,
                'request' => $request,
                'members' => $members,
            ]);
        }

        if($letter->code == 'urmin_trouble_personel') {
            $letterManager->processMapingPersonel([
                'path'    => $path,
                'request' => $request,
                'members' => $members
            ]);
        }

        if($letter->code == 'urmin_yearly_sick') {
            $letterManager->processMapingPersonel([
                'path'    => $path,
                'request' => $request,
                'members' => $members
            ]);
        }

        if($letter->code == 'urmin_member_house') {
            $letterManager->processMapingPersonel([
                'path'    => $path,
                'request' => $request,
                'members' => $members
            ]);
        }

        \Flash::success('Dokumen berhasil dibuat');
        return \Redirect::to('print/result/'.$request->parameter);
    }
}
