<?php namespace Web\Admin\Components;

use Cms\Classes\ComponentBase;

class AdminLetter extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminLetter Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getAll()
    {
        return \Sdm\Letter\Models\Letter::orderBy('name')->get();
    }

    public function onSave()
    {
        $rules = [
            'name'       => 'required',
        ];
        $attributeNames = [
            'name'       => 'nama',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            \Flash::error($validator->messages()->first());
            return;
        }

        $letter       = new \Sdm\Letter\Models\Letter;
        $letter->name = post('name');
        $letter->save(null, post('_session_key'));

        \Flash::success('Surat berhasil disimpan');
        return \Redirect::to('master/letter');
    }

    public function onGenerate()
    {
        $arrContextOptions = [
            "ssl" => [
                "verify_peer"      => false,
                "verify_peer_name" => false
            ]
        ];

        $letter      = \Sdm\Letter\Models\Letter::find(1);

        // \Flash::info($letter->document->path);
        // return;
        // $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(storage_path($letter->document->path, false, stream_context_create($arrContextOptions)));
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(storage_path('/app/uploads/public/5e1/8cb/959/5e18cb959a66c387086022.xlsx'));
        $worksheet   = $spreadsheet->getActiveSheet();
        $worksheet->getCell('C8')->setValue('John');
        $worksheet->getCell('C9')->setValue('Smith');
        $writer      = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save('write.xls');
    }

    public function onGenerates()
    {
        $member            = \Sdm\Member\Models\Member::get();
        $letter            = \Sdm\Letter\Models\Letter::find(1);

        $templateProcessor = new  \PhpOffice\PhpWord\TemplateProcessor($letter->document->path);
        $templateProcessor->cloneRow('rowName', count($member));

        foreach ($member as $key => $m) {
            $templateProcessor->setValue('rowName#'.($key+1), $m->name);
            $templateProcessor->setValue('rowValue#'.($key+1), $m->nrp);
        }
        // $replacements = array(
        //     array(
        //         'name' => 'Batman', '
        //         grade' => 'Gotham City'
        //     ),
        //     array(
        //         'name' => 'Superman',
        //         'grade' => 'Metropolis'
        //     ),
        // );
        // $phpWord = new \PhpOffice\PhpWord\PhpWord();
        // $section = $phpWord->addSection();

        // $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($templateProcessor, 'Word2007');
        // $templateProcessor->cloneBlock('block_name', count($replacements), true, false, $replacements);
        $templateProcessor->saveAs('helloWorld.docx');
    }
}
