<?php namespace Web\Admin\Components;

use Cms\Classes\ComponentBase;

class AdminLetterDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminLetterDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'name'        => 'AdminLetterDetail Component',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    function init() {
        $letter    = $this->getCurrent();
        $component = $this->addComponent(
            'Responsiv\Uploader\Components\FileUploader',
            'letterUploader',
            ['deferredBinding' => true]
        );

        $component->bindModel('document', \Sdm\Letter\Models\Letter::find($letter->id));
    }

    public function onRun()
    {
        $letter = $this->getCurrent();
        if(!$letter) {
            \Flash::error('Halaman tidak ditemukan');
            return \Redirect::back();
        }

        $this->page['letter'] = $letter;
    }

    public function getCurrent()
    {
        return \Sdm\Letter\Models\Letter::whereParameter($this->property('parameter'))->first();
    }

    public function onSave()
    {
        $rules = [
            'name'       => 'required',
        ];
        $attributeNames = [
            'name'       => 'nama',
        ];
        $messages  = [];
        $validator = \Validator::make(post(), $rules, $messages, $attributeNames);

        if ($validator->fails()) {
            \Flash::error($validator->messages()->first());
            return;
        }

        $letter       = $this->getCurrent();
        $letter->name = post('name');
        $letter->save(null, post('_session_key'));

        \Flash::success('Surat berhasil disimpan');
        return \Redirect::to('master/letter');
    }
}
