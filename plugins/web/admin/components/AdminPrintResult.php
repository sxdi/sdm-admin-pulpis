<?php namespace Web\Admin\Components;

use Cms\Classes\ComponentBase;

class AdminPrintResult extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminPrintResult Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'name'        => 'AdminPrintResult Component',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function onRun()
    {
        $result = $this->getCurrent();
        if(!$result) {
            return \Redirect::to('404');
        }

        $this->page['result'] = $result;
    }

    public function getCurrent()
    {
        return \Sdm\Letter\Models\Request::whereParameter($this->property('parameter'))->first();
    }

    public function onRegenerate()
    {
        $request           = $this->getCurrent();
        $path              = $request->letter->document->path;
        $members           = \Sdm\Member\Models\Member::whereId(2)->get();
        $letterManager     = new \Sdm\Core\Classes\LetterManager;
        $letterManager->processPersonel([
            'path'    => $path,
            'request' => $request,
            'members' => $members
        ]);
        \Flash::success('Berhasil generate');
        return \Redirect::refresh();
    }
}
